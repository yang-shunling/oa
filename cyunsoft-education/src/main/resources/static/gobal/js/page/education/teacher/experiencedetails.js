$(function () {
    $.ajax({
        url: "/ret/eduget/getEduTeacherExperienceById",
        type: "post",
        dataType: "json",
        data: {recordId: recordId},
        success: function (data) {
            if (data.status == "200") {
                var info = data.list;
                for (var id in info) {
                    if (id == "courseType") {
                        $("#" + id).html(getEduClassCodeName("courseType", info[id]));
                    } else if (id == "grade") {
                        $("#" + id).html(getEduClassCodeName("grade", info[id]));
                    } else if (id == "teacherId") {
                        $.ajax({
                            url: "/ret/eduget/getEduTeacherById",
                            type: "post",
                            dataType: "json",
                            data: {
                                teacherId: info[id]
                            },
                            success: function (res) {
                                $("#" + id).html(res.list.userName);
                            }
                        });
                    } else {
                        $("#" + id).html(info[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})
