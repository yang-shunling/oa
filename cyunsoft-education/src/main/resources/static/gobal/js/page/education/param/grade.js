$(function () {
    getAllEduGradeList();
    $(".js-add").unbind("click").click(function () {
        add();
    })
    $(".js-update").unbind("click").click(function () {
        update();
    })
    $(".js-del").unbind("click").click(function () {
        del();
    })
    var oc = $('#chart-container').orgchart({
        'data': '/ret/eduget/getEduGradeChart?nextGrade=0',
        'nodeContent': 'remark',
        'nodeId': 'id'
    });
    oc.$chartContainer.on('click', '.node', function () {
        var nodes = $(this);
        setUpdate(nodes.context.id)
    });
});

function add() {
    if ($("#title").val() == "" || $("#sortNo").val() == "") {
        layer.msg("年级名称或排序号不能为空!");
        return;
    } else {
        $.ajax({
            url: "/set/eduset/insertEduGrade",
            type: "post",
            dataType: "json",
            data: {
                title: $("#title").val(),
                nextGrade: $("#nextGrade").val(),
                sortNo: $("#sortNo").val(),
                remark: $("#remark").val()
            },
            success: function (data) {
                if (data.status == 200) {
                    layer.msg(sysmsg[data.msg]);
                    location.reload();
                } else if (data.status == 100) {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    }
}

function update() {
    $.ajax({
        url: "/set/eduset/updateEduGrade",
        type: "post",
        dataType: "json",
        data: {
            gradeId: $("#gradeId").val(),
            title: $("#title").val(),
            nextGrade: $("#nextGrade").val(),
            sortNo: $("#sortNo").val(),
            remark: $("#remark").val()
        },
        success: function (data) {
            if (data.status == 200) {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function setUpdate(gradeId) {
    document.getElementById("form1").reset();
    $.ajax({
        url: "/ret/eduget/getEduGradeById",
        type: "post",
        dataType: "json",
        data: {
            gradeId: gradeId
        },
        success: function (data) {
            if (data.status == 200) {
                for (var name in data.list) {
                    $("#" + name).val(data.list[name]);
                }
            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function del() {
    var msg = "您真的确定要删除吗？\n\n请确认！";
    if (confirm(msg) == true) {
        $.ajax({
            url: "/set/eduset/deleteEduGrade",
            type: "post",
            dataType: "json",
            data: {
                gradeId: $("#gradeId").val()
            },
            success: function (data) {
                if (data.status == 200) {
                    location.reload();
                    layer.msg(sysmsg[data.msg]);
                } else if (data.status == 100) {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    } else {
        return;
    }
}


function getAllEduGradeList() {
    $.ajax({
        url: "/ret/eduget/getAllEduGradeList",
        type: "post",
        dataType: "json",
        data: {},
        success: function (data) {
            if (data.status == 200) {
                for (var i = 0; i < data.list.length; i++) {
                    $("#nextGrade").append("<option value='" + data.list[i].gradeId + "'>" + data.list[i].title + "</option>")
                }
            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}
