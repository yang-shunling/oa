package com.core136.service.education;

import com.core136.bean.education.EduTeacher;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.education.EduTeacherMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class EduTeacherService {

    private EduTeacherMapper eduTeacherMapper;

    @Autowired
    public void setEduTeacherMapper(EduTeacherMapper eduTeacherMapper) {
        this.eduTeacherMapper = eduTeacherMapper;
    }

    public int insertEduTeacher(EduTeacher eduTeacher) {
        return eduTeacherMapper.insert(eduTeacher);
    }

    public int deleteEduTeacher(EduTeacher eduTeacher) {
        return eduTeacherMapper.delete(eduTeacher);
    }

    public int updateEduTeacher(Example example, EduTeacher eduTeacher) {
        return eduTeacherMapper.updateByExampleSelective(eduTeacher, example);
    }

    public EduTeacher selectOneEduTeacher(EduTeacher eduTeacher) {
        return eduTeacherMapper.selectOne(eduTeacher);
    }

    /**
     * 获取教师档案列表
     *
     * @param orgId
     * @param accountId
     * @param major
     * @param status
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String, String>> getTeacherList(String orgId, String accountId, String major, String status, String beginTime, String endTime, String search) {
        return eduTeacherMapper.getTeacherList(orgId, accountId, major, status, beginTime, endTime, "%" + search + "%");
    }

    /**
     * 获取教师档案列表
     *
     * @param pageParam
     * @param accountId
     * @param major
     * @param status
     * @param beginTime
     * @param endTime
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getTeacherList(PageParam pageParam, String accountId, String major, String status, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getTeacherList(pageParam.getOrgId(), accountId, major, status, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * 获取教师列表for select2
     *
     * @param orgId
     * @param search
     * @return
     */
    public List<Map<String, String>> getTeacherListForSelect2(String orgId, String search) {
        return eduTeacherMapper.getTeacherListForSelect2(orgId, "%" + search + "%");
    }
}
