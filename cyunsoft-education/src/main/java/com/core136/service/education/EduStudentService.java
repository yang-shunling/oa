package com.core136.service.education;

import com.core136.bean.education.EduStudent;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.education.EduStudentMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class EduStudentService {
    private EduStudentMapper eduStudentMapper;

    @Autowired
    public void setEduStudentMapper(EduStudentMapper eduStudentMapper) {
        this.eduStudentMapper = eduStudentMapper;
    }

    public int insertEduStudent(EduStudent eduStudent) {
        return eduStudentMapper.insert(eduStudent);
    }

    public int deleteEduStudent(EduStudent dEduStudent) {
        return eduStudentMapper.delete(dEduStudent);
    }

    public int updateEduStudent(Example example, EduStudent eduStudent) {
        return eduStudentMapper.updateByExampleSelective(eduStudent, example);
    }

    public EduStudent selectOneEduStudent(EduStudent eduStudent) {
        return eduStudentMapper.selectOne(eduStudent);
    }

    /**
     * @param orgId
     * @param semesterId
     * @param major
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getDutyStudentList
     * @Description:  获取入学学生管理列表
     */
    public List<Map<String, String>> getDutyStudentList(String orgId, String semesterId, String major, String status, String beginTime, String endTime, String search) {
        return eduStudentMapper.getDutyStudentList(orgId, semesterId, major, status, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param semesterId
     * @param major
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getDutyStudentList
     * @Description:  获取入学学生管理列表
     */
    public PageInfo<Map<String, String>> getDutyStudentList(PageParam pageParam, String semesterId, String major, String status, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getDutyStudentList(pageParam.getOrgId(), semesterId, major, status, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }


}
