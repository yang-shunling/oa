package com.core136.service.education;

import com.core136.bean.education.EduClass;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.education.EduClassMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class EduClassService {

    private EduClassMapper eduClassMapper;

    @Autowired
    public void setEduClassMapper(EduClassMapper eduClassMapper) {
        this.eduClassMapper = eduClassMapper;
    }

    public int insertEduClass(EduClass eduClass) {
        return eduClassMapper.insert(eduClass);
    }

    public int deleteEduClass(EduClass eduClass) {
        return eduClassMapper.delete(eduClass);
    }

    public int updateEduClass(Example example, EduClass eduClass) {
        return eduClassMapper.updateByExampleSelective(eduClass, example);
    }


    public EduClass selectOneEduClass(EduClass eduClass) {
        return eduClassMapper.selectOne(eduClass);
    }

    /**
     * @param orgId
     * @param departmentsId
     * @param semesterId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getEduClassList
     * @Description:  获取班级列表
     */
    public List<Map<String, String>> getEduClassList(String orgId, String departmentsId, String gradeId, String majorId, String semesterId, String search) {
        return eduClassMapper.getEduClassList(orgId, departmentsId, gradeId, majorId, semesterId, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param departmentsId
     * @param semesterId
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getEduClassList
     * @Description:  获取班级列表
     */
    public PageInfo<Map<String, String>> getEduClassList(PageParam pageParam, String departmentsId, String gradeId, String majorId, String semesterId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getEduClassList(pageParam.getOrgId(), departmentsId, gradeId, majorId, semesterId, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * 获取当前学期的班级列表
     *
     * @param orgId
     * @param opFlag
     * @param accountId
     * @param departmentsId
     * @param gradeId
     * @param majorId
     * @return
     */
    public List<Map<String, String>> getEduClassListForSet(String orgId, String opFlag, String accountId, String departmentsId, String gradeId, String majorId) {
        return eduClassMapper.getEduClassListForSet(orgId, opFlag, accountId, departmentsId, gradeId, majorId);
    }

}
