package com.core136.service.education;

import com.core136.bean.education.EduTeacherExperience;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.education.EduTeacherExperienceMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class EduTeacherExperienceService {
    private EduTeacherExperienceMapper eduTeacherExperienceMapper;

    @Autowired
    public void setEduTeacherExperienceMapper(EduTeacherExperienceMapper eduTeacherExperienceMapper) {
        this.eduTeacherExperienceMapper = eduTeacherExperienceMapper;
    }

    public int insertEduTeacherExperience(EduTeacherExperience eduTeacherExperience) {
        return eduTeacherExperienceMapper.insert(eduTeacherExperience);
    }

    public int deleteEduTeacherExperience(EduTeacherExperience eduTeacherExperience) {
        return eduTeacherExperienceMapper.delete(eduTeacherExperience);
    }

    public EduTeacherExperience selectOneEduTeacherExperience(EduTeacherExperience eduTeacherExperience) {
        return eduTeacherExperienceMapper.selectOne(eduTeacherExperience);
    }

    public int updateEduTeacherExperience(Example example, EduTeacherExperience eduTeacherExperience) {
        return eduTeacherExperienceMapper.updateByExampleSelective(eduTeacherExperience, example);
    }

    /**
     * 获取任教经历列表
     *
     * @param orgId
     * @param courseType
     * @param grade
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String, String>> getEduTeacherExperienceList(String orgId, String courseType, String grade, String beginTime, String endTime, String search) {
        return eduTeacherExperienceMapper.getEduTeacherExperienceList(orgId, courseType, grade, beginTime, endTime, "%" + search + "%");
    }

    /**
     * 获取任教经历列表
     *
     * @param pageParam
     * @param courseType
     * @param grade
     * @param beginTime
     * @param endTime
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getEduTeacherExperienceList(PageParam pageParam, String courseType, String grade, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getEduTeacherExperienceList(pageParam.getOrgId(), courseType, grade, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
