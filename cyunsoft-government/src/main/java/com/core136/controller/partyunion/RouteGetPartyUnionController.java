package com.core136.controller.partyunion;

import com.core136.bean.account.Account;
import com.core136.bean.partyunion.*;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.service.account.AccountService;
import com.core136.service.partyunion.*;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/ret/partyunionget")
public class RouteGetPartyUnionController {
    private AccountService accountService;

    @Autowired

    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private PartyUnionOrgService partyUnionOrgService;

    @Autowired
    public void setPartyUnionOrgService(PartyUnionOrgService partyUnionOrgService) {
        this.partyUnionOrgService = partyUnionOrgService;
    }

    private PartyUnionMemberService partyUnionMemberService;

    @Autowired
    public void setPartyUnionMemberService(PartyUnionMemberService partyUnionMemberService) {
        this.partyUnionMemberService = partyUnionMemberService;
    }

    private PartyUnionLevelService partyUnionLevelService;

    @Autowired
    public void setPartyUnionLevelService(PartyUnionLevelService partyUnionLevelService) {
        this.partyUnionLevelService = partyUnionLevelService;
    }

    private PartyUnionDeptService partyUnionDeptService;

    @Autowired
    public void setParyUnionDeptService(PartyUnionDeptService partyUnionDeptService) {
        this.partyUnionDeptService = partyUnionDeptService;
    }

    private PartyUnionHelpService partyUnionHelpService;

    @Autowired
    public void setPartyUnionHelpService(PartyUnionHelpService partyUnionHelpService) {
        this.partyUnionHelpService = partyUnionHelpService;
    }

    private PartyUnionActivityService partyUnionActivityService;

    @Autowired
    public void setPartyUnionActivityService(PartyUnionActivityService partyUnionActivityService) {
        this.partyUnionActivityService = partyUnionActivityService;
    }

    private PartyUnionEssenceService partyUnionEssenceService;

    @Autowired
    public void setPartyUnionEssenceService(PartyUnionEssenceService partyUnionEssenceService) {
        this.partyUnionEssenceService = partyUnionEssenceService;
    }

    private PartyUnionStoryService partyUnionStoryService;

    @Autowired
    public void setPartyUnionStoryService(PartyUnionStoryService partyUnionStoryService) {
        this.partyUnionStoryService = partyUnionStoryService;
    }

    private PartyUnionModelService partyUnionModelService;

    @Autowired
    public void setPartyUnionModelService(PartyUnionModelService partyUnionModelService) {
        this.partyUnionModelService = partyUnionModelService;
    }

    private PartyUnionMsgBoxService partyUnionMsgBoxService;

    @Autowired
    public void setPartyunionMsgBoxService(PartyUnionMsgBoxService partyUnionMsgBoxService) {
        this.partyUnionMsgBoxService = partyUnionMsgBoxService;
    }


    private PartyUnionTipService partyUnionTipService;

    @Autowired
    public void setPartyUnionTipService(PartyUnionTipService partyUnionTipService) {
        this.partyUnionTipService = partyUnionTipService;
    }

    private PartyUnionMediateService partyUnionMediateService;

    @Autowired
    public void setPartyUnionMediateService(PartyUnionMediateService partyUnionMediateService) {
        this.partyUnionMediateService = partyUnionMediateService;
    }

    private PartyUnionInstSortService partyUnionInstSortService;

    @Autowired
    public void setPartyUnionInstSortService(PartyUnionInstSortService partyUnionInstSortService) {
        this.partyUnionInstSortService = partyUnionInstSortService;
    }

    private PartyUnionInstService partyUnionInstService;

    @Autowired
    public void setPartyUnionInstService(PartyUnionInstService partyUnionInstService) {
        this.partyUnionInstService = partyUnionInstService;
    }

    private PartyUnionWomanService partyUnionWomanService;

    @Autowired
    public void setPartyUnionWomanService(PartyUnionWomanService partyUnionWomanService) {
        this.partyUnionWomanService = partyUnionWomanService;
    }

    private PartyUnionTrainingService partyUnionTrainingService;

    @Autowired
    public void setPartyUnionTrainingService(PartyUnionTrainingService partyUnionTrainingService) {
        this.partyUnionTrainingService = partyUnionTrainingService;
    }

    private PartyUnionWorkerService partyUnionWorkerService;

    @Autowired
    public void setPartyUnionWorkerService(PartyUnionWorkerService partyUnionWorkerService) {
        this.partyUnionWorkerService = partyUnionWorkerService;
    }

    private PartyUnionFundsService partyUnionFundsService;

    @Autowired
    public void setPartyUnionFundsService(PartyUnionFundsService partyUnionFundsService) {
        this.partyUnionFundsService = partyUnionFundsService;
    }

    /**
     * 获取工会经费
     *
     * @param partyUnionFunds
     * @return
     */
    @RequestMapping(value = "/getPartyUnionFundsById", method = RequestMethod.POST)
    public RetDataBean getPartyUnionFundsById(PartyUnionFunds partyUnionFunds) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionFunds.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyUnionFundsService.selectOnePartyUnionFunds(partyUnionFunds));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取农民工列表
     *
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @param beginTimeBirthday
     * @param endTimeBirthday
     * @return
     */
    @RequestMapping(value = "/getPartyUnionWorkersList", method = RequestMethod.POST)
    public RetDataBean getPartyUnionWorkersList(PageParam pageParam, String beginTime, String endTime, String beginTimeBirthday, String endTimeBirthday) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("a.create_time");
            } else {
                pageParam.setSort("a." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyUnionWorkerService.getPartyUnionWorkersList(pageParam, beginTime, endTime, beginTimeBirthday, endTimeBirthday);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 获取农民工详情
     *
     * @param partyUnionWorker
     * @return
     */
    @RequestMapping(value = "/getPartyUnionWorkerById", method = RequestMethod.POST)
    public RetDataBean getPartyUnionWorkerById(PartyUnionWorker partyUnionWorker) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionWorker.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyUnionWorkerService.selectOnePartyUnionWorker(partyUnionWorker));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取就业培训记录列表
     *
     * @param pageParam
     * @param trainType
     * @param beginTime
     * @param endTime
     * @return
     */
    @RequestMapping(value = "/getPartyUnionTrainingList", method = RequestMethod.POST)
    public RetDataBean getPartyUnionTrainingList(PageParam pageParam, String trainType, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("l.create_time");
            } else {
                pageParam.setSort("l." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyUnionTrainingService.getPartyUnionTrainingList(pageParam, trainType, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取困难帮扶记录列表
     *
     * @param pageParam
     * @param helpType
     * @param beginTime
     * @param endTime
     * @return
     */
    @RequestMapping(value = "/getPartyUnionHelpList", method = RequestMethod.POST)
    public RetDataBean getPartyUnionHelpList(PageParam pageParam, String helpType, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("a.create_time");
            } else {
                pageParam.setSort("a." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyUnionHelpService.getPartyUnionHelpList(pageParam, helpType, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 就业培训详情
     *
     * @param partyUnionTraining
     * @return
     */
    @RequestMapping(value = "/getPartyUnionTrainingById", method = RequestMethod.POST)
    public RetDataBean getPartyUnionTrainingById(PartyUnionTraining partyUnionTraining) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionTraining.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyUnionTrainingService.selectOnePartyUnionTraining(partyUnionTraining));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取三八红旗手详情
     *
     * @param partyUnionWoman
     * @return
     */
    @RequestMapping(value = "/getPartyUnionWomanById", method = RequestMethod.POST)
    public RetDataBean getPartyUnionWomanById(PartyUnionWoman partyUnionWoman) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionWoman.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyUnionWomanService.selectOnePartyUnionWoman(partyUnionWoman));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 获取法律法规列表
     *
     * @param pageParam
     * @param sortId
     * @param beginTime
     * @param endTime
     * @return
     */
    @RequestMapping(value = "/getPartyUnionInstList", method = RequestMethod.POST)
    public RetDataBean getPartyUnionInstList(PageParam pageParam, String sortId, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("a.create_time");
            } else {
                pageParam.setSort("a." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyUnionInstService.getPartyUnionInstList(pageParam, sortId, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取法律法规详情
     *
     * @param partyUnionInst
     * @return
     */
    @RequestMapping(value = "/getPartyUnionInstById", method = RequestMethod.POST)
    public RetDataBean getPartyUnionInstById(PartyUnionInst partyUnionInst) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionInst.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyUnionInstService.selectOnePartyUnionInst(partyUnionInst));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取法律法规分类
     *
     * @param sortId
     * @return
     */
    @RequestMapping(value = "/getPartyUnionInstSortTree", method = RequestMethod.POST)
    public List<Map<String, String>> getPartyUnionInstSortTree(String sortId) {
        try {
            String levelId = "0";
            if (StringUtils.isNotBlank(sortId)) {
                levelId = sortId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return partyUnionInstSortService.getPartyUnionInstSortTree(account.getOrgId(), levelId);
        } catch (Exception e) {
            return null;
        }
    }


    /**
     * 法律法规分类详情
     *
     * @param partyUnionInstSort
     * @return
     */
    @RequestMapping(value = "/getPartyUnionInstSortById", method = RequestMethod.POST)
    public RetDataBean getPartyUnionInstSortById(PartyUnionInstSort partyUnionInstSort) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionInstSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyUnionInstSortService.selectOnePartyUnionInstSort(partyUnionInstSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取调解事件列表
     *
     * @param pageParam
     * @param mediateType
     * @param beginTime
     * @param endTime
     * @return
     */
    @RequestMapping(value = "/getPartyUnionMediateList", method = RequestMethod.POST)
    public RetDataBean getPartyUnionMediateList(PageParam pageParam, String mediateType, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("a.create_time");
            } else {
                pageParam.setSort("a." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyUnionMediateService.getPartyUnionMediateList(pageParam, mediateType, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 维权调解详情
     *
     * @param partyUnionMediate
     * @return
     */
    @RequestMapping(value = "/getPartyUnionMediateById", method = RequestMethod.POST)
    public RetDataBean getPartyUnionMediateById(PartyUnionMediate partyUnionMediate) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionMediate.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyUnionMediateService.selectOnePartyUnionMediate(partyUnionMediate));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取举报记录列表
     *
     * @param pageParam
     * @param tipType
     * @param beginTime
     * @param endTime
     * @return
     */
    @RequestMapping(value = "/getPartyUnionTipList", method = RequestMethod.POST)
    public RetDataBean getPartyUnionTipList(PageParam pageParam, String tipType, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("l.create_time");
            } else {
                pageParam.setSort("l." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyUnionTipService.getPartyUnionTipList(pageParam, tipType, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取举报记录详情
     *
     * @param partyUnionTip
     * @return
     */
    @RequestMapping(value = "/getPartyUnionTipById", method = RequestMethod.POST)
    public RetDataBean getPartyUnionTipById(PartyUnionTip partyUnionTip) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionTip.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyUnionTipService.selectOnePartyUnionTip(partyUnionTip));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取劳模记录列表
     *
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return
     */
    @RequestMapping(value = "/getPartyUnionModelList", method = RequestMethod.POST)
    public RetDataBean getPartyUnionModelList(PageParam pageParam, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("a.sort_no");
            } else {
                pageParam.setSort("a." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyUnionModelService.getPartyUnionModelList(pageParam, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取三八红旗手列表
     *
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return
     */
    @RequestMapping(value = "/getPartyUnionWomanList", method = RequestMethod.POST)
    public RetDataBean getPartyUnionWomanList(PageParam pageParam, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("a.sort_no");
            } else {
                pageParam.setSort("a." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyUnionWomanService.getPartyUnionWomanList(pageParam, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取职工信箱详情
     *
     * @param partyunionMsgBox
     * @return
     */
    @RequestMapping(value = "/getPartyUnionMsgBoxById", method = RequestMethod.POST)
    public RetDataBean getPartyUnionMsgBoxById(PartyUnionMsgBox partyunionMsgBox) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyunionMsgBox.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyUnionMsgBoxService.selectOnePartyUnionMsgBox(partyunionMsgBox));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @return RetDataBean
     * @Title: getMyMsgBoxList
     * @Description:  获取领导信箱记录列表
     */
    @RequestMapping(value = "/getMyMsgBoxList", method = RequestMethod.POST)
    public RetDataBean getMyMsgBoxList(PageParam pageParam) {
        Account account = accountService.getRedisAUserInfoToAccount();
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("l.create_time");
            } else {
                pageParam.setSort("l." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            pageParam.setOrderBy(orderBy);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyUnionMsgBoxService.getMyMsgBoxList(pageParam));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @return RetDataBean
     * @Title: getLeaderMsgBoxList
     * @Description:  获取领导信箱信息列表
     */
    @RequestMapping(value = "/getLeaderMsgBoxList", method = RequestMethod.POST)
    public RetDataBean getLeaderMsgBoxList(PageParam pageParam) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("l.create_time");
            } else {
                pageParam.setSort("l." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            pageParam.setOrderBy(orderBy);
            PageInfo<Map<String, String>> pageInfo = partyUnionMsgBoxService.getLeaderMsgBoxList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 获取劳模详情
     *
     * @param partyUnionModel
     * @return
     */
    @RequestMapping(value = "/getPartyUnionModelById", method = RequestMethod.POST)
    public RetDataBean getPartyUnionModelById(PartyUnionModel partyUnionModel) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionModel.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyUnionModelService.selectOnePartyUnionModel(partyUnionModel));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 职场故事列表
     *
     * @param pageParam
     * @param storyType
     * @param beginTime
     * @param endTime
     * @return
     */
    @RequestMapping(value = "/getPartyUnionStoryList", method = RequestMethod.POST)
    public RetDataBean getPartyUnionStoryList(PageParam pageParam, String storyType, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("a.sort_no");
            } else {
                pageParam.setSort("a." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyUnionStoryService.getPartyUnionStoryList(pageParam, storyType, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取职场故事详情
     *
     * @param partyUnionStory
     * @return
     */
    @RequestMapping(value = "/getPartyUnionStoryById", method = RequestMethod.POST)
    public RetDataBean getPartyUnionStoryById(PartyUnionStory partyUnionStory) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionStory.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyUnionStoryService.selectOnePartyUnionStory(partyUnionStory));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取职场精英列表
     *
     * @param pageParam
     * @param essenceType
     * @param beginTime
     * @param endTime
     * @return
     */
    @RequestMapping(value = "/getPartyUnionEssenceList", method = RequestMethod.POST)
    public RetDataBean getPartyUnionEssenceList(PageParam pageParam, String essenceType, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("a.sort_no");
            } else {
                pageParam.setSort("a." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyUnionEssenceService.getPartyUnionEssenceList(pageParam, essenceType, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取职场精英详情
     *
     * @param partyUnionEssence
     * @return
     */
    @RequestMapping(value = "/getPartyUnionEssenceById", method = RequestMethod.POST)
    public RetDataBean getPartyUnionEssenceById(PartyUnionEssence partyUnionEssence) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionEssence.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyUnionEssenceService.selectOnePartyUnionEssence(partyUnionEssence));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取工会活动列表
     *
     * @param pageParam
     * @param activityType
     * @param beginTime
     * @param endTime
     * @return
     */
    @RequestMapping(value = "/getPartyUnionActivityList", method = RequestMethod.POST)
    public RetDataBean getPartyUnionActivityList(PageParam pageParam, String activityType, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("a.sort_no");
            } else {
                pageParam.setSort("a." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyUnionActivityService.getPartyUnionActivityList(pageParam, activityType, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取工会活动详情
     *
     * @param partyUnionActivity
     * @return
     */
    @RequestMapping(value = "/getPartyUnionActivityById", method = RequestMethod.POST)
    public RetDataBean getPartyUnionActivityById(PartyUnionActivity partyUnionActivity) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionActivity.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyUnionActivityService.selectOnePartyUnionActivity(partyUnionActivity));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取工会部门
     *
     * @param deptId
     * @return
     */
    @RequestMapping(value = "/getPartyUnionDeptTree", method = RequestMethod.POST)
    public List<Map<String, String>> getPartyUnionDeptTree(String deptId) {
        try {
            String orgLevelId = "0";
            if (StringUtils.isNotBlank(deptId)) {
                orgLevelId = deptId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return partyUnionDeptService.getPartyUnionDeptTree(account.getOrgId(), orgLevelId);
        } catch (Exception e) {
            return null;
        }
    }

    @RequestMapping(value = "/getPartyUnionHelpById", method = RequestMethod.POST)
    public RetDataBean getPartyUnionHelpById(PartyUnionHelp partyUnionHelp) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionHelp.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyUnionHelpService.selectOnePartyUnionHelp(partyUnionHelp));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取历史工会列表
     *
     * @param pageParam
     * @return
     */
    @RequestMapping(value = "/getAllPartyUnionOrgList", method = RequestMethod.POST)
    public RetDataBean getAllPartyUnionOrgList(PageParam pageParam) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("begin_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<PartyUnionOrg> pageInfo = partyUnionOrgService.getAllPartyUnionOrgList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    @RequestMapping(value = "/getPartyUnionOrgById", method = RequestMethod.POST)
    public RetDataBean getPartyUnionOrgById(PartyUnionOrg partyUnionOrg) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionOrg.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyUnionOrgService.selectOnePartyUnionOrg(partyUnionOrg));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    @RequestMapping(value = "/getPartyUnionOrg", method = RequestMethod.POST)
    public RetDataBean getPartyUnionOrg() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyUnionOrgService.getPartyUnionOrg(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    @RequestMapping(value = "/getPartyUnionMemberById", method = RequestMethod.POST)
    public RetDataBean getPartyUnionMemberById(PartyUnionMember partyUnionMember) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionMember.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyUnionMemberService.selectOnePartyUnionMember(partyUnionMember));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    @RequestMapping(value = "/getPartyUnionLevelById", method = RequestMethod.POST)
    public RetDataBean getParyUnionDeptById(PartyUnionLevel partyUnionLevel) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionLevel.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyUnionLevelService.selectOnePartyUnionLevel(partyUnionLevel));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    @RequestMapping(value = "/getPartyUnionDeptById", method = RequestMethod.POST)
    public RetDataBean getPartyUnionDeptById(PartyUnionDept partyUnionDept) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionDept.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyUnionDeptService.selectOnePartyUnionDept(partyUnionDept));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    @RequestMapping(value = "/getPartyUnionLevelList", method = RequestMethod.POST)
    public RetDataBean getPartyUnionLevelList(
            Integer pageNumber,
            Integer pageSize,
            String search,
            String sort,
            String sortOrder
    ) {
        try {
            if (StringUtils.isBlank(sort)) {
                sort = "sort_no";
            } else {
                sort = StrTools.lowerCharToUnderLine(sort);
            }
            if (StringUtils.isBlank(sortOrder)) {
                sortOrder = "asc";
            }
            String orderBy = sort + " " + sortOrder;
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyUnionLevel.class);
            example.setOrderByClause(orderBy);
            Example.Criteria criteria = example.createCriteria();
            criteria.andEqualTo("orgId", account.getOrgId());
            if (StringUtils.isNotEmpty(search)) {
                Example.Criteria criteria2 = example.createCriteria();
                criteria2.orLike("levelName", "%" + search + "%").orLike("remark", "%" + search + "%");
                example.and(criteria2);
            }
            PageInfo<PartyUnionLevel> pageInfo = partyUnionLevelService.getPartyUnionLevelList(example, pageNumber, pageSize);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取工会职务列表
     *
     * @return
     */
    @RequestMapping(value = "/getPartyUnionLevelForList", method = RequestMethod.POST)
    public RetDataBean getPartyUnionLevelForList() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyUnionLevelService.getPartyUnionLevelForList(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 按基层获取成员列表
     *
     * @param pageParam
     * @param deptId
     * @return
     */
    @RequestMapping(value = "/getPartyUnionMemberListByDeptId", method = RequestMethod.POST)
    public RetDataBean getPartyUnionMemberListByDeptId(PageParam pageParam, String deptId) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("m.sort_no");
            } else {
                pageParam.setSort("m." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyUnionMemberService.getPartyUnionMemberListByDeptId(pageParam, deptId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

}
