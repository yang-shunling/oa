package com.core136.controller.partyunion;

import com.core136.bean.account.Account;
import com.core136.bean.account.UserInfo;
import com.core136.bean.partyunion.*;
import com.core136.bean.sys.CodeClass;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.account.UserInfoService;
import com.core136.service.partyunion.*;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/set/partyunionset")
public class RouteSetPartyUnionController {
    private AccountService accountService;

    @Autowired

    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private UserInfoService userInfoService;

    @Autowired
    public void setUserInfoService(UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }

    private PartyUnionOrgService partyUnionOrgService;

    @Autowired
    public void setPartyUnionOrgService(PartyUnionOrgService partyUnionOrgService) {
        this.partyUnionOrgService = partyUnionOrgService;
    }

    private PartyUnionMemberService partyUnionMemberService;

    @Autowired
    public void setPartyUnionMemberService(PartyUnionMemberService partyUnionMemberService) {
        this.partyUnionMemberService = partyUnionMemberService;
    }

    private PartyUnionLevelService partyUnionLevelService;

    @Autowired
    public void setPartyUnionLevelService(PartyUnionLevelService partyUnionLevelService) {
        this.partyUnionLevelService = partyUnionLevelService;
    }

    private PartyUnionDeptService partyUnionDeptService;

    @Autowired
    public void setPartyUnionDeptService(PartyUnionDeptService partyUnionDeptService) {
        this.partyUnionDeptService = partyUnionDeptService;
    }

    private PartyUnionHelpService partyUnionHelpService;

    @Autowired
    public void setPartyUnionHelpService(PartyUnionHelpService partyUnionHelpService) {
        this.partyUnionHelpService = partyUnionHelpService;
    }

    private PartyUnionActivityService partyUnionActivityService;

    @Autowired
    public void setPartyUnionActivityService(PartyUnionActivityService partyUnionActivityService) {
        this.partyUnionActivityService = partyUnionActivityService;
    }

    private PartyUnionEssenceService partyUnionEssenceService;

    @Autowired
    public void setPartyUnionEssenceService(PartyUnionEssenceService partyUnionEssenceService) {
        this.partyUnionEssenceService = partyUnionEssenceService;
    }

    private PartyUnionStoryService partyUnionStoryService;

    @Autowired
    public void setPartyUnionStoryService(PartyUnionStoryService partyUnionStoryService) {
        this.partyUnionStoryService = partyUnionStoryService;
    }

    private PartyUnionModelService partyUnionModelService;

    @Autowired
    public void setPartyUnionModelService(PartyUnionModelService partyUnionModelService) {
        this.partyUnionModelService = partyUnionModelService;
    }

    private PartyUnionMsgBoxService partyUnionMsgBoxService;

    @Autowired
    public void setPartyUnionMsgBoxService(PartyUnionMsgBoxService partyUnionMsgBoxService) {
        this.partyUnionMsgBoxService = partyUnionMsgBoxService;
    }

    private PartyUnionTipService partyUnionTipService;

    @Autowired
    public void setPartyUnionTipService(PartyUnionTipService partyUnionTipService) {
        this.partyUnionTipService = partyUnionTipService;
    }

    private PartyUnionMediateService partyUnionMediateService;

    @Autowired
    public void setPartyUnionMediateService(PartyUnionMediateService partyUnionMediateService) {
        this.partyUnionMediateService = partyUnionMediateService;
    }

    private PartyUnionInstSortService partyUnionInstSortService;

    @Autowired
    public void setPartyUnionInstSortService(PartyUnionInstSortService partyUnionInstSortService) {
        this.partyUnionInstSortService = partyUnionInstSortService;
    }

    private PartyUnionInstService partyUnionInstService;

    @Autowired
    public void setPartyUnionInstService(PartyUnionInstService partyUnionInstService) {
        this.partyUnionInstService = partyUnionInstService;
    }

    private PartyUnionWomanService partyUnionWomanService;

    @Autowired
    public void setPartyUnionWomanService(PartyUnionWomanService partyUnionWomanService) {
        this.partyUnionWomanService = partyUnionWomanService;
    }

    private PartyUnionTrainingService partyUnionTrainingService;

    @Autowired
    public void setPartyUnionTrainingService(PartyUnionTrainingService partyUnionTrainingService) {
        this.partyUnionTrainingService = partyUnionTrainingService;
    }

    private PartyUnionWorkerService partyUnionWorkerService;

    @Autowired
    public void setPartyUnionWorkerService(PartyUnionWorkerService partyUnionWorkerService) {
        this.partyUnionWorkerService = partyUnionWorkerService;
    }

    private PartyUnionFundsService partyUnionFundsService;

    @Autowired
    public void setPartyUnionFundsService(PartyUnionFundsService partyUnionFundsService) {
        this.partyUnionFundsService = partyUnionFundsService;
    }

    /**
     * 创建工会经费记录
     *
     * @param partyUnionFunds
     * @return
     */
    @RequestMapping(value = "/insertPartyUnionFunds", method = RequestMethod.POST)
    public RetDataBean insertPartyUnionWorker(PartyUnionFunds partyUnionFunds) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionFunds.setRecordId(SysTools.getGUID());
            partyUnionFunds.setCreateUser(account.getAccountId());
            partyUnionFunds.setStatus("0");
            partyUnionFunds.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyUnionFunds.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyUnionFundsService.insertPartyUnionFunds(partyUnionFunds));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除工会经费记录
     *
     * @param partyUnionFunds
     * @return
     */
    @RequestMapping(value = "/deletePartyUnionFunds", method = RequestMethod.POST)
    public RetDataBean deletePartyUnionFunds(PartyUnionFunds partyUnionFunds) {
        try {
            if (StringUtils.isBlank(partyUnionFunds.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionFunds.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyUnionFundsService.deletePartyUnionFunds(partyUnionFunds));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新工会经费记录
     *
     * @param partyUnionFunds
     * @return
     */
    @RequestMapping(value = "/updatePartyUnionFunds", method = RequestMethod.POST)
    public RetDataBean updatePartyUnionFunds(PartyUnionFunds partyUnionFunds) {
        try {
            if (StringUtils.isBlank(partyUnionFunds.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyUnionFunds.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyUnionFunds.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyUnionFundsService.updatePartyUnionFunds(example, partyUnionFunds));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 添加农民工记录
     *
     * @param partyUnionWorker
     * @return
     */
    @RequestMapping(value = "/insertPartyUnionWorker", method = RequestMethod.POST)
    public RetDataBean insertPartyUnionWorker(PartyUnionWorker partyUnionWorker) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionWorker.setRecordId(SysTools.getGUID());
            partyUnionWorker.setCreateUser(account.getAccountId());
            partyUnionWorker.setStatus("0");
            partyUnionWorker.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyUnionWorker.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyUnionWorkerService.insertPartyUnionWorker(partyUnionWorker));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除农民工记录
     *
     * @param partyUnionWorker
     * @return
     */
    @RequestMapping(value = "/deletePartyUnionWorker", method = RequestMethod.POST)
    public RetDataBean deletePartyUnionWorker(PartyUnionWorker partyUnionWorker) {
        try {
            if (StringUtils.isBlank(partyUnionWorker.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionWorker.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyUnionWorkerService.deletePartyUnionWorker(partyUnionWorker));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新农民工记录
     *
     * @param partyUnionWorker
     * @return
     */
    @RequestMapping(value = "/updatePartyUnionWorker", method = RequestMethod.POST)
    public RetDataBean updatePartyUnionWorker(PartyUnionWorker partyUnionWorker) {
        try {
            if (StringUtils.isBlank(partyUnionWorker.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyUnionWorker.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyUnionWorker.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyUnionWorkerService.updatePartyUnionWorker(example, partyUnionWorker));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 添加就业培训记录
     *
     * @param partyUnionTraining
     * @return
     */
    @RequestMapping(value = "/insertPartyUnionTraining", method = RequestMethod.POST)
    public RetDataBean insertPartyUnionTraining(PartyUnionTraining partyUnionTraining) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionTraining.setRecordId(SysTools.getGUID());
            partyUnionTraining.setCreateUser(account.getAccountId());
            partyUnionTraining.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyUnionTraining.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyUnionTrainingService.insertPartyUnionTraining(partyUnionTraining));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除就业培训记录
     *
     * @param partyUnionTraining
     * @return
     */
    @RequestMapping(value = "/deletePartyUnionTraining", method = RequestMethod.POST)
    public RetDataBean deletePartyUnionTraining(PartyUnionTraining partyUnionTraining) {
        try {
            if (StringUtils.isBlank(partyUnionTraining.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionTraining.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyUnionTrainingService.deletePartyUnionTraining(partyUnionTraining));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新就业培训记录
     *
     * @param partyUnionTraining
     * @return
     */
    @RequestMapping(value = "/updatePartyUnionTraining", method = RequestMethod.POST)
    public RetDataBean updatePartyUnionTraining(PartyUnionTraining partyUnionTraining) {
        try {
            if (StringUtils.isBlank(partyUnionTraining.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyUnionTraining.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyUnionTraining.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyUnionTrainingService.updatePartyUnionTraining(example, partyUnionTraining));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 创建三八红旗手
     *
     * @param partyUnionWoman
     * @return
     */
    @RequestMapping(value = "/insertPartyUnionWoman", method = RequestMethod.POST)
    public RetDataBean insertPartyUnionWoman(PartyUnionWoman partyUnionWoman) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionWoman.setRecordId(SysTools.getGUID());
            partyUnionWoman.setCreateUser(account.getAccountId());
            partyUnionWoman.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyUnionWoman.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyUnionWomanService.insertPartyUnionWoman(partyUnionWoman));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除三八红旗手记录
     *
     * @param partyUnionWoman
     * @return
     */
    @RequestMapping(value = "/deletePartyUnionWoman", method = RequestMethod.POST)
    public RetDataBean deletePartyUnionWoman(PartyUnionWoman partyUnionWoman) {
        try {
            if (StringUtils.isBlank(partyUnionWoman.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionWoman.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyUnionWomanService.deletePartyUnionWoman(partyUnionWoman));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新三八红旗手记录
     *
     * @param partyUnionWoman
     * @return
     */
    @RequestMapping(value = "/updatePartyUnionWoman", method = RequestMethod.POST)
    public RetDataBean updatePartyUnionWoman(PartyUnionWoman partyUnionWoman) {
        try {
            if (StringUtils.isBlank(partyUnionWoman.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyUnionWoman.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyUnionWoman.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyUnionWomanService.updatePartyUnionWoman(example, partyUnionWoman));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 添加法律法规
     *
     * @param partyUnionInst
     * @return
     */
    @RequestMapping(value = "/insertPartyUnionInst", method = RequestMethod.POST)
    public RetDataBean insertPartyUnionInst(PartyUnionInst partyUnionInst) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionInst.setRecordId(SysTools.getGUID());
            Document htmlDoc = Jsoup.parse(partyUnionInst.getContent());
            String subheading = htmlDoc.text();
            if (subheading.length() > 50) {
                subheading = subheading.substring(0, 50) + "...";
            }
            partyUnionInst.setSubheading(subheading);
            partyUnionInst.setCreateUser(account.getAccountId());
            partyUnionInst.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyUnionInst.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyUnionInstService.insertPartyUnionInst(partyUnionInst));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除法律法规
     *
     * @param partyUnionInst
     * @return
     */
    @RequestMapping(value = "/deletePartyUnionInst", method = RequestMethod.POST)
    public RetDataBean deletePartyUnionInst(PartyUnionInst partyUnionInst) {
        try {
            if (StringUtils.isBlank(partyUnionInst.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionInst.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyUnionInstService.deletePartyUnionInst(partyUnionInst));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新法律法规
     *
     * @param partyUnionInst
     * @return
     */
    @RequestMapping(value = "/updatePartyUnionInst", method = RequestMethod.POST)
    public RetDataBean updatePartyUnionInst(PartyUnionInst partyUnionInst) {
        try {
            if (StringUtils.isBlank(partyUnionInst.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyUnionInst.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyUnionInst.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyUnionInstService.updatePartyUnionInst(example, partyUnionInst));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 创建法律法规分类
     *
     * @param partyUnionInstSort
     * @return
     */
    @RequestMapping(value = "/insertPartyUnionInstSort", method = RequestMethod.POST)
    public RetDataBean insertPartyUnionInstSort(PartyUnionInstSort partyUnionInstSort) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionInstSort.setSortId(SysTools.getGUID());
            partyUnionInstSort.setCreateUser(account.getAccountId());
            if (StringUtils.isBlank(partyUnionInstSort.getLevelId())) {
                partyUnionInstSort.setLevelId("0");
            }
            partyUnionInstSort.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyUnionInstSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyUnionInstSortService.insertPartyUnionInstSort(partyUnionInstSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除法律法规分类
     *
     * @param partyUnionInstSort
     * @return
     */
    @RequestMapping(value = "/deletePartyUnionInstSort", method = RequestMethod.POST)
    public RetDataBean deletePartyUnionInstSort(PartyUnionInstSort partyUnionInstSort) {
        try {
            if (StringUtils.isBlank(partyUnionInstSort.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionInstSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyUnionInstSortService.deletePartyUnionInstSort(partyUnionInstSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新法律法规分类
     *
     * @param partyUnionInstSort
     * @return
     */
    @RequestMapping(value = "/updatePartyUnionInstSort", method = RequestMethod.POST)
    public RetDataBean updatePartyUnionInstSort(PartyUnionInstSort partyUnionInstSort) {
        try {
            if (StringUtils.isBlank(partyUnionInstSort.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            if (StringUtils.isBlank(partyUnionInstSort.getLevelId())) {
                partyUnionInstSort.setLevelId("0");
            }
            if (partyUnionInstSort.getSortId().equals(partyUnionInstSort.getLevelId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_PARENT_ID_CANNOT_TO_USE);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyUnionInstSort.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("sortId", partyUnionInstSort.getSortId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyUnionInstSortService.updatePartyUnionInstSort(example, partyUnionInstSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 添加维权调解
     *
     * @param partyUnionMediate
     * @return
     */
    @RequestMapping(value = "/insertPartyUnionMediate", method = RequestMethod.POST)
    public RetDataBean insertPartyUnionMediate(PartyUnionMediate partyUnionMediate) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionMediate.setRecordId(SysTools.getGUID());
            partyUnionMediate.setCreateUser(account.getAccountId());
            partyUnionMediate.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyUnionMediate.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyUnionMediateService.insertPartyUnionMediate(partyUnionMediate));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除维权调解
     *
     * @param partyUnionMediate
     * @return
     */
    @RequestMapping(value = "/deletePartyUnionMediate", method = RequestMethod.POST)
    public RetDataBean deletePartyUnionMediate(PartyUnionMediate partyUnionMediate) {
        try {
            if (StringUtils.isBlank(partyUnionMediate.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionMediate.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyUnionMediateService.deletePartyUnionMediate(partyUnionMediate));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新维权调解
     *
     * @param partyUnionMediate
     * @return
     */
    @RequestMapping(value = "/updatePartyUnionMediate", method = RequestMethod.POST)
    public RetDataBean updatePartyUnionMediate(PartyUnionMediate partyUnionMediate) {
        try {
            if (StringUtils.isBlank(partyUnionMediate.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyUnionMediate.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyUnionMediate.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyUnionMediateService.updatePartyUnionMediate(example, partyUnionMediate));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 添加举报记录
     *
     * @param partyUnionTip
     * @return
     */
    @RequestMapping(value = "/insertPartyUnionTip", method = RequestMethod.POST)
    public RetDataBean insertPartyUnionTip(PartyUnionTip partyUnionTip) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionTip.setRecordId(SysTools.getGUID());
            partyUnionTip.setCreateUser(account.getAccountId());
            partyUnionTip.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyUnionTip.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyUnionTipService.insertPartyUnionTip(partyUnionTip));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除举报记录
     *
     * @param partyUnionTip
     * @return
     */
    @RequestMapping(value = "/deletePartyUnionTip", method = RequestMethod.POST)
    public RetDataBean deletePartyUnionTip(PartyUnionTip partyUnionTip) {
        try {
            if (StringUtils.isBlank(partyUnionTip.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionTip.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyUnionTipService.deletePartyUnionTip(partyUnionTip));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新职举报记录
     *
     * @param partyUnionTip
     * @return
     */
    @RequestMapping(value = "/updatePartyUnionTip", method = RequestMethod.POST)
    public RetDataBean updatePartyUnionTip(PartyUnionTip partyUnionTip) {
        try {
            if (StringUtils.isBlank(partyUnionTip.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyUnionTip.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyUnionTip.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyUnionTipService.updatePartyUnionTip(example, partyUnionTip));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 添加职工信息记录
     *
     * @param partyUnionMsgBox
     * @return
     */
    @RequestMapping(value = "/insertPartyUnionMsgBox", method = RequestMethod.POST)
    public RetDataBean insertPartyUnionMsgBox(PartyUnionMsgBox partyUnionMsgBox) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionMsgBox.setRecordId(SysTools.getGUID());
            partyUnionMsgBox.setCreateUser(account.getAccountId());
            partyUnionMsgBox.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyUnionMsgBox.setStatus("0");
            partyUnionMsgBox.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyUnionMsgBoxService.insertPartyUnionMsgBox(partyUnionMsgBox));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除职工信箱记录
     *
     * @param partyUnionMsgBox
     * @return
     */
    @RequestMapping(value = "/deletePartyUnionMsgBox", method = RequestMethod.POST)
    public RetDataBean deletePartyUnionMsgBox(PartyUnionMsgBox partyUnionMsgBox) {
        try {
            if (StringUtils.isBlank(partyUnionMsgBox.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionMsgBox.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyUnionMsgBoxService.deletePartyUnionMsgBox(partyUnionMsgBox));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新职工信箱记录
     *
     * @param partyUnionMsgBox
     * @return
     */
    @RequestMapping(value = "/updatePartyUnionMsgBox", method = RequestMethod.POST)
    public RetDataBean updatePartyUnionMsgBox(PartyUnionMsgBox partyUnionMsgBox) {
        try {
            if (StringUtils.isBlank(partyUnionMsgBox.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyUnionMsgBox.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyUnionMsgBox.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyUnionMsgBoxService.updatePartyUnionMsgBox(example, partyUnionMsgBox));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 创建劳模记录
     *
     * @param partyUnionModel
     * @return
     */
    @RequestMapping(value = "/insertPartyUnionModel", method = RequestMethod.POST)
    public RetDataBean insertPartyUnionModel(PartyUnionModel partyUnionModel) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isNotBlank(partyUnionModel.getAccountId())) {
                UserInfo userInfo = new UserInfo();
                userInfo.setAccountId(partyUnionModel.getAccountId());
                userInfo.setOrgId(account.getOrgId());
                userInfo = userInfoService.selectOneUserInfo(userInfo);
                partyUnionModel.setUserName(userInfo.getUserName());
            }
            partyUnionModel.setRecordId(SysTools.getGUID());
            partyUnionModel.setCreateUser(account.getAccountId());
            partyUnionModel.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyUnionModel.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyUnionModelService.insertPartyUnionModel(partyUnionModel));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除劳模记录
     *
     * @param partyUnionModel
     * @return
     */
    @RequestMapping(value = "/deletePartyUnionModel", method = RequestMethod.POST)
    public RetDataBean deletePartyUnionModel(PartyUnionModel partyUnionModel) {
        try {
            if (StringUtils.isBlank(partyUnionModel.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionModel.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyUnionModelService.deletePartyUnionModel(partyUnionModel));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 劳模记录更新
     *
     * @param partyUnionModel
     * @return
     */
    @RequestMapping(value = "/updatePartyUnionModel", method = RequestMethod.POST)
    public RetDataBean updatePartyUnionModel(PartyUnionModel partyUnionModel) {
        try {
            if (StringUtils.isBlank(partyUnionModel.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isNotBlank(partyUnionModel.getAccountId())) {
                UserInfo userInfo = new UserInfo();
                userInfo.setAccountId(partyUnionModel.getAccountId());
                userInfo.setOrgId(account.getOrgId());
                userInfo = userInfoService.selectOneUserInfo(userInfo);
                partyUnionModel.setUserName(userInfo.getUserName());
            }
            Example example = new Example(PartyUnionModel.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyUnionModel.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyUnionModelService.updatePartyUnionModel(example, partyUnionModel));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 添加职场故事
     *
     * @param partyUnionStory
     * @return
     */
    @RequestMapping(value = "/insertPartyUnionStory", method = RequestMethod.POST)
    public RetDataBean insertPartyUnionStory(PartyUnionStory partyUnionStory) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionStory.setRecordId(SysTools.getGUID());
            partyUnionStory.setCreateUser(account.getAccountId());
            Document htmlDoc = Jsoup.parse(partyUnionStory.getContent());
            String subheading = htmlDoc.text();
            if (subheading.length() > 50) {
                subheading = subheading.substring(0, 50) + "...";
            }
            partyUnionStory.setSubheading(subheading);
            partyUnionStory.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyUnionStory.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyUnionStoryService.insertPartyUnionStory(partyUnionStory));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除职场故事
     *
     * @param partyUnionStory
     * @return
     */
    @RequestMapping(value = "/deletePartyUnionStory", method = RequestMethod.POST)
    public RetDataBean deletePartyUnionStory(PartyUnionStory partyUnionStory) {
        try {
            if (StringUtils.isBlank(partyUnionStory.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionStory.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyUnionStoryService.deletePartyUnionStory(partyUnionStory));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除职场故事
     *
     * @param partyUnionStory
     * @return
     */
    @RequestMapping(value = "/updatePartyUnionStory", method = RequestMethod.POST)
    public RetDataBean updatePartyUnionStory(PartyUnionStory partyUnionStory) {
        try {
            if (StringUtils.isBlank(partyUnionStory.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Document htmlDoc = Jsoup.parse(partyUnionStory.getContent());
            String subheading = htmlDoc.text();
            if (subheading.length() > 50) {
                subheading = subheading.substring(0, 50) + "...";
            }
            partyUnionStory.setSubheading(subheading);
            Example example = new Example(PartyUnionStory.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyUnionStory.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyUnionStoryService.updatePartyUnionStory(example, partyUnionStory));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 添加职场精英
     *
     * @param partyUnionEssence
     * @return
     */
    @RequestMapping(value = "/insertPartyUnionEssence", method = RequestMethod.POST)
    public RetDataBean insertPartyUnionEssence(PartyUnionEssence partyUnionEssence) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionEssence.setRecordId(SysTools.getGUID());
            partyUnionEssence.setCreateUser(account.getAccountId());
            Document htmlDoc = Jsoup.parse(partyUnionEssence.getContent());
            String subheading = htmlDoc.text();
            if (subheading.length() > 50) {
                subheading = subheading.substring(0, 50) + "...";
            }
            if (StringUtils.isNotBlank(partyUnionEssence.getAccountId())) {
                UserInfo userInfo = new UserInfo();
                userInfo.setOrgId(account.getOrgId());
                userInfo.setAccountId(partyUnionEssence.getAccountId());
                userInfo = userInfoService.selectOneUserInfo(userInfo);
                if (StringUtils.isNotBlank(userInfo.getUserName())) {
                    partyUnionEssence.setUserName(userInfo.getUserName());
                }
            }
            partyUnionEssence.setSubheading(subheading);
            partyUnionEssence.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyUnionEssence.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyUnionEssenceService.insertPartyUnionEssence(partyUnionEssence));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除职场精英
     *
     * @param partyUnionEssence
     * @return
     */
    @RequestMapping(value = "/deletePartyUnionEssence", method = RequestMethod.POST)
    public RetDataBean deletePartyUnionEssence(PartyUnionEssence partyUnionEssence) {
        try {
            if (StringUtils.isBlank(partyUnionEssence.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionEssence.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyUnionEssenceService.deletePartyUnionEssence(partyUnionEssence));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新职场精英
     *
     * @param partyUnionEssence
     * @return
     */
    @RequestMapping(value = "/updatePartyUnionEssence", method = RequestMethod.POST)
    public RetDataBean updatePartyUnionEssence(PartyUnionEssence partyUnionEssence) {
        try {
            if (StringUtils.isBlank(partyUnionEssence.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Document htmlDoc = Jsoup.parse(partyUnionEssence.getContent());
            String subheading = htmlDoc.text();
            if (subheading.length() > 50) {
                subheading = subheading.substring(0, 50) + "...";
            }
            partyUnionEssence.setSubheading(subheading);
            if (StringUtils.isNotBlank(partyUnionEssence.getAccountId())) {
                UserInfo userInfo = new UserInfo();
                userInfo.setOrgId(account.getOrgId());
                userInfo.setAccountId(partyUnionEssence.getAccountId());
                userInfo = userInfoService.selectOneUserInfo(userInfo);
                if (StringUtils.isNotBlank(userInfo.getUserName())) {
                    partyUnionEssence.setUserName(userInfo.getUserName());
                }
            }
            Example example = new Example(PartyUnionEssence.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyUnionEssence.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyUnionEssenceService.updatePartyUnionEssence(example, partyUnionEssence));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 创建工会活动
     *
     * @param partyUnionActivity
     * @return
     */
    @RequestMapping(value = "/insertPartyUnionActivity", method = RequestMethod.POST)
    public RetDataBean insertPartyUnionActivity(PartyUnionActivity partyUnionActivity) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionActivity.setRecordId(SysTools.getGUID());
            partyUnionActivity.setCreateUser(account.getAccountId());
            Document htmlDoc = Jsoup.parse(partyUnionActivity.getContent());
            String subheading = htmlDoc.text();
            if (subheading.length() > 50) {
                subheading = subheading.substring(0, 50) + "...";
            }
            partyUnionActivity.setSubheading(subheading);
            partyUnionActivity.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyUnionActivity.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyUnionActivityService.insertPartyUnionActivity(partyUnionActivity));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除工会活动
     *
     * @param partyUnionActivity
     * @return
     */
    @RequestMapping(value = "/deletePartyUnionActivity", method = RequestMethod.POST)
    public RetDataBean deletePartyUnionActivity(PartyUnionActivity partyUnionActivity) {
        try {
            if (StringUtils.isBlank(partyUnionActivity.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionActivity.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyUnionActivityService.deletePartyUnionActivity(partyUnionActivity));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新工会活动
     *
     * @param partyUnionActivity
     * @return
     */
    @RequestMapping(value = "/updatePartyUnionActivity", method = RequestMethod.POST)
    public RetDataBean updatePartyUnionActivity(PartyUnionActivity partyUnionActivity) {
        try {
            if (StringUtils.isBlank(partyUnionActivity.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Document htmlDoc = Jsoup.parse(partyUnionActivity.getContent());
            String subheading = htmlDoc.text();
            if (subheading.length() > 50) {
                subheading = subheading.substring(0, 50) + "...";
            }
            partyUnionActivity.setSubheading(subheading);
            Example example = new Example(PartyUnionActivity.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyUnionActivity.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyUnionActivityService.updatePartyUnionActivity(example, partyUnionActivity));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 添加帮扶记录
     *
     * @param partyUnionHelp
     * @return
     */
    @RequestMapping(value = "/insertPartyUnionHelp", method = RequestMethod.POST)
    public RetDataBean insertPartyUnionHelp(PartyUnionHelp partyUnionHelp) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionHelp.setRecordId(SysTools.getGUID());
            if (StringUtils.isNotBlank(partyUnionHelp.getAccountId())) {
                UserInfo userInfo = new UserInfo();
                userInfo.setOrgId(account.getOrgId());
                userInfo.setAccountId(partyUnionHelp.getAccountId());
                userInfo = userInfoService.selectOneUserInfo(userInfo);
                partyUnionHelp.setUserName(userInfo.getUserName());
            }
            partyUnionHelp.setCreateUser(account.getAccountId());
            partyUnionHelp.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyUnionHelp.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyUnionHelpService.insertPartyUnionHelp(partyUnionHelp));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除帮扶记录
     *
     * @param partyUnionHelp
     * @return
     */
    @RequestMapping(value = "/deletePartyUnionHelp", method = RequestMethod.POST)
    public RetDataBean deletePartyUnionHelp(PartyUnionHelp partyUnionHelp) {
        try {
            if (StringUtils.isBlank(partyUnionHelp.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionHelp.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyUnionHelpService.deletePartyUnionHelp(partyUnionHelp));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新帮扶记录
     *
     * @param partyUnionHelp
     * @return
     */
    @RequestMapping(value = "/updatePartyUnionHelp", method = RequestMethod.POST)
    public RetDataBean updatePartyUnionHelp(PartyUnionHelp partyUnionHelp) {
        try {
            if (StringUtils.isBlank(partyUnionHelp.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isNotBlank(partyUnionHelp.getAccountId())) {
                UserInfo userInfo = new UserInfo();
                userInfo.setOrgId(account.getOrgId());
                userInfo.setAccountId(partyUnionHelp.getAccountId());
                userInfo = userInfoService.selectOneUserInfo(userInfo);
                partyUnionHelp.setUserName(userInfo.getUserName());
            }
            Example example = new Example(PartyUnionHelp.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyUnionHelp.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyUnionHelpService.updatePartyUnionHelp(example, partyUnionHelp));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 工会部门
     *
     * @param partyUnionDept
     * @return
     */
    @RequestMapping(value = "/insertPartyUnionDept", method = RequestMethod.POST)
    public RetDataBean insertPartyUnionDept(PartyUnionDept partyUnionDept) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionDept.setDeptId(SysTools.getGUID());
            if (StringUtils.isBlank(partyUnionDept.getOrgLevelId())) {
                partyUnionDept.setOrgLevelId("0");
            }
            partyUnionDept.setCreateUser(account.getAccountId());
            partyUnionDept.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyUnionDept.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyUnionDeptService.insertPartyUnionDept(partyUnionDept));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除工作部门
     *
     * @param partyUnionDept
     * @return
     */
    @RequestMapping(value = "/deletePartyUnionDept", method = RequestMethod.POST)
    public RetDataBean deletePartyUnionDept(PartyUnionDept partyUnionDept) {
        try {
            if (StringUtils.isBlank(partyUnionDept.getDeptId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionDept.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyUnionDeptService.deletePartyUnionDept(partyUnionDept));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新工会部门
     *
     * @param partyUnionDept
     * @return
     */
    @RequestMapping(value = "/updatePartyUnionDept", method = RequestMethod.POST)
    public RetDataBean updatePartyUnionDept(PartyUnionDept partyUnionDept) {
        try {
            if (StringUtils.isBlank(partyUnionDept.getDeptId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyUnionDept.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("deptId", partyUnionDept.getDeptId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyUnionDeptService.updatePartyUnionDept(example, partyUnionDept));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 工会成员职务
     *
     * @param partyUnionLevel
     * @return
     */
    @RequestMapping(value = "/insertPartyUnionLevel", method = RequestMethod.POST)
    public RetDataBean insertPartyUnionLevel(PartyUnionLevel partyUnionLevel) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionLevel.setLevelId(SysTools.getGUID());
            partyUnionLevel.setCreateUser(account.getAccountId());
            partyUnionLevel.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyUnionLevel.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyUnionLevelService.insertPartyUnionLevel(partyUnionLevel));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除工会成员职务
     *
     * @param partyUnionLevel
     * @return
     */
    @RequestMapping(value = "/deletePartyUnionLevel", method = RequestMethod.POST)
    public RetDataBean deletePartyUnionLevel(PartyUnionLevel partyUnionLevel) {
        try {
            if (StringUtils.isBlank(partyUnionLevel.getLevelId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionLevel.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyUnionLevelService.deletePartyUnionLevel(partyUnionLevel));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新工会成员职务
     *
     * @param partyUnionLevel
     * @return
     */
    @RequestMapping(value = "/updatePartyUnionLevel", method = RequestMethod.POST)
    public RetDataBean updatePartyUnionLevel(PartyUnionLevel partyUnionLevel) {
        try {
            if (StringUtils.isBlank(partyUnionLevel.getLevelId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyUnionLevel.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("levelId", partyUnionLevel.getLevelId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyUnionLevelService.updatePartyUnionLevel(example, partyUnionLevel));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 添加工会成员
     *
     * @param partyUnionMember
     * @return
     */
    @RequestMapping(value = "/insertPartyUnionMember", method = RequestMethod.POST)
    public RetDataBean insertPartyUnionMember(PartyUnionMember partyUnionMember) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionMember.setMemberId(SysTools.getGUID());
            partyUnionMember.setCreateUser(account.getAccountId());
            if (StringUtils.isNotBlank(partyUnionMember.getAccountId())) {
                UserInfo userInfo = new UserInfo();
                userInfo.setAccountId(account.getAccountId());
                userInfo.setOrgId(account.getOrgId());
                userInfo = userInfoService.selectOneUserInfo(userInfo);
                partyUnionMember.setUserName(userInfo.getUserName());
                if (StringUtils.isNotBlank(partyUnionMember.getBirthday())) {
                    partyUnionMember.setBirthday(userInfo.getBirthday());
                }
                if (StringUtils.isBlank(partyUnionMember.getTel())) {
                    partyUnionMember.setTel(userInfo.getTel());
                }
            }
            partyUnionMember.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyUnionMember.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyUnionMemberService.insertPartyUnionMember(partyUnionMember));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除工会成员
     *
     * @param partyUnionMember
     * @return
     */
    @RequestMapping(value = "/deletePartyUnionMember", method = RequestMethod.POST)
    public RetDataBean deletePartyUnionMember(PartyUnionMember partyUnionMember) {
        try {
            if (StringUtils.isBlank(partyUnionMember.getMemberId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionMember.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyUnionMemberService.deletePartyUnionMember(partyUnionMember));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新工会成员信息
     *
     * @param partyUnionMember
     * @return
     */
    @RequestMapping(value = "/updatePartyUnionMember", method = RequestMethod.POST)
    public RetDataBean updatePartyUnionMember(PartyUnionMember partyUnionMember) {
        try {
            if (StringUtils.isBlank(partyUnionMember.getMemberId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyUnionMember.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("memberId", partyUnionMember.getMemberId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyUnionMemberService.updatePartyUnionMember(example, partyUnionMember));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 创建工会组织
     *
     * @param partyUnionOrg
     * @return
     */
    @RequestMapping(value = "/insertPartyUnionOrg", method = RequestMethod.POST)
    public RetDataBean insertPartyUnionOrg(PartyUnionOrg partyUnionOrg) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionOrg.setUnionOrgId(SysTools.getGUID());
            partyUnionOrg.setCreateUser(account.getAccountId());
            partyUnionOrg.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyUnionOrg.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyUnionOrgService.insertPartyUnionOrg(partyUnionOrg));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除工会组织
     *
     * @param partyUnionOrg
     * @return
     */
    @RequestMapping(value = "/deletePartyUnionOrg", method = RequestMethod.POST)
    public RetDataBean deletePartyUnionOrg(PartyUnionOrg partyUnionOrg) {
        try {
            if (StringUtils.isBlank(partyUnionOrg.getUnionOrgId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnionOrg.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyUnionOrgService.deletePartyUnionOrg(partyUnionOrg));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新工会组织
     *
     * @param partyUnionOrg
     * @return
     */
    @RequestMapping(value = "/updatePartyUnionOrg", method = RequestMethod.POST)
    public RetDataBean updatePartyUnionOrg(PartyUnionOrg partyUnionOrg) {
        try {
            if (StringUtils.isBlank(partyUnionOrg.getUnionOrgId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyUnionOrg.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("unionOrgId", partyUnionOrg.getUnionOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyUnionOrgService.updatePartyUnionOrg(example, partyUnionOrg));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 导入工会部门
     *
     * @param file
     * @return
     */
    @RequestMapping(value = "/importPartyUnionDept", method = RequestMethod.POST)
    public RetDataBean importPartyUnionDept(MultipartFile file) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return partyUnionDeptService.importPartyUnionDept(account, file);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 批量删除职务
     *
     * @param levelArr
     * @return
     */
    @RequestMapping(value = "/deletePartyUnionLevelBatch", method = RequestMethod.POST)
    public RetDataBean deletePartyUnionLevelBatch(@RequestParam(value = "levelArr[]") String[] levelArr) {
        try {
            if (levelArr.length == 0) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            }
            List<String> list = new ArrayList<String>(Arrays.asList(levelArr));
            Example example = new Example(CodeClass.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andIn("levelId", list);
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyUnionLevelService.deletePartyUnionLevelByIds(example));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }
}
