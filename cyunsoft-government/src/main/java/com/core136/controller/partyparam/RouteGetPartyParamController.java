package com.core136.controller.partyparam;

import com.core136.bean.account.Account;
import com.core136.bean.partyparam.*;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.service.account.AccountService;
import com.core136.service.partyparam.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/ret/partyparamget")
public class RouteGetPartyParamController {
    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private PartyTypeService partyTypeService;

    @Autowired
    public void setPartyTypeService(PartyTypeService partyTypeService) {
        this.partyTypeService = partyTypeService;
    }

    private PartySuboService partySuboService;

    @Autowired
    public void setPartySuboService(PartySuboService partySuboService) {
        this.partySuboService = partySuboService;
    }

    private PartyWorkTypeService partyWorkTypeService;

    @Autowired
    public void setPartyWorkTypeService(PartyWorkTypeService partyWorkTypeService) {
        this.partyWorkTypeService = partyWorkTypeService;
    }

    private PartyUnitTypeService partyUnitTypeService;

    @Autowired
    public void setPartyUnitTypeService(PartyUnitTypeService partyUnitTypeService) {
        this.partyUnitTypeService = partyUnitTypeService;
    }

    private PartyUnitSuboService partyUnitSuboService;

    @Autowired
    public void setPartyUnitSuboService(PartyUnitSuboService partyUnitSuboService) {
        this.partyUnitSuboService = partyUnitSuboService;
    }

    private PartyUnitIndustryService partyUnitIndustryService;

    @Autowired
    public void setPartyUnitIndustryService(PartyUnitIndustryService partyUnitIndustryService) {
        this.partyUnitIndustryService = partyUnitIndustryService;
    }

    private PartyUnitServiceService partyUnitServiceService;

    @Autowired
    public void setPartyUnitServiceService(PartyUnitServiceService partyUnitServiceService) {
        this.partyUnitServiceService = partyUnitServiceService;
    }

    private PartyNativePlaceService partyNativePlaceService;

    @Autowired
    public void setPartyNativePlaceService(PartyNativePlaceService partyNativePlaceService) {
        this.partyNativePlaceService = partyNativePlaceService;
    }

    private PartyDegreeService partyDegreeService;

    @Autowired
    public void setPartyDegreeService(PartyDegreeService partyDegreeService) {
        this.partyDegreeService = partyDegreeService;
    }

    private PartyEducationService partyEducationService;

    @Autowired
    public void setPartyEducationService(PartyEducationService partyEducationService) {
        this.partyEducationService = partyEducationService;
    }

    private PartyPostService partyPostService;

    @Autowired
    public void setPartyPostService(PartyPostService partyPostService) {
        this.partyPostService = partyPostService;
    }

    private PartyGovPostService partyGovPostService;

    @Autowired
    public void setPartyGovPostService(PartyGovPostService partyGovPostService) {
        this.partyGovPostService = partyGovPostService;
    }

    private PartyAdmPositionService partyAdmPositionService;

    @Autowired
    public void setPartyAdmPositionService(PartyAdmPositionService partyAdmPositionService) {
        this.partyAdmPositionService = partyAdmPositionService;
    }

    private PartyStunitTypeService partyStunitTypeService;

    @Autowired
    public void setPartyStunitTypeService(PartyStunitTypeService partyStunitTypeService) {
        this.partyStunitTypeService = partyStunitTypeService;
    }

    private PartyTitleService partyTitleService;

    @Autowired
    public void setPartyTitleService(PartyTitleService partyTitleService) {
        this.partyTitleService = partyTitleService;
    }

    private PartyStratumService partyStratumService;

    @Autowired
    public void setPartyStratumService(PartyStratumService partyStratumService) {
        this.partyStratumService = partyStratumService;
    }

    private PartyAppReaService partyAppReaService;

    @Autowired
    public void setPartyAppReaService(PartyAppReaService partyAppReaService) {
        this.partyAppReaService = partyAppReaService;
    }

    private PartyAppSituService partyAppSituService;

    @Autowired
    public void setPartyAppSituService(PartyAppSituService partyAppSituService) {
        this.partyAppSituService = partyAppSituService;
    }

    private PartyPostLevelService partyPostLevelService;

    @Autowired
    public void setPartyPostLevelService(PartyPostLevelService partyPostLevelService) {
        this.partyPostLevelService = partyPostLevelService;
    }

    /**
     * 获取职务级别分类详情
     * @param partyPostLevel
     * @return
     */
    @RequestMapping(value = "/getPartyPostLevelById", method = RequestMethod.POST)
    public RetDataBean getPartyPostLevelById( PartyPostLevel partyPostLevel) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyPostLevel.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyPostLevelService.selectOnePartyPostLevel(partyPostLevel));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取职务级别类别树
     * @param sortId
     * @return
     */
    @RequestMapping(value = "/getPostLevelTree", method = RequestMethod.POST)
    public List<Map<String, String>> getPostLevelTree(String sortId) {
        try {
            String levelId = "0";
            if (StringUtils.isNotBlank(sortId)) {
                levelId = sortId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return partyPostLevelService.getPostLevelTree(account.getOrgId(), levelId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 评议奖惩情况详情
     * @param partyAppSitu
     * @return
     */
    @RequestMapping(value = "/getPartyAppSituById", method = RequestMethod.POST)
    public RetDataBean getPartyAppSituById(PartyAppSitu partyAppSitu) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyAppSitu.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyAppSituService.selectOnePartyAppSitu(partyAppSitu));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyAppRea
     * @return RetDataBean
     * @Title: getPartyAppReaById
     * @Description:  评议奖惩原因详情
     */
    @RequestMapping(value = "/getPartyAppReaById", method = RequestMethod.POST)
    public RetDataBean getPartyAppReaById( PartyAppRea partyAppRea) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyAppRea.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyAppReaService.selectOnePartyAppRea(partyAppRea));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取职称分类详情
     * @param partyTitle
     * @return
     */
    @RequestMapping(value = "/getPartyTitleById", method = RequestMethod.POST)
    public RetDataBean getPartyTitleById(PartyTitle partyTitle) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyTitle.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyTitleService.selectOnePartyTitle(partyTitle));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取社会阶层分类详情
     * @param partyStratum
     * @return
     */
    @RequestMapping(value = "/getPartyStratumById", method = RequestMethod.POST)
    public RetDataBean getPartyStratumById(PartyStratum partyStratum) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyStratum.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyStratumService.selectOnePartyStratum(partyStratum));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取从学单位类别详情
     * @param partyStunitType
     * @return
     */
    @RequestMapping(value = "/getPartyStunitTypeById", method = RequestMethod.POST)
    public RetDataBean getPartyStunitTypeById(PartyStunitType partyStunitType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyStunitType.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyStunitTypeService.selectOnePartyStunitType(partyStunitType));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyAdmPosition
     * @return RetDataBean
     * @Title: getPartyAdmPositionById
     * @Description:  获取行政职务分类详情
     */
    @RequestMapping(value = "/getPartyAdmPositionById", method = RequestMethod.POST)
    public RetDataBean getPartyAdmPositionById( PartyAdmPosition partyAdmPosition) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyAdmPosition.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyAdmPositionService.selectOnePartyAdmPosition(partyAdmPosition));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取党内职务分类详情
     * @param partyGovPost
     * @return
     */
    @RequestMapping(value = "/getPartyGovPostById", method = RequestMethod.POST)
    public RetDataBean getPartyGovPostById(PartyGovPost partyGovPost) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyGovPost.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyGovPostService.selectOnePartyGovPost(partyGovPost));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取工作岗位详情
     * @param partyPost
     * @return
     */
    @RequestMapping(value = "/getPartyPostById", method = RequestMethod.POST)
    public RetDataBean getPartyPostById(PartyPost partyPost) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyPost.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyPostService.selectOnePartyPost(partyPost));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取学历分类
     * @param partyEducation
     * @return
     */
    @RequestMapping(value = "/getPartyEducationById", method = RequestMethod.POST)
    public RetDataBean getPartyEducationById(PartyEducation partyEducation) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyEducation.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyEducationService.selectOnePartyEducation(partyEducation));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取学位详情
     * @param partyDegree
     * @return
     */
    @RequestMapping(value = "/getPartyDegreeById", method = RequestMethod.POST)
    public RetDataBean getPartyDegreeById(PartyDegree partyDegree) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyDegree.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyDegreeService.selectOnePartyDegree(partyDegree));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyNativePlace
     * @return RetDataBean
     * @Title: getPartyNativePlaceById
     * @Description:  获取籍贯详情
     */
    @RequestMapping(value = "/getPartyNativePlaceById", method = RequestMethod.POST)
    public RetDataBean getPartyNativePlaceById( PartyNativePlace partyNativePlace) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyNativePlace.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyNativePlaceService.selectOnePartyNativePlace(partyNativePlace));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取单位所属行业详情
     * @param partyUnitIndustry
     * @return
     */
    @RequestMapping(value = "/getPartyUnitIndustryById", method = RequestMethod.POST)
    public RetDataBean getPartyUnitIndustryById( PartyUnitIndustry partyUnitIndustry) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnitIndustry.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyUnitIndustryService.selectOnePartyUnitIndustry(partyUnitIndustry));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyUnitService
     * @return RetDataBean
     * @Title: getPartyUnitServiceById
     * @Description:  获取单位所属服务分类详情
     */
    @RequestMapping(value = "/getPartyUnitServiceById", method = RequestMethod.POST)
    public RetDataBean getPartyUnitServiceById( PartyUnitService partyUnitService) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnitService.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyUnitServiceService.selectOnePartyUnitService(partyUnitService));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyUnitSubo
     * @return RetDataBean
     * @Title: getPartyUnitSuboById
     * @Description:  获取单位隶属关系详情
     */
    @RequestMapping(value = "/getPartyUnitSuboById", method = RequestMethod.POST)
    public RetDataBean getPartyUnitSuboById( PartyUnitSubo partyUnitSubo) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnitSubo.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyUnitSuboService.selectOnePartyUnitSubo(partyUnitSubo));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param partyUnitType
     * @return RetDataBean
     * @Title: getPartyUnitTypeById
     * @Description:  获取单位分类详情
     */
    @RequestMapping(value = "/getPartyUnitTypeById", method = RequestMethod.POST)
    public RetDataBean getPartyUnitTypeById(PartyUnitType partyUnitType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnitType.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyUnitTypeService.selectOnePartyUnitType(partyUnitType));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取党组织工作分类详情
     * @param partyWorkType
     * @return
     */
    @RequestMapping(value = "/getPartyWorkTypeById", method = RequestMethod.POST)
    public RetDataBean getPartyWorkTypeById(PartyWorkType partyWorkType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyWorkType.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyWorkTypeService.selectOnePartyWorkType(partyWorkType));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partySubo
     * @return RetDataBean
     * @Title: getPartySuboById
     * @Description: 获取党组织隶属关系详情
     */
    @RequestMapping(value = "/getPartySuboById", method = RequestMethod.POST)
    public RetDataBean getPartySuboById( PartySubo partySubo) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partySubo.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partySuboService.selectOnePartySubo(partySubo));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyType
     * @return RetDataBean
     * @Title: getPartyTypeById
     * @Description:  获取党组织类别详情
     */
    @RequestMapping(value = "/getPartyTypeById", method = RequestMethod.POST)
    public RetDataBean getPartyTypeById(PartyType partyType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyType.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyTypeService.selectOnePartyType(partyType));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param sortId
     * @return List<Map < String, String>>
     * @Title: getPartyTypeTree
     * @Description:  获取党组织类别树
     */
    @RequestMapping(value = "/getPartyTypeTree", method = RequestMethod.POST)
    public List<Map<String, String>> getPartyTypeTree( String sortId) {
        try {
            String levelId = "0";
            if (StringUtils.isNotBlank(sortId)) {
                levelId = sortId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return partyTypeService.getPartyTypeTree(account.getOrgId(), levelId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 获取党组织隶属关系树
     * @param sortId
     * @return
     */
    @RequestMapping(value = "/getPartySuboTree", method = RequestMethod.POST)
    public List<Map<String, String>> getPartySuboTree(String sortId) {
        try {
            String levelId = "0";
            if (StringUtils.isNotBlank(sortId)) {
                levelId = sortId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return partySuboService.getPartySuboTree(account.getOrgId(), levelId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * @param sortId
     * @return List<Map < String, String>>
     * @Title: getWorkTypeTree
     * @Description:  获取党组织工作分灰树
     */
    @RequestMapping(value = "/getWorkTypeTree", method = RequestMethod.POST)
    public List<Map<String, String>> getWorkTypeTree( String sortId) {
        try {
            String levelId = "0";
            if (StringUtils.isNotBlank(sortId)) {
                levelId = sortId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return partyWorkTypeService.getWorkTypeTree(account.getOrgId(), levelId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * @param sortId
     * @return List<Map < String, String>>
     * @Title: getUnitTypeTree
     * @Description:  获取单位分类树
     */
    @RequestMapping(value = "/getUnitTypeTree", method = RequestMethod.POST)
    public List<Map<String, String>> getUnitTypeTree( String sortId) {
        try {
            String levelId = "0";
            if (StringUtils.isNotBlank(sortId)) {
                levelId = sortId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return partyUnitTypeService.getUnitTypeTree(account.getOrgId(), levelId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * @param sortId
     * @return List<Map < String, String>>
     * @Title: getUnitSuboTree
     * @Description:  获取单位隶属关系树
     */
    @RequestMapping(value = "/getUnitSuboTree", method = RequestMethod.POST)
    public List<Map<String, String>> getUnitSuboTree( String sortId) {
        try {
            String levelId = "0";
            if (StringUtils.isNotBlank(sortId)) {
                levelId = sortId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return partyUnitSuboService.getUnitSuboTree(account.getOrgId(), levelId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * @param sortId
     * @return List<Map < String, String>>
     * @Title: getUnitIndustryTree
     * @Description:  单位所属行业树
     */
    @RequestMapping(value = "/getUnitIndustryTree", method = RequestMethod.POST)
    public List<Map<String, String>> getUnitIndustryTree( String sortId) {
        try {
            String levelId = "0";
            if (StringUtils.isNotBlank(sortId)) {
                levelId = sortId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return partyUnitIndustryService.getUnitIndustryTree(account.getOrgId(), levelId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 获取单位服务行业类型树
     * @param sortId
     * @return
     */
    @RequestMapping(value = "/getUnitServiceTree", method = RequestMethod.POST)
    public List<Map<String, String>> getUnitServiceTree(String sortId) {
        try {
            String levelId = "0";
            if (StringUtils.isNotBlank(sortId)) {
                levelId = sortId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return partyUnitServiceService.getUnitServiceTree(account.getOrgId(), levelId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * @param sortId
     * @return List<Map < String, String>>
     * @Title: getNativePlaceTree
     * @Description:  获取籍贯分类树
     */
    @RequestMapping(value = "/getNativePlaceTree", method = RequestMethod.POST)
    public List<Map<String, String>> getNativePlaceTree( String sortId) {
        try {
            String levelId = "0";
            if (StringUtils.isNotBlank(sortId)) {
                levelId = sortId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return partyNativePlaceService.getNativePlaceTree(account.getOrgId(), levelId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * @param sortId
     * @return List<Map < String, String>>
     * @Title: getDegreeTree
     * @Description:  获取学位分类树
     */
    @RequestMapping(value = "/getDegreeTree", method = RequestMethod.POST)
    public List<Map<String, String>> getDegreeTree( String sortId) {
        try {
            String levelId = "0";
            if (StringUtils.isNotBlank(sortId)) {
                levelId = sortId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return partyDegreeService.getDegreeTree(account.getOrgId(), levelId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 获取学历分类树
     * @param sortId
     * @return
     */
    @RequestMapping(value = "/getEducationTree", method = RequestMethod.POST)
    public List<Map<String, String>> getEducationTree(String sortId) {
        try {
            String levelId = "0";
            if (StringUtils.isNotBlank(sortId)) {
                levelId = sortId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return partyEducationService.getGovEducationTree(account.getOrgId(), levelId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 获取工作岗位树
     * @param sortId
     * @return
     */
    @RequestMapping(value = "/getPostTree", method = RequestMethod.POST)
    public List<Map<String, String>> getPostTree(String sortId) {
        try {
            String levelId = "0";
            if (StringUtils.isNotBlank(sortId)) {
                levelId = sortId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return partyPostService.getPostTree(account.getOrgId(), levelId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 获取党内职务分类树
     * @param sortId
     * @return
     */
    @RequestMapping(value = "/getGovPostTree", method = RequestMethod.POST)
    public List<Map<String, String>> getGovPostTree(String sortId) {
        try {
            String levelId = "0";
            if (StringUtils.isNotBlank(sortId)) {
                levelId = sortId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return partyGovPostService.getGovPostTree(account.getOrgId(), levelId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * @param sortId
     * @return List<Map < String, String>>
     * @Title: getAdmPositionTree
     * @Description:  获取行政职务分类树
     */
    @RequestMapping(value = "/getAdmPositionTree", method = RequestMethod.POST)
    public List<Map<String, String>> getAdmPositionTree( String sortId) {
        try {
            String levelId = "0";
            if (StringUtils.isNotBlank(sortId)) {
                levelId = sortId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return partyAdmPositionService.getAdmPositionTree(account.getOrgId(), levelId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 获取从学单位类别树
     * @param sortId
     * @return
     */
    @RequestMapping(value = "/getStunitTypeTree", method = RequestMethod.POST)
    public List<Map<String, String>> getStunitTypeTree(String sortId) {
        try {
            String levelId = "0";
            if (StringUtils.isNotBlank(sortId)) {
                levelId = sortId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return partyStunitTypeService.getStunitTypeTree(account.getOrgId(), levelId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 获取职称分类树
     * @param sortId
     * @return
     */
    @RequestMapping(value = "/getTitleTree", method = RequestMethod.POST)
    public List<Map<String, String>> getTitleTree(String sortId) {
        try {
            String levelId = "0";
            if (StringUtils.isNotBlank(sortId)) {
                levelId = sortId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return partyTitleService.getTitleTree(account.getOrgId(), levelId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 获取社会阶层分类树
     * @param sortId
     * @return
     */
    @RequestMapping(value = "/getStratumTree", method = RequestMethod.POST)
    public List<Map<String, String>> getStratumTree(String sortId) {
        try {
            String levelId = "0";
            if (StringUtils.isNotBlank(sortId)) {
                levelId = sortId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return partyStratumService.getStratumTree(account.getOrgId(), levelId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 评议奖惩原因分类树
     * @param sortId
     * @return
     */
    @RequestMapping(value = "/getAppReaTree", method = RequestMethod.POST)
    public List<Map<String, String>> getAppReaTree(String sortId) {
        try {
            String levelId = "0";
            if (StringUtils.isNotBlank(sortId)) {
                levelId = sortId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return partyAppReaService.getAppReaTree(account.getOrgId(), levelId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 评议奖惩情况分类树
     * @param sortId
     * @return
     */
    @RequestMapping(value = "/getAppSituTree", method = RequestMethod.POST)
    public List<Map<String, String>> getAppSituTree(String sortId) {
        try {
            String levelId = "0";
            if (StringUtils.isNotBlank(sortId)) {
                levelId = sortId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return partyAppSituService.getAppSituTree(account.getOrgId(), levelId);
        } catch (Exception e) {
            return null;
        }
    }
}
