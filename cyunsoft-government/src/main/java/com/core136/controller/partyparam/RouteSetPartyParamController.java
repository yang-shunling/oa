package com.core136.controller.partyparam;

import com.core136.bean.account.Account;
import com.core136.bean.partyparam.*;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.partyparam.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

@RestController
@RequestMapping("/set/partyparamset")
public class RouteSetPartyParamController {
    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private PartyTypeService partyTypeService;

    @Autowired
    public void setPartyTypeService(PartyTypeService partyTypeService) {
        this.partyTypeService = partyTypeService;
    }

    private PartySuboService partySuboService;

    @Autowired
    public void setPartySuboService(PartySuboService partySuboService) {
        this.partySuboService = partySuboService;
    }

    private PartyWorkTypeService partyWorkTypeService;

    @Autowired
    public void setPartyWorkTypeService(PartyWorkTypeService partyWorkTypeService) {
        this.partyWorkTypeService = partyWorkTypeService;
    }

    private PartyUnitTypeService partyUnitTypeService;

    @Autowired
    public void setPartyUnitTypeService(PartyUnitTypeService partyUnitTypeService) {
        this.partyUnitTypeService = partyUnitTypeService;
    }

    private PartyUnitSuboService partyUnitSuboService;

    @Autowired
    public void setPartyUnitSuboService(PartyUnitSuboService partyUnitSuboService) {
        this.partyUnitSuboService = partyUnitSuboService;
    }

    private PartyUnitIndustryService partyUnitIndustryService;

    @Autowired
    public void setPartyUnitIndustryService(PartyUnitIndustryService partyUnitIndustryService) {
        this.partyUnitIndustryService = partyUnitIndustryService;
    }

    private PartyUnitServiceService partyUnitServiceService;

    @Autowired
    public void setPartyUnitServiceService(PartyUnitServiceService partyUnitServiceService) {
        this.partyUnitServiceService = partyUnitServiceService;
    }

    private PartyNativePlaceService partyNativePlaceService;

    @Autowired
    public void setPartyNativePlaceService(PartyNativePlaceService partyNativePlaceService) {
        this.partyNativePlaceService = partyNativePlaceService;
    }

    private PartyDegreeService partyDegreeService;

    @Autowired
    public void setPartyDegreeService(PartyDegreeService partyDegreeService) {
        this.partyDegreeService = partyDegreeService;
    }

    private PartyEducationService partyEducationService;

    @Autowired
    public void setPartyEducationService(PartyEducationService partyEducationService) {
        this.partyEducationService = partyEducationService;
    }

    private PartyPostService partyPostService;

    @Autowired
    public void setPartyPostService(PartyPostService partyPostService) {
        this.partyPostService = partyPostService;
    }

    private PartyGovPostService partyGovPostService;

    @Autowired
    public void setPartyGovPostService(PartyGovPostService partyGovPostService) {
        this.partyGovPostService = partyGovPostService;
    }

    private PartyAdmPositionService partyAdmPositionService;

    @Autowired
    public void setPartyAdmPositionService(PartyAdmPositionService partyAdmPositionService) {
        this.partyAdmPositionService = partyAdmPositionService;
    }

    private PartyStunitTypeService partyStunitTypeService;

    @Autowired
    public void setPartyStunitTypeService(PartyStunitTypeService partyStunitTypeService) {
        this.partyStunitTypeService = partyStunitTypeService;
    }

    private PartyTitleService partyTitleService;

    @Autowired
    public void setPartyTitleService(PartyTitleService partyTitleService) {
        this.partyTitleService = partyTitleService;
    }

    private PartyStratumService partyStratumService;

    @Autowired
    public void setPartyStratumService(PartyStratumService partyStratumService) {
        this.partyStratumService = partyStratumService;
    }

    private PartyAppReaService partyAppReaService;

    @Autowired
    public void setPartyAppReaService(PartyAppReaService partyAppReaService) {
        this.partyAppReaService = partyAppReaService;
    }

    private PartyAppSituService partyAppSituService;

    @Autowired
    public void setPartyAppSituService(PartyAppSituService partyAppSituService) {
        this.partyAppSituService = partyAppSituService;
    }

    private PartyPostLevelService partyPostLevelService;

    @Autowired
    public void setPartyPostLevelService(PartyPostLevelService partyPostLevelService) {
        this.partyPostLevelService = partyPostLevelService;
    }

    /**
    
     * @param partyPostLevel
     * @return RetDataBean
     * @Title: insertPartyPostLevel
     * @Description:  添加职务级别分类
     */
    @RequestMapping(value = "/insertPartyPostLevel", method = RequestMethod.POST)
    public RetDataBean insertPartyPostLevel( PartyPostLevel partyPostLevel) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyPostLevel.setSortId(SysTools.getGUID());
            if (StringUtils.isBlank(partyPostLevel.getLevelId())) {
                partyPostLevel.setLevelId("0");
            }
            partyPostLevel.setCreateUser(account.getAccountId());
            partyPostLevel.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyPostLevel.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyPostLevelService.insertPartyPostLevel(partyPostLevel));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyPostLevel
     * @return RetDataBean
     * @Title: deletePartyPostLevel
     * @Description:  删除职务级别分类
     */
    @RequestMapping(value = "/deletePartyPostLevel", method = RequestMethod.POST)
    public RetDataBean deletePartyPostLevel( PartyPostLevel partyPostLevel) {
        try {
            if (StringUtils.isBlank(partyPostLevel.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyPostLevel.setOrgId(account.getOrgId());
            if (partyPostLevelService.isExistChild(partyPostLevel.getOrgId(), partyPostLevel.getSortId()) > 0) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_HAVE_CHILDREN_ITEM);
            } else {
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyPostLevelService.deletePartyPostLevel(partyPostLevel));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyPostLevel
     * @return RetDataBean
     * @Title: updatePartyPostLevel
     * @Description:  更新职务级别分类
     */
    @RequestMapping(value = "/updatePartyPostLevel", method = RequestMethod.POST)
    public RetDataBean updatePartyPostLevel(PartyPostLevel partyPostLevel) {
        try {
            if (partyPostLevel.getSortId().equals(partyPostLevel.getLevelId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_PARENT_ID_CANNOT_TO_USE);
            }
            if (StringUtils.isBlank(partyPostLevel.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(partyPostLevel.getLevelId())) {
                partyPostLevel.setLevelId("0");
            }
            Example example = new Example(PartyPostLevel.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("sortId", partyPostLevel.getSortId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyPostLevelService.updatePartyPostLevel(example, partyPostLevel));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyAppSitu
     * @return RetDataBean
     * @Title: insertPartyAppSitu
     * @Description:  添加评议奖惩情况
     */
    @RequestMapping(value = "/insertPartyAppSitu", method = RequestMethod.POST)
    public RetDataBean insertPartyAppSitu( PartyAppSitu partyAppSitu) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyAppSitu.setSortId(SysTools.getGUID());
            if (StringUtils.isBlank(partyAppSitu.getLevelId())) {
                partyAppSitu.setLevelId("0");
            }
            partyAppSitu.setCreateUser(account.getAccountId());
            partyAppSitu.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyAppSitu.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyAppSituService.insertPartyAppSitu(partyAppSitu));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyAppSitu
     * @return RetDataBean
     * @Title: deletePartyAppSitu
     * @Description:  删除评议奖惩情况
     */
    @RequestMapping(value = "/deletePartyAppSitu", method = RequestMethod.POST)
    public RetDataBean deletePartyAppSitu( PartyAppSitu partyAppSitu) {
        try {
            if (StringUtils.isBlank(partyAppSitu.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyAppSitu.setOrgId(account.getOrgId());
            if (partyAppSituService.isExistChild(partyAppSitu.getOrgId(), partyAppSitu.getSortId()) > 0) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_HAVE_CHILDREN_ITEM);
            } else {
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyAppSituService.deletePartyAppSitu(partyAppSitu));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyAppSitu
     * @return RetDataBean
     * @Title: updatePartyAppSitu
     * @Description:  更新评议奖惩情况
     */
    @RequestMapping(value = "/updatePartyAppSitu", method = RequestMethod.POST)
    public RetDataBean updatePartyAppSitu( PartyAppSitu partyAppSitu) {
        try {
            if (partyAppSitu.getSortId().equals(partyAppSitu.getLevelId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_PARENT_ID_CANNOT_TO_USE);
            }
            if (StringUtils.isBlank(partyAppSitu.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(partyAppSitu.getLevelId())) {
                partyAppSitu.setLevelId("0");
            }
            Example example = new Example(PartyAppSitu.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("sortId", partyAppSitu.getSortId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyAppSituService.updatePartyAppSitu(example, partyAppSitu));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
    
     * @param partyAppRea
     * @return RetDataBean
     * @Title: insertPartyAppRea
     * @Description:  添加评议奖惩原因
     */
    @RequestMapping(value = "/insertPartyAppRea", method = RequestMethod.POST)
    public RetDataBean insertPartyAppRea( PartyAppRea partyAppRea) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyAppRea.setSortId(SysTools.getGUID());
            if (StringUtils.isBlank(partyAppRea.getLevelId())) {
                partyAppRea.setLevelId("0");
            }
            partyAppRea.setCreateUser(account.getAccountId());
            partyAppRea.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyAppRea.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyAppReaService.insertPartyAppRea(partyAppRea));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyAppRea
     * @return RetDataBean
     * @Title: deletePartyAppRea
     * @Description:  删除评议奖惩原因
     */
    @RequestMapping(value = "/deletePartyAppRea", method = RequestMethod.POST)
    public RetDataBean deletePartyAppRea( PartyAppRea partyAppRea) {
        try {
            if (StringUtils.isBlank(partyAppRea.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyAppRea.setOrgId(account.getOrgId());
            if (partyAppReaService.isExistChild(partyAppRea.getOrgId(), partyAppRea.getSortId()) > 0) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_HAVE_CHILDREN_ITEM);
            } else {
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyAppReaService.deletePartyAppRea(partyAppRea));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyAppRea
     * @return RetDataBean
     * @Title: updatePartyAppRea
     * @Description:  更新评议奖惩原因
     */
    @RequestMapping(value = "/updatePartyAppRea", method = RequestMethod.POST)
    public RetDataBean updatePartyAppRea( PartyAppRea partyAppRea) {
        try {
            if (partyAppRea.getSortId().equals(partyAppRea.getLevelId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_PARENT_ID_CANNOT_TO_USE);
            }
            if (StringUtils.isBlank(partyAppRea.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(partyAppRea.getLevelId())) {
                partyAppRea.setLevelId("0");
            }
            Example example = new Example(PartyAppRea.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("sortId", partyAppRea.getSortId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyAppReaService.updatePartyAppRea(example, partyAppRea));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyTitle
     * @return RetDataBean
     * @Title: insertPartyTitle
     * @Description:  添加职称分类
     */
    @RequestMapping(value = "/insertPartyTitle", method = RequestMethod.POST)
    public RetDataBean insertPartyTitle( PartyTitle partyTitle) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyTitle.setSortId(SysTools.getGUID());
            if (StringUtils.isBlank(partyTitle.getLevelId())) {
                partyTitle.setLevelId("0");
            }
            partyTitle.setCreateUser(account.getAccountId());
            partyTitle.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyTitle.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyTitleService.insertPartyTitle(partyTitle));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyTitle
     * @return RetDataBean
     * @Title: deletePartyTitle
     * @Description:  删除职称分类
     */
    @RequestMapping(value = "/deletePartyTitle", method = RequestMethod.POST)
    public RetDataBean deletePartyTitle( PartyTitle partyTitle) {
        try {
            if (StringUtils.isBlank(partyTitle.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyTitle.setOrgId(account.getOrgId());
            if (partyTitleService.isExistChild(partyTitle.getOrgId(), partyTitle.getSortId()) > 0) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_HAVE_CHILDREN_ITEM);
            } else {
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyTitleService.deletePartyTitle(partyTitle));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyTitle
     * @return RetDataBean
     * @Title: updatePartyTitle
     * @Description:  更新职称分类
     */
    @RequestMapping(value = "/updatePartyTitle", method = RequestMethod.POST)
    public RetDataBean updatePartyTitle(PartyTitle partyTitle) {
        try {
            if (partyTitle.getSortId().equals(partyTitle.getLevelId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_PARENT_ID_CANNOT_TO_USE);
            }
            if (StringUtils.isBlank(partyTitle.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(partyTitle.getLevelId())) {
                partyTitle.setLevelId("0");
            }
            Example example = new Example(PartyTitle.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("sortId", partyTitle.getSortId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyTitleService.updatePartyTitle(example, partyTitle));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyStratum
     * @return RetDataBean
     * @Title: insertPartyStratum
     * @Description:  添加社会阶层分类
     */
    @RequestMapping(value = "/insertPartyStratum", method = RequestMethod.POST)
    public RetDataBean insertPartyStratum( PartyStratum partyStratum) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyStratum.setSortId(SysTools.getGUID());
            if (StringUtils.isBlank(partyStratum.getLevelId())) {
                partyStratum.setLevelId("0");
            }
            partyStratum.setCreateUser(account.getAccountId());
            partyStratum.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyStratum.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyStratumService.insertPartyStratum(partyStratum));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyStratum
     * @return RetDataBean
     * @Title: deletePartyStratum
     * @Description:  删除社会阶层分类
     */
    @RequestMapping(value = "/deletePartyStratum", method = RequestMethod.POST)
    public RetDataBean deletePartyStratum( PartyStratum partyStratum) {
        try {
            if (StringUtils.isBlank(partyStratum.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyStratum.setOrgId(account.getOrgId());
            if (partyStratumService.isExistChild(partyStratum.getOrgId(), partyStratum.getSortId()) > 0) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_HAVE_CHILDREN_ITEM);
            } else {
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyStratumService.deletePartyStratum(partyStratum));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyStratum
     * @return RetDataBean
     * @Title: updatePartyStratum
     * @Description:  更新社会阶层
     */
    @RequestMapping(value = "/updatePartyStratum", method = RequestMethod.POST)
    public RetDataBean updatePartyStratum(PartyStratum partyStratum) {
        try {
            if (partyStratum.getSortId().equals(partyStratum.getLevelId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_PARENT_ID_CANNOT_TO_USE);
            }
            if (StringUtils.isBlank(partyStratum.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(partyStratum.getLevelId())) {
                partyStratum.setLevelId("0");
            }
            Example example = new Example(PartyStratum.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("sortId", partyStratum.getSortId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyStratumService.updatePartyStratum(example, partyStratum));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
    
     * @param partyStunitType
     * @return RetDataBean
     * @Title: insertPartyStunitType
     * @Description:  添加从学单位分类
     */
    @RequestMapping(value = "/insertPartyStunitType", method = RequestMethod.POST)
    public RetDataBean insertPartyStunitType( PartyStunitType partyStunitType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyStunitType.setSortId(SysTools.getGUID());
            if (StringUtils.isBlank(partyStunitType.getLevelId())) {
                partyStunitType.setLevelId("0");
            }
            partyStunitType.setCreateUser(account.getAccountId());
            partyStunitType.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyStunitType.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyStunitTypeService.insertPartyStunitType(partyStunitType));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyStunitType
     * @return RetDataBean
     * @Title: deletePartyStunitType
     * @Description:  删除从学单位分类
     */
    @RequestMapping(value = "/deletePartyStunitType", method = RequestMethod.POST)
    public RetDataBean deletePartyStunitType( PartyStunitType partyStunitType) {
        try {
            if (StringUtils.isBlank(partyStunitType.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyStunitType.setOrgId(account.getOrgId());
            if (partyStunitTypeService.isExistChild(partyStunitType.getOrgId(), partyStunitType.getSortId()) > 0) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_HAVE_CHILDREN_ITEM);
            } else {
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyStunitTypeService.deletePartyStunitType(partyStunitType));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyStunitType
     * @return RetDataBean
     * @Title: updatePartyStunitType
     * @Description:  更新从学单位分类
     */
    @RequestMapping(value = "/updatePartyStunitType", method = RequestMethod.POST)
    public RetDataBean updatePartyStunitType(PartyStunitType partyStunitType) {
        try {
            if (partyStunitType.getSortId().equals(partyStunitType.getLevelId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_PARENT_ID_CANNOT_TO_USE);
            }
            if (StringUtils.isBlank(partyStunitType.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(partyStunitType.getLevelId())) {
                partyStunitType.setLevelId("0");
            }
            Example example = new Example(PartyStunitType.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("sortId", partyStunitType.getSortId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyStunitTypeService.updatePartyStunitType(example, partyStunitType));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyAdmPosition
     * @return RetDataBean
     * @Title: insertPartyAdmPosition
     * @Description:  添加行政职务分类
     */
    @RequestMapping(value = "/insertPartyAdmPosition", method = RequestMethod.POST)
    public RetDataBean insertPartyAdmPosition( PartyAdmPosition partyAdmPosition) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyAdmPosition.setSortId(SysTools.getGUID());
            if (StringUtils.isBlank(partyAdmPosition.getLevelId())) {
                partyAdmPosition.setLevelId("0");
            }
            partyAdmPosition.setCreateUser(account.getAccountId());
            partyAdmPosition.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyAdmPosition.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyAdmPositionService.insertPartyAdmPosition(partyAdmPosition));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyAdmPosition
     * @return RetDataBean
     * @Title: deletePartyAdmPosition
     * @Description:  删除行政职务分类
     */
    @RequestMapping(value = "/deletePartyAdmPosition", method = RequestMethod.POST)
    public RetDataBean deletePartyAdmPosition( PartyAdmPosition partyAdmPosition) {
        try {
            if (StringUtils.isBlank(partyAdmPosition.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyAdmPosition.setOrgId(account.getOrgId());
            if (partyAdmPositionService.isExistChild(partyAdmPosition.getOrgId(), partyAdmPosition.getSortId()) > 0) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_HAVE_CHILDREN_ITEM);
            } else {
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyAdmPositionService.deletePartyAdmPosition(partyAdmPosition));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyAdmPosition
     * @return RetDataBean
     * @Title: updatePartyAdmPosition
     * @Description:  更新行政职务分类
     */
    @RequestMapping(value = "/updatePartyAdmPosition", method = RequestMethod.POST)
    public RetDataBean updatePartyAdmPosition( PartyAdmPosition partyAdmPosition) {
        try {
            if (partyAdmPosition.getSortId().equals(partyAdmPosition.getLevelId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_PARENT_ID_CANNOT_TO_USE);
            }
            if (StringUtils.isBlank(partyAdmPosition.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(partyAdmPosition.getLevelId())) {
                partyAdmPosition.setLevelId("0");
            }
            Example example = new Example(PartyAdmPosition.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("sortId", partyAdmPosition.getSortId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyAdmPositionService.updatePartyAdmPosition(example, partyAdmPosition));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyGovPost
     * @return RetDataBean
     * @Title: insertPartyGovPost
     * @Description:  添加党内职务分类
     */
    @RequestMapping(value = "/insertPartyGovPost", method = RequestMethod.POST)
    public RetDataBean insertPartyGovPost( PartyGovPost partyGovPost) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyGovPost.setSortId(SysTools.getGUID());
            if (StringUtils.isBlank(partyGovPost.getLevelId())) {
                partyGovPost.setLevelId("0");
            }
            partyGovPost.setCreateUser(account.getAccountId());
            partyGovPost.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyGovPost.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyGovPostService.insertPartyGovPost(partyGovPost));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyGovPost
     * @return RetDataBean
     * @Title: deletePartyGovPost
     * @Description:  删除党内职务分类
     */
    @RequestMapping(value = "/deletePartyGovPost", method = RequestMethod.POST)
    public RetDataBean deletePartyGovPost( PartyGovPost partyGovPost) {
        try {
            if (StringUtils.isBlank(partyGovPost.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyGovPost.setOrgId(account.getOrgId());
            if (partyGovPostService.isExistChild(partyGovPost.getOrgId(), partyGovPost.getSortId()) > 0) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_HAVE_CHILDREN_ITEM);
            } else {
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyGovPostService.deletePartyGovPost(partyGovPost));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyGovPost
     * @return RetDataBean
     * @Title: updatePartyGovPost
     * @Description:  更新党内职务分类
     */
    @RequestMapping(value = "/updatePartyGovPost", method = RequestMethod.POST)
    public RetDataBean updatePartyGovPost( PartyGovPost partyGovPost) {
        try {
            if (partyGovPost.getSortId().equals(partyGovPost.getLevelId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_PARENT_ID_CANNOT_TO_USE);
            }
            if (StringUtils.isBlank(partyGovPost.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(partyGovPost.getLevelId())) {
                partyGovPost.setLevelId("0");
            }
            Example example = new Example(PartyGovPost.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("sortId", partyGovPost.getSortId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyGovPostService.updatePartyGovPost(example, partyGovPost));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
    
     * @param partyPost
     * @return RetDataBean
     * @Title: insertPartyPost
     * @Description:  创建工作岗位分类
     */
    @RequestMapping(value = "/insertPartyPost", method = RequestMethod.POST)
    public RetDataBean insertPartyPost( PartyPost partyPost) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyPost.setSortId(SysTools.getGUID());
            if (StringUtils.isBlank(partyPost.getLevelId())) {
                partyPost.setLevelId("0");
            }
            partyPost.setCreateUser(account.getAccountId());
            partyPost.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyPost.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyPostService.insertPartyPost(partyPost));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyPost
     * @return RetDataBean
     * @Title: deletePartyPost
     * @Description:  删除工作岗位分类
     */
    @RequestMapping(value = "/deletePartyPost", method = RequestMethod.POST)
    public RetDataBean deletePartyPost( PartyPost partyPost) {
        try {
            if (StringUtils.isBlank(partyPost.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyPost.setOrgId(account.getOrgId());
            if (partyPostService.isExistChild(partyPost.getOrgId(), partyPost.getSortId()) > 0) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_HAVE_CHILDREN_ITEM);
            } else {
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyPostService.deletePartyPost(partyPost));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyPost
     * @return RetDataBean
     * @Title: updatePartyPost
     * @Description:  更新工作岗位分类
     */
    @RequestMapping(value = "/updatePartyPost", method = RequestMethod.POST)
    public RetDataBean updatePartyPost(PartyPost partyPost) {
        try {
            if (partyPost.getSortId().equals(partyPost.getLevelId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_PARENT_ID_CANNOT_TO_USE);
            }
            if (StringUtils.isBlank(partyPost.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(partyPost.getLevelId())) {
                partyPost.setLevelId("0");
            }
            Example example = new Example(PartyPost.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("sortId", partyPost.getSortId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyPostService.updatePartyPost(example, partyPost));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyEducation
     * @return RetDataBean
     * @Title: insertPartyEducation
     * @Description:  添加学历分类信息
     */
    @RequestMapping(value = "/insertPartyEducation", method = RequestMethod.POST)
    public RetDataBean insertPartyEducation( PartyEducation partyEducation) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyEducation.setSortId(SysTools.getGUID());
            if (StringUtils.isBlank(partyEducation.getLevelId())) {
                partyEducation.setLevelId("0");
            }
            partyEducation.setCreateUser(account.getAccountId());
            partyEducation.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyEducation.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyEducationService.insertPartyEducation(partyEducation));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyEducation
     * @return RetDataBean
     * @Title: deletePartyEducation
     * @Description:  删除学历分类
     */
    @RequestMapping(value = "/deletePartyEducation", method = RequestMethod.POST)
    public RetDataBean deletePartyEducation( PartyEducation partyEducation) {
        try {
            if (StringUtils.isBlank(partyEducation.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyEducation.setOrgId(account.getOrgId());
            if (partyEducationService.isExistChild(partyEducation.getOrgId(), partyEducation.getSortId()) > 0) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_HAVE_CHILDREN_ITEM);
            } else {
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyEducationService.deletePartyEducation(partyEducation));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyEducation
     * @return RetDataBean
     * @Title: updatePartyEducation
     * @Description:  更新学位分类
     */
    @RequestMapping(value = "/updatePartyEducation", method = RequestMethod.POST)
    public RetDataBean updatePartyEducation( PartyEducation partyEducation) {
        try {
            if (partyEducation.getSortId().equals(partyEducation.getLevelId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_PARENT_ID_CANNOT_TO_USE);
            }
            if (StringUtils.isBlank(partyEducation.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(partyEducation.getLevelId())) {
                partyEducation.setLevelId("0");
            }
            Example example = new Example(PartyEducation.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("sortId", partyEducation.getSortId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyEducationService.updatePartyEducation(example, partyEducation));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyDegree
     * @return RetDataBean
     * @Title: insertPartyDegree
     * @Description:  添加学位分类
     */
    @RequestMapping(value = "/insertPartyDegree", method = RequestMethod.POST)
    public RetDataBean insertPartyDegree( PartyDegree partyDegree) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyDegree.setSortId(SysTools.getGUID());
            if (StringUtils.isBlank(partyDegree.getLevelId())) {
                partyDegree.setLevelId("0");
            }
            partyDegree.setCreateUser(account.getAccountId());
            partyDegree.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyDegree.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyDegreeService.insertPartyDegree(partyDegree));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyDegree
     * @return RetDataBean
     * @Title: deletePartyDegree
     * @Description:  删除学位分类
     */
    @RequestMapping(value = "/deletePartyDegree", method = RequestMethod.POST)
    public RetDataBean deletePartyDegree( PartyDegree partyDegree) {
        try {
            if (StringUtils.isBlank(partyDegree.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyDegree.setOrgId(account.getOrgId());
            if (partyDegreeService.isExistChild(partyDegree.getOrgId(), partyDegree.getSortId()) > 0) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_HAVE_CHILDREN_ITEM);
            } else {
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyDegreeService.deletePartyDegree(partyDegree));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyDegree
     * @return RetDataBean
     * @Title: updatePartyDegree
     * @Description:  更新学位分类
     */
    @RequestMapping(value = "/updatePartyDegree", method = RequestMethod.POST)
    public RetDataBean updatePartyDegree(PartyDegree partyDegree) {
        try {
            if (partyDegree.getSortId().equals(partyDegree.getLevelId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_PARENT_ID_CANNOT_TO_USE);
            }
            if (StringUtils.isBlank(partyDegree.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(partyDegree.getLevelId())) {
                partyDegree.setLevelId("0");
            }
            Example example = new Example(PartyDegree.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("sortId", partyDegree.getSortId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyDegreeService.updatePartyDegree(example, partyDegree));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyNativePlace
     * @return RetDataBean
     * @Title: insertPartyNativePlace
     * @Description:  添加籍贯分类信息
     */
    @RequestMapping(value = "/insertPartyNativePlace", method = RequestMethod.POST)
    public RetDataBean insertPartyNativePlace( PartyNativePlace partyNativePlace) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyNativePlace.setSortId(SysTools.getGUID());
            if (StringUtils.isBlank(partyNativePlace.getLevelId())) {
                partyNativePlace.setLevelId("0");
            }
            partyNativePlace.setCreateUser(account.getAccountId());
            partyNativePlace.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyNativePlace.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyNativePlaceService.insertPartyNativePlace(partyNativePlace));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyNativePlace
     * @return RetDataBean
     * @Title: deletePartyNativePlace
     * @Description:  删除籍贯分类信息
     */
    @RequestMapping(value = "/deletePartyNativePlace", method = RequestMethod.POST)
    public RetDataBean deletePartyNativePlace( PartyNativePlace partyNativePlace) {
        try {
            if (StringUtils.isBlank(partyNativePlace.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyNativePlace.setOrgId(account.getOrgId());
            if (partyNativePlaceService.isExistChild(partyNativePlace.getOrgId(), partyNativePlace.getSortId()) > 0) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_HAVE_CHILDREN_ITEM);
            } else {
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyNativePlaceService.deletePartyNativePlace(partyNativePlace));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyNativePlace
     * @return RetDataBean
     * @Title: updatePartyNativePlace
     * @Description:  更新籍贯信息
     */
    @RequestMapping(value = "/updatePartyNativePlace", method = RequestMethod.POST)
    public RetDataBean updatePartyNativePlace( PartyNativePlace partyNativePlace) {
        try {
            if (partyNativePlace.getSortId().equals(partyNativePlace.getLevelId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_PARENT_ID_CANNOT_TO_USE);
            }
            if (StringUtils.isBlank(partyNativePlace.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(partyNativePlace.getLevelId())) {
                partyNativePlace.setLevelId("0");
            }
            Example example = new Example(PartyNativePlace.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("sortId", partyNativePlace.getSortId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyNativePlaceService.updatePartyNativePlace(example, partyNativePlace));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyUnitIndustry
     * @return RetDataBean
     * @Title: insertPartyUnitIndustry
     * @Description:  添加单位所属行业
     */
    @RequestMapping(value = "/insertPartyUnitIndustry", method = RequestMethod.POST)
    public RetDataBean insertPartyUnitIndustry( PartyUnitIndustry partyUnitIndustry) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnitIndustry.setSortId(SysTools.getGUID());
            if (StringUtils.isBlank(partyUnitIndustry.getLevelId())) {
                partyUnitIndustry.setLevelId("0");
            }
            partyUnitIndustry.setCreateUser(account.getAccountId());
            partyUnitIndustry.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyUnitIndustry.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyUnitIndustryService.insertPartyUnitIndustry(partyUnitIndustry));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyUnitIndustry
     * @return RetDataBean
     * @Title: deletePartyUnitIndustry
     * @Description:  删除单位所属行业
     */
    @RequestMapping(value = "/deletePartyUnitIndustry", method = RequestMethod.POST)
    public RetDataBean deletePartyUnitIndustry( PartyUnitIndustry partyUnitIndustry) {
        try {
            if (StringUtils.isBlank(partyUnitIndustry.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnitIndustry.setOrgId(account.getOrgId());
            if (partyUnitIndustryService.isExistChild(partyUnitIndustry.getOrgId(), partyUnitIndustry.getSortId()) > 0) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_HAVE_CHILDREN_ITEM);
            } else {
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyUnitIndustryService.deletePartyUnitIndustry(partyUnitIndustry));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyUnitIndustry
     * @return RetDataBean
     * @Title: updatePartyUnitIndustry
     * @Description:  更新单位所属行业
     */
    @RequestMapping(value = "/updatePartyUnitIndustry", method = RequestMethod.POST)
    public RetDataBean updatePartyUnitIndustry( PartyUnitIndustry partyUnitIndustry) {
        try {
            if (partyUnitIndustry.getSortId().equals(partyUnitIndustry.getLevelId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_PARENT_ID_CANNOT_TO_USE);
            }
            if (StringUtils.isBlank(partyUnitIndustry.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(partyUnitIndustry.getLevelId())) {
                partyUnitIndustry.setLevelId("0");
            }
            Example example = new Example(PartyUnitIndustry.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("sortId", partyUnitIndustry.getSortId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyUnitIndustryService.updatePartyUnitIndustry(example, partyUnitIndustry));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyUnitService
     * @return RetDataBean
     * @Title: insertPartyUnitService
     * @Description:  添加单位所属服务类型
     */
    @RequestMapping(value = "/insertPartyUnitService", method = RequestMethod.POST)
    public RetDataBean insertPartyUnitService( PartyUnitService partyUnitService) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnitService.setSortId(SysTools.getGUID());
            if (StringUtils.isBlank(partyUnitService.getLevelId())) {
                partyUnitService.setLevelId("0");
            }
            partyUnitService.setCreateUser(account.getAccountId());
            partyUnitService.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyUnitService.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyUnitServiceService.insertPartyUnitService(partyUnitService));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyUnitService
     * @return RetDataBean
     * @Title: deletePartyUnitService
     * @Description:  删除单位所属行业
     */
    @RequestMapping(value = "/deletePartyUnitService", method = RequestMethod.POST)
    public RetDataBean deletePartyUnitService( PartyUnitService partyUnitService) {
        try {
            if (StringUtils.isBlank(partyUnitService.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnitService.setOrgId(account.getOrgId());
            if (partyUnitServiceService.isExistChild(partyUnitService.getOrgId(), partyUnitService.getSortId()) > 0) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_HAVE_CHILDREN_ITEM);
            } else {
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyUnitServiceService.deletePartyUnitService(partyUnitService));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyUnitService
     * @return RetDataBean
     * @Title: updatePartyUnitSubo
     * @Description:  更新单位所属行业分类
     */
    @RequestMapping(value = "/updatePartyUnitService", method = RequestMethod.POST)
    public RetDataBean updatePartyUnitService(PartyUnitService partyUnitService) {
        try {
            if (partyUnitService.getSortId().equals(partyUnitService.getLevelId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_PARENT_ID_CANNOT_TO_USE);
            }
            if (StringUtils.isBlank(partyUnitService.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(partyUnitService.getLevelId())) {
                partyUnitService.setLevelId("0");
            }
            Example example = new Example(PartyUnitService.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("sortId", partyUnitService.getSortId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyUnitServiceService.updatePartyUnitService(example, partyUnitService));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
    
     * @param partyUnitSubo
     * @return RetDataBean
     * @Title: insertPartyUnitSubo
     * @Description:  添加单位隶属关系分类
     */
    @RequestMapping(value = "/insertPartyUnitSubo", method = RequestMethod.POST)
    public RetDataBean insertPartyUnitSubo( PartyUnitSubo partyUnitSubo) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnitSubo.setSortId(SysTools.getGUID());
            if (StringUtils.isBlank(partyUnitSubo.getLevelId())) {
                partyUnitSubo.setLevelId("0");
            }
            partyUnitSubo.setCreateUser(account.getAccountId());
            partyUnitSubo.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyUnitSubo.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyUnitSuboService.insertPartyUnitSubo(partyUnitSubo));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyUnitSubo
     * @return RetDataBean
     * @Title: deletePartyUnitSubo
     * @Description:  删除单位隶属关系分类
     */
    @RequestMapping(value = "/deletePartyUnitSubo", method = RequestMethod.POST)
    public RetDataBean deletePartyUnitSubo(PartyUnitSubo partyUnitSubo) {
        try {
            if (StringUtils.isBlank(partyUnitSubo.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnitSubo.setOrgId(account.getOrgId());
            if (partyUnitSuboService.isExistChild(partyUnitSubo.getOrgId(), partyUnitSubo.getSortId()) > 0) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_HAVE_CHILDREN_ITEM);
            } else {
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyUnitSuboService.deletePartyUnitSubo(partyUnitSubo));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyUnitSubo
     * @return RetDataBean
     * @Title: updatePartyUnitSubo
     * @Description:  更新单位隶属关系分类信息
     */
    @RequestMapping(value = "/updatePartyUnitSubo", method = RequestMethod.POST)
    public RetDataBean updatePartyUnitSubo(PartyUnitSubo partyUnitSubo) {
        try {
            if (partyUnitSubo.getSortId().equals(partyUnitSubo.getLevelId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_PARENT_ID_CANNOT_TO_USE);
            }
            if (StringUtils.isBlank(partyUnitSubo.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(partyUnitSubo.getLevelId())) {
                partyUnitSubo.setLevelId("0");
            }
            Example example = new Example(PartyUnitSubo.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("sortId", partyUnitSubo.getSortId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyUnitSuboService.updatePartyUnitSubo(example, partyUnitSubo));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyUnitType
     * @return RetDataBean
     * @Title: insertPartyUnitType
     * @Description:  添加单位分类
     */
    @RequestMapping(value = "/insertPartyUnitType", method = RequestMethod.POST)
    public RetDataBean insertPartyUnitType(PartyUnitType partyUnitType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnitType.setSortId(SysTools.getGUID());
            if (StringUtils.isBlank(partyUnitType.getLevelId())) {
                partyUnitType.setLevelId("0");
            }
            partyUnitType.setCreateUser(account.getAccountId());
            partyUnitType.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyUnitType.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyUnitTypeService.insertPartyUnitType(partyUnitType));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyUnitType
     * @return RetDataBean
     * @Title: deletePartyUnitType
     * @Description:  删除单位分类
     */
    @RequestMapping(value = "/deletePartyUnitType", method = RequestMethod.POST)
    public RetDataBean deletePartyUnitType(PartyUnitType partyUnitType) {
        try {
            if (StringUtils.isBlank(partyUnitType.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnitType.setOrgId(account.getOrgId());
            if (partyUnitTypeService.isExistChild(partyUnitType.getOrgId(), partyUnitType.getSortId()) > 0) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_HAVE_CHILDREN_ITEM);
            } else {
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyUnitTypeService.deletePartyUnitType(partyUnitType));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyUnitType
     * @return RetDataBean
     * @Title: updatePartyUnitType
     * @Description:  更新单位分类信息
     */
    @RequestMapping(value = "/updatePartyUnitType", method = RequestMethod.POST)
    public RetDataBean updatePartyUnitType(PartyUnitType partyUnitType) {
        try {
            if (partyUnitType.getSortId().equals(partyUnitType.getLevelId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_PARENT_ID_CANNOT_TO_USE);
            }
            if (StringUtils.isBlank(partyUnitType.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(partyUnitType.getLevelId())) {
                partyUnitType.setLevelId("0");
            }
            Example example = new Example(PartyUnitType.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("sortId", partyUnitType.getSortId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyUnitTypeService.updatePartyUnitType(example, partyUnitType));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partySubo
     * @return RetDataBean
     * @Title: insertPartySubo
     * @Description:  添加党组织隶属关系
     */
    @RequestMapping(value = "/insertPartySubo", method = RequestMethod.POST)
    public RetDataBean insertPartySubo( PartySubo partySubo) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partySubo.setSortId(SysTools.getGUID());
            if (StringUtils.isBlank(partySubo.getLevelId())) {
                partySubo.setLevelId("0");
            }
            partySubo.setCreateUser(account.getAccountId());
            partySubo.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partySubo.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partySuboService.insertPartySubo(partySubo));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partySubo
     * @return RetDataBean
     * @Title: deletePartySubo
     * @Description:  删除党组织隶属关系
     */
    @RequestMapping(value = "/deletePartySubo", method = RequestMethod.POST)
    public RetDataBean deletePartySubo( PartySubo partySubo) {
        try {
            if (StringUtils.isBlank(partySubo.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partySubo.setOrgId(account.getOrgId());
            if (partySuboService.isExistChild(partySubo.getOrgId(), partySubo.getSortId()) > 0) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_HAVE_CHILDREN_ITEM);
            } else {
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partySuboService.deletePartySubo(partySubo));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partySubo
     * @return RetDataBean
     * @Title: updatePartySubo
     * @Description:  更新党组织隶属关系
     */
    @RequestMapping(value = "/updatePartySubo", method = RequestMethod.POST)
    public RetDataBean updatePartySubo(PartySubo partySubo) {
        try {
            if (partySubo.getSortId().equals(partySubo.getLevelId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_PARENT_ID_CANNOT_TO_USE);
            }
            if (StringUtils.isBlank(partySubo.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(partySubo.getLevelId())) {
                partySubo.setLevelId("0");
            }
            Example example = new Example(PartySubo.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("sortId", partySubo.getSortId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partySuboService.updatePartySubo(example, partySubo));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyWorkType
     * @return RetDataBean
     * @Title: insertPartyWorkType
     * @Description:  添加党组织工作分类
     */
    @RequestMapping(value = "/insertPartyWorkType", method = RequestMethod.POST)
    public RetDataBean insertPartyWorkType( PartyWorkType partyWorkType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyWorkType.setSortId(SysTools.getGUID());
            if (StringUtils.isBlank(partyWorkType.getLevelId())) {
                partyWorkType.setLevelId("0");
            }
            partyWorkType.setCreateUser(account.getAccountId());
            partyWorkType.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyWorkType.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyWorkTypeService.insertPartyWorkType(partyWorkType));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyWorkType
     * @return RetDataBean
     * @Title: deletePartyWorkType
     * @Description:  删除党组织工作分类
     */
    @RequestMapping(value = "/deletePartyWorkType", method = RequestMethod.POST)
    public RetDataBean deletePartyWorkType(PartyWorkType partyWorkType) {
        try {
            if (StringUtils.isBlank(partyWorkType.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyWorkType.setOrgId(account.getOrgId());
            if (partyWorkTypeService.isExistChild(partyWorkType.getOrgId(), partyWorkType.getSortId()) > 0) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_HAVE_CHILDREN_ITEM);
            } else {
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyWorkTypeService.deletePartyWorkType(partyWorkType));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyWorkType
     * @return RetDataBean
     * @Title: updatePartyWorkType
     * @Description:  更新党组织工作类别
     */
    @RequestMapping(value = "/updatePartyWorkType", method = RequestMethod.POST)
    public RetDataBean updatePartyWorkType(PartyWorkType partyWorkType) {
        try {
            if (partyWorkType.getSortId().equals(partyWorkType.getLevelId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_PARENT_ID_CANNOT_TO_USE);
            }
            if (StringUtils.isBlank(partyWorkType.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(partyWorkType.getLevelId())) {
                partyWorkType.setLevelId("0");
            }
            Example example = new Example(PartyWorkType.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("sortId", partyWorkType.getSortId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyWorkTypeService.updatePartyWorkType(example, partyWorkType));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
    
     * @param partyType
     * @return RetDataBean
     * @Title: insertPartyType
     * @Description:  添加党组织分类
     */
    @RequestMapping(value = "/insertPartyType", method = RequestMethod.POST)
    public RetDataBean insertPartyType( PartyType partyType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyType.setSortId(SysTools.getGUID());
            if (StringUtils.isBlank(partyType.getLevelId())) {
                partyType.setLevelId("0");
            }
            partyType.setCreateUser(account.getAccountId());
            partyType.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyType.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyTypeService.insertPartyType(partyType));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyType
     * @return RetDataBean
     * @Title: deletePartyType
     * @Description:  删除党组织分类
     */
    @RequestMapping(value = "/deletePartyType", method = RequestMethod.POST)
    public RetDataBean deletePartyType( PartyType partyType) {
        try {
            if (StringUtils.isBlank(partyType.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyType.setOrgId(account.getOrgId());
            if (partyTypeService.isExistChild(partyType.getOrgId(), partyType.getSortId()) > 0) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_HAVE_CHILDREN_ITEM);
            } else {
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyTypeService.deletePartyType(partyType));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyType
     * @return RetDataBean
     * @Title: updatePartyType
     * @Description:  更新党组织类别
     */
    @RequestMapping(value = "/updatePartyType", method = RequestMethod.POST)
    public RetDataBean updatePartyType(PartyType partyType) {
        try {
            if (partyType.getSortId().equals(partyType.getLevelId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_PARENT_ID_CANNOT_TO_USE);
            }
            if (StringUtils.isBlank(partyType.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(partyType.getLevelId())) {
                partyType.setLevelId("0");
            }
            Example example = new Example(PartyType.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("sortId", partyType.getSortId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyTypeService.updatePartyType(example, partyType));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

}
