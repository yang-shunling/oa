package com.core136.service.partyparam;


import com.core136.bean.partyparam.PartyWorkType;
import com.core136.mapper.partyparam.PartyWorkTypeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyWorkTypeService {
    private PartyWorkTypeMapper partyWorkTypeMapper;

    @Autowired
    public void setPartyWorkTypeMapper(PartyWorkTypeMapper partyWorkTypeMapper) {
        this.partyWorkTypeMapper = partyWorkTypeMapper;
    }

    public int insertPartyWorkType(PartyWorkType partyWorkType) {
        return partyWorkTypeMapper.insert(partyWorkType);
    }

    public int deletePartyWorkType(PartyWorkType partyWorkType) {
        return partyWorkTypeMapper.delete(partyWorkType);
    }

    public int updatePartyWorkType(Example example, PartyWorkType partyWorkType) {
        return partyWorkTypeMapper.updateByExampleSelective(partyWorkType, example);
    }

    public PartyWorkType selectOnePartyWorkType(PartyWorkType partyWorkType) {
        return partyWorkTypeMapper.selectOne(partyWorkType);
    }

    /**
     * @param orgId
     * @param levelId
     * @return List<Map < String, String>>
     * @Title: getWorkTypeTree
     * @Description:  获取党组织工作分类
     */
    public List<Map<String, String>> getWorkTypeTree(String orgId, String levelId) {
        return partyWorkTypeMapper.getWorkTypeTree(orgId, levelId);
    }

    /**
     * 判断是否还有子集
     */

    public int isExistChild(String orgId, String sortId) {
        return partyWorkTypeMapper.isExistChild(orgId, sortId);
    }
}
