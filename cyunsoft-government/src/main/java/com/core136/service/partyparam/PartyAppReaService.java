package com.core136.service.partyparam;

import com.core136.bean.partyparam.PartyAppRea;
import com.core136.mapper.partyparam.PartyAppReaMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyAppReaService {
    private PartyAppReaMapper partyAppReaMapper;

    @Autowired
    public void setPartyAppReaMapper(PartyAppReaMapper partyAppReaMapper) {
        this.partyAppReaMapper = partyAppReaMapper;
    }

    public int insertPartyAppRea(PartyAppRea partyAppRea) {
        return partyAppReaMapper.insert(partyAppRea);
    }

    public int deletePartyAppRea(PartyAppRea partyAppRea) {
        return partyAppReaMapper.delete(partyAppRea);
    }

    public int updatePartyAppRea(Example example, PartyAppRea partyAppRea) {
        return partyAppReaMapper.updateByExampleSelective(partyAppRea, example);
    }

    public PartyAppRea selectOnePartyAppRea(PartyAppRea partyAppRea) {
        return partyAppReaMapper.selectOne(partyAppRea);
    }

    /**
     * @param orgId
     * @param levelId
     * @return List<Map < String, String>>
     * @Title: getAppReaTree
     * @Description:  评议奖惩原因类别树
     */
    public List<Map<String, String>> getAppReaTree(String orgId, String levelId) {
        return partyAppReaMapper.getAppReaTree(orgId, levelId);
    }

    /**
     * 判断是否还有子集
     */

    public int isExistChild(String orgId, String sortId) {
        //  Auto-generated method stub
        return partyAppReaMapper.isExistChild(orgId, sortId);
    }
}
