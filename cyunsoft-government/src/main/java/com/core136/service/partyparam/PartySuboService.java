package com.core136.service.partyparam;


import com.core136.bean.partyparam.PartySubo;
import com.core136.mapper.partyparam.PartySuboMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartySuboService {
    private PartySuboMapper partySuboMapper;

    @Autowired
    public void setPartySuboMapper(PartySuboMapper partySuboMapper) {
        this.partySuboMapper = partySuboMapper;
    }

    public int insertPartySubo(PartySubo partySubo) {
        return partySuboMapper.insert(partySubo);
    }

    public int deletePartySubo(PartySubo partySubo) {
        return partySuboMapper.delete(partySubo);
    }

    public int updatePartySubo(Example example, PartySubo partySubo) {
        return partySuboMapper.updateByExampleSelective(partySubo, example);
    }

    public PartySubo selectOnePartySubo(PartySubo partySubo) {
        return partySuboMapper.selectOne(partySubo);
    }

    /**
     * @param orgId
     * @param levelId
     * @return List<Map < String, String>>
     * @Title: getPartySuboTree
     * @Description:  获取党组织隶属关系树
     */
    public List<Map<String, String>> getPartySuboTree(String orgId, String levelId) {
        return partySuboMapper.getPartySuboTree(orgId, levelId);
    }

    /**
     * 判断是否还有子集
     */

    public int isExistChild(String orgId, String sortId) {
        //  Auto-generated method stub
        return partySuboMapper.isExistChild(orgId, sortId);
    }
}
