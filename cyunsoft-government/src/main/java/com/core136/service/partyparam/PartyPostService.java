package com.core136.service.partyparam;

import com.core136.bean.partyparam.PartyPost;
import com.core136.mapper.partyparam.PartyPostMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyPostService {
    private PartyPostMapper partyPostMapper;

    @Autowired
    public void setPartyPostMapper(PartyPostMapper partyPostMapper) {
        this.partyPostMapper = partyPostMapper;
    }

    public int insertPartyPost(PartyPost partyPost) {
        return partyPostMapper.insert(partyPost);
    }

    public int deletePartyPost(PartyPost partyPost) {
        return partyPostMapper.delete(partyPost);
    }

    public int updatePartyPost(Example example, PartyPost partyPost) {
        return partyPostMapper.updateByExampleSelective(partyPost, example);
    }

    public PartyPost selectOnePartyPost(PartyPost partyPost) {
        return partyPostMapper.selectOne(partyPost);
    }

    /**
     * @param orgId
     * @param levelId
     * @return List<Map < String, String>>
     * @Title: getPostTree
     * @Description:  获取工作岗位树
     */
    public List<Map<String, String>> getPostTree(String orgId, String levelId) {
        return partyPostMapper.getPostTree(orgId, levelId);
    }

    /**
     * 判断是否还有子集
     */

    public int isExistChild(String orgId, String sortId) {
        return partyPostMapper.isExistChild(orgId, sortId);
    }
}
