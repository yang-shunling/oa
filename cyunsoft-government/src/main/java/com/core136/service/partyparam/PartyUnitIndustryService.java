package com.core136.service.partyparam;

import com.core136.bean.partyparam.PartyUnitIndustry;
import com.core136.mapper.partyparam.PartyUnitIndustryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyUnitIndustryService {
    private PartyUnitIndustryMapper partyUnitIndustryMapper;

    @Autowired
    public void setPartyUnitIndustryMapper(PartyUnitIndustryMapper partyUnitIndustryMapper) {
        this.partyUnitIndustryMapper = partyUnitIndustryMapper;
    }

    public int insertPartyUnitIndustry(PartyUnitIndustry giPartyUnitIndustry) {
        return partyUnitIndustryMapper.insert(giPartyUnitIndustry);
    }

    public int deletePartyUnitIndustry(PartyUnitIndustry partyUnitIndustry) {
        return partyUnitIndustryMapper.delete(partyUnitIndustry);
    }

    public int updatePartyUnitIndustry(Example example, PartyUnitIndustry partyUnitIndustry) {
        return partyUnitIndustryMapper.updateByExampleSelective(partyUnitIndustry, example);
    }

    public PartyUnitIndustry selectOnePartyUnitIndustry(PartyUnitIndustry partyUnitIndustry) {
        return partyUnitIndustryMapper.selectOne(partyUnitIndustry);
    }

    /**
     * @param orgId
     * @param levelId
     * @return List<Map < String, String>>
     * @Title: getUnitIndustryTree
     * @Description:  单位所属行业树
     */
    public List<Map<String, String>> getUnitIndustryTree(String orgId, String levelId) {
        return partyUnitIndustryMapper.getUnitIndustryTree(orgId, levelId);
    }

    /**
     * 判断是否还有子集
     */

    public int isExistChild(String orgId, String sortId) {
        //  Auto-generated method stub
        return partyUnitIndustryMapper.isExistChild(orgId, sortId);
    }


}
