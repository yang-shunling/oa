package com.core136.service.partyparam;

import com.core136.bean.partyparam.PartyTitle;
import com.core136.mapper.partyparam.PartyTitleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyTitleService {
    private PartyTitleMapper partyTitleMapper;

    @Autowired
    public void setPartyTitleMapper(PartyTitleMapper partyTitleMapper) {
        this.partyTitleMapper = partyTitleMapper;
    }

    public int insertPartyTitle(PartyTitle partyTitle) {
        return partyTitleMapper.insert(partyTitle);
    }

    public int deletePartyTitle(PartyTitle partyTitle) {
        return partyTitleMapper.delete(partyTitle);
    }

    public int updatePartyTitle(Example example, PartyTitle partyTitle) {
        return partyTitleMapper.updateByExampleSelective(partyTitle, example);
    }

    public PartyTitle selectOnePartyTitle(PartyTitle partyTitle) {
        return partyTitleMapper.selectOne(partyTitle);
    }

    /**
     * @param orgId
     * @param levelId
     * @return List<Map < String, String>>
     * @Title: getTitleTree
     * @Description:  获取职称分类树
     */
    public List<Map<String, String>> getTitleTree(String orgId, String levelId) {
        return partyTitleMapper.getTitleTree(orgId, levelId);
    }

    /**
     * 判断是否还有子集
     */

    public int isExistChild(String orgId, String sortId) {
        return partyTitleMapper.isExistChild(orgId, sortId);
    }
}
