package com.core136.service.partyunion;

import com.core136.bean.partyunion.PartyUnionFunds;
import com.core136.mapper.partyunion.PartyUnionFundsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

@Service
public class PartyUnionFundsService {

    private PartyUnionFundsMapper partyUnionFundsMapper;

    @Autowired
    public void setPartyUnionFundsMapper(PartyUnionFundsMapper partyUnionFundsMapper) {
        this.partyUnionFundsMapper = partyUnionFundsMapper;
    }

    public int insertPartyUnionFunds(PartyUnionFunds partyUnionFunds) {
        return partyUnionFundsMapper.insert(partyUnionFunds);
    }

    public int deletePartyUnionFunds(PartyUnionFunds partyUnionFunds) {
        return partyUnionFundsMapper.delete(partyUnionFunds);
    }

    public int updatePartyUnionFunds(Example example, PartyUnionFunds partyUnionFunds) {
        return partyUnionFundsMapper.updateByExampleSelective(partyUnionFunds, example);
    }

    public PartyUnionFunds selectOnePartyUnionFunds(PartyUnionFunds partyUnionFunds) {
        return partyUnionFundsMapper.selectOne(partyUnionFunds);
    }

}
