package com.core136.service.partyunion;

import com.core136.bean.partyunion.PartyUnionStory;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partyunion.PartyUnionStoryMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyUnionStoryService {
    private PartyUnionStoryMapper partyUnionStoryMapper;

    @Autowired
    public void setPartyUnionStoryMapper(PartyUnionStoryMapper partyUnionStoryMapper) {
        this.partyUnionStoryMapper = partyUnionStoryMapper;
    }

    public int insertPartyUnionStory(PartyUnionStory partyUnionStory) {
        return partyUnionStoryMapper.insert(partyUnionStory);
    }

    public int deletePartyUnionStory(PartyUnionStory partyUnionStory) {
        return partyUnionStoryMapper.delete(partyUnionStory);
    }

    public int updatePartyUnionStory(Example example, PartyUnionStory partyUnionStory) {
        return partyUnionStoryMapper.updateByExampleSelective(partyUnionStory, example);
    }

    public PartyUnionStory selectOnePartyUnionStory(PartyUnionStory partyUnionStory) {
        return partyUnionStoryMapper.selectOne(partyUnionStory);
    }

    /**
     * 职场故事列表
     *
     * @param orgId
     * @param storyType
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String, String>> getPartyUnionStoryList(String orgId, String storyType, String beginTime, String endTime, String search) {
        return partyUnionStoryMapper.getPartyUnionStoryList(orgId, storyType, beginTime, endTime, "%" + search + "%");
    }

    /**
     * 职场故事列表
     *
     * @param pageParam
     * @param storyType
     * @param beginTime
     * @param endTime
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getPartyUnionStoryList(PageParam pageParam, String storyType, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getPartyUnionStoryList(pageParam.getOrgId(), storyType, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }
}
