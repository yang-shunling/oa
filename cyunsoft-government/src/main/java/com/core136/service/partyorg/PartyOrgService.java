package com.core136.service.partyorg;

import com.core136.bean.partyorg.PartyOrg;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partyorg.PartyOrgMapper;
import com.core136.unit.map.BaiduMapTools;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PartyOrgService {
    @Value("${app.baidu.map.ak}")
    private String ak;
    private PartyOrgMapper partyOrgMapper;

    @Autowired
    public void setPartyOrgMapper(PartyOrgMapper partyOrgMapper) {
        this.partyOrgMapper = partyOrgMapper;
    }

    private List<String> childOrgId = new ArrayList<String>();


    public int insertPartyOrg(PartyOrg partyOrg) {
        return partyOrgMapper.insert(partyOrg);
    }

    public int deletePartyOrg(PartyOrg partyOrg) {
        return partyOrgMapper.delete(partyOrg);
    }

    public int updatePartyOrg(Example example, PartyOrg partyOrg) {
        return partyOrgMapper.updateByExampleSelective(partyOrg, example);
    }

    public PartyOrg selectOnePartyOrg(PartyOrg partyOrg) {
        return partyOrgMapper.selectOne(partyOrg);
    }

    /**
     * @param orgId
     * @return List<Map < String, Object>>
     * @Title: getPartyOrgLngAndLat
     * @Description:  获取党组织坐标
     */
    public List<Map<String, Object>> getPartyOrgLngAndLat(String orgId) {
        PartyOrg partyOrg = new PartyOrg();
        partyOrg.setOrgId(orgId);
        List<PartyOrg> partyOrgList = partyOrgMapper.select(partyOrg);
        List<Map<String, String>> addressList = new ArrayList<Map<String, String>>();
        for (int i = 0; i < partyOrgList.size(); i++) {
            if (StringUtils.isNotBlank(partyOrgList.get(i).getAddress())) {
                Map<String, String> tempMap = new HashMap<String, String>();
                tempMap.put("title", partyOrgList.get(i).getPartyOrgName());
                tempMap.put("content", partyOrgList.get(i).getAddress());
                addressList.add(tempMap);
            }
        }

        return BaiduMapTools.getLngAndLat(addressList, ak);
    }


    /**
     * @param orgId
     * @param levelId
     * @return List<String>
     * @Title: getChildOrgIdStr
     * @Description:  获取党组织所有支部ID
     */
    public List<String> getChildOrgIdStr(String orgId, String levelId) {
        childOrgId.clear();
        childOrgId.add(levelId);
        getChildPartyOrg(orgId, levelId);
        return childOrgId;
    }

    /**
     * @param orgId
     * @param levelId
     * @Title: getChildPartyOrg
     * @Description:  递归获取党组织所有支部ID
     */
    public void getChildPartyOrg(String orgId, String levelId) {
        PartyOrg partyOrg = new PartyOrg();
        partyOrg.setOrgId(orgId);
        partyOrg.setLevelId(levelId);
        List<PartyOrg> pol = new ArrayList<PartyOrg>();
        pol = partyOrgMapper.select(partyOrg);
        for (int i = 0; i < pol.size(); i++) {
            childOrgId.add(pol.get(i).getPartyOrgId());
            getChildPartyOrg(orgId, pol.get(i).getPartyOrgId());
        }
    }


    /**
     * @param orgId
     * @param levelId
     * @return List<Map < String, String>>
     * @Title: getPartyOrgTree
     * @Description:  获取党组织树形结构
     */
    public List<Map<String, String>> getPartyOrgTree(String orgId, String levelId) {
        return partyOrgMapper.getPartyOrgTree(orgId, levelId);
    }

    public List<Map<String, String>> getPartyOrgAllParentTree(String orgId, String levelId) {
        return partyOrgMapper.getPartyOrgAllParentTree(orgId, levelId);
    }

    /**
     * @param orgId
     * @param partyOrgId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPartyOrgListById
     * @Description:  获取党组织下组织单位列表
     */
    public List<Map<String, String>> getPartyOrgListById(String orgId, String partyOrgId, String search) {
        return partyOrgMapper.getPartyOrgListById(orgId, partyOrgId, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param partyOrgId
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getPartyOrgListById
     * @Description:   获取党组织下组织单位列表
     */
    public PageInfo<Map<String, String>> getPartyOrgListById(PageParam pageParam, String partyOrgId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getPartyOrgListById(pageParam.getOrgId(), partyOrgId, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
