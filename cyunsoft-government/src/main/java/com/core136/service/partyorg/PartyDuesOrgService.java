package com.core136.service.partyorg;

import com.core136.bean.partyorg.PartyDuesOrg;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partyorg.PartyDuesOrgMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyDuesOrgService {
    private PartyDuesOrgMapper partyDuesOrgMapper;

    @Autowired
    public void setPartyDuesOrgMapper(PartyDuesOrgMapper partyDuesOrgMapper) {
        this.partyDuesOrgMapper = partyDuesOrgMapper;
    }

    public int insertPartyDuesOrg(PartyDuesOrg partyDuesOrg) {
        return partyDuesOrgMapper.insert(partyDuesOrg);
    }

    public int deletePartyDuesOrg(PartyDuesOrg partyDuesOrg) {
        return partyDuesOrgMapper.delete(partyDuesOrg);
    }

    public int updatePartyDuesOrg(Example example, PartyDuesOrg partyDuesOrg) {
        return partyDuesOrgMapper.updateByExampleSelective(partyDuesOrg, example);
    }

    public PartyDuesOrg selectOnePartyDuesOrg(PartyDuesOrg partyDuesOrg) {
        return partyDuesOrgMapper.selectOne(partyDuesOrg);
    }

    /**
     * @param orgId
     * @param onTime
     * @param partyOrgId
     * @param year
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPartyDeusOrgList
     * @Description:  组织党费记录
     */
    public List<Map<String, String>> getPartyDeusOrgList(String orgId, String onTime, String partyOrgId, String year, String search) {
        return partyDuesOrgMapper.getPartyDeusOrgList(orgId, onTime, partyOrgId, year, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param onTime
     * @param partyOrgId
     * @param year
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getPartyDeusOrgList
     * @Description:  组织党费记录
     */
    public PageInfo<Map<String, String>> getPartyDeusOrgList(PageParam pageParam, String onTime, String partyOrgId, String year) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getPartyDeusOrgList(pageParam.getOrgId(), onTime, partyOrgId, year, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
