package com.core136.service.partymember;

import com.core136.bean.account.Account;
import com.core136.bean.partymember.PartyMemberPoints;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partymember.PartyMemberPointsMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PartyMemberPointsService {
    private PartyMemberPointsMapper partyMemberPointsMapper;

    @Autowired
    public void setPartyMemberPointsMapper(PartyMemberPointsMapper partyMemberPointsMapper) {
        this.partyMemberPointsMapper = partyMemberPointsMapper;
    }

    public int insertPartyMemberPoints(PartyMemberPoints partyMemberPoints) {
        return partyMemberPointsMapper.insert(partyMemberPoints);
    }


    public int deletePartyMemberPoints(PartyMemberPoints partyMemberPoints) {
        return partyMemberPointsMapper.delete(partyMemberPoints);
    }

    public int updatePartyMemberPoints(Example example, PartyMemberPoints partyMemberPoints) {
        return partyMemberPointsMapper.updateByExampleSelective(partyMemberPoints, example);
    }

    public PartyMemberPoints selectOnePartyMemberPoints(PartyMemberPoints partyMemberPoints) {
        return partyMemberPointsMapper.selectOne(partyMemberPoints);
    }


    /**
     * 获取党员积分列表
     *
     * @param orgId
     * @param pointsYear
     * @param partyOrgId
     * @return
     */
    public List<Map<String, String>> getMemberPointsList(String orgId, String pointsYear, String memberId) {
        return partyMemberPointsMapper.getMemberPointsList(orgId, pointsYear, memberId);
    }

    /**
     * 获取党员积分列表
     *
     * @param pageParam
     * @param pointsYear
     * @param partyOrgId
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getMemberPointsList(PageParam pageParam, String pointsYear, String memberId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMemberPointsList(pageParam.getOrgId(), pointsYear, memberId);
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }


    /**
     * 获取党员汇总积分列表
     *
     * @param orgId
     * @param pointsYear
     * @param partyOrgId
     * @return
     */
    public List<Map<String, String>> getMemberPointsTotalList(String orgId, String pointsYear, String memberId) {
        return partyMemberPointsMapper.getMemberPointsTotalList(orgId, pointsYear, memberId);
    }

    /**
     * 获取党员汇总列表
     *
     * @param pageParam
     * @param pointsYear
     * @param partyOrgId
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getMemberPointsTotalList(PageParam pageParam, String pointsYear, String memberId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMemberPointsTotalList(pageParam.getOrgId(), pointsYear, memberId);
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param pointsYear
     * @param memberId
     * @return Map<String, Object>
     * @Title: getMyPointsByYear
     * @Description:  按年度获取党员个人积分总和
     */
    public Map<String, Object> getMyPointsByYear(String orgId, String pointsYear, String memberId) {
        return partyMemberPointsMapper.getMyPointsByYear(orgId, pointsYear, memberId);
    }

    /**
     * @param orgId
     * @param memberId
     * @return Map<String, Object>
     * @Title: getMyPointsByAllYear
     * @Description:  获取党员历史积分总和
     */
    public Map<String, Object> getMyPointsByAllYear(String orgId, String memberId) {
        return partyMemberPointsMapper.getMyPointsByAllYear(orgId, memberId);
    }

    public Map<String, String> getMyPointsInfo(Account account) {
        String pointsYear = SysTools.getTime("yyyy");
        Map<String, String> tempMap = new HashMap<String, String>();
        Map<String, Object> map1 = getMyPointsByYear(account.getOrgId(), pointsYear, account.getMemberId());
        Map<String, Object> map2 = getMyPointsByAllYear(account.getOrgId(), account.getMemberId());
        tempMap.put("total", map1.get("total").toString());
        tempMap.put("totalAll", map2.get("total").toString());
        return tempMap;
    }


}
