package com.core136.service.partymember;

import com.core136.bean.partymember.PartyMemberActivists;
import com.core136.bean.partymember.PartyMemberJoin;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partymember.PartyMemberActivistsMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyMemberActivistsService {
    private PartyMemberActivistsMapper partyMemberActivistsMapper;

    @Autowired
    public void setPartyMemberActivistsMapper(PartyMemberActivistsMapper partyMemberActivistsMapper) {
        this.partyMemberActivistsMapper = partyMemberActivistsMapper;
    }

    private PartyMemberJoinService partyMemberJoinService;

    @Autowired
    public void setPartyMemberJoinService(PartyMemberJoinService partyMemberJoinService) {
        this.partyMemberJoinService = partyMemberJoinService;
    }

    public int insertPartyMemberActivists(PartyMemberActivists partyMemberActivists) {
        return partyMemberActivistsMapper.insert(partyMemberActivists);
    }

    public int deletePartyMemberActivists(PartyMemberActivists partyMemberActivists) {
        return partyMemberActivistsMapper.delete(partyMemberActivists);
    }

    public int updatePartyMemberActivists(Example example, PartyMemberActivists partyMemberActivists) {
        return partyMemberActivistsMapper.updateByExampleSelective(partyMemberActivists, example);
    }

    public PartyMemberActivists selectOnePartyMemberActivists(PartyMemberActivists partyMemberActivists) {
        return partyMemberActivistsMapper.selectOne(partyMemberActivists);
    }

    /**
     * @param partyMemberActivists
     * @return RetDataBean
     * @Title: setPartyMemberActivists
     * @Description:  设置状态为积极份子
     */
    @Transactional(value = "generalTM")
    public RetDataBean setPartyMemberActivists(PartyMemberActivists partyMemberActivists) {
        if (StringUtils.isBlank(partyMemberActivists.getJoinRecordId())) {
            return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
        } else {
            if (partyMemberActivists.getStatus().equals("1")) {
                PartyMemberJoin partyMemberJoin = new PartyMemberJoin();
                partyMemberJoin.setJoinTime(partyMemberActivists.getJoinTime());
                partyMemberJoin.setStatusFlag(1);
                partyMemberJoin.setTrainUser1(partyMemberActivists.getTrainUser1());
                partyMemberJoin.setTrainUser2(partyMemberActivists.getTrainUser2());
                Example example = new Example(PartyMemberJoin.class);
                example.createCriteria().andEqualTo("orgId", partyMemberActivists.getOrgId()).andEqualTo("recordId", partyMemberActivists.getJoinRecordId());
                partyMemberJoinService.updatePartyMemberJoin(example, partyMemberJoin);
                return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, insertPartyMemberActivists(partyMemberActivists));
            } else {
                PartyMemberJoin partyMemberJoin = new PartyMemberJoin();
                partyMemberJoin.setJoinTime(partyMemberActivists.getJoinTime());
                partyMemberJoin.setStatusFlag(100);
                Example example = new Example(PartyMemberJoin.class);
                example.createCriteria().andEqualTo("orgId", partyMemberActivists.getOrgId()).andEqualTo("recordId", partyMemberActivists.getJoinRecordId());
                partyMemberJoinService.updatePartyMemberJoin(example, partyMemberJoin);
                return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, insertPartyMemberActivists(partyMemberActivists));
            }
        }
    }

    /**
     * @param orgId
     * @param status
     * @param partyOrgId
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getActivistsRecordList
     * @Description:  获取积极份子审批记录
     */
    public List<Map<String, String>> getActivistsRecordList(String orgId, String status, String partyOrgId, String beginTime, String endTime, String search) {
        return partyMemberActivistsMapper.getActivistsRecordList(orgId, status, partyOrgId, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param status
     * @param partyOrgId
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getActivistsRecordList
     * @Description:  获取积极份子审批记录
     */
    public PageInfo<Map<String, String>> getActivistsRecordList(PageParam pageParam, String status, String partyOrgId, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getActivistsRecordList(pageParam.getOrgId(), status, partyOrgId, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
