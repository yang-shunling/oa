package com.core136.service.partymember;


import com.core136.bean.partymember.PartyMemberDeath;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partymember.PartyMemberDeathMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyMemberDeathService {

    private PartyMemberDeathMapper partyMemberDeathMapper;

    @Autowired
    public void setPartyMemberDeathMapper(PartyMemberDeathMapper partyMemberDeathMapper) {
        this.partyMemberDeathMapper = partyMemberDeathMapper;
    }

    public int insertPartyMemberDeath(PartyMemberDeath partyMemberDeath) {
        return partyMemberDeathMapper.insert(partyMemberDeath);
    }

    public int deletePartyMemberDeath(PartyMemberDeath partyMemberDeath) {
        return partyMemberDeathMapper.delete(partyMemberDeath);
    }

    public int updatePartyMemberDeath(Example example, PartyMemberDeath partyMemberDeath) {
        return partyMemberDeathMapper.updateByExampleSelective(partyMemberDeath, example);
    }

    public PartyMemberDeath selectOnePartyMemberDeath(PartyMemberDeath partyMemberDeath) {
        return partyMemberDeathMapper.selectOne(partyMemberDeath);
    }

    /**
     * @param orgId
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getMemberDeathList
     * @Description:  党员去世列表
     */
    public List<Map<String, String>> getMemberDeathList(String orgId, String beginTime, String endTime, String search) {
        return partyMemberDeathMapper.getMemberDeathList(orgId, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getMemberDeathList
     * @Description:  党员去世列表
     */
    public PageInfo<Map<String, String>> getMemberDeathList(PageParam pageParam, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMemberDeathList(pageParam.getOrgId(), beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }
}
