package com.core136.service.partymember;

import com.core136.bean.partymember.PartyLifeRecord;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partymember.PartyLifeRecordMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyLifeRecordService {

    private PartyLifeRecordMapper partyLifeRecordMapper;

    @Autowired
    public void setPartyLifeRecordMapper(PartyLifeRecordMapper partyLifeRecordMapper) {
        this.partyLifeRecordMapper = partyLifeRecordMapper;
    }

    public int insertPartyLifeRecord(PartyLifeRecord partyLifeRecord) {
        return partyLifeRecordMapper.insert(partyLifeRecord);
    }

    public int deletePartyLifeRecord(PartyLifeRecord partyLifeRecord) {
        return partyLifeRecordMapper.delete(partyLifeRecord);
    }

    public int updatePartyLifeRecord(Example example, PartyLifeRecord partyLifeRecord) {
        return partyLifeRecordMapper.updateByExampleSelective(partyLifeRecord, example);
    }

    public PartyLifeRecord selectOnePartyLifeRecord(PartyLifeRecord partyLifeRecord) {
        return partyLifeRecordMapper.selectOne(partyLifeRecord);
    }

    /**
     * @param orgId
     * @param memberId
     * @return List<Map < String, String>>
     * @Title: getMyPartyLifeList
     * @Description:  获取个人组织生活列表
     */
    public List<Map<String, String>> getMyPartyLifeList(String orgId, String memberId) {
        return partyLifeRecordMapper.getMyPartyLifeList(orgId, memberId);
    }

    /**
     * @param orgId
     * @param reason
     * @param lifeContent
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPartyLifeList
     * @Description:  获取组织生活列表
     */
    public List<Map<String, String>> getPartyLifeList(String orgId, String reason, String lifeContent, String beginTime, String endTime, String search) {
        return partyLifeRecordMapper.getPartyLifeList(orgId, reason, lifeContent, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param orgId
     * @param pageParam
     * @param reason
     * @param lifeContent
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getPartyLifeList
     * @Description:  获取组织生活列表
     */
    public PageInfo<Map<String, String>> getPartyLifeList(PageParam pageParam, String reason, String lifeContent, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getPartyLifeList(pageParam.getOrgId(), reason, lifeContent, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
