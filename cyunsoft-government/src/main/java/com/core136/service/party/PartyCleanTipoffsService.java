package com.core136.service.party;


import com.core136.bean.party.PartyCleanTipoffs;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.party.PartyCleanTipoffsMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyCleanTipoffsService {
    private PartyCleanTipoffsMapper partyCleanTipoffsMapper;

    @Autowired
    public void setPartyCleanTipoffsMapper(PartyCleanTipoffsMapper partyCleanTipoffsMapper) {
        this.partyCleanTipoffsMapper = partyCleanTipoffsMapper;
    }

    public int insertPartyCleanTipoffs(PartyCleanTipoffs partyCleanTipoffs) {
        return partyCleanTipoffsMapper.insert(partyCleanTipoffs);
    }

    public int deletePartyCleanTipoffs(PartyCleanTipoffs partyCleanTipoffs) {
        return partyCleanTipoffsMapper.delete(partyCleanTipoffs);
    }

    public int updatePartyCleanTipoffs(Example example, PartyCleanTipoffs partyCleanTipoffs) {
        return partyCleanTipoffsMapper.updateByExampleSelective(partyCleanTipoffs, example);
    }

    public PartyCleanTipoffs selectOnePartyCleanTipoffs(PartyCleanTipoffs partyCleanTipoffs) {
        return partyCleanTipoffsMapper.selectOne(partyCleanTipoffs);
    }

    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @param tipofficeType
     * @param status
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getCleanTipoffsList
     * @Description:  获取举报列表
     */
    public List<Map<String, String>> getCleanTipoffsList(String orgId, String opFlag, String accountId, String tipofficeType, String status, String beginTime, String endTime, String search) {
        return partyCleanTipoffsMapper.getCleanTipoffsList(orgId, opFlag, accountId, tipofficeType, status, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param tipofficeType
     * @param status
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getCleanTipoffsList
     * @Description:  获取举报列表
     */
    public PageInfo<Map<String, String>> getCleanTipoffsList(PageParam pageParam, String tipofficeType, String status, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getCleanTipoffsList(pageParam.getOrgId(), pageParam.getOpFlag(), pageParam.getAccountId(), tipofficeType, status, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getMyCleanTipoffsListForPortal
     * @Description:  获取门户举报列表
     */
    public List<Map<String, String>> getMyCleanTipoffsListForPortal(String orgId) {
        return partyCleanTipoffsMapper.getMyCleanTipoffsListForPortal(orgId);
    }

    /**
     * @param orgId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getMyCleanTipoffsList
     * @Description:  获取更多举报记录
     */
    public List<Map<String, String>> getMyCleanTipoffsList(String orgId, String search) {
        return partyCleanTipoffsMapper.getMyCleanTipoffsList(orgId, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getMyCleanTipoffsList
     * @Description:  获取更多举报记录
     */
    public PageInfo<Map<String, String>> getMyCleanTipoffsList(PageParam pageParam) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyCleanTipoffsList(pageParam.getOrgId(), pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }
}
