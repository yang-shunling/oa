package com.core136.service.party;

import com.core136.bean.party.PartyAlbumHome;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.mapper.party.PartyAlbumHomeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

@Service
public class PartyAlbumHomeService {

    private PartyAlbumHomeMapper partyAlbumHomeMapper;

    @Autowired
    public void setPartyAlbumHomeMapper(PartyAlbumHomeMapper partyAlbumHomeMapper) {
        this.partyAlbumHomeMapper = partyAlbumHomeMapper;
    }

    public int insertPartyAlbumHome(PartyAlbumHome partyAlbumHome) {
        return partyAlbumHomeMapper.insert(partyAlbumHome);
    }

    public int deletePartyAlbumHome(PartyAlbumHome partyAlbumHome) {
        return partyAlbumHomeMapper.delete(partyAlbumHome);
    }

    public int updatePartyAlbumHome(Example example, PartyAlbumHome partyAlbumHome) {
        return partyAlbumHomeMapper.updateByExampleSelective(partyAlbumHome, example);
    }

    public PartyAlbumHome selectOnePartyAlbumHome(PartyAlbumHome partyAlbumHome) {
        return partyAlbumHomeMapper.selectOne(partyAlbumHome);
    }

    /**
     * @param partyAlbumHome
     * @return RetDataBean
     * @Title: setAlbumHomeInfo
     * @Description:  设置首页面
     */
    public RetDataBean setAlbumHomeInfo(PartyAlbumHome partyAlbumHome) {
        PartyAlbumHome partyAlbumHomeTemp = new PartyAlbumHome();
        partyAlbumHomeTemp.setOrgId(partyAlbumHome.getOrgId());
        int count = partyAlbumHomeMapper.selectCount(partyAlbumHomeTemp);
        if (count == 0) {
            insertPartyAlbumHome(partyAlbumHome);
        } else {
            Example example = new Example(PartyAlbumHome.class);
            example.createCriteria().andEqualTo("orgId", partyAlbumHome.getOrgId());
            updatePartyAlbumHome(example, partyAlbumHome);
        }
        return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
    }
}
