package com.core136.service.party;

import com.core136.bean.party.PartyFunds;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.party.PartyFundsMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyFundsService {
    private PartyFundsMapper partyFundsMapper;

    @Autowired
    public void setPartyFundsMapper(PartyFundsMapper partyFundsMapper) {
        this.partyFundsMapper = partyFundsMapper;
    }

    public int insertPartyFunds(PartyFunds partyFunds) {
        return partyFundsMapper.insert(partyFunds);
    }

    public int deletePartyFunds(PartyFunds partyFunds) {
        return partyFundsMapper.delete(partyFunds);
    }

    public int updatePartyFunds(Example example, PartyFunds partyFunds) {
        return partyFundsMapper.updateByExampleSelective(partyFunds, example);
    }

    public PartyFunds selectOnePartyFunds(PartyFunds partyFunds) {
        return partyFundsMapper.selectOne(partyFunds);
    }

    /**
     * @param orgId
     * @param fundsType
     * @param applyUser
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getGovFundsList
     * @Description:  获取经费申请列表
     */
    public List<Map<String, String>> getGovFundsList(String orgId, String status, String fundsType, String approvalUser, String beginTime, String endTime, String search) {
        return partyFundsMapper.getGovFundsList(orgId, status, fundsType, approvalUser, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param fundsType
     * @param applyUser
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getGovFundsList
     * @Description:  获取经费申请列表
     */
    public PageInfo<Map<String, String>> getGovFundsList(PageParam pageParam, String status, String fundsType, String approvalUser, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getGovFundsList(pageParam.getOrgId(), status, fundsType, approvalUser, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param fundsType
     * @param applyUser
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getApprovalFundsList
     * @Description:  获取审批列表
     */
    public List<Map<String, String>> getApprovalFundsList(String orgId, String fundsType, String applyUser, String beginTime, String endTime, String search) {
        return partyFundsMapper.getApprovalFundsList(orgId, fundsType, applyUser, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param fundsType
     * @param applyUser
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getApprovalFundsList
     * @Description:  获取审批列表
     */
    public PageInfo<Map<String, String>> getApprovalFundsList(PageParam pageParam, String fundsType, String applyUser, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getApprovalFundsList(pageParam.getOrgId(), fundsType, applyUser, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    public List<Map<String, String>> getApprovalFundsOldList(String orgId, String status, String fundsType, String applyUser, String beginTime, String endTime, String search) {
        return partyFundsMapper.getApprovalFundsOldList(orgId, status, fundsType, applyUser, beginTime, endTime, "%" + search + "%");
    }

    public PageInfo<Map<String, String>> getApprovalFundsOldList(PageParam pageParam, String status, String fundsType, String applyUser, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getApprovalFundsOldList(pageParam.getOrgId(), status, fundsType, applyUser, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }


}
