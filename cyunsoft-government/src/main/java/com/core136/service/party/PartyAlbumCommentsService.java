package com.core136.service.party;

import com.core136.bean.party.PartyAlbumComments;
import com.core136.mapper.party.PartyAlbumCommentsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyAlbumCommentsService {


    private PartyAlbumCommentsMapper partyAlbumCommentsMapper;

    @Autowired
    public void setPartyAlbumCommentsMapper(PartyAlbumCommentsMapper partyAlbumCommentsMapper) {
        this.partyAlbumCommentsMapper = partyAlbumCommentsMapper;
    }

    public int insertPartyAlbumComments(PartyAlbumComments partyAlbumComments) {
        return partyAlbumCommentsMapper.insert(partyAlbumComments);
    }

    public int deletePartyAlbumComments(PartyAlbumComments partyAlbumComments) {
        return partyAlbumCommentsMapper.delete(partyAlbumComments);
    }

    public int updatePartyAlbumComments(Example example, PartyAlbumComments partyAlbumComments) {
        return partyAlbumCommentsMapper.updateByExampleSelective(partyAlbumComments, example);
    }

    public PartyAlbumComments selectOnePartyAlbumComments(PartyAlbumComments partyAlbumComments) {
        return partyAlbumCommentsMapper.selectOne(partyAlbumComments);
    }

    /**
     * @param orgId
     * @param videoId
     * @return List<Map < String, String>>
     * @Title: getAlbumCommentsList
     * @Description:  获取专辑评论
     */
    public List<Map<String, String>> getAlbumCommentsList(String orgId, String videoId) {
        return partyAlbumCommentsMapper.getAlbumCommentsList(orgId, videoId);
    }
}

