package com.core136.service.party;

import com.core136.bean.account.Account;
import com.core136.bi.option.bean.OptionConfig;
import com.core136.bi.option.property.*;
import com.core136.bi.option.resdata.LegendData;
import com.core136.bi.option.resdata.SeriesData;
import com.core136.bi.option.style.AxisPointer;
import com.core136.bi.option.style.AxisTick;
import com.core136.bi.option.style.Emphasis;
import com.core136.bi.option.style.ItemStyle;
import com.core136.bi.option.units.BarOption;
import com.core136.bi.option.units.LineOption;
import com.core136.bi.option.units.PieOption;
import com.core136.common.utils.SysTools;
import com.core136.mapper.party.EchartsPartyMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class EchartsPartyService {
    private EchartsPartyMapper echartsPartyMapper;

    @Autowired
    public void setEchartsPartyMapper(EchartsPartyMapper echartsPartyMapper) {
        this.echartsPartyMapper = echartsPartyMapper;
    }

    private PieOption pieOption = new PieOption();
    private LineOption lineOption = new LineOption();
    private BarOption barOption = new BarOption();

    /**
     * 按类型统计三会一课
     *
     * @param account
     * @param seriesDataName
     * @return
     */
    public OptionConfig getPartyLesMeetForBar(Account account, String seriesDataName) {
        List<Map<String, String>> resdataList = getPartyLesMeetForBar(account.getOrgId());
        OptionConfig optionConfig = new OptionConfig();
        String[] xdata = new String[resdataList.size()];
        Double[] ydata = new Double[resdataList.size()];
        for (int i = 0; i < resdataList.size(); i++) {
            if (StringUtils.isNotBlank(resdataList.get(i).get("name"))) {
                xdata[i] = resdataList.get(i).get("name");
            } else {
                xdata[i] = "other" + i;
            }
            ydata[i] = Double.valueOf(String.valueOf(resdataList.get(i).get("value")));
        }
        OptionSeries optionSeries = new OptionSeries();
        optionSeries.setName(seriesDataName);
        optionSeries.setType("bar");
        optionSeries.setBarWidth("60%");
        optionSeries.setData(ydata);
        OptionXAxis xAxis = new OptionXAxis();
        xAxis.setType("category");
        xAxis.setData(xdata);
        AxisTick axisTick = new AxisTick();
        axisTick.setAlignWithLabel(true);
        xAxis.setAxisTick(axisTick);
        optionConfig = barOption.getBarTickAlignChartOption(new OptionXAxis[]{xAxis}, new OptionSeries[]{optionSeries});
        OptionTooltip optionTooltip = new OptionTooltip();
        optionTooltip.setTrigger("axis");
        AxisPointer axisPointer = new AxisPointer();
        axisPointer.setType("shadow");
        optionTooltip.setAxisPointer(axisPointer);
        optionConfig.setTooltip(optionTooltip);
        OptionGrid optionGrid = new OptionGrid();
        optionGrid.setLeft("3%");
        optionGrid.setRight("4%");
        optionGrid.setBottom("3%");
        optionGrid.setContainLabel(true);
        optionConfig.setGrid(optionGrid);
        return optionConfig;
    }

    /**
     * 按类型统计三会一课
     *
     * @param orgId
     * @return
     */
    public List<Map<String, String>> getPartyLesMeetForBar(String orgId) {
        return echartsPartyMapper.getPartyLesMeetForBar(orgId);
    }

    /**
     * @param account
     * @return OptionConfig
     * @Title: getPartyJoinByMonthLine
     * @Description:  近一年内党员发展情况
     */
    public OptionConfig getPartyJoinByMonthLine(Account account) {
        OptionConfig optionConfig = new OptionConfig();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM");
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        Date y = c.getTime();
        String endTime = format.format(y);
        c.add(Calendar.YEAR, -1);
        y = c.getTime();
        String beginTime = format.format(y);
        List<Map<String, Object>> resList = getPartyJoinByMonthLine(account.getOrgId(), beginTime, endTime);
        String[] xAxisData = new String[resList.size()];
        Double[] resData = new Double[resList.size()];
        for (int i = 0; i < resList.size(); i++) {
            xAxisData[i] = resList.get(i).get("createTime").toString();
            resData[i] = Double.valueOf(resList.get(i).get("total").toString());
        }
        optionConfig = lineOption.getBasicLineChartOption(xAxisData, resData);
        return optionConfig;
    }

    /**
     * @param orgId
     * @param beginTime
     * @param endTime
     * @return List<Map < String, Object>>
     * @Title: getPartyJoinByMonthLine
     * @Description:  党员发展情况
     */
    public List<Map<String, Object>> getPartyJoinByMonthLine(String orgId, String beginTime, String endTime) {
        return echartsPartyMapper.getPartyJoinByMonthLine(orgId, beginTime, endTime);
    }

    /**
     * @param account
     * @return OptionConfig
     * @Title: getMyPointsByMonthLine
     * @Description:   获取个人一年内积分获取情况
     */
    public OptionConfig getMyPointsByMonthLine(Account account) {
        OptionConfig optionConfig = new OptionConfig();
        String year = SysTools.getTime("yyyy");
        List<Map<String, Object>> resList = getMyPointsByMonthLine(account.getOrgId(), year, account.getMemberId());
        String[] xAxisData = new String[resList.size()];
        Double[] resData = new Double[resList.size()];
        for (int i = 0; i < resList.size(); i++) {
            xAxisData[i] = resList.get(i).get("createTime").toString();
            resData[i] = Double.valueOf(resList.get(i).get("total").toString());
        }
        optionConfig = lineOption.getBasicLineChartOption(xAxisData, resData);
        return optionConfig;
    }


    /**
     * @param orgId
     * @param year
     * @return List<Map < String, Object>>
     * @Title: getMyPointsByMonthLine
     * @Description:  获取个人一年内积分获取情况
     */
    public List<Map<String, Object>> getMyPointsByMonthLine(String orgId, String year, String memberId) {
        return echartsPartyMapper.getMyPointsByMonthLine(orgId, year, memberId);
    }

    /**
     * @param account
     * @return OptionConfig
     * @Title: getPartyMemberJoinToPartyOrgPie
     * @Description:  获取各党组织党员发展情况
     */
    public OptionConfig getPartyMemberJoinToPartyOrgPie(Account account) {
        OptionConfig optionConfig = new OptionConfig();
        List<Map<String, String>> resdataList = getPartyMemberJoinToPartyOrgPie(account.getOrgId());
        OptionSeries[] optionSeriesArr = new OptionSeries[1];
        SeriesData[] dataArr = new SeriesData[resdataList.size()];
        int selectedLeng = 0;
        if (dataArr.length >= 10) {
            selectedLeng = 10;
        } else {
            selectedLeng = dataArr.length;
        }
        String[] selected = new String[selectedLeng];
        LegendData[] legendDatas = new LegendData[dataArr.length];
        for (int i = 0; i < dataArr.length; i++) {
            if (StringUtils.isBlank(resdataList.get(i).get("name"))) {
                resdataList.get(i).put("name", "other" + i);
            }
            if (i < selectedLeng) {
                selected[i] = resdataList.get(i).get("name").toString();
            }
            LegendData legendData = new LegendData();
            legendData.setName(resdataList.get(i).get("name").toString());
            legendDatas[i] = legendData;
            SeriesData seriesData = new SeriesData();
            seriesData.setName(resdataList.get(i).get("name").toString());
            seriesData.setValue(Double.valueOf(String.valueOf(resdataList.get(i).get("value"))));
            dataArr[i] = seriesData;
        }
        OptionSeries optionSeries = new OptionSeries();
        optionSeries.setName("各党组织");
        optionSeries.setType("pie");
        optionSeries.setRadius("55%");
        optionSeries.setCenter(new String[]{"40%", "50%"});
        Emphasis emphasis = new Emphasis();
        ItemStyle itemStyle = new ItemStyle();
        itemStyle.setShadowBlur(10);
        itemStyle.setShadowOffsetX(0);
        itemStyle.setShadowColor("rgba(0, 0, 0, 0.5)");
        emphasis.setItemStyle(itemStyle);
        optionSeries.setData(dataArr);
        optionSeriesArr[0] = optionSeries;
        optionConfig.setSeries(optionSeriesArr);
        optionConfig = pieOption.getPieLegendChartOption(legendDatas, selected, optionSeriesArr);
        OptionTitle optionTitle = new OptionTitle();
        optionTitle.setText("入党人员统计");
        optionTitle.setSubtext("入党人员占比");
        optionTitle.setLeft("center");
        optionConfig.setTitle(optionTitle);
        return optionConfig;
    }

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getPartyMemberJoinToPartyOrgPie
     * @Description:  获取各党组织党员发展情况
     */
    public List<Map<String, String>> getPartyMemberJoinToPartyOrgPie(String orgId) {
        return echartsPartyMapper.getPartyMemberJoinToPartyOrgPie(orgId);
    }


    /**
     * @param account
     * @param seriesDataName
     * @return OptionConfig
     * @Title: getBarPartyMemberJoinByStatus
     * @Description:  党员发展各阶段对比
     */
    public OptionConfig getBarPartyMemberJoinByStatus(Account account, String seriesDataName) {
        List<Map<String, String>> resdataList = getBarPartyMemberJoinByStatus(account.getOrgId());
        OptionConfig optionConfig = new OptionConfig();
        String[] xdata = new String[resdataList.size()];
        Double[] ydata = new Double[resdataList.size()];
        for (int i = 0; i < resdataList.size(); i++) {
            if (StringUtils.isNotBlank(resdataList.get(i).get("name"))) {
                xdata[i] = resdataList.get(i).get("name");
            } else {
                xdata[i] = "other" + i;
            }
            ydata[i] = Double.valueOf(String.valueOf(resdataList.get(i).get("value")));
        }
        OptionSeries optionSeries = new OptionSeries();
        optionSeries.setName(seriesDataName);
        optionSeries.setType("bar");
        optionSeries.setBarWidth("60%");
        optionSeries.setData(ydata);
        OptionXAxis xAxis = new OptionXAxis();
        xAxis.setType("category");
        xAxis.setData(xdata);
        AxisTick axisTick = new AxisTick();
        axisTick.setAlignWithLabel(true);
        xAxis.setAxisTick(axisTick);
        optionConfig = barOption.getBarTickAlignChartOption(new OptionXAxis[]{xAxis}, new OptionSeries[]{optionSeries});
        OptionTooltip optionTooltip = new OptionTooltip();
        optionTooltip.setTrigger("axis");
        AxisPointer axisPointer = new AxisPointer();
        axisPointer.setType("shadow");
        optionTooltip.setAxisPointer(axisPointer);
        optionConfig.setTooltip(optionTooltip);
        OptionGrid optionGrid = new OptionGrid();
        optionGrid.setLeft("3%");
        optionGrid.setRight("4%");
        optionGrid.setBottom("3%");
        optionGrid.setContainLabel(true);
        optionConfig.setGrid(optionGrid);
        return optionConfig;
    }

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getBarPartyMemberJoinByStatus
     * @Description:  党员发展各阶段对比
     */
    public List<Map<String, String>> getBarPartyMemberJoinByStatus(String orgId) {
        return echartsPartyMapper.getBarPartyMemberJoinByStatus(orgId);
    }


    /**
     * @param account
     * @return OptionConfig
     * @Title: getPartyJoinByYearLine
     * @Description:  获取过去10年内的党员入党情况
     */
    public OptionConfig getPartyJoinByYearLine(Account account) {
        OptionConfig optionConfig = new OptionConfig();
        List<Map<String, Object>> resList = getPartyJoinByYearLine(account.getOrgId());
        String[] xAxisData = new String[resList.size()];
        Double[] resData = new Double[resList.size()];
        for (int i = 0; i < resList.size(); i++) {
            xAxisData[i] = resList.get(i).get("createTime").toString();
            resData[i] = Double.valueOf(resList.get(i).get("total").toString());
        }
        optionConfig = lineOption.getBasicLineChartOption(xAxisData, resData);
        return optionConfig;
    }

    /**
     * @param orgId
     * @return List<Map < String, Object>>
     * @Title: getPartyJoinByYearLine
     * @Description:  获取过去10年内的党员入党情况
     */
    public List<Map<String, Object>> getPartyJoinByYearLine(String orgId) {
        return echartsPartyMapper.getPartyJoinByYearLine(orgId);
    }


    /**
     * @param account
     * @param seriesDataName
     * @return OptionConfig
     * @Title: getBiPartyFundsAnalysis
     * @Description:  按党组织对比经费使用情况
     */
    public OptionConfig getBiPartyFundsAnalysis(Account account, String seriesDataName) {
        List<Map<String, String>> resdataList = getBiPartyFundsAnalysis(account.getOrgId());
        OptionConfig optionConfig = new OptionConfig();
        String[] xdata = new String[resdataList.size()];
        Double[] ydata = new Double[resdataList.size()];
        for (int i = 0; i < resdataList.size(); i++) {
            if (StringUtils.isNotBlank(resdataList.get(i).get("name"))) {
                xdata[i] = resdataList.get(i).get("name");
            } else {
                xdata[i] = "other" + i;
            }
            ydata[i] = Double.valueOf(String.valueOf(resdataList.get(i).get("value")));
        }
        OptionSeries optionSeries = new OptionSeries();
        optionSeries.setName(seriesDataName);
        optionSeries.setType("bar");
        optionSeries.setBarWidth("60%");
        optionSeries.setData(ydata);
        OptionXAxis xAxis = new OptionXAxis();
        xAxis.setType("category");
        xAxis.setData(xdata);
        AxisTick axisTick = new AxisTick();
        axisTick.setAlignWithLabel(true);
        xAxis.setAxisTick(axisTick);
        optionConfig = barOption.getBarTickAlignChartOption(new OptionXAxis[]{xAxis}, new OptionSeries[]{optionSeries});
        OptionTooltip optionTooltip = new OptionTooltip();
        optionTooltip.setTrigger("axis");
        AxisPointer axisPointer = new AxisPointer();
        axisPointer.setType("shadow");
        optionTooltip.setAxisPointer(axisPointer);
        optionConfig.setTooltip(optionTooltip);
        OptionGrid optionGrid = new OptionGrid();
        optionGrid.setLeft("3%");
        optionGrid.setRight("4%");
        optionGrid.setBottom("3%");
        optionGrid.setContainLabel(true);
        optionConfig.setGrid(optionGrid);
        return optionConfig;
    }


    /**
     * @param orgId
     * @return List<Map < String, Object>>
     * @Title: getBiPartyFundsAnalysis
     * @Description:  按党组织对比经费使用情况
     */
    public List<Map<String, String>> getBiPartyFundsAnalysis(String orgId) {
        String nowTime = SysTools.getCalculationYear(-1);
        return echartsPartyMapper.getBiPartyFundsAnalysis(orgId, nowTime);
    }


    /**
     * @param account
     * @return OptionConfig
     * @Title: getPartyFundsPie
     * @Description:  按经费类型饼状图
     */
    public OptionConfig getPartyFundsPie(Account account) {
        OptionConfig optionConfig = new OptionConfig();
        List<Map<String, String>> resdataList = getPartyFundsPie(account.getOrgId());
        OptionSeries[] optionSeriesArr = new OptionSeries[1];
        SeriesData[] dataArr = new SeriesData[resdataList.size()];
        int selectedLeng = 0;
        if (dataArr.length >= 10) {
            selectedLeng = 10;
        } else {
            selectedLeng = dataArr.length;
        }
        String[] selected = new String[selectedLeng];
        LegendData[] legendDatas = new LegendData[dataArr.length];
        for (int i = 0; i < dataArr.length; i++) {
            if (StringUtils.isBlank(resdataList.get(i).get("name"))) {
                resdataList.get(i).put("name", "other" + i);
            }
            if (i < selectedLeng) {
                selected[i] = resdataList.get(i).get("name").toString();
            }
            LegendData legendData = new LegendData();
            legendData.setName(resdataList.get(i).get("name").toString());
            legendDatas[i] = legendData;
            SeriesData seriesData = new SeriesData();
            seriesData.setName(resdataList.get(i).get("name").toString());
            seriesData.setValue(Double.valueOf(String.valueOf(resdataList.get(i).get("value"))));
            dataArr[i] = seriesData;
        }
        OptionSeries optionSeries = new OptionSeries();
        optionSeries.setName("经费分类");
        optionSeries.setType("pie");
        optionSeries.setRadius("55%");
        optionSeries.setCenter(new String[]{"40%", "50%"});
        Emphasis emphasis = new Emphasis();
        ItemStyle itemStyle = new ItemStyle();
        itemStyle.setShadowBlur(10);
        itemStyle.setShadowOffsetX(0);
        itemStyle.setShadowColor("rgba(0, 0, 0, 0.5)");
        emphasis.setItemStyle(itemStyle);
        optionSeries.setData(dataArr);
        optionSeriesArr[0] = optionSeries;
        optionConfig.setSeries(optionSeriesArr);
        optionConfig = pieOption.getPieLegendChartOption(legendDatas, selected, optionSeriesArr);
        OptionTitle optionTitle = new OptionTitle();
        optionTitle.setText("经费总数统计");
        optionTitle.setSubtext("经费总数占比");
        optionTitle.setLeft("center");
        optionConfig.setTitle(optionTitle);
        return optionConfig;
    }

    public List<Map<String, String>> getPartyFundsPie(String orgId) {
        return echartsPartyMapper.getPartyFundsPie(orgId);
    }


    /**
     * @param account
     * @return OptionConfig
     * @Title: getBiPartyFundsByMonthLine
     * @Description:  按月份汇总经费使用情况
     */
    public OptionConfig getBiPartyFundsByMonthLine(Account account) {
        OptionConfig optionConfig = new OptionConfig();
        List<Map<String, Object>> resList = getBiPartyFundsByMonthLine(account.getOrgId());
        String[] xAxisData = new String[resList.size()];
        Double[] resData = new Double[resList.size()];
        for (int i = 0; i < resList.size(); i++) {
            xAxisData[i] = resList.get(i).get("createTime").toString();
            resData[i] = Double.valueOf(resList.get(i).get("total").toString());
        }
        optionConfig = lineOption.getBasicLineChartOption(xAxisData, resData);
        return optionConfig;
    }

    /**
     * @param orgId
     * @return List<Map < String, Object>>
     * @Title: getBiPartyFundsByMonthLine
     * @Description:  按月份汇总经费使用情况
     */
    public List<Map<String, Object>> getBiPartyFundsByMonthLine(String orgId) {
        String nowTime = SysTools.getCalculationYear(-1);
        return echartsPartyMapper.getBiPartyFundsByMonthLine(orgId, nowTime);
    }


    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getPartyMemberByAgeCountList
     * @Description:  按年龄段获取党员数量
     */
    public List<Map<String, Object>> getPartyMemberByAgeCountList(String orgId) {
        List<Map<String, Object>> returnList = new ArrayList<Map<String, Object>>();
        String year1 = SysTools.getCalculationYear(-20);
        String year2 = SysTools.getCalculationYear(-30);
        String year3 = SysTools.getCalculationYear(-40);
        String year4 = SysTools.getCalculationYear(-50);
        String year5 = SysTools.getCalculationYear(-60);
        String year6 = SysTools.getCalculationYear(-70);
        String year7 = SysTools.getCalculationYear(-80);
        Map<String, Integer> map = echartsPartyMapper.getPartyMemberByAgeCountList(orgId, year1, year2, year3, year4, year5, year6, year7);
        for (String key : map.keySet()) {
            Map<String, Object> tempMap = new HashMap<String, Object>();
            if (key.equals("zs2030")) {
                tempMap.put("name", "20~30岁");

            } else if (key.equals("zs3040")) {
                tempMap.put("name", "30~40岁");
            } else if (key.equals("zs4050")) {
                tempMap.put("name", "40~50岁");
            } else if (key.equals("zs5060")) {
                tempMap.put("name", "50~60岁");
            } else if (key.equals("zs6070")) {
                tempMap.put("name", "60~70岁");
            } else if (key.equals("zs7080")) {
                tempMap.put("name", "70~80岁");
            } else if (key.equals("zs80100")) {
                tempMap.put("name", "80岁以上");
            }
            tempMap.put("value", map.get(key));
            returnList.add(tempMap);
        }
        return returnList;

    }


    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getPartyBigEventList
     * @Description:  重大事件前10条记录
     */
    public List<Map<String, String>> getPartyBigEventList(String orgId) {
        return echartsPartyMapper.getPartyBigEventList(orgId);
    }

    /**
     * @param account
     * @return OptionConfig
     * @Title: getBiPartyDeusByYearLine
     * @Description:  按年份汇总收缴的党费
     */
    public OptionConfig getBiPartyDeusByYearLine(Account account) {
        OptionConfig optionConfig = new OptionConfig();
        List<Map<String, Object>> resList = getBiPartyDeusByYearLine(account.getOrgId());
        String[] xAxisData = new String[resList.size()];
        Double[] resData = new Double[resList.size()];
        for (int i = 0; i < resList.size(); i++) {
            xAxisData[i] = resList.get(i).get("createTime").toString();
            resData[i] = Double.valueOf(resList.get(i).get("total").toString());
        }
        optionConfig = lineOption.getBasicLineChartOption(xAxisData, resData);
        return optionConfig;
    }

    public List<Map<String, Object>> getBiPartyDeusByYearLine(String orgId) {
        return echartsPartyMapper.getBiPartyDeusByYearLine(orgId);
    }

    /**
     * @param account
     * @return OptionConfig
     * @Title: getPartyRulesLearnByMonthLine
     * @Description:  按月份学习趋势分析
     */
    public OptionConfig getPartyRulesLearnByMonthLine(Account account) {
        OptionConfig optionConfig = new OptionConfig();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM");
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        Date y = c.getTime();
        String endTime = format.format(y);
        c.add(Calendar.YEAR, -1);
        y = c.getTime();
        String beginTime = format.format(y);
        List<Map<String, Object>> resList = getPartyRulesLearnByMonthLine(account.getOrgId(), beginTime, endTime);
        String[] xAxisData = new String[resList.size()];
        Double[] resData = new Double[resList.size()];
        for (int i = 0; i < resList.size(); i++) {
            xAxisData[i] = resList.get(i).get("createTime").toString();
            resData[i] = Double.valueOf(resList.get(i).get("total").toString());
        }
        optionConfig = lineOption.getBasicLineChartOption(xAxisData, resData);
        return optionConfig;
    }

    public List<Map<String, Object>> getPartyRulesLearnByMonthLine(String orgId, String beginTime, String endTime) {
        return echartsPartyMapper.getPartyRulesLearnByMonthLine(orgId, beginTime, endTime);
    }

    /**
     * 预备党员转正
     *
     * @param account
     * @return
     */
    public OptionConfig getPartyLesMeetByMonthLine(Account account) {
        OptionConfig optionConfig = new OptionConfig();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM");
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        Date y = c.getTime();
        String endTime = format.format(y);
        c.add(Calendar.YEAR, -1);
        y = c.getTime();
        String beginTime = format.format(y);
        List<Map<String, Object>> resList = getPartyLesMeetByMonthLine(account.getOrgId(), beginTime, endTime);
        String[] xAxisData = new String[resList.size()];
        Double[] resData = new Double[resList.size()];
        for (int i = 0; i < resList.size(); i++) {
            xAxisData[i] = resList.get(i).get("regularTime").toString();
            resData[i] = Double.valueOf(resList.get(i).get("total").toString());
        }
        optionConfig = lineOption.getBasicLineChartOption(xAxisData, resData);
        return optionConfig;
    }

    /**
     * 预备党员转正
     *
     * @param orgId
     * @param beginTime
     * @param endTime
     * @return
     */
    public List<Map<String, Object>> getPartyLesMeetByMonthLine(String orgId, String beginTime, String endTime) {
        return echartsPartyMapper.getPartyLesMeetByMonthLine(orgId, beginTime, endTime);
    }

    /**
     * @param account
     * @return OptionConfig
     * @Title: getPartyRulesLearnByAccountPie
     * @Description:  前10位人员学习次数占比
     */
    public OptionConfig getPartyRulesLearnByAccountPie(Account account) {
        OptionConfig optionConfig = new OptionConfig();
        List<Map<String, String>> resdataList = getPartyRulesLearnByAccountPie(account.getOrgId());
        OptionSeries[] optionSeriesArr = new OptionSeries[1];
        SeriesData[] dataArr = new SeriesData[resdataList.size()];
        int selectedLeng = 0;
        if (dataArr.length >= 10) {
            selectedLeng = 10;
        } else {
            selectedLeng = dataArr.length;
        }
        String[] selected = new String[selectedLeng];
        LegendData[] legendDatas = new LegendData[dataArr.length];
        for (int i = 0; i < dataArr.length; i++) {
            if (StringUtils.isBlank(resdataList.get(i).get("name"))) {
                resdataList.get(i).put("name", "other" + i);
            }
            if (i < selectedLeng) {
                selected[i] = resdataList.get(i).get("name").toString();
            }
            LegendData legendData = new LegendData();
            legendData.setName(resdataList.get(i).get("name").toString());
            legendDatas[i] = legendData;
            SeriesData seriesData = new SeriesData();
            seriesData.setName(resdataList.get(i).get("name").toString());
            seriesData.setValue(Double.valueOf(String.valueOf(resdataList.get(i).get("value"))));
            dataArr[i] = seriesData;
        }
        OptionSeries optionSeries = new OptionSeries();
        optionSeries.setName("人员");
        optionSeries.setType("pie");
        optionSeries.setRadius("55%");
        optionSeries.setCenter(new String[]{"40%", "50%"});
        Emphasis emphasis = new Emphasis();
        ItemStyle itemStyle = new ItemStyle();
        itemStyle.setShadowBlur(10);
        itemStyle.setShadowOffsetX(0);
        itemStyle.setShadowColor("rgba(0, 0, 0, 0.5)");
        emphasis.setItemStyle(itemStyle);
        optionSeries.setData(dataArr);
        optionSeriesArr[0] = optionSeries;
        optionConfig.setSeries(optionSeriesArr);
        optionConfig = pieOption.getPieLegendChartOption(legendDatas, selected, optionSeriesArr);
        OptionTitle optionTitle = new OptionTitle();
        optionTitle.setText("人员学习频率统计");
        optionTitle.setSubtext("人员学习次数占比");
        optionTitle.setLeft("center");
        optionConfig.setTitle(optionTitle);
        return optionConfig;
    }

    public List<Map<String, String>> getPartyRulesLearnByAccountPie(String orgId) {
        return echartsPartyMapper.getPartyRulesLearnByAccountPie(orgId);
    }

    /**
     * @param account
     * @return OptionConfig
     * @Title: getPartyRulesLearnByDeptPie
     * @Description:  按部门学习对比前10的占比
     */
    public OptionConfig getPartyRulesLearnByDeptPie(Account account) {
        OptionConfig optionConfig = new OptionConfig();
        List<Map<String, String>> resdataList = getPartyRulesLearnByDeptPie(account.getOrgId());
        OptionSeries[] optionSeriesArr = new OptionSeries[1];
        SeriesData[] dataArr = new SeriesData[resdataList.size()];
        int selectedLeng = 0;
        if (dataArr.length >= 10) {
            selectedLeng = 10;
        } else {
            selectedLeng = dataArr.length;
        }
        String[] legendDatas = new String[dataArr.length];
        for (int i = 0; i < dataArr.length; i++) {
            if (StringUtils.isBlank(resdataList.get(i).get("name"))) {
                legendDatas[i] = "other" + i;
            }
            if (i < selectedLeng) {
                legendDatas[i] = resdataList.get(i).get("name").toString();
            }
            SeriesData seriesData = new SeriesData();
            seriesData.setName(resdataList.get(i).get("name").toString());
            seriesData.setValue(Double.valueOf(String.valueOf(resdataList.get(i).get("value"))));
            dataArr[i] = seriesData;
        }
        OptionSeries optionSeries = new OptionSeries();
        optionSeries.setType("bar");
        optionSeries.setData(dataArr);
        optionSeriesArr[0] = optionSeries;
        optionConfig = barOption.getBarSimpleChartOption(legendDatas, optionSeriesArr);

        OptionTitle optionTitle = new OptionTitle();
        optionTitle.setText("部门学习次数统计");
        optionTitle.setSubtext("部门学习次数对比");
        optionTitle.setLeft("center");
        optionConfig.setTitle(optionTitle);
        return optionConfig;
    }

    public List<Map<String, String>> getPartyRulesLearnByDeptPie(String orgId) {
        return echartsPartyMapper.getPartyRulesLearnByDeptPie(orgId);
    }

    /**
     * @param account
     * @return OptionConfig
     * @Title: getPartyRulesLearnPie
     * @Description:  获取规章制度分类前10的占比
     */
    public OptionConfig getPartyRulesLearnPie(Account account) {
        OptionConfig optionConfig = new OptionConfig();
        List<Map<String, String>> resdataList = getPartyRulesLearnPie(account.getOrgId());
        OptionSeries[] optionSeriesArr = new OptionSeries[1];
        SeriesData[] dataArr = new SeriesData[resdataList.size()];
        int selectedLeng = 0;
        if (dataArr.length >= 10) {
            selectedLeng = 10;
        } else {
            selectedLeng = dataArr.length;
        }
        String[] selected = new String[selectedLeng];
        LegendData[] legendDatas = new LegendData[dataArr.length];
        for (int i = 0; i < dataArr.length; i++) {
            if (StringUtils.isBlank(resdataList.get(i).get("name"))) {
                resdataList.get(i).put("name", "other" + i);
            }
            if (i < selectedLeng) {
                selected[i] = resdataList.get(i).get("name").toString();
            }
            LegendData legendData = new LegendData();
            legendData.setName(resdataList.get(i).get("name").toString());
            legendDatas[i] = legendData;
            SeriesData seriesData = new SeriesData();
            seriesData.setName(resdataList.get(i).get("name").toString());
            seriesData.setValue(Double.valueOf(String.valueOf(resdataList.get(i).get("value"))));
            dataArr[i] = seriesData;
        }
        OptionSeries optionSeries = new OptionSeries();
        optionSeries.setName("规章制度分类");
        optionSeries.setType("pie");
        optionSeries.setRadius("55%");
        optionSeries.setCenter(new String[]{"40%", "50%"});
        Emphasis emphasis = new Emphasis();
        ItemStyle itemStyle = new ItemStyle();
        itemStyle.setShadowBlur(10);
        itemStyle.setShadowOffsetX(0);
        itemStyle.setShadowColor("rgba(0, 0, 0, 0.5)");
        emphasis.setItemStyle(itemStyle);
        optionSeries.setData(dataArr);
        optionSeriesArr[0] = optionSeries;
        optionConfig.setSeries(optionSeriesArr);
        optionConfig = pieOption.getPieLegendChartOption(legendDatas, selected, optionSeriesArr);
        OptionTitle optionTitle = new OptionTitle();
        optionTitle.setText("规章制度分类总数统计");
        optionTitle.setSubtext("分类总数占比");
        optionTitle.setLeft("center");
        optionConfig.setTitle(optionTitle);
        return optionConfig;
    }

    public List<Map<String, String>> getPartyRulesLearnPie(String orgId) {
        return echartsPartyMapper.getPartyRulesLearnPie(orgId);
    }

    /**
     * @param account
     * @return OptionConfig
     * @Title: getBiJoinPartyByYearLine
     * @Description:  按年份分析入党趋势
     */
    public OptionConfig getBiJoinPartyByYearLine(Account account) {
        OptionConfig optionConfig = new OptionConfig();
        List<Map<String, Object>> resList = getBiJoinPartyByYearLine(account.getOrgId());
        String[] xAxisData = new String[resList.size()];
        Double[] resData = new Double[resList.size()];
        for (int i = 0; i < resList.size(); i++) {
            xAxisData[i] = resList.get(i).get("createTime").toString();
            resData[i] = Double.valueOf(resList.get(i).get("total").toString());
        }
        optionConfig = lineOption.getBasicLineChartOption(xAxisData, resData);
        return optionConfig;
    }

    public List<Map<String, Object>> getBiJoinPartyByYearLine(String orgId) {
        return echartsPartyMapper.getBiJoinPartyByYearLine(orgId);
    }

    /**
     * @param account
     * @return OptionConfig
     * @Title: getBiMemberBySexPie
     * @Description:  党员性别统计
     */
    public OptionConfig getBiMemberBySexPie(Account account) {
        OptionConfig optionConfig = new OptionConfig();
        List<Map<String, String>> resdataList = getBiMemberBySexPie(account.getOrgId());
        OptionSeries[] optionSeriesArr = new OptionSeries[1];
        SeriesData[] dataArr = new SeriesData[resdataList.size()];
        int selectedLeng = 0;
        if (dataArr.length >= 10) {
            selectedLeng = 10;
        } else {
            selectedLeng = dataArr.length;
        }
        String[] selected = new String[selectedLeng];
        LegendData[] legendDatas = new LegendData[dataArr.length];
        for (int i = 0; i < dataArr.length; i++) {
            if (StringUtils.isBlank(resdataList.get(i).get("name"))) {
                resdataList.get(i).put("name", "other" + i);
            }
            if (i < selectedLeng) {
                selected[i] = resdataList.get(i).get("name").toString();
            }
            LegendData legendData = new LegendData();
            legendData.setName(resdataList.get(i).get("name").toString());
            legendDatas[i] = legendData;
            SeriesData seriesData = new SeriesData();
            seriesData.setName(resdataList.get(i).get("name").toString());
            seriesData.setValue(Double.valueOf(String.valueOf(resdataList.get(i).get("value"))));
            dataArr[i] = seriesData;
        }
        OptionSeries optionSeries = new OptionSeries();
        optionSeries.setName("性别");
        optionSeries.setType("pie");
        optionSeries.setRadius("55%");
        optionSeries.setCenter(new String[]{"40%", "50%"});
        Emphasis emphasis = new Emphasis();
        ItemStyle itemStyle = new ItemStyle();
        itemStyle.setShadowBlur(10);
        itemStyle.setShadowOffsetX(0);
        itemStyle.setShadowColor("rgba(0, 0, 0, 0.5)");
        emphasis.setItemStyle(itemStyle);
        optionSeries.setData(dataArr);
        optionSeriesArr[0] = optionSeries;
        optionConfig.setSeries(optionSeriesArr);
        optionConfig = pieOption.getPieLegendChartOption(legendDatas, selected, optionSeriesArr);
        OptionTitle optionTitle = new OptionTitle();
        optionTitle.setText("党员性别统计");
        optionTitle.setSubtext("党员性别占比");
        optionTitle.setLeft("center");
        optionConfig.setTitle(optionTitle);
        return optionConfig;
    }

    public List<Map<String, String>> getBiMemberBySexPie(String orgId) {
        return echartsPartyMapper.getBiMemberBySexPie(orgId);
    }

    /**
     * @param account
     * @return OptionConfig
     * @Title: getBiMemberByPartyOrgIdPie
     * @Description:  所属党组织占比
     */
    public OptionConfig getBiMemberByPartyOrgIdPie(Account account) {
        OptionConfig optionConfig = new OptionConfig();
        List<Map<String, String>> resdataList = getBiMemberByPartyOrgIdPie(account.getOrgId());
        OptionSeries[] optionSeriesArr = new OptionSeries[1];
        SeriesData[] dataArr = new SeriesData[resdataList.size()];
        int selectedLeng = 0;
        if (dataArr.length >= 10) {
            selectedLeng = 10;
        } else {
            selectedLeng = dataArr.length;
        }
        String[] selected = new String[selectedLeng];
        LegendData[] legendDatas = new LegendData[dataArr.length];
        for (int i = 0; i < dataArr.length; i++) {
            if (StringUtils.isBlank(resdataList.get(i).get("name"))) {
                resdataList.get(i).put("name", "other" + i);
            }
            if (i < selectedLeng) {
                selected[i] = resdataList.get(i).get("name").toString();
            }
            LegendData legendData = new LegendData();
            legendData.setName(resdataList.get(i).get("name").toString());
            legendDatas[i] = legendData;
            SeriesData seriesData = new SeriesData();
            seriesData.setName(resdataList.get(i).get("name").toString());
            seriesData.setValue(Double.valueOf(String.valueOf(resdataList.get(i).get("value"))));
            dataArr[i] = seriesData;
        }
        OptionSeries optionSeries = new OptionSeries();
        optionSeries.setName("党组织");
        optionSeries.setType("pie");
        optionSeries.setRadius("55%");
        optionSeries.setCenter(new String[]{"40%", "50%"});
        Emphasis emphasis = new Emphasis();
        ItemStyle itemStyle = new ItemStyle();
        itemStyle.setShadowBlur(10);
        itemStyle.setShadowOffsetX(0);
        itemStyle.setShadowColor("rgba(0, 0, 0, 0.5)");
        emphasis.setItemStyle(itemStyle);
        optionSeries.setData(dataArr);
        optionSeriesArr[0] = optionSeries;
        optionConfig.setSeries(optionSeriesArr);
        optionConfig = pieOption.getPieLegendChartOption(legendDatas, selected, optionSeriesArr);
        OptionTitle optionTitle = new OptionTitle();
        optionTitle.setText("所属党组织统计");
        optionTitle.setSubtext("所属党组织占比");
        optionTitle.setLeft("center");
        optionConfig.setTitle(optionTitle);
        return optionConfig;
    }

    public List<Map<String, String>> getBiMemberByPartyOrgIdPie(String orgId) {
        return echartsPartyMapper.getBiMemberByPartyOrgIdPie(orgId);
    }

    /**
     * @param account
     * @return OptionConfig
     * @Title: getBiMemberByEducationPie
     * @Description:  党员学历占比
     */
    public OptionConfig getBiMemberByEducationPie(Account account) {
        OptionConfig optionConfig = new OptionConfig();
        List<Map<String, String>> resdataList = getBiMemberByEducationPie(account.getOrgId());
        OptionSeries[] optionSeriesArr = new OptionSeries[1];
        SeriesData[] dataArr = new SeriesData[resdataList.size()];
        int selectedLeng = 0;
        if (dataArr.length >= 10) {
            selectedLeng = 10;
        } else {
            selectedLeng = dataArr.length;
        }
        String[] selected = new String[selectedLeng];
        LegendData[] legendDatas = new LegendData[dataArr.length];
        for (int i = 0; i < dataArr.length; i++) {
            if (StringUtils.isBlank(resdataList.get(i).get("name"))) {
                resdataList.get(i).put("name", "other" + i);
            }
            if (i < selectedLeng) {
                selected[i] = resdataList.get(i).get("name").toString();
            }
            LegendData legendData = new LegendData();
            legendData.setName(resdataList.get(i).get("name").toString());
            legendDatas[i] = legendData;
            SeriesData seriesData = new SeriesData();
            seriesData.setName(resdataList.get(i).get("name").toString());
            seriesData.setValue(Double.valueOf(String.valueOf(resdataList.get(i).get("value"))));
            dataArr[i] = seriesData;
        }
        OptionSeries optionSeries = new OptionSeries();
        optionSeries.setName("学历");
        optionSeries.setType("pie");
        optionSeries.setRadius("55%");
        optionSeries.setCenter(new String[]{"40%", "50%"});
        Emphasis emphasis = new Emphasis();
        ItemStyle itemStyle = new ItemStyle();
        itemStyle.setShadowBlur(10);
        itemStyle.setShadowOffsetX(0);
        itemStyle.setShadowColor("rgba(0, 0, 0, 0.5)");
        emphasis.setItemStyle(itemStyle);
        optionSeries.setData(dataArr);
        optionSeriesArr[0] = optionSeries;
        optionConfig.setSeries(optionSeriesArr);
        optionConfig = pieOption.getPieLegendChartOption(legendDatas, selected, optionSeriesArr);
        OptionTitle optionTitle = new OptionTitle();
        optionTitle.setText("党员学历统计");
        optionTitle.setSubtext("党员学历占比");
        optionTitle.setLeft("center");
        optionConfig.setTitle(optionTitle);
        return optionConfig;
    }

    public List<Map<String, String>> getBiMemberByEducationPie(String orgId) {
        return echartsPartyMapper.getBiMemberByEducationPie(orgId);
    }

    /**
     * @param orgId
     * @return Map<String, String>
     * @Title: getPartyOrgCountList
     * @Description:  获取各类型党组织数量
     */
    public Map<String, String> getPartyOrgCountList(String orgId) {
        return echartsPartyMapper.getPartyOrgCountList(orgId);
    }


}
