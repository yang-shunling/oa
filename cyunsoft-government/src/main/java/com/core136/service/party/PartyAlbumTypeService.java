package com.core136.service.party;

import com.core136.bean.party.PartyAlbumType;
import com.core136.mapper.party.PartyAlbumTypeMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyAlbumTypeService {

    private PartyAlbumTypeMapper partyAlbumTypeMapper;

    @Autowired
    public void setPartyAlbumTypeMapper(PartyAlbumTypeMapper partyAlbumTypeMapper) {
        this.partyAlbumTypeMapper = partyAlbumTypeMapper;
    }

    public int insertPartyAlbumType(PartyAlbumType partyAlbumType) {
        return partyAlbumTypeMapper.insert(partyAlbumType);
    }


    public int deletePartyAlbumType(PartyAlbumType partyAlbumType) {
        return partyAlbumTypeMapper.delete(partyAlbumType);
    }

    public int updatePartyAlbumType(Example example, PartyAlbumType partyAlbumType) {
        return partyAlbumTypeMapper.updateByExampleSelective(partyAlbumType, example);
    }

    public PartyAlbumType selectOnePartyAlbumType(PartyAlbumType partyAlbumType) {
        return partyAlbumTypeMapper.selectOne(partyAlbumType);
    }

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getAlbumTypeFroSelect
     * @Description:  获取专辑分类
     */
    public List<Map<String, String>> getAlbumTypeForSelect(String orgId) {
        return partyAlbumTypeMapper.getAlbumTypeForSelect(orgId);
    }

    public Map<String, String> getAlbumTypeName(String orgId,String albumTypeId) {
        return partyAlbumTypeMapper.getAlbumTypeName(orgId,albumTypeId);
    }

    public List<Map<String, String>> getAlbumTypeForHome(String orgId) {
        return partyAlbumTypeMapper.getAlbumTypeForHome(orgId);
    }

    /**
     * @Title: deletePartyAlbumType
     * @Description:  按条件删除
     * @param: example
     * @param: @return
     * @return: int
     */
    public int deletePartyAlbumType(Example example) {
        return partyAlbumTypeMapper.deleteByExample(example);
    }

    public PageInfo<PartyAlbumType> getPartyAlbumTypeList(Example example, int pageNumber, int pageSize) {
        PageHelper.startPage(pageNumber, pageSize);
        List<PartyAlbumType> datalist = getPartyAlbumTypeList(example);
        PageInfo<PartyAlbumType> pageInfo = new PageInfo<PartyAlbumType>(datalist);
        return pageInfo;
    }

    public List<PartyAlbumType> getPartyAlbumTypeList(Example example) {
        return partyAlbumTypeMapper.selectByExample(example);
    }

}
