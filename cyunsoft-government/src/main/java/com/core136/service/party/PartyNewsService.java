package com.core136.service.party;

import com.core136.bean.party.PartyNews;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.party.PartyNewsMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyNewsService {
    private PartyNewsMapper partyNewsMapper;

    @Autowired
    public void setPartyNewsMapper(PartyNewsMapper partyNewsMapper) {
        this.partyNewsMapper = partyNewsMapper;
    }

    public int insertPartyNews(PartyNews partyNews) {
        return partyNewsMapper.insert(partyNews);
    }

    public int deletePartyNews(PartyNews partyNews) {
        return partyNewsMapper.delete(partyNews);
    }

    public int updatePartyNews(Example example, PartyNews partyNews) {
        return partyNewsMapper.updateByExampleSelective(partyNews, example);
    }

    public PartyNews selectOnePartyNews(PartyNews partyNews) {
        return partyNewsMapper.selectOne(partyNews);
    }

    /**
     * @param orgId
     * @param guideType
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPartyNewsList
     * @Description:  获取党建新闻列表
     */
    public List<Map<String, String>> getPartyNewsList(String orgId, String newsType, String beginTime, String endTime, String search) {
        return partyNewsMapper.getPartyNewsList(orgId, newsType, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param guideType
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getPartyNewsList
     * @Description:  获取党建新闻列表
     */
    public PageInfo<Map<String, String>> getPartyNewsList(PageParam pageParam, String newsType, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getPartyNewsList(pageParam.getOrgId(), newsType, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param accountId
     * @param deptId
     * @param levelId
     * @param newsType
     * @param status
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, Object>>
     * @Title: getMyPartyNewsList
     * @Description:  获取个人权限内的新闻
     */
    public List<Map<String, Object>> getMyPartyNewsList(String orgId, String accountId, String deptId, String levelId, String newsType, String status, String beginTime, String endTime, String search) {
        return partyNewsMapper.getMyPartyNewsList(orgId, accountId, deptId, levelId, newsType, status, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param deptId
     * @param levelId
     * @param newsType
     * @param status
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, Object>>
     * @throws Exception
     * @Title: getMyPartyNewsList
     * @Description:  获取个人权限内的新闻
     */
    public PageInfo<Map<String, Object>> getMyPartyNewsList(PageParam pageParam, String deptId, String levelId, String newsType, String status, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, Object>> datalist = getMyPartyNewsList(pageParam.getOrgId(), pageParam.getAccountId(), deptId, levelId, newsType, status, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<Map<String, Object>>(datalist);
        return pageInfo;
    }

}
