package com.core136.service.party;

import com.core136.bean.account.Account;
import com.core136.bean.account.UserInfo;
import com.core136.bean.party.PartyPayStandard;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.ExcelUtil;
import com.core136.common.utils.SysTools;
import com.core136.mapper.party.PartyPayStandardMapper;
import com.core136.service.account.UserInfoService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PartyPayStandardService {
    private PartyPayStandardMapper partyPayStandardMapper;

    @Autowired
    public void setPartyPayStandardMapper(PartyPayStandardMapper partyPayStandardMapper) {
        this.partyPayStandardMapper = partyPayStandardMapper;
    }

    private UserInfoService userInfoService;

    @Autowired
    public void setUserInfoService(UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    public int insertPartyPayStandard(PartyPayStandard partyPayStandard) {
        return partyPayStandardMapper.insert(partyPayStandard);
    }

    public int deletePartyPayStandard(PartyPayStandard partyPayStandard) {
        return partyPayStandardMapper.delete(partyPayStandard);
    }

    public int updatePartyPayStandard(Example example, PartyPayStandard partyPayStandard) {
        return partyPayStandardMapper.updateByExampleSelective(partyPayStandard, example);
    }

    public PartyPayStandard selectOnePartyPayStandard(PartyPayStandard partyPayStandard) {
        return partyPayStandardMapper.selectOne(partyPayStandard);
    }

    /**
     * @param orgId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPayStandardList
     * @Description:  获取党费缴纳标准列表
     */
    public List<Map<String, String>> getPayStandardList(String orgId, String search) {
        return partyPayStandardMapper.getPayStandardList(orgId, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getPayStandardList
     * @Description:  获取党费缴纳标准列表
     */
    public PageInfo<Map<String, String>> getPayStandardList(PageParam pageParam) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getPayStandardList(pageParam.getOrgId(), pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param account
     * @param file
     * @return
     * @throws IOException RetDataBean
     * @Title: importPayStandard
     * @Description:  批量导入党费缴纳标准
     */
    @Transactional(value = "generalTM")
    public RetDataBean importPayStandard(Account account, MultipartFile file) throws IOException {
        Map<String, String> fieldMap = new HashMap<String, String>();
        fieldMap.put("排序号", "sort_no");
        fieldMap.put("关联账号", "account_id");
        fieldMap.put("党员姓名", "user_name");
        fieldMap.put("缴纳标准", "je");
        fieldMap.put("备注", "remark");
        List<String> fieldList = new ArrayList<String>();
        List<String> titleList = new ArrayList<String>();
        for (Map.Entry<String, String> entry : fieldMap.entrySet()) {
            fieldList.add(entry.getValue());
            titleList.add(entry.getKey());
        }
        String[] fieldArr = new String[fieldList.size()];
        fieldList.toArray(fieldArr);
        String fieldString = StringUtils.join(fieldArr, ",");
        List<Map<String, String>> recordList = ExcelUtil.readExcel(file);
        for (int i = 0; i < recordList.size(); i++) {
            Map<String, String> tempMap = recordList.get(i);
            String valueString = "'" + SysTools.getGUID() + "',";
            for (int k = 0; k < titleList.size(); k++) {
                String userName = "";
                if (titleList.get(k).equals("关联账号")) {
                    if (StringUtils.isNotBlank(tempMap.get(titleList.get(k)))) {
                        UserInfo userInfo = new UserInfo();
                        userInfo.setAccountId(tempMap.get(titleList.get(k)));
                        userInfo.setOrgId(account.getOrgId());
                        userInfo = userInfoService.selectOneUserInfo(userInfo);
                        userName = userInfo.getUserName();
                        valueString += "'" + tempMap.get(titleList.get(k)) + "',";
                    } else {
                        valueString += "'',";
                    }
                } else if (titleList.get(k).equals("党员姓名")) {
                    if (StringUtils.isNotBlank(userName)) {
                        valueString += "'" + userName + "',";
                    } else {
                        valueString += "'" + tempMap.get(titleList.get(k)) + "',";
                    }
                } else {
                    valueString += "'" + tempMap.get(titleList.get(k)) + "',";
                }
            }
            valueString += "'" + SysTools.getTime("yyyy-MM-dd HH:mm:ss") + "',";
            valueString += "'" + account.getAccountId() + "',";
            valueString += "'" + account.getOrgId() + "'";
            String insertSql = "insert into party_pay_standard(record_id," + fieldString + ",create_time,create_user,org_id) values" + "(" + valueString + ")";
            jdbcTemplate.execute(insertSql);
        }
        return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS);
    }

}
