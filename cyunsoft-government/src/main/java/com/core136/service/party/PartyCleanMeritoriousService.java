package com.core136.service.party;

import com.core136.bean.party.PartyCleanMeritorious;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.party.PartyCleanMeritoriousMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyCleanMeritoriousService {

    private PartyCleanMeritoriousMapper partyCleanMeritoriousMapper;

    @Autowired
    public void setPartyCleanMeritoriousMapper(PartyCleanMeritoriousMapper partyCleanMeritoriousMapper) {
        this.partyCleanMeritoriousMapper = partyCleanMeritoriousMapper;
    }

    public int insertPartyCleanMeritorious(PartyCleanMeritorious partyCleanMeritorious) {
        return partyCleanMeritoriousMapper.insert(partyCleanMeritorious);
    }

    public int deletePartyCleanMeritorious(PartyCleanMeritorious partyCleanMeritorious) {
        return partyCleanMeritoriousMapper.delete(partyCleanMeritorious);
    }

    public int updatePartyCleanMeritorious(Example example, PartyCleanMeritorious partyCleanMeritorious) {
        return partyCleanMeritoriousMapper.updateByExampleSelective(partyCleanMeritorious, example);
    }

    public PartyCleanMeritorious selectOnePartyCleanMeritorious(PartyCleanMeritorious partyCleanMeritorious) {
        return partyCleanMeritoriousMapper.selectOne(partyCleanMeritorious);
    }

    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @param meritoriousType
     * @param status
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getCleanMeritoriousList
     * @Description:  获取先进事迹列表
     */
    public List<Map<String, String>> getCleanMeritoriousList(String orgId, String opFlag, String accountId, String meritoriousType, String status, String beginTime, String endTime, String search) {
        return partyCleanMeritoriousMapper.getCleanMeritoriousList(orgId, opFlag, accountId, meritoriousType, status, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param meritoriousType
     * @param status
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getCleanMeritoriousList
     * @Description:  获取先进事迹列表
     */
    public PageInfo<Map<String, String>> getCleanMeritoriousList(PageParam pageParam, String meritoriousType, String status, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getCleanMeritoriousList(pageParam.getOrgId(), pageParam.getOpFlag(), pageParam.getAccountId(), meritoriousType, status, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param memberId
     * @return List<Map < String, String>>
     * @Title: getMyCleanMeritoriousListForPortal
     * @Description:  获门户先进事迹列表
     */
    public List<Map<String, String>> getMyCleanMeritoriousListForPortal(String orgId, String memberId) {
        return partyCleanMeritoriousMapper.getMyCleanMeritoriousListForPortal(orgId, memberId);
    }

    /**
     * @param orgId
     * @param accountId
     * @param deptId
     * @param levelId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getMyCleanMeritoriousList
     * @Description:  获取更多先进事迹
     */
    public List<Map<String, String>> getMyCleanMeritoriousList(String orgId, String accountId, String deptId, String levelId, String search) {
        return partyCleanMeritoriousMapper.getMyCleanMeritoriousList(orgId, accountId, deptId, levelId, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getMyCleanMeritoriousList
     * @Description:  获取更多先进事迹
     */
    public PageInfo<Map<String, String>> getMyCleanMeritoriousList(PageParam pageParam) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyCleanMeritoriousList(pageParam.getOrgId(), pageParam.getAccountId(), pageParam.getDeptId(), pageParam.getLevelId(), pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
