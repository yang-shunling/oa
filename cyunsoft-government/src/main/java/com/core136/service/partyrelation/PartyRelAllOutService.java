package com.core136.service.partyrelation;

import com.core136.bean.partymember.PartyMember;
import com.core136.bean.partyrelation.PartyRelAllOut;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partyrelation.PartyRelAllOutMapper;
import com.core136.service.partymember.PartyMemberService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class PartyRelAllOutService {
    private PartyRelAllOutMapper partyRelAllOutMapper;

    @Autowired
    public void setPartyRelAllOutMapper(PartyRelAllOutMapper partyRelAllOutMapper) {
        this.partyRelAllOutMapper = partyRelAllOutMapper;
    }

    private PartyMemberService partyMemberService;

    @Autowired
    public void setPartyMemberService(PartyMemberService partyMemberService) {
        this.partyMemberService = partyMemberService;
    }

    public int insertPartyRelAllOut(PartyRelAllOut partyRelAllOut) {
        return partyRelAllOutMapper.insert(partyRelAllOut);
    }

    public int deletePartyRelAllOut(PartyRelAllOut partyRelAllOut) {
        return partyRelAllOutMapper.delete(partyRelAllOut);
    }

    public int updatePartyRelAllOut(Example example, PartyRelAllOut partyRelAllOut) {
        return partyRelAllOutMapper.updateByExampleSelective(partyRelAllOut, example);
    }

    public PartyRelAllOut selectOnePartyRelAllOut(PartyRelAllOut partyRelAllOut) {
        return partyRelAllOutMapper.selectOne(partyRelAllOut);
    }

    /**
     * @param partyRelAllOut
     * @return RetDataBean
     * @Title: setRelAllToOutOtherOrg
     * @Description:  整建制转出
     */
    @Transactional(value = "generalTM")
    public RetDataBean setRelAllToOutOtherOrg(PartyRelAllOut partyRelAllOut) {
        try {
            if (partyRelAllOut.getIsInSys().equals("1")) {
                String oldPartyOrgId = partyRelAllOut.getOldPartyOrgId();
                PartyMember partyMember = new PartyMember();
                partyMember.setPartyOrgId(oldPartyOrgId);
                partyMember.setOrgId(partyRelAllOut.getOrgId());
                List<PartyMember> memberList = partyMemberService.getPartyMemeberList(partyMember);
                List<String> memberIdList = new ArrayList<String>();
                for (int i = 0; i < memberList.size(); i++) {
                    memberIdList.add(memberList.get(i).getMemberId());
                    PartyMember tempMember = new PartyMember();
                    tempMember.setPartyOrgId(partyRelAllOut.getOutPartyOrgId());
                    Example example = new Example(PartyMember.class);
                    example.createCriteria().andEqualTo("orgId", partyRelAllOut.getOrgId()).andEqualTo("memberId", memberList.get(i).getMemberId());
                    partyMemberService.updatePartyMember(example, tempMember);
                }
                partyRelAllOut.setMemberIds(StringUtils.join(memberIdList, ","));
            } else {
                String oldPartyOrgId = partyRelAllOut.getOldPartyOrgId();
                PartyMember partyMember = new PartyMember();
                partyMember.setPartyOrgId(oldPartyOrgId);
                partyMember.setOrgId(partyRelAllOut.getOrgId());
                List<PartyMember> memberList = partyMemberService.getPartyMemeberList(partyMember);
                List<String> memberIdList = new ArrayList<String>();
                for (int i = 0; i < memberList.size(); i++) {
                    memberIdList.add(memberList.get(i).getMemberId());
                    PartyMember tempMember = new PartyMember();
                    tempMember.setPartyOrgId("");
                    tempMember.setDelFlag("1");
                    Example example = new Example(PartyMember.class);
                    example.createCriteria().andEqualTo("orgId", partyRelAllOut.getOrgId()).andEqualTo("memberId", memberList.get(i).getMemberId());
                    partyMemberService.updatePartyMember(example, tempMember);
                }
                partyRelAllOut.setMemberIds(StringUtils.join(memberIdList, ","));
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, insertPartyRelAllOut(partyRelAllOut));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param orgId
     * @param isInSys
     * @param beginTime
     * @param endTime
     * @param oldPartyOrgId
     * @param outPartyOrgId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getRelAllOutList
     * @Description:  获取整建制关系转出记录
     */
    public List<Map<String, String>> getRelAllOutList(String orgId, String isInSys, String beginTime, String endTime, String oldPartyOrgId, String outPartyOrgId, String search) {
        return partyRelAllOutMapper.getRelAllOutList(orgId, isInSys, beginTime, endTime, oldPartyOrgId, outPartyOrgId, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param isInSys
     * @param beginTime
     * @param endTime
     * @param oldPartyOrgId
     * @param outPartyOrgId
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getRelAllOutList
     * @Description:  获取整建制关系转出记录
     */
    public PageInfo<Map<String, String>> getRelAllOutList(PageParam pageParam, String isInSys, String beginTime, String endTime, String oldPartyOrgId, String outPartyOrgId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getRelAllOutList(pageParam.getOrgId(), isInSys, beginTime, endTime, oldPartyOrgId, outPartyOrgId, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }
}
