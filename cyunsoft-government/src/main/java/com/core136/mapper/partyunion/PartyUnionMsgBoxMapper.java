package com.core136.mapper.partyunion;

import com.core136.bean.partyunion.PartyUnionMsgBox;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface PartyUnionMsgBoxMapper extends MyMapper<PartyUnionMsgBox> {
    public List<Map<String, String>> getMyMsgBoxList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId, @Param(value = "search") String search);

    public List<Map<String, String>> getLeaderMsgBoxList(@Param(value = "orgId") String orgId, @Param(value = "search") String search);
}
