package com.core136.mapper.partyunion;

import com.core136.bean.partyunion.PartyUnionStory;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface PartyUnionStoryMapper extends MyMapper<PartyUnionStory> {
    /**
     * 职场故事列表
     *
     * @param orgId
     * @param storyType
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String, String>> getPartyUnionStoryList(@Param(value = "orgId") String orgId, @Param(value = "storyType") String storyType,
                                                            @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);

}
