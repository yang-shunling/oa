package com.core136.mapper.partyunion;

import com.core136.bean.partyunion.PartyUnionDept;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface PartyUnionDeptMapper extends MyMapper<PartyUnionDept> {
    /**
     * 获取工会部门
     *
     * @param orgId
     * @param orgLevelId
     * @return
     */
    public List<Map<String, String>> getPartyUnionDeptTree(@Param(value = "orgId") String orgId, @Param(value = "orgLevelId") String orgLevelId);

}
