package com.core136.mapper.partyunion;

import com.core136.bean.partyunion.PartyUnionWorker;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface PartyUnionWorkerMapper extends MyMapper<PartyUnionWorker> {
    /**
     * 获取农民工列表
     *
     * @param orgId
     * @param beginTime
     * @param endTime
     * @param beginTimeBirthday
     * @param endTimeBirthday
     * @param search
     * @return
     */
    public List<Map<String, String>> getPartyUnionWorkersList(@Param(value = "orgId") String orgId, @Param(value = "beginTime") String beginTime,
                                                              @Param(value = "endTime") String endTime, @Param(value = "beginTimeBirthday") String beginTimeBirthday,
                                                              @Param(value = "endTimeBirthday") String endTimeBirthday, @Param(value = "search") String search);

}
