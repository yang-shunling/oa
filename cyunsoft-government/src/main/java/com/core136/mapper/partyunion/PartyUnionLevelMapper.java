package com.core136.mapper.partyunion;

import com.core136.bean.partyunion.PartyUnionLevel;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface PartyUnionLevelMapper extends MyMapper<PartyUnionLevel> {
    /**
     * 获取工会职务列表
     *
     * @param orgId
     * @return
     */
    public List<Map<String, String>> getPartyUnionLevelForList(@Param(value = "orgId") String orgId);
}
