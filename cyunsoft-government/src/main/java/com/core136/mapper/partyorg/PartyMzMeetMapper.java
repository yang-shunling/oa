package com.core136.mapper.partyorg;

import com.core136.bean.partyorg.PartyMzMeet;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface PartyMzMeetMapper extends MyMapper<PartyMzMeet> {

    /**
     * @param orgId
     * @param meetType
     * @param beginTime
     * @param endTime
     * @param partyOrgId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getMzMeetList
     * @Description:  获取民主生活会记录
     */
    public List<Map<String, String>> getMzMeetList(@Param(value = "orgId") String orgId, @Param(value = "meetType") String meetType, @Param(value = "meetYear") String meetYear,
                                                   @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "partyOrgId") String partyOrgId,
                                                   @Param(value = "search") String search);

}
