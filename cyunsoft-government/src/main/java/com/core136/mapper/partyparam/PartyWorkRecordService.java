package com.core136.mapper.partyparam;

import com.core136.bean.partymember.PartyWorkRecord;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partymember.PartyWorkRecordMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyWorkRecordService {
    @Autowired
    private PartyWorkRecordMapper partyWorkRecordMapper;

    public int insertPartyWorkRecord(PartyWorkRecord partyPartyWork) {
        return partyWorkRecordMapper.insert(partyPartyWork);
    }

    public int deletePartyWorkRecord(PartyWorkRecord partyPartyWork) {
        return partyWorkRecordMapper.delete(partyPartyWork);
    }

    public int updatePartyWorkRecord(Example example, PartyWorkRecord partyPartyWork) {
        return partyWorkRecordMapper.updateByExampleSelective(partyPartyWork, example);
    }

    public PartyWorkRecord selectOnePartyWorkRecord(PartyWorkRecord partyPartyWork) {
        return partyWorkRecordMapper.selectOne(partyPartyWork);
    }

    /**
     * @param orgId
     * @param workType
     * @param joinUser
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPartyWorkRecordList
     * @Description:  历史活动列表
     */
    public List<Map<String, String>> getPartyWorkRecordList(String orgId, String workType, String joinUser, String beginTime, String endTime, String search) {
        return partyWorkRecordMapper.getPartyWorkRecordList(orgId, workType, joinUser, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param workType
     * @param joinUser
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getPartyWorkRecordList
     * @Description:  历史活动列表
     */
    public PageInfo<Map<String, String>> getPartyWorkRecordList(PageParam pageParam, String workType, String joinUser, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getPartyWorkRecordList(pageParam.getOrgId(), workType, joinUser, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param workType
     * @param accountId
     * @param nowTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getMyPartyWorkRecordList
     * @Description:  获取需我参加的党员活动列表
     */
    public List<Map<String, String>> getMyPartyWorkRecordList(String orgId, String workType, String accountId, String nowTime, String search) {
        return partyWorkRecordMapper.getMyPartyWorkRecordList(orgId, workType, accountId, nowTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param workType
     * @param nowTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getMyPartyWorkRecordList
     * @Description:  获取需我参加的党员活动列表
     */
    public PageInfo<Map<String, String>> getMyPartyWorkRecordList(PageParam pageParam, String workType, String nowTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyPartyWorkRecordList(pageParam.getOrgId(), workType, pageParam.getAccountId(), nowTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param workType
     * @param accountId
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getHistoryPartyWorkRecordList
     * @Description:  获取我参加的党员活动
     */
    public List<Map<String, String>> getHistoryPartyWorkRecordList(String orgId, String workType, String accountId, String beginTime, String endTime, String search) {
        return partyWorkRecordMapper.getHistoryPartyWorkRecordList(orgId, workType, accountId, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param workType
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getHistoryPartyWorkRecordList
     * @Description:  获取我参加的党员活动
     */
    public PageInfo<Map<String, String>> getHistoryPartyWorkRecordList(PageParam pageParam, String workType, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHistoryPartyWorkRecordList(pageParam.getOrgId(), workType, pageParam.getAccountId(), beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
