package com.core136.mapper.partycommission;

import com.core136.bean.partycommission.PartyBuildClean;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface PartyBuildCleanMapper extends MyMapper<PartyBuildClean> {

    /**
     * @param orgId
     * @param eventType
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPartyBuildCleanList
     * @Description:  获取党风廉政记录列表
     */
    public List<Map<String, String>> getPartyBuildCleanList(@Param(value = "orgId") String orgId, @Param(value = "eventType") String eventType,
                                                            @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);

}
