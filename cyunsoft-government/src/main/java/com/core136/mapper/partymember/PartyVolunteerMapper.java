package com.core136.mapper.partymember;

import com.core136.bean.partymember.PartyVolunteer;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyVolunteerMapper extends MyMapper<PartyVolunteer> {

    /**
     * @param orgId
     * @param volType
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getVolunteerList
     * @Description:  获取志愿者活动列表
     */
    public List<Map<String, String>> getVolunteerList(@Param(value = "orgId") String orgId, @Param(value = "volType") String volType,
                                                      @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);

}
