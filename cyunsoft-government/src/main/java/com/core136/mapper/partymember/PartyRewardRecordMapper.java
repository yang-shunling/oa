package com.core136.mapper.partymember;

import com.core136.bean.partymember.PartyRewardRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyRewardRecordMapper extends MyMapper<PartyRewardRecord> {
    /**
     * @param orgId
     * @param beginTime
     * @param rewardLevel
     * @param memberId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getMemberRewardList
     * @Description:  获取党员奖励列表
     */
    public List<Map<String, String>> getMemberRewardList(@Param(value = "orgId") String orgId,
                                                         @Param(value = "beginTime") String beginTime,
                                                         @Param(value = "endTime") String endTime,
                                                         @Param(value = "rewardLevel") String rewardLevel,
                                                         @Param(value = "memberId") String memberId,
                                                         @Param(value = "search") String search);

    /**
     * @param orgId
     * @param memberId
     * @return List<Map < String, String>>
     * @Title: getMyMemberRewardList
     * @Description:  获取个人奖励列表
     */
    public List<Map<String, String>> getMyMemberRewardList(@Param(value = "orgId") String orgId, @Param(value = "memberId") String memberId);

}
