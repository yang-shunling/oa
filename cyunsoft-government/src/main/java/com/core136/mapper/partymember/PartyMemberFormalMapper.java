package com.core136.mapper.partymember;

import com.core136.bean.partymember.PartyMemberFormal;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyMemberFormalMapper extends MyMapper<PartyMemberFormal> {
    /**
     * @param orgId
     * @param status
     * @param partyOrgId
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getFormalRecordList
     * @Description:  获取党员转正记录
     */
    public List<Map<String, String>> getFormalRecordList(@Param(value = "orgId") String orgId, @Param(value = "status") String status,
                                                         @Param(value = "partyOrgId") String partyOrgId, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime,
                                                         @Param(value = "search") String search);
}
