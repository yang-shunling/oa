package com.core136.mapper.party;

import com.core136.bean.party.PartyCleanSuggestions;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyCleanSuggestionsMapper extends MyMapper<PartyCleanSuggestions> {
    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @param suggestionsType
     * @param status
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getCleanSuggestionsList
     * @Description:  获取计策列表
     */
    public List<Map<String, String>> getCleanSuggestionsList(@Param(value = "orgId") String orgId, @Param(value = "opFlag") String opFlag, @Param(value = "accountId") String accountId,
                                                             @Param(value = "suggestionsType") String suggestionsType, @Param(value = "status") String status, @Param("beginTime") String beginTime, @Param(value = "endTime") String endTime,
                                                             @Param(value = "search") String search);

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getMyCleanSuggestionsListForPortal
     * @Description:  获取门户计策列表
     */
    public List<Map<String, String>> getMyCleanSuggestionsListForPortal(@Param(value = "orgId") String orgId);

    /**
     * @param orgId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getMyCleanSuggestionsList
     * @Description:  获取更多计策记录
     */
    public List<Map<String, String>> getMyCleanSuggestionsList(@Param(value = "orgId") String orgId, @Param(value = "search") String search);

}
