package com.core136.mapper.party;

import com.core136.bean.party.PartyAlbumConfig;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface PartyAlbumConfigMapper extends MyMapper<PartyAlbumConfig> {

}
