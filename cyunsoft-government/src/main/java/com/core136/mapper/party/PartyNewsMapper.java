package com.core136.mapper.party;

import com.core136.bean.party.PartyNews;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface PartyNewsMapper extends MyMapper<PartyNews> {
    /**
     * @param orgId
     * @param guide
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPartyNewsList
     * @Description:  获取党建新闻列表
     */
    public List<Map<String, String>> getPartyNewsList(@Param(value = "orgId") String orgId, @Param(value = "newsType") String newsType,
                                                      @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);

    /**
     * @param orgId
     * @param accountId
     * @param deptId
     * @param levelId
     * @param newsType
     * @param status
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, Object>>
     * @Title: getMyPartyNewsList
     * @Description:  获取个人权限内的新闻
     */
    public List<Map<String, Object>> getMyPartyNewsList(@Param(value = "orgId") String orgId,
                                                        @Param(value = "accountId") String accountId,
                                                        @Param(value = "deptId") String deptId,
                                                        @Param(value = "levelId") String levelId,
                                                        @Param(value = "newsType") String newsType,
                                                        @Param(value = "status") String status,
                                                        @Param(value = "beginTime") String beginTime,
                                                        @Param(value = "endTime") String endTime,
                                                        @Param(value = "search") String search);

}
