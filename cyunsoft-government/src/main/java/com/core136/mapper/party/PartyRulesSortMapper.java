package com.core136.mapper.party;

import com.core136.bean.party.PartyRulesSort;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyRulesSortMapper extends MyMapper<PartyRulesSort> {

    /**
     * @param orgId
     * @param sortLevel
     * @return List<Map < String, String>>
     * @Title: getRulesSortTree
     * @Description:  获取制度分类树型结构
     */
    public List<Map<String, String>> getRulesSortTree(@Param(value = "orgId") String orgId, @Param(value = "sortLevel") String sortLevel);

    /**
     * @param orgId
     * @param sortId
     * @return int
     * @Title: isExistChild
     * @Description:  判断是否存在子分类
     */
    public int isExistChild(@Param(value = "orgId") String orgId, @Param(value = "sortId") String sortId);

}
