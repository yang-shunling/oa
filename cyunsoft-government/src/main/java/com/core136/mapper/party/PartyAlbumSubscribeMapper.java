package com.core136.mapper.party;

import com.core136.bean.party.PartyAlbumSubscribe;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface PartyAlbumSubscribeMapper extends MyMapper<PartyAlbumSubscribe> {
    /**
     * @param orgId
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getSubscribeMyUserList
     * @Description:  获取关注我的人员列表
     */
    public List<Map<String, String>> getSubscribeMyUserList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId);
}
