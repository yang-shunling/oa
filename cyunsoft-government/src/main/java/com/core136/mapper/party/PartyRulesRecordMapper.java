package com.core136.mapper.party;

import com.core136.bean.party.PartyRulesRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface PartyRulesRecordMapper extends MyMapper<PartyRulesRecord> {

    /**
     * @param orgId
     * @param sortId
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPartyRulesRecordList
     * @Description:  获取规章制度列表
     */
    public List<Map<String, String>> getPartyRulesRecordList(@Param(value = "orgId") String orgId, @Param(value = "sortId") String sortId,
                                                             @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);

}
