package com.core136.mapper.partyrelation;

import com.core136.bean.partyrelation.PartyRelIn;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyRelInMapper extends MyMapper<PartyRelIn> {

    /**
     * @param orgId
     * @param inType
     * @param beginTime
     * @param endTime
     * @param partyOrgId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getRelInList
     * @Description:  获取关系转入列表
     */
    public List<Map<String, String>> getRelInList(@Param(value = "orgId") String orgId, @Param(value = "inType") String inType, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime,
                                                  @Param(value = "partyOrgId") String partyOrgId, @Param(value = "search") String search);

}
