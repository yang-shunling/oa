package com.core136.bean.partymember;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: PartyMemberTarget
 * @Description: 确定为发展对象
 * @author: 稠云技术
 * @date: 2020年11月21日 下午1:45:40
 * @Copyright: 2020 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "party_member_target")
public class PartyMemberTarget implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String recordId;
    private String joinRecordId;
    private String linkMan1;
    private String linkMan2;
    private String joinTime;
    private String status;
    private String remark;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getJoinRecordId() {
        return joinRecordId;
    }

    public void setJoinRecordId(String joinRecordId) {
        this.joinRecordId = joinRecordId;
    }

    public String getLinkMan1() {
        return linkMan1;
    }

    public void setLinkMan1(String linkMan1) {
        this.linkMan1 = linkMan1;
    }

    public String getLinkMan2() {
        return linkMan2;
    }

    public void setLinkMan2(String linkMan2) {
        this.linkMan2 = linkMan2;
    }

    public String getJoinTime() {
        return joinTime;
    }

    public void setJoinTime(String joinTime) {
        this.joinTime = joinTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


}
