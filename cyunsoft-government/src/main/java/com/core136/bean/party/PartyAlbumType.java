package com.core136.bean.party;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: PartyAlbumType
 * @Description: 视频专辑分类
 * @author: 稠云技术
 * @date: 2020年11月4日 下午4:45:04
 * @Copyright: 2020 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "party_album_type")
public class PartyAlbumType implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String albumTypeId;
    private Integer sortNo;
    private String typeName;
    private String status;
    private String remark;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getAlbumTypeId() {
        return albumTypeId;
    }

    public void setAlbumTypeId(String albumTypeId) {
        this.albumTypeId = albumTypeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
