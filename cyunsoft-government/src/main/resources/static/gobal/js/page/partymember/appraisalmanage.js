$(function () {
    jeDate("#beginTime", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTime", {
        format: "YYYY-MM-DD"
    });
    $('#remark').summernote({height: 300});

    $.ajax({
        url: "/ret/partyparamget/getAppReaTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            var topNode = [{
                sortName: "空",
                isParent: "false",
                sortId: ""
            }];
            var newTreeNodes = topNode.concat(data);
            $.fn.zTree.init($("#reasonTree"), reasonsetting, newTreeNodes);
        }
    });

    $("#reason").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#reasonContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });


    $.ajax({
        url: "/ret/partyparamget/getAppSituTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            var topNode = [{
                sortName: "空",
                isParent: "false",
                sortId: ""
            }];
            var newTreeNodes = topNode.concat(data);
            $.fn.zTree.init($("#situationTree"), situationsetting, newTreeNodes);
        }
    });

    $("#situation").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#situationContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });

    $("body").unbind("click").click(function () {
        $(".menuContent").hide();
    });

    $(".menuContent").unbind("click").click(function (e) {
        e.stopPropagation();
    });

    $("#createbut").unbind("click").click(function () {
        updateAppraisal();
    })
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })
    query()
})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/partymemberget/getAppraisalList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',// 工具列
        striped: true,// 隔行换色
        cache: false,// 禁用缓存
        pagination: true,// 启动分页
        sidePagination: 'server',// 分页方式
        pageNumber: 1,// 初始化table时显示的页码
        pageSize: 10,// 每页条目
        showFooter: false,// 是否显示列脚
        showPaginationSwitch: true,// 是否显示 数据条数选择框
        sortable: true,// 排序
        search: true,// 启用搜索
        sortOrder: "asc",
        showColumns: true,// 是否显示 内容列下拉框
        showRefresh: true,// 显示刷新按钮
        idField: 'recordId',// key值栏位
        clickToSelect: true,// 点击选中checkbox
        pageList: [10, 20, 30, 50],// 可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',// 标题 可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        },
            {
                field: 'userName',
                width: '50px',
                title: '党员姓名',
                formatter:function (value,row,index)
                {
                    return "<a href=\"javascript:void(0);details('" + row.recordId + "')\" style='cursor: pointer'>"+value+"</a>";
                }
            }, {
                field: 'beginTime',
                width: '150px',
                title: '评议日期',
                formatter: function (value, row, index) {
                    return value + " 至 " + row.endTime;
                }
            }, {
                field: 'situation',
                width: '100px',
                title: '民主评议情况',
                formatter: function (value, row, index) {
                    var returnStr = "";
                    $.ajax({
                        url: "/ret/partyparamget/getPartyAppSituById",
                        type: "post",
                        dataType: "json",
                        async: false,
                        data: {
                            sortId: value
                        },
                        success: function (res) {
                            if (res.status == "200") {
                                if (res.list) {
                                    returnStr = res.list.sortName;
                                }
                            }
                        }
                    });
                    return returnStr;
                }
            }, {
                field: 'reason',
                width: '100px',
                title: '民主评议原因',
                formatter: function (value, row, index) {
                    var returnStr = "";
                    $.ajax({
                        url: "/ret/partyparamget/getPartyAppReaById",
                        type: "post",
                        dataType: "json",
                        async: false,
                        data: {
                            sortId: value
                        },
                        success: function (res) {
                            if (res.status == "200") {
                                if (res.list) {
                                    returnStr = res.list.sortName;
                                }
                            }
                        }
                    });
                    return returnStr;
                }
            }, {
                field: 'appResult',
                width: '100px',
                title: '评议结果',
                formatter: function (value, row, index) {
                    if (value == "1") {
                        return "优秀";
                    } else if (value == "2") {
                        return "合格";
                    } else if (value == "3") {
                        return "不合格";
                    } else {
                        return "未知";
                    }
                }
            }, {
                field: 'opt',
                title: '操作',
                align: 'center',
                width: '100px',
                formatter: function (value, row, index) {
                    return createOptBtn(row.recordId);
                }
            }],
        onClickCell: function (field, value, row, $element) {
            // alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, // 总页数,前面的key必须为"total"
                    rows: res.list.list
                    // 行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        appResult: $("#appResultQuery").val(),
        endTime: $("#endTimeQuery").val(),
        beginTime: $("#beginTimeQuery").val()
    };
    return temp;
};

function createOptBtn(recordId) {
    var html = "<a href=\"javascript:void(0);edit('" + recordId + "')\" class=\"btn btn-primary btn-xs\">编辑</a>&nbsp;&nbsp;" +
        "<a href=\"javascript:void(0);deleterecord('" + recordId + "')\" class=\"btn btn-darkorange btn-xs\" >删除</a>";
    return html;
}

function edit(recordId) {
    $("#appraisallistdiv").hide();
    $("#appraisaldiv").show();
    $("#memberId").attr("data-value", "");
    $("#situation").attr("data-value", "");
    $("#reason").attr("data-value", "");
    $("#remark").code("");
    $("#attach").attr("data_value", "");
    $("#show_attach").empty();
    $(".js-back-btn").unbind("click").click(function () {
        goback();
    })
    $.ajax({
        url: "/ret/partymemberget/getPartyAppraisalRecordById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", recordInfo.attach);
                        createAttach("attach", 4);
                    } else if (id == "remark") {
                        $("#remark").code(recordInfo[id]);
                    } else if (id == "memberId") {
                        $("#memberId").attr("data-value", recordInfo[id]);
                        $.ajax({
                            url: "/ret/partymemberget/getPartyMemberById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {memberId: recordInfo[id]},
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#" + id).val(res.list.userName);
                                    } else {
                                        $("#" + id).val("");
                                    }
                                }
                            }
                        });
                    } else if (id == "situation") {
                        $("#situation").attr("data-value", recordInfo[id]);
                        $.ajax({
                            url: "/ret/partyparamget/getPartyAppSituById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {sortId: recordInfo[id]},
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#" + id).val(res.list.sortName);
                                    } else {
                                        $("#" + id).val("");
                                    }
                                }
                            }
                        });
                    } else if (id == "reason") {
                        $("#reason").attr("data-value", recordInfo[id]);
                        $.ajax({
                            url: "/ret/partyparamget/getPartyAppReaById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {sortId: recordInfo[id]},
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#" + id).val(res.list.sortName);
                                    } else {
                                        $("#" + id).val("");
                                    }
                                }
                            }
                        });
                    } else {
                        $("#" + id).val(recordInfo[id]);
                    }
                }
                $(".js-update-save").unbind("click").click(function () {
                    updateAppraisal(recordId);
                })
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function deleterecord(recordId) {
    if (confirm("确定删除当前记录吗？")) {
        $.ajax({
            url: "/set/partymemberset/deletePartyAppraisalRecord",
            type: "post",
            dataType: "json",
            data: {
                recordId: recordId
            },
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable("refresh");
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    }
}


function goback() {
    $("#appraisaldiv").hide();
    $("#appraisallistdiv").show();
}

function details(recordId) {
    window.open("/app/core/partymember/appraisaldetails?recordId=" + recordId);
}

function updateAppraisal(recordId) {
    if($("#memberId").attr("data-value")=="")
    {
        layer.msg("党员不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partymemberset/updatePartyAppraisalRecord",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId,
            sortNo: $("#sortNo").val(),
            memberId: $("#memberId").attr("data-value"),
            beginTime: $("#beginTime").val(),
            endTime: $("#endTime").val(),
            appResult: $("#appResult").val(),
            situation: $("#situation").attr("data-value"),
            reason: $("#reason").attr("data-value"),
            attach: $("#attach").attr("data_value"),
            remark: $("#remark").code()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

var reasonsetting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyparamget/getAppReaTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("reasonTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.id;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].sortName + ",";
                vid += nodes[i].sortId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#reason");
            idem.attr("data-value", vid);
            idem.val(v);
        }
    }
};

var situationsetting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyparamget/getAppSituTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("situationTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.id;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].sortName + ",";
                vid += nodes[i].sortId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#situation");
            idem.attr("data-value", vid);
            idem.val(v);
        }
    }
};
