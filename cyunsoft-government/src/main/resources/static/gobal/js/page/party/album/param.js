$(function () {
    $(".js-setbtn").unbind("click").click(function () {
        setAlbumConfig();
    })
})

function setAlbumConfig() {
    $.ajax({
        url: "/set/partyset/setAlbumConfig",
        type: "post",
        dataType: "json",
        data: {
            anonymousFlag: $("input[name='anonymousFlag']:checked").val(),
            approvelUser: $("#approvelUser").attr("data-value")
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}
