$(function () {
    jeDate("#deathTime", {
        format: "YYYY-MM-DD",
        maxDate: getSysDate(),
        isinitVal: true
    });
    $('#deathReason').summernote({height: 300});
    $(".js-add-save").unbind("click").click(function () {
        addDeathRecord();
    })
})

function addDeathRecord() {
    if($("#memberId").attr("data-value")=="")
    {
        layer.msg("党员不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partymemberset/insertPartyMemberDeath",
        type: "post",
        dataType: "json",
        data: {
            memberId: $("#memberId").attr("data-value"),
            deathTime: $("#deathTime").val(),
            attach: $("#attach").attr("data_value"),
            deathReason: $("#deathReason").code()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}
