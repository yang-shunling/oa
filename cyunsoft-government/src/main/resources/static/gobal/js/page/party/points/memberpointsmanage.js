var zTree;
var setting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyorgget/getPartyOrgAllParentTree",// Ajax 获取数据的 URL 地址
        autoParam: ["partyOrgId"],// 异步加载时需要自动提交父节点属性的参数
    },
    callback: {
        onExpand: function (event, treeId, treeNode) {
            var partyOrgId = treeNode.partyOrgId;
            if (treeNode.isParent) {
                $.ajax({
                    url: "/ret/partymemberget/getSelectPartyMemberByPartyId",
                    type: "post",
                    data: {
                        partyOrgId: partyOrgId
                    },
                    dataType: "json",
                    success: function (data) {
                        var appNode = [];
                        if (data.list.length > 0) {
                            for (var i = 0; i < data.list.length; i++) {
                                var newnode = {};
                                newnode.partyOrgId = data.list[i].memberId;
                                newnode.partyOrgName = data.list[i].userName;
                                newnode.isParent = false;
                                if (data.list[i].userSex == '男') {
                                    newnode.icon = '/gobal/img/org/U01.png';
                                } else if (data.list[i].userSex == '女') {
                                    newnode.icon = '/gobal/img/org/U11.png';
                                }
                                appNode.push(newnode);
                            }
                            zTree.reAsyncChildNodes(treeNode, "refresh");
                            zTree.addNodes(treeNode, appNode);
                        }
                    }
                });
            }
        },
        onClick: zTreeOnClick
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "partyOrgId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "partyOrgName"
        }
    }
};
$(function () {
    jeDate("#pointsYearQuery", {
        format: "YYYY"
    });
    var topNode = [{
        partyOrgName: orgName,
        levelId: '',
        isParent: "true",
        partyOrgId: "0",
        icon: "/gobal/img/org/org.png"
    }];

    zTree = $.fn.zTree.init($("#tree"), setting, topNode);// 初始化树节点时，添加同步获取的数据
    var nodes = zTree.getNodes();
    for (var i = 0; i < nodes.length; i++) {
        zTree.expandNode(nodes[i], true, false, false);//默认展开第一级节点
    }
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })
    query("")
    $("#cquery").unbind("click").click(function () {
        $("#editdiv").hide();
        $("#listdiv").show();
        $("#myTable").bootstrapTable('destroy');
        query("");
    })
})

function zTreeOnClick(event, treeId, treeNode) {
    if (treeNode.isParent == false) {
        $("#myTable").bootstrapTable('destroy');
        if (treeNode.partyOrgId == "0") {
            query("");
        } else {
            query(treeNode.partyOrgId);
        }
    }
}


function query(memberId) {
    $("#myTable").bootstrapTable({
        url: '/ret/partymemberget/getMemberPointsList?memberId=' + memberId,
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: false,//是否显示 数据条数选择框
        sortable: true,//排序
        search: false,//启用搜索
        showColumns: false,//是否显示 内容列下拉框
        showRefresh: false,//显示刷新按钮
        idField: 'recordId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',//标题  可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'partyOrgName',
            width: '100px',
            title: '党组织名称'
        }, {
            field: 'userName',
            width: '100px',
            title: '党员姓名'
        }, {
            field: 'pointsYear',
            title: '年度',
            width: '100px',
            formatter: function (value, row, index) {
                return "【" + value + "】年度"
            }
        }, {
            field: 'item',
            width: '100px',
            title: '获取积分项目',
            formatter: function (value, row, index) {
                if (value == "1") {
                    return "三会一课";
                } else if (value == "2") {
                    return "党组织生活会";
                } else if (value == "3") {
                    return "民主生活会";
                } else if (value == "4") {
                    return "主题党日";
                } else if (value == "5") {
                    return "民主评议";
                } else if (value == "6") {
                    return "党建述评考";
                } else {
                    return "未知";
                }
            }
        }, {
            field: 'points',
            width: '100px',
            title: '获取积分',
            formatter: function (value, row, index) {
                return value + "分"
            }
        }, {
            field: 'remark',
            width: '300px',
            title: '备注'
        }, {
            field: 'opt',
            title: '操作',
            align: 'center',
            width: '180px',
            formatter: function (value, row, index) {
                return createOptBtn(row.recordId);
            }
        }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list
                    //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

// 创建表格行操作按钮
function createOptBtn(recordId) {
    var html = "<a href=\"javascript:void(0);edit('" + recordId + "')\" class=\"btn btn-primary btn-xs\">编辑</a>";
    return html;
}

function goback() {
    $("#editdiv").hide();
    $("#listdiv").show();
}

function edit(recordId) {
    $("#listdiv").hide();
    $("#editdiv").show();
    $(".js-back-btn").unbind("click").click(function () {
        goback();
    })
    document.getElementById("form1").reset();
    $("#memberId").attr("data-value", "");
    $.ajax({
        url: "/ret/partymemberget/getPartyMemberPointsById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "memberId") {
                        $("#" + id).attr("data-value", recordInfo[id]);
                        $("#" + id).val(getPartyMemberName(recordInfo[id]));

                    } else {
                        $("#" + id).val(recordInfo[id]);
                    }
                }
                $(".js-update-save").unbind("click").click(function () {
                    updatePartyMemberPoints(recordId);
                })
                $(".js-delete").unbind("click").click(function () {
                    deletePartyMemberPoints(recordId);
                })
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function deletePartyMemberPoints(recordId) {
    if (confirm(sysmsg['OPT_CONFIRM_DELETE'])) {
        $.ajax({
            url: "/set/partymemberset/deletePartyMemberPoints",
            type: "post",
            dataType: "json",
            data: {
                recordId: recordId
            },
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable("refresh");
                    goback();
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    }
}


function updatePartyMemberPoints(recordId) {
    $.ajax({
        url: "/set/partymemberset/updatePartyMemberPoints",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId,
            memberId: $("#memberId").attr("data-value"),
            item: $("#item").val(),
            pointsYear: $("#pointsYear").val(),
            points: $("#points").val(),
            remark: $("#remark").val()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                $("#myTable").bootstrapTable("refresh");
                goback();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

// 查询参数
function queryParams(params) {
    var temp = {
        search: "",
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        deleted: 0,
        pointsYear: $("#pointsYearQuery").val()
    };
    return temp;
}


function getPartyMemberName(memberIds) {
    var userNameStr = "";
    $.ajax({
        url: "/ret/partymemberget/getPartyMemeberByIds",
        type: "post",
        dataType: "json",
        async: false,
        data: {
            memberIds: memberIds
        },
        success: function (data) {
            if (data.status == "200") {
                var userNameArr = [];
                if (data.list) {
                    for (var i = 0; i < data.list.length; i++) {
                        userNameArr.push(data.list[i].userName);
                    }
                }
                userNameStr = userNameArr.join(",");
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
    return userNameStr;
}
