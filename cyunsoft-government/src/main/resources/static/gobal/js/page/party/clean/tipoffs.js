$(function () {
    getCodeClass("tipoffsType", "party_tipoffs");
    $("#createbut").unbind("click").click(function () {
        addExposeRecord();
    })
    jeDate("#tipoffsTime", {
        format: "YYYY-MM-DD"
    });
    $('#content').summernote({height: 300});
})

function addExposeRecord() {
    if($("#title").val()=="")
    {
        layer.msg("标题不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partyset/insertPartyCleanTipoffs",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            title: $("#title").val(),
            tipoffsType: $("#tipoffsType").val(),
            tipoffsUserName: $("#tipoffsUserName").val(),
            tipoffsTime: $("#tipoffsTime").val(),
            content: $("#content").code(),
            attach: $("#attach").attr("data_value")
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                window.location.reload();
                layer.msg(sysmsg[data.msg]);
            }
        }
    })
}
