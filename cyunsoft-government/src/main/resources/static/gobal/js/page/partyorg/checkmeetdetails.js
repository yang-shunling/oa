$(function () {
    $.ajax({
        url: "/ret/partyorgget/getPartyCheckMeetById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", recordInfo.attach);
                        createAttach("attach", 1);
                    } else if (id == "partyOrgId") {
                        $.ajax({
                            url: "/ret/partyorgget/getPartyOrgById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {partyOrgId: recordInfo[id]},
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#" + id).html(res.list.partyOrgName);
                                    } else {
                                        $("#" + id).val("");
                                    }
                                }
                            }
                        });

                    } else if (id == "checkStatus") {
                        if (recordInfo[id] == "1") {
                            $("#" + id).html("开展");
                        } else if (recordInfo[id] == "2") {
                            $("#" + id).html("未开展");
                        } else {
                            $("#" + id).html("未知");
                        }
                    } else if (id == "checkLevel") {
                        if (recordInfo[id] == "1") {
                            $("#" + id).html("好");
                        } else if (recordInfo[id] == "2") {
                            $("#" + id).html("较好");
                        } else if (recordInfo[id] == "3") {
                            $("#" + id).html("一般");
                        } else if (recordInfo[id] == "4") {
                            $("#" + id).html("差");
                        } else {
                            $("#" + id).html("未知");
                        }
                    } else if (id == "checkYear") {
                        $("#" + id).html(recordInfo[id] + "年度");
                    } else {
                        $("#" + id).html(recordInfo[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})
