$(function () {
    jeDate("#loseTime", {
        format: "YYYY-MM-DD"
    });
    $('#reason').summernote({height: 300});
    $("#createbut").unbind("click").click(function () {
        addloserecord()
    })
})

function addloserecord() {
    if($("#memberId").attr("data-value")=="")
    {
        layer.msg("党员不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partymemberset/insertPartyMemberLose",
        type: "post",
        dataType: "json",
        data: {
            memberId: $("#memberId").attr("data-value"),
            loseType: $("#loseType").val(),
            loseTime: $("#loseTime").val(),
            remark: $("#remark").val(),
            reason: $("#reason").code()
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                window.location.reload();
                layer.msg(sysmsg[data.msg]);
            }
        }
    })
}
