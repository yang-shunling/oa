$(function () {
    jeDate("#approvalTime", {
        format: "YYYY-MM-DD",
        minDate: getSysDate(),
        isinitVal: true
    });
    $('#remark').summernote({height: 300});
    $(".js-add-save").unbind("click").click(function () {
        addRebuildRecord();
    })

    $.ajax({
        url: "/ret/partyorgget/getPartyOrgTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            $.fn.zTree.init($("#menuTree"), setting1, data);
        }
    });
    $("#partyOrgId").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#menuContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });

    $.ajax({
        url: "/ret/partyparamget/getPartyTypeTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            $.fn.zTree.init($("#newTypeTree"), newTypesetting, data);
        }
    });
    $("#newType").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#newTypeContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });

    $("body").unbind("click").click(function () {
        $(".menuContent").hide();
    });

    $(".menuContent").unbind("click").click(function (e) {
        e.stopPropagation();
    });
})

function addRebuildRecord() {
    $.ajax({
        url: "/set/partyorgset/insertPartyRebuild",
        type: "post",
        dataType: "json",
        data: {
            partyOrgId: $("#partyOrgId").attr("data-value"),
            oldType: $("#oldType").attr("data-value"),
            newType: $("#newType").attr("data-value"),
            newName: $("#newName").val(),
            newShortName: $("#newShortName").val(),
            approvalTime: $("#approvalTime").val(),
            docNum: $("#docNum").val(),
            remark: $("#remark").code(),
            attach: $("#attach").attr("data_value")
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                window.location.reload();
                layer.msg(sysmsg[data.msg]);
            }
        }
    })
}

var newTypesetting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyparamget/getPartyTypeTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("newTypeTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.id;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].sortName + ",";
                vid += nodes[i].sortId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#newType");
            idem.attr("data-value", vid);
            idem.val(v);
        }
    }
};
var setting1 = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyorgget/getPartyOrgTree",// Ajax 获取数据的 URL 地址
        autoParam: ["partyOrgId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        // 禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "partyOrgId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "partyOrgName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("menuTree");
            var nodes = zTree.getSelectedNodes();
            var v = "";
            var vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.partyOrgId;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].partyOrgName + ",";
                vid += nodes[i].partyOrgId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#partyOrgId");
            idem.val(v);
            idem.attr("data-value", vid);
            var partyType = nodes[0].partyType;
            $("#oldType").attr("data-value", partyType);
            $.ajax({
                url: "/ret/partyparamget/getPartyTypeById",
                type: "post",
                dataType: "json",
                data: {
                    sortId: partyType
                },
                success: function (data) {
                    if (data.status == "200") {
                        $("#oldType").html(data.list.sortName);
                    } else if (data.status == "100") {
                        layer.msg(sysmsg[data.msg]);
                    } else if (data.status == "500") {
                        console.log(data.msg);
                    }
                }
            })
        }
    }
};
