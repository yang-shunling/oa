$(function () {
    $(".js-add-save").unbind("click").click(function () {
        addPayStandard();
    })
})

function addPayStandard() {
    var userName = "";
    if ($("#memberId").val() != "") {
        userName = $("#memberId").val();
    } else {
        userName = $("#userName").val();
    }
    $.ajax({
        url: "/set/partyset/insertPartyPayStandard",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            memberId: $("#memberId").attr("data-value"),
            userName: userName,
            je: $("#je").val(),
            remark: $("#remark").val()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}
