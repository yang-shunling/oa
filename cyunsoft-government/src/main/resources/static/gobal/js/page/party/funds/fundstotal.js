$(function () {
    getBiPartyFundsByMonthLine();
    getPartyFundsPie();
    getBiPartyFundsAnalysis();
})


function getBiPartyFundsAnalysis() {
    $.ajax({
        url: "/ret/partychartsget/getBiPartyFundsAnalysis",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var option = data.list;
                var myChart = echarts.init(document.getElementById('main3'));
                myChart.setOption(option);
                window.addEventListener("resize", function () {
                    myChart.resize();
                });
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.list);
            }
        }
    })
}

function getPartyFundsPie() {
    $.ajax({
        url: "/ret/partychartsget/getPartyFundsPie",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var option = data.list;
                var myChart = echarts.init(document.getElementById('main2'));
                myChart.setOption(option);
                window.addEventListener("resize", function () {
                    myChart.resize();
                });
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.list);
            }
        }
    })
}

function getBiPartyFundsByMonthLine() {
    $.ajax({
        url: "/ret/partychartsget/getBiPartyFundsByMonthLine",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var option = data.list;
                var myChart = echarts.init(document.getElementById('main1'));
                myChart.setOption(option);
                window.addEventListener("resize", function () {
                    myChart.resize();
                });
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.list);
            }
        }
    })
}
