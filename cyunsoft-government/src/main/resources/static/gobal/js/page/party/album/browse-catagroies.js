$(function () {
    getSubscribeUserList();
    if (albumTypeId != "") {
        $(".js-tab").each(function (index) {
            var albumType = $(this).attr("data-value");
            if (albumTypeId != albumType) {
                $(this).removeClass("uk-active");
            } else {
                $(this).addClass("uk-active");
            }
        })
        query(albumTypeId);
    } else {
        $(".js-tab").each(function (index) {
            if (index == 0) {
                var albumType = $(this).attr("data-value");
                query(albumType);
            }
        })
    }
})

function setTab(Obj) {
    $(".js-tab").each(function () {
        $(this).removeClass("uk-active");
    })
    $(Obj).addClass("uk-active");
    var albumType = $(Obj).attr("data-value");
    $("#myTable").bootstrapTable("destroy");
    query(albumType);
}

function getSubscribeUserList() {
    $.ajax({
        url: "/ret/partyget/getSubscribeUserList",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                if(data.list)
                {
                    var userList = data.list;
                    for (var i = 0; i < userList.length; i++) {
                        var tempLate = ['<li> <a href="#"> <img src="/sys/file/getOtherHeadImg?headImg=' + userList[i].headImg + '" alt="">',
                            '                                    <span>' + userList[i].userName + ' </span> <span class="dot-notiv"></span></a></li>'].join("");
                        $("#mySubscribeUserList").append(tempLate);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function query(albumType) {
    console.log(albumType);
    $("#myTable").bootstrapTable({
        url: '/ret/partyget/getAlbumVideoByAlbumType?albumType=' + albumType,
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: false,//是否显示 数据条数选择框
        sortable: true,//排序
        search: false,//启用搜索
        showColumns: false,//是否显示 内容列下拉框
        showRefresh: false,//显示刷新按钮
        idField: 'videoId',//key值栏位
        clickToSelect: false,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                $("#videoList").empty();
                var infoList = res.list.list;
                for (var i = 0; i < infoList.length; i++) {
                    var template = ['                          <div class="col-md-4">',
                        '                            <a href="/app/core/party/album/single-video?attachId=' + infoList[i].attach + '&videoId=' + infoList[i].videoId + '">',
                        '                                <div  class="catagroy-card animate-this col-md-12" style="background-image: url(/sys/file/getVideoPic?attachId=' + infoList[i].attach + '&fileName=' + infoList[i].pics + ');" uk-img>',
                        '                                    <div class="catagroy-card-content">',
                        '                                        <h4> ' + infoList[i].title + ' </h4>',
                        '                                    </div>',
                        '                                </div>',
                        '                            </a>',
                        '                        </div>'].join("");
                    $("#videoList").append(template);
                }
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order
    };
    return temp;
};
