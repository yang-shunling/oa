$(function () {
    $('#content').summernote({height: 200});
    $(".js-add-save").unbind("click").click(function () {
        insertPartyUnionTip();
    })
    getCodeClass("tipType", "partyunion_tip");
})

function insertPartyUnionTip() {
    if($("#title").val()=="")
    {
        layer.msg("标题不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partyunionset/insertPartyUnionTip",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            tipType: $("#tipType").val(),
            title: $("#title").val(),
            userName: $("#userName").val(),
            isFlag: $('input[name="isFlag"]:checked').val(),
            tel: $("#tel").val(),
            address: $("#address").val(),
            content: $("#content").code(),
            attach: $("#attach").attr("data_value")
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                window.location.reload();
                layer.msg(sysmsg[data.msg]);
            }
        }
    })
}
