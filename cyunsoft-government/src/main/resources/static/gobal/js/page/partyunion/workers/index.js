$(function () {
    jeDate("#joinTime", {
        format: "YYYY-MM-DD"
    });
    jeDate("#leaveTime", {
        format: "YYYY-MM-DD"
    });
    jeDate("#birthday", {
        format: "YYYY-MM-DD"
    });
    $('#remark').summernote({height: 200});
    $(".js-add-save").unbind("click").click(function () {
        insertPartyUnionWorker();
    })
})

function insertPartyUnionWorker() {
    if($("#userName").val()=="")
    {
        layer.msg("人员不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partyunionset/insertPartyUnionWorker",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            userName: $("#userName").val(),
            cardId: $("#cardId").val(),
            homeAddress: $("#homeAddress").val(),
            tel: $("#tel").val(),
            birthday: $("#birthday").val(),
            workOrgName: $("#workOrgName").val(),
            job: $("#job").val(),
            joinTime: $("#joinTime").val(),
            leaveTime: $("#leaveTime").val(),
            remark: $("#remark").code()
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                window.location.reload();
                layer.msg(sysmsg[data.msg]);
            }
        }
    })
}
