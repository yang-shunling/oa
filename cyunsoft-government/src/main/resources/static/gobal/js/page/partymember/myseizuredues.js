$(function () {
    getMyduesInfo();
    var nowtime = getSysYear();
    $("#nowtime").html(nowtime);
    $("#nowtime").attr("data-value", nowtime);
    $(".js-reduceday").unbind("click").click(function () {
        reduceDay();
    });
    $(".js-addday").unbind("click").click(function () {
        addDay()
    });
})

function reduceDay() {
    var nowtime = $("#nowtime").attr("data-value");
    nowtime = parseInt(nowtime) - 1;
    $("#nowtime").html(nowtime);
    $("#nowtime").attr("data-value", nowtime);
    getMyduesInfo();
}

function addDay() {
    var nowtime = $("#nowtime").attr("data-value");
    nowtime = parseInt(nowtime) + 1;
    if (getSysYear() < nowtime) {
        layer.msg("年份不能超过当前年份！");
        return;
    } else {
        $("#nowtime").html(nowtime);
        $("#nowtime").attr("data-value", nowtime);
        getMyduesInfo();
    }
}

function getMyduesInfo() {
    var year = $("#nowtime").attr("data-value");
    $.ajax({
        url: "/ret/partyget/getMyPartyDeusList",
        type: "post",
        dataType: "json",
        data: {year: year},
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                if (recordInfo.length == 0) {
                    $("#tablePage1").html("<td colspan='11' align='center' style='font-size:14px;line-height:40px;'>未查到相关数据</td>");
                } else {
                    var html = "";
                    for (var i = 0; i < recordInfo.length; i++) {
                        html += "<tr>";
                        html += "<td>" + (i + 1) + "</td>";
                        html += "<td>" + recordInfo[i].userName + "</td>";
                        html += "<td>【" + recordInfo[i].year + "年度】</td>";
                        html += "<td>【" + recordInfo[i].month + "月份】</td>";
                        html += "<td>" + recordInfo[i].dues + "</td>";
                        html += "<td>" + recordInfo[i].realDues + "</td>";
                        html += "<td>";
                        if (recordInfo[i].duesType == "0") {
                            html += "其它";
                        } else if (recordInfo[i].duesType == "1") {
                            html += "个体经营党员党费";
                        } else if (recordInfo[i].duesType == "2") {
                            html += "离退休党员党费";
                        } else if (recordInfo[i].duesType == "3") {
                            html += "农民党员党费";
                        } else if (recordInfo[i].duesType == "4") {
                            html += "学生党员党费";
                        } else if (recordInfo[i].duesType == "5") {
                            html += "下岗失业党员党费";
                        } else if (recordInfo[i].duesType == "6") {
                            html += "生活有困难党员党费";
                        } else if (recordInfo[i].duesType == "7") {
                            html += "预备党员党费";
                        } else if (recordInfo[i].duesType == "8") {
                            html += "流动党员党费";
                        } else if (recordInfo[i].duesType == "9") {
                            html += "自愿多交党费";
                        } else {
                            html += "未知";
                        }
                        html += "</td>";
                        html += "<td>";
                        if (recordInfo[i].onTime == "0") {
                            html += "拖延";
                        } else if (recordInfo[i].onTime == "1") {
                            html += "按时";
                        } else {
                            html += "未知";
                        }
                        html += "</td>";
                        html += "<td>" + recordInfo[i].duesTime + "</td>";
                        html += "<td>" + recordInfo[i].remark + "</td>";
                        html += "<td><a href=\"javascript:void(0);seizureduesdetails('" + recordInfo[i].recordId + "')\" class=\"btn btn-sky btn-xs\" >详情</a></td>";
                        html += "</tr>";
                    }
                    $("#tablePage1").html(html);
                }
            } else if (data.status == "100") {
                $("#tablePage1").html("<td colspan='11' align='center' style='font-size:14px;line-height:40px;'>" + data.msg + "</td>");
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function seizureduesdetails(recordId) {
    window.open("/app/core/party/seizureduesdetails?recordId=" + recordId);
}

function getFormatDate(arg) {
    if (arg == undefined || arg == '') {
        return '';
    }

    var re = arg + '';
    if (re.length < 2) {
        re = '0' + re;
    }
    return re;
}
