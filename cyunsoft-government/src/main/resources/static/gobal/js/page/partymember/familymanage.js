$(function () {
    jeDate("#birthday", {
        format: "YYYY-MM-DD"
    });
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })
    query();
})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/partymemberget/getMemberFamilyList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',// 工具列
        striped: true,// 隔行换色
        cache: false,// 禁用缓存
        pagination: true,// 启动分页
        sidePagination: 'server',// 分页方式
        pageNumber: 1,// 初始化table时显示的页码
        pageSize: 10,// 每页条目
        showFooter: false,// 是否显示列脚
        showPaginationSwitch: true,// 是否显示 数据条数选择框
        sortable: true,// 排序
        search: false,// 启用搜索
        sortOrder: "asc",
        showColumns: false,// 是否显示 内容列下拉框
        showRefresh: false,// 显示刷新按钮
        idField: 'recordId',// key值栏位
        clickToSelect: true,// 点击选中checkbox
        pageList: [10, 20, 30, 50],// 可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',// 标题 可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'memberUserName',
            title: '党员姓名',
            width: '50px',
            formatter:function (value,row,index)
            {
                return "<a href=\"javascript:void(0);details('" + row.recordId + "')\" style='cursor: pointer'>"+value+"</a>";
            }
        }, {
            field: 'partyOrgName',
            title: '党组织名称',
            width: '100px'
        }, {
            field: 'userName',
            width: '50px',
            title: '成员姓名'
        }, {
            field: 'userSex',
            width: '50px',
            title: '性别'
        }, {
            field: 'birthday',
            width: '100px',
            title: '出生日期'
        }, {
            field: 'relation',
            width: '100px',
            title: '关系',
            formatter: function (value, row, index) {
                if (value == "1") {
                    return "丈夫";
                } else if (value == "2") {
                    return "妻子";
                } else if (value == "3") {
                    return "子女";
                } else if (value == "4") {
                    return "父母";
                } else if (value == "5") {
                    return "兄弟";
                } else if (value == "6") {
                    return "姐妹";
                } else {
                    return "未知";
                }
            }
        }, {
            field: 'workUnit',
            width: '100px',
            title: '工作单位'
        }, {
            field: 'opt',
            title: '操作',
            align: 'center',
            width: '120px',
            formatter: function (value, row, index) {
                return createOptBtn(row.recordId);
            }
        }],
        onClickCell: function (field, value, row, $element) {
            // alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, // 总页数,前面的key必须为"total"
                    rows: res.list.list
                    // 行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        userName: $("#userNameQuery").val(),
        memberId: $("#memberIdQuery").attr("data-value")
    };
    return temp;
};

function createOptBtn(recordId) {
    var html = "<a href=\"javascript:void(0);edit('" + recordId + "')\" class=\"btn btn-primary btn-xs\">编辑</a>&nbsp;&nbsp;" +
        "<a href=\"javascript:void(0);deleterecord('" + recordId + "')\" class=\"btn btn-darkorange btn-xs\" >删除</a>";
    return html;
}

function edit(recordId) {
    $("#familylistdiv").hide();
    $("#familydiv").show();
    $(".js-back-btn").unbind("click").click(function () {
        goback();
    })
    $.ajax({
        url: "/ret/partymemberget/getPartyFamilyRecordById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "memberId") {
                        $("#memberId").attr("data-value", recordInfo[id]);
                        $.ajax({
                            url: "/ret/partymemberget/getPartyMemberById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {
                                memberId: recordInfo[id]
                            },
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#" + id).val(res.list.userName);
                                    } else {
                                        $("#" + id).val("");
                                    }
                                }
                            }
                        });
                    } else if (id == "userSex") {
                        $("input[name='userSex'][value='" + recordInfo[id] + "']").attr("checked", "checked");
                    } else {
                        $("#" + id).val(recordInfo[id]);
                    }
                }
                $(".js-update-save").unbind("click").click(function () {
                    updatePartyMemberFamily(recordId);
                })
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function deleterecord(recordId) {
    if (confirm("确定删除当前记录吗？")) {
        $.ajax({
            url: "/set/partymemberset/deletePartyFamilyRecord",
            type: "post",
            dataType: "json",
            data: {
                recordId: recordId
            },
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable("refresh");
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    }
}

function goback() {
    $("#familydiv").hide();
    $("#familylistdiv").show();
}

function details(recordId) {
    window.open("/app/core/partymember/familydetails?recordId=" + recordId);
}

function updatePartyMemberFamily(recordId) {
    if($("#memberId").attr("data-value")==""&&$("#userName").val()=="")
    {
        layer.msg("党员不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partymemberset/updatePartyFamilyRecord",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId,
            sortNo: $("#sortNo").val(),
            memberId: $("#memberId").attr("data-value"),
            userName: $("#userName").val(),
            userSex: $("input[name='userSex']:checked").val(),
            tel: $("#tel").val(),
            relation: $("#relation").val(),
            birthday: $("#birthday").val(),
            positionStatus: $("#positionStatus").val(),
            workUnit: $("#workUnit").val(),
            userPost: $("#userPost").val(),
            workAddress: $("#workAddress").val(),
            homeAddress: $("#homeAddress").val(),
            remark: $("#remark").val()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}
