var zTree;
var setting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyorgget/getPartyOrgTree",// Ajax 获取数据的 URL 地址
        autoParam: ["partyOrgId"],// 异步加载时需要自动提交父节点属性的参数
    },
    callback: {
        onClick: zTreeOnClick
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "partyOrgId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "partyOrgName"
        }
    }
};
$(function () {
    query("");
    var topNode = [{
        partyOrgName: "全部",
        levelId: '',
        isParent: "true",
        partyOrgId: "",
        icon: "/gobal/img/org/org.png"
    }];
    zTree = $.fn.zTree.init($("#tree"), setting, topNode);// 初始化树节点时，添加同步获取的数据
    var nodes = zTree.getNodes();
    for (var i = 0; i < nodes.length; i++) {
        zTree.expandNode(nodes[i], true, false, false);//默认展开第一级节点
    }
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })

    $("#partyOrgId").unbind("click").click(function (e) {
        e.stopPropagation();
        $(".menuContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });
    $("#partyOrgIdQuery").unbind("click").click(function (e) {
        e.stopPropagation();
        $(".menuContent").css({
            "left": "80px",
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });
    $("body").unbind("click").click(function () {
        $(".menuContent").hide();
    });

    $(".menuContent").unbind("click").click(function (e) {
        e.stopPropagation();
    });
    jeDate("#yearQuery", {
        format: "YYYY"
    });
});

function zTreeOnClick(event, treeId, treeNode) {
    var zTree = $.fn.zTree.getZTreeObj("tree"), nodes = zTree.getSelectedNodes(), v = "";
    vid = "";
    nodes.sort(function compare(a, b) {
        return a.id - b.partyOrgId;
    });
    for (var i = 0, l = nodes.length; i < l; i++) {
        v += nodes[i].partyOrgName + ",";
        vid += nodes[i].partyOrgId + ",";
    }
    if (v.length > 0)
        v = v.substring(0, v.length - 1);
    if (vid.length > 0)
        vid = vid.substring(0, vid.length - 1);
    var idem = $("#partyOrgIdQuery");
    idem.val(v);
    idem.attr("data-value", vid);
}

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/partyget/getPartyVerifyAllList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        search: true,//启用搜索
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'recordId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',//标题  可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'userName',
            width: '100px',
            title: '姓名',
            formatter: function (value, row, index) {
                return "<a href=\"javascript:void(0);details('" + row.recordId + "')\" style='cursor: pointer'>" + value + "</a>";
            }
        }, {
            field: 'accountId',
            title: '关联账号',
            width: '50px'
        }, {
            field: 'userSex',
            width: '50px',
            title: '性别'
        }, {
            field: 'partyOrgName',
            width: '100px',
            title: '所属党组织'
        }, {
            field: 'partyStatus',
            title: '党籍状态',
            width: '100px',
            formatter: function (value, row, index) {
                if (value == "1") {
                    return "正式党员";
                } else if (value == "2") {
                    return "预备党员";
                } else if (value == "3") {
                    return "已出党";
                } else if (value == "4") {
                    return "已停止党籍";
                } else if (value == "5") {
                    return "已死亡";
                } else {
                    return "未知";
                }
            }
        }, {
            field: 'joinTime',
            title: '入党时间',
            width: '100px'
        }, {
            field: 'year',
            title: '考核年度',
            width: '100px'
        }, {
            field: 'score',
            title: '考核得分',
            width: '100px'
        }, {
            field: 'verifyTime',
            title: '考核时间',
            width: '100px'
        }, {
            field: 'opt',
            title: '操作',
            align: 'center',
            width: '120px',
            formatter: function (value, row, index) {
                return createOptBtn(row.recordId);
            }
        }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list
                    //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function createOptBtn(recordId) {
    var html = "<a href=\"javascript:void(0);edit('" + recordId + "')\" class=\"btn btn-primary btn-xs\">编辑</a>&nbsp;&nbsp;" +
        "<a href=\"javascript:void(0);deleteRecord('" + recordId + "')\" class=\"btn btn-darkorange btn-xs\" >删除</a>";
    return html;
}

function details(recordId) {
    window.open("/app/core/party/verifydetails?recordId=" + recordId);
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        year: $("#yearQuery").val(),
        partyOrgId: $("#partyOrgIdQuery").attr("data-value")
    };
    return temp;
};

function edit(recordId) {
    document.getElementById("form1").reset();
    $.ajax({
        url: "/ret/partyget/getPartyIntegralById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId,
        },
        success: function (data) {
            if (data.status == 200) {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    $("#" + id).val(recordInfo[id]);
                }
            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
    $("#verifyModal").modal("show");
    $(".js-save").unbind("click").click(function () {
        updatePartyIntegral(recordId)
    })
}

function updatePartyIntegral(recordId) {
    if (confirm("确定更新党员考核吗？")) {
        $.ajax({
            url: "/set/partyset/updatePartyIntegral",
            type: "post",
            dataType: "json",
            data: {
                recordId: recordId,
                score: $("#score").val(),
                verifyTeam: $("#verifyTeam").val(),
                verifyTime: $("#verifyTime").val(),
                verifyIdea: $("#verifyIdea").val()
            },
            success: function (data) {
                if (data.status == 200) {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable("refresh");
                    $("#verifyYearModal").modal("hide");
                } else if (data.status == 100) {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    }
}

function deleteRecord(recordId) {

    if (confirm("确定要删除当前考核记录吗？")) {
        $.ajax({
            url: "/set/partyset/deletePartyIntegral",
            type: "post",
            dataType: "json",
            data: {
                recordId: recordId,
            },
            success: function (data) {
                if (data.status == 200) {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable("refresh");
                } else if (data.status == 100) {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    }
}

