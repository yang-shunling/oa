package com.core136.service.personnel;

import com.core136.bean.personnel.PerDept;
import com.core136.mapper.personnel.PerDeptMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

@Service
public class PerDeptService {
    private PerDeptMapper perDeptMapper;

    @Autowired
    public void setPerDeptMapper(PerDeptMapper perDeptMapper) {
        this.perDeptMapper = perDeptMapper;
    }

    public int insertPerDept(PerDept perDept) {
        return perDeptMapper.insert(perDept);
    }

    public int deletePerDept(PerDept perDept) {
        return perDeptMapper.delete(perDept);
    }

    public int updatePerDept(Example example, PerDept perDept) {
        return perDeptMapper.updateByExampleSelective(perDept, example);
    }

    public PerDept selectOnePerDept(PerDept perDept) {
        return perDeptMapper.selectOne(perDept);
    }

}
