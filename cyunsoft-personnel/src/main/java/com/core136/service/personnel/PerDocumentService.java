package com.core136.service.personnel;

import com.core136.bean.personnel.PerDocument;
import com.core136.mapper.personnel.PerDocumentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

@Service
public class PerDocumentService {
    private PerDocumentMapper perDocumentMapper;

    @Autowired
    public void setPerDocumentMapper(PerDocumentMapper perDocumentMapper) {
        this.perDocumentMapper = perDocumentMapper;
    }

    public int insertPerDocument(PerDocument perDocument) {
        return perDocumentMapper.insert(perDocument);
    }

    public int deletePerDocument(PerDocument perDocument) {
        return perDocumentMapper.delete(perDocument);
    }

    public int updatePerDocument(Example example, PerDocument perDocument) {
        return perDocumentMapper.updateByExampleSelective(perDocument, example);
    }

    public PerDocument selectOnePerDocument(PerDocument perDocument) {
        return perDocumentMapper.selectOne(perDocument);
    }

}
