let ue=UE.getEditor('content');
$(function () {
    getCodeClass("noticeType", "notice");
    getSmsConfig("msgType", "notice");
    $("#editbut").unbind("click").click(function () {
        updateNotice();
    });
    jeDate("#sendTime", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTime", {
        format: "YYYY-MM-DD"
    });
    ue.addListener("ready", function () {
        $.ajax({
            url: "/ret/noticeget/getNoticeInfo",
            type: "post",
            dataType: "json",
            data: {
                noticeId: noticeId
            },
            success: function (data) {
                if (data.status == 200) {
                    for (var name in data.list) {
                        if (name == "content") {
                            ue.setContent(data.list[name]);
                        } else if (name == "attach") {
                            $("#noticeattach").attr("data_value", data.list[name]);
                            createAttach("noticeattach");
                        } else if (name == "attachPriv") {
                            $("input:radio[name='attachPriv'][value='" + data.list[name] + "']").prop("checked", "checked");
                        } else if (name == "userPriv") {
                            $("#userPriv").attr("data-value", data.list[name]);
                            $("#userPriv").val(getUserNameByStr(data.list[name]));
                        } else if (name == "deptPriv") {
                            $("#deptPriv").attr("data-value", data.list[name]);
                            $("#deptPriv").val(getDeptNameByDeptIds(data.list[name]));
                        } else if (name == "levelPriv") {
                            $("#levelPriv").attr("data-value", data.list[name]);
                            $("#levelPriv").val(getUserLevelStr(data.list[name]));
                        }else if (name == "redHead") {
                            $("#templateType").val();
                        } else {
                            $("#" + name).val(data.list[name]);
                        }
                    }
                } else if (data.status == 100) {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        })
    });
})


function updateNotice() {
    if ($("#noticeTitle").val() == "") {
        layer.msg("通知公告标题不能为空");
        return;
    }
    $.ajax({
        url: "/set/noticeset/updateNotice",
        type: "post",
        dataType: "json",
        data: {
            noticeId: noticeId,
            noticeTitle: $("#noticeTitle").val(),
            noticeType: $("#noticeType").val(),
            userPriv: $("#userPriv").attr("data-value"),
            deptPriv: $("#deptPriv").attr("data-value"),
            levelPriv: $("#levelPriv").attr("data-value"),
            content: ue.getContent(),
            sendTime: $("#sendTime").val(),
            endTime: $("#endTime").val(),
            readHead: $("#templateType").val(),
            attach: $("#noticeattach").attr("data_value"),
            isTop: $("input:radio[name='isTop']:checked").val(),
            attachPriv: $("input:radio[name='attachPriv']:checked").val(),
            msgType: getCheckBoxValue("msgType")
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                window.location.reload();
                layer.msg(sysmsg[data.msg]);
            }
        }
    })
}
