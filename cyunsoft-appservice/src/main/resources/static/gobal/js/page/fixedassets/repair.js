$(function () {
    jeDate("#fixedTime", {
        format: "YYYY-MM-DD"
    });
    $("#createbut").unbind("click").click(function () {
        addFixedRecord();
    })
})

function addFixedRecord() {
    if ($("#assetsCode").val() == "") {
        layer.msg("资产编号不能为空！");
        return;
    }
    $.ajax({
        url: "/set/fixedassetsset/insertFixedAssetsRepair",
        type: "post",
        dataType: "json",
        data: {
            assetsCode: $("#assetsCode").val(),
            applicant: $("#applicant").attr("data-value"),
            fixedUser: $("#fixedUser").attr("data-value"),
            fixedTime: $("#fixedTime").val(),
            problemDescription: $("#problemDescription").val(),
            remark: $("#remark").val(),
            attach: $("#fixedattach").attr("data_value")
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                window.location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}
