$(function () {
    jeDate("#issueYear", {
        format: "YYYY-MM-DD",
    });
    query();
})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/vehicleget/getVehicleDriverList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: false,//是否显示 数据条数选择框
        sortable: true,//排序
        search: false,//启用搜索
        sortOrder: "asc",
        showColumns: false,//是否显示 内容列下拉框
        showRefresh: false,//显示刷新按钮
        idField: 'driverId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',//标题  可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'userName',
            title: '司机姓名',
            sortable: true,
            width: '100px'
        }, {
            field: 'issueYear',
            width: '100px',
            title: '驾龄'
        }, {
            field: 'licenceType',
            width: '100px',
            title: '驾驶证类型',
            formatter: function (value, row, index) {
                if (value == "1") {
                    return "A1";
                } else if (value == "2") {
                    return "A2";
                } else if (value == "3") {
                    return "A3";
                } else if (value == "4") {
                    return "B1";
                } else if (value == "5") {
                    return "B2";
                } else if (value == "6") {
                    return "C1";
                } else if (value == "7") {
                    return "C2";
                } else if (value == "8") {
                    return "C3";
                } else if (value == "9") {
                    return "C4";
                } else if (value == "10") {
                    return "D";
                } else if (value == "11") {
                    return "E";
                } else if (value == "12") {
                    return "F";
                } else if (value == "13") {
                    return "M";
                } else if (value == "14") {
                    return "N";
                } else if (value == "15") {
                    return "P";
                }
            }
        }, {
            field: 'status',
            width: '100px',
            title: '状态',
            formatter: function (value, row, index) {
                if (value == "0") {
                    return "待班";
                } else if (value == "1") {
                    return "休假中";
                }
            }
        }, {
            field: 'remark',
            width: '300px',
            title: '备注'
        }, {
            field: 'opt',
            title: '操作',
            align: 'center',
            width: '120px',
            formatter: function (value, row, index) {
                return createOptBtn(row.driverId, row.status);
            }
        }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list
                    //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order
    };
    return temp;
};

function createOptBtn(driverId, status) {
    var html = "<a href=\"javascript:void(0);edit('" + driverId + "')\" class=\"btn btn-primary btn-xs\">编辑</a>&nbsp;&nbsp;"
        + "<a href=\"javascript:void(0);deleteReocrd('" + driverId + "')\" class=\"btn btn-darkorange btn-xs\" >删除</a>&nbsp;&nbsp;";
    if (status == "0") {
        html += "<a href=\"javascript:void(0);setStatus('" + driverId + "','1')\" class=\"btn btn-success btn-xs\" >休假</a>&nbsp;&nbsp;";
    } else {
        html += "<a href=\"javascript:void(0);setStatus('" + driverId + "','0')\" class=\"btn btn-purple btn-xs\" >待班</a>&nbsp;&nbsp;";
    }
    return html;
}

function goback() {
    $("#infodiv").hide();
    $("#listdiv").show();
}

function edit(driverId) {
    $("#infodiv").show();
    $("#listdiv").hide();
    $(".js-back-btn").unbind("click").click(function () {
        goback();
    })
    document.getElementById("form1").reset();
    $.ajax({
        url: "/ret/vehicleget/getVehicleDriverById",
        type: "post",
        dataType: "json",
        data: {
            driverId: driverId
        },
        success: function (data) {
            if (data.status == "200") {
                for (var id in data.list) {
                    if (id == "accountId") {
                        $("#" + id).attr("data-value", data.list[id]);
                        $("#" + id).val(getUserNameByStr(data.list[id]));
                    } else {
                        $("#" + id).val(data.list[id]);
                    }
                }
                $(".js-update-save").unbind("click").click(function () {
                    updateRecord(driverId);
                })
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function deleteRecord(driverId) {
    if (confirm(sysmsg['OPT_CONFIRM_DELETE'])) {
        $.ajax({
            url: "/set/vehicleset/deleteVehicleDriver",
            type: "post",
            dataType: "json",
            data: {
                driverId: driverId
            },
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable("refresh");
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else if (data.status == "500") {
                    console.log(data.msg);
                }
            }
        })
    }
}

function updateRecord(driverId) {
    $.ajax({
        url: "/set/vehicleset/updateVehicleDriver",
        type: "post",
        dataType: "json",
        data: {
            driverId: driverId,
            sortNo: $("#sortNo").val(),
            accountId: $("#accountId").attr("data-value"),
            licenceType: $("#licenceType").val(),
            issueYear: $("#issueYear").val(),
            remark: $("#remark").val()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                $("#myTable").bootstrapTable("refresh");
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function setStatus(driverId, status) {
    $.ajax({
        url: "/set/vehicleset/updateVehicleDriver",
        type: "post",
        dataType: "json",
        data: {
            driverId: driverId,
            status: status
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                $("#myTable").bootstrapTable("refresh");
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}
