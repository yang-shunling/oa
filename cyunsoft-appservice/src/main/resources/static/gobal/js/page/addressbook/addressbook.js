$(function () {
    crateAZlist();
    getCodeClassAndList("bookType", "addressbook");
    const handler = function (e) {
        if (location.hash === this.getAttribute('href')) {
            e.preventDefault();
            location.href = location.href.slice(0, -location.hash.length + 1)
        }
    }
    document.querySelectorAll('.btn').forEach(a => {
        a.addEventListener('click', handler, false);
    });
    $('.container').show();
    $('body').on('click', '.city-list p', function () {
        var type = $(this).attr("data-type")
        var recordId = $(this).attr("data-id");
        if (type == "ab") {
            getUserInfo(recordId)
        } else if (type == "friend") {
            getMyFriendInfo(recordId)
        } else {
            getMySharUserInfo(recordId);
        }
    });
    $('body').on('click', '.letter a', function () {
        $("#list").scrollTop(0);
        var s = $(this).html();
        if (s != "#") {
            $("#list").scrollTop($('#' + s + '1').offset().top);
        } else {
            $("#list").scrollTop($('#other1').offset().top);
        }
        $("#showLetter span").html(s);
        $("#showLetter").show().delay(500).hide(0);
    });
    $('body').on('onMouse', '.showLetter span', function () {
        $("#showLetter").show().delay(500).hide(0);
    });
    getMyAddressBookList();
    getAllUserLevelList();
    $(".js-query").unbind("click").click(function () {
        queryaddressbook();
    })
})

function queryaddressbook() {
    var search = $("#queryfield").val();
    if (search == "") {
        layer.msg("请输入查询条件！");
        return;
    } else {
        crateAZlist();
        $.ajax({
            url: "/ret/oaget/getQueryAddressBookList",
            type: "post",
            dataType: "json",
            data: {search: search},
            success: function (data) {
                if (data.status == "500") {
                    console.log(data.msg);
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    for (var i = 0; i < data.list.length; i++) {
                        if (data.list[i].firstPinYin != "") {
                            if (data.list[i].isMy == "true") {
                                $("#" + data.list[i].firstPinYin + "1").after("<p data-type='ab' data-id=\"" + data.list[i].recordId + "\">" + data.list[i].userName + "</p>")
                            } else {
                                $("#" + data.list[i].firstPinYin + "1").after("<p data-type='share' data-id=\"" + data.list[i].recordId + "\">" + data.list[i].userName + "</p>")
                            }
                        } else {
                            if (data.list[i].isMy == "true") {
                                $("#other1").after("<p data-type='ab' data-id=\"" + data.list[i].recordId + "\">" + data.list[i].userName + "</p>")
                            } else {
                                $("#other1").after("<p data-type='share' data-id=\"" + data.list[i].recordId + "\">" + data.list[i].userName + "</p>")
                            }

                        }
                    }

                }
            }
        })
    }
}

function importaddressbook() {
    $("#exdiv").modal("show");
}

function importAddressBookForExcel() {
    $.ajaxFileUpload({
        url: '/set/unitset/importAddressBookForExcel', //上传文件的服务端
        secureuri: false,  //是否启用安全提交
        async: false,
        dataType: 'json',   //数据类型
        fileElementId: 'file', //表示文件域ID
        success: function (data, status) {
            if (data.status == 200) {
                layer.msg(sysmsg[data.msg]);
                $("#exdiv").modal("hide");
            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        },
        //提交失败处理函数
        error: function (data, status, e) {
            layer.msg("文件上传出错!请检查文件格式!");
            console.log(data.msg);
        }
    });
}

function getMyFriendInfo(recordId) {
    $.ajax({
        url: "/ret/oaget/getMyFriendsInfo",
        type: "post",
        dataType: "json",
        data: {
            accountId: recordId
        },
        success: function (data) {
            if (data.status == 200) {
                $("#iUserName").html(data.list.userName);
                $("#iDeptName").html(data.list.deptName);
                $("#iUserPost").html(getUserLevelStr(data.list.levelId));
                $("#iBookType").html(data.list.bookType);
                $("#iTel").html(data.list.tel);
                $("#iMobileNo").html(data.list.mobileNo);
                $("#iEmail").html(data.list.email);
                $("#iAddress").html(data.list.address);
            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    })
}


function getMySharUserInfo(recordId) {
    $.ajax({
        url: "/ret/oaget/getAddressBookById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == 200) {
                $("#iUserName").html(data.list.userName);
                $("#iDeptName").html(data.list.deptName);
                $("#iUserPost").html(data.list.userPost);
                $("#iBookType").html(getCodeClassName(data.list.bookType, "addressbook"));
                $("#iTel").html(data.list.tel);
                $("#iMobileNo").html(data.list.mobileNo);
                $("#iEmail").html(data.list.email);
                $("#iAddress").html(data.list.address);
            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    })
}

function getmyfriendsbylevel(Obj) {
    crateAZlist();
    var levelId = $(Obj).attr("value");
    $.ajax({
        url: "/ret/oaget/getMyFriendsByLevel",
        type: "post",
        dataType: "json",
        data: {levelId: levelId},
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                for (var i = 0; i < data.list.length; i++) {
                    var pinyin = data.list[i].pinYin;
                    if (pinyin != "") {
                        var firstPinYin = pinyin.substring(0, 1).toUpperCase();

                        $("#" + firstPinYin + "1").after("<p data-type='friend' data-id=\"" + data.list[i].accountId + "\">" + data.list[i].userName + "</p>")
                    } else {
                        $("#other1").after("<p data-type='friend' data-id=\"" + data.list[i].accountId + "\">" + data.list[i].userName + "</p>")
                    }
                }

            }
        }
    })

}


function getUserInfo(recordId) {
    $.ajax({
        url: "/ret/oaget/getAddressBookById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == 200) {
                $("#iUserName").html(data.list.userName + "<a style=\"margin-left: 20px;cursor: pointer;\" data-value=\"" + data.list.recordId + "\" onclick=\"updatelinkmane(this);\">修改</a>" +
                    "<a style=\"margin-left: 20px;cursor: pointer;\" data-value=\"" + data.list.recordId + "\" onclick=\"deletelinkmane(this);\">删除</a>");
                $("#iDeptName").html(data.list.deptName);
                $("#iUserPost").html(data.list.userPost);
                $("#iBookType").html(getCodeClassName(data.list.bookType, "addressbook"));
                $("#iTel").html(data.list.tel);
                $("#iMobileNo").html(data.list.mobileNo);
                $("#iEmail").html(data.list.email);
                $("#iAddress").html(data.list.address);
            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    })
}

function deletelinkmane(Obj) {
    var recordId = $(Obj).attr("data-value");
    if (confirm(sysmsg['OPT_CONFIRM_DELETE'])) {
        $.ajax({
            url: "/set/oaset/deleteAddressBook",
            type: "post",
            dataType: "json",
            data: {
                recordId: recordId,
                sortNo: $("#sortNo").val(),
                userName: $("#userName").val(),
                bookType: $("#bookType").val(),
                deptName: $("#deptName").val(),
                userPost: $("#userPost").val(),
                tel: $("#tel").val(),
                mobileNo: $("#mobileNo").val(),
                email: $("#email").val(),
                address: $("#address").val(),
                isShare: $("#isShare").val()
            },
            success: function (data) {
                if (data.status == "500") {
                    console.log(data.msg);
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    layer.msg(sysmsg[data.msg]);
                    $("#createdivmodal").modal("hide");
                }
            }
        })
    }
}

function updatelinkmane(Obj) {
    document.getElementById("form1").reset();
    var recordId = $(Obj).attr("data-value");
    $.ajax({
        url: "/ret/oaget/getAddressBookById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == 200) {
                for (var id in data.list) {
                    $("#" + id).val(data.list[id]);
                }
                $("#createdivmodal").modal("show");
                $(".js-save").unbind("click").click(function () {
                    if ($("#userName").val() == "") {
                        layer.msg("姓名不能为空！");
                        return;
                    } else {
                        $.ajax({
                            url: "/set/oaset/updateAddressBook",
                            type: "post",
                            dataType: "json",
                            data: {
                                recordId: recordId,
                                sortNo: $("#sortNo").val(),
                                userName: $("#userName").val(),
                                bookType: $("#bookType").val(),
                                deptName: $("#deptName").val(),
                                userPost: $("#userPost").val(),
                                tel: $("#tel").val(),
                                mobileNo: $("#mobileNo").val(),
                                email: $("#email").val(),
                                address: $("#address").val(),
                                isShare: $("#isShare").val()
                            },
                            success: function (data) {
                                if (data.status == "500") {
                                    console.log(data.msg);
                                } else if (data.status == "100") {
                                    layer.msg(sysmsg[data.msg]);
                                } else {
                                    layer.msg(sysmsg[data.msg]);
                                    $("#createdivmodal").modal("hide");
                                }
                            }
                        })
                    }
                })
            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    })
}


function getCodeClassAndList(eId, module) {
    $.ajax({
        url: "/ret/sysget/getCodeClassByModule",
        type: "post",
        dataType: "json",
        data: {
            module: module
        },
        success: function (data) {
            if (data.status == 200) {
                var menuList = "";
                var shareList = "";
                var html = "<option value=''>请选择</option>";
                for (var i = 0; i < data.list.length; i++) {
                    html += "<option value='" + data.list[i].codeValue + "'>" + data.list[i].codeName + "</option>";
                    menuList += "<a href=\"javascript:void(0);\" data-value='" + data.list[i].codeValue + "' onclick=\"getmylinkman(this);\">" + data.list[i].codeName + "</a>";
                    shareList += "<a href=\"javascript:void(0);\" data-value='" + data.list[i].codeValue + "' onclick=\"getmysharelinkman(this);\">" + data.list[i].codeName + "</a>";
                }
                $("#" + eId).html(html);
                $("#mygroup").html(menuList);
                $("#myshare").html(shareList);
            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    })
}

function getAllUserLevelList() {
    $.ajax({
        url: "/ret/unitget/getUserLevelList",
        type: "post",
        dataType: "json",
        data: {},
        success: function (data) {
            if (data.status == 200) {
                var menuList = "";
                for (var i = 0; i < data.list.length; i++) {
                    menuList += "<a href=\"javascript:void(0);\" value='" + data.list[i].levelId + "' onclick=\"getmyfriendsbylevel(this);\">" + data.list[i].levelName + "</a>";
                }
                $("#myfriend").html(menuList);
            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function getMyAddressBookList() {
    crateAZlist();
    $.ajax({
        url: "/ret/oaget/getMyAddressBookList",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                for (var i = 0; i < data.list.length; i++) {
                    if (data.list[i].firstPinYin != "") {
                        $("#" + data.list[i].firstPinYin + "1").after("<p data-type='ab' data-id=\"" + data.list[i].recordId + "\">" + data.list[i].userName + "</p>")
                    } else {
                        $("#other1").after("<p data-type='ab' data-id=\"" + data.list[i].recordId + "\">" + data.list[i].userName + "</p>")
                    }
                }

            }
        }
    })
}

function getmylinkman(Obj) {
    crateAZlist();
    var bookType = $(Obj).attr("data-value");
    $.ajax({
        url: "/ret/oaget/getMyAddressBookList",
        type: "post",
        dataType: "json",
        data: {bookType: bookType},
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                for (var i = 0; i < data.list.length; i++) {
                    $("#" + data.list[i].firstPinYin + "1").after("<p data-type='ab' data-id=\"" + data.list[i].recordId + "\">" + data.list[i].userName + "</p>")
                }

            }
        }
    })
}

function getmysharelinkman(Obj) {
    crateAZlist();
    var bookType = $(Obj).attr("data-value");
    $.ajax({
        url: "/ret/oaget/getMyShareAddressBookList",
        type: "post",
        dataType: "json",
        data: {bookType: bookType},
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                for (var i = 0; i < data.list.length; i++) {
                    $("#" + data.list[i].firstPinYin + "1").after("<p data-type='share' data-id=\"" + data.list[i].recordId + "\">" + data.list[i].userName + "</p>")
                }

            }
        }
    })
}

function createaddressbook() {
    document.getElementById("form1").reset();
    $("#createdivmodal").modal("show");
    $(".js-save").unbind("click").click(function () {
        if ($("#userName").val() == "") {
            layer.msg("姓名不能为空！");
            return;
        } else {
            $.ajax({
                url: "/set/oaset/insertAddressBook",
                type: "post",
                dataType: "json",
                data: {
                    sortNo: $("#sortNo").val(),
                    userName: $("#userName").val(),
                    bookType: $("#bookType").val(),
                    deptName: $("#deptName").val(),
                    userPost: $("#userPost").val(),
                    tel: $("#tel").val(),
                    mobileNo: $("#mobileNo").val(),
                    email: $("#email").val(),
                    address: $("#address").val(),
                    isShare: $("#isShare").val()
                },
                success: function (data) {
                    if (data.status == "500") {
                        console.log(data.msg);
                    } else if (data.status == "100") {
                        layer.msg(sysmsg[data.msg]);
                    } else {
                        layer.msg(sysmsg[data.msg]);
                        $("#createdivmodal").modal("hide");
                    }
                }
            })
        }
    })
}


function crateAZlist() {
    var arr = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "#"];
    var htmlStr = "";
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] == "#") {
            htmlStr += "<div class=\"city-list\"><span class=\"city-letter\" id=\"other1\">" + arr[i] + "</span></div>";
        } else {
            htmlStr += "<div class=\"city-list\"><span class=\"city-letter\" id=\"" + arr[i] + "1\">" + arr[i] + "</span></div>";
        }
    }
    $(".city").html(htmlStr);
}
