$(function () {
    $('#sendbpm').modal({backdrop: 'static', keyboard: false});
    getDocNumByBpmFlow(flowId);
    $("#sendBpmBtn").unbind("click").click(function () {
        if ($("#flowTitle").val() == "" || $("#flowTitle").val() == null) {
            layer.msg("BPM标题不能为空！");
            return;
        }
        $.ajax({
            url: "/set/bpmset/startBpm",
            type: "post",
            dataType: "json",
            data: {
                flowId: flowId,
                flowTitle: $("#flowTitle").val(),
                follow: getCheckBoxValue("follow"),
                urgency: $("#urgency").val()
            },
            success: function (data) {
                if (data.status == 500) {
                    console.log(data.msg);
                } else if (data.status == 500) {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    $("#sendbpm").modal("hide");
                    //window.location.href = data.redirect;
                    open(data.redirect, "_self");
                }
            }
        });
    });
})

function getCheckBoxValue(name) {
    var returnStr = "";
    $('input[name="' + name + '"]:checked').each(function () {
        returnStr += $(this).val() + ",";
    });
    return returnStr;
}

function getDocNumByBpmFlow(flowId) {
    $.ajax({
        url: "/ret/bpmget/getDocNumByBpmFlow",
        type: "post",
        dataType: "json",
        data: {
            flowId: flowId
        },
        success: function (data) {
            if (data.status == 500) {
                console.log(data.msg);
            } else if (data.status == 500) {
                layer.msg(sysmsg[data.msg]);
            } else {
                $("#flowTitle").val(data.redirect)
            }
        }
    });
}
