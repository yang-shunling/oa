let ue = UE.getEditor("remark");
$(function () {
    jeDate("#beginTime", {
        format: "YYYY-MM-DD hh:mm"
    });
    jeDate("#endTime", {
        format: "YYYY-MM-DD hh:mm"
    });
    $(".js-add-save").unbind("click").click(function () {
        addLeadActivity();
    });
})

function addLeadActivity() {
    if ($("#title").val() == "") {
        layer.msg("行程标题不能为空！")
        return;
    }
    if ($("#beginTime").val() == ""||$("#endTime").val() == "") {
        layer.msg("开始时间与结束时间不能为空！")
        return;
    }
    $.ajax({
        url: "/set/oaset/insertLeadActivity",
        type: "post",
        dataType: "json",
        data: {
            title: $("#title").val(),
            leader: $("#leader").attr("data-value"),
            userPriv: $("#userPriv").attr("data-value"),
            deptPriv: $("#deptPriv").attr("data-value"),
            levelPriv: $("#levelPriv").attr("data-value"),
            remark: ue.getContent(),
            beginTime: $("#beginTime").val(),
            endTime: $("#endTime").val()
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                window.location.reload();
                layer.msg(sysmsg[data.msg]);
            }
        }
    })
}
