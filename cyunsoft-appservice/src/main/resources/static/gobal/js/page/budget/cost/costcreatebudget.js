let ue = UE.getEditor("remark");
$(function () {
    jeDate("#happenTime", {
        format: "YYYY-MM-DD",
    });
    $(".js-add-save").unbind("click").click(function () {
        addCost();
    });
    $.ajax({
        url: "/ret/budgetget/getBudgetAccountTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            var topNode = [{
                sortName: "TOP分类",
                isParent: "false",
                sortId: ""
            }];
            var newTreeNodes = topNode.concat(data);
            $.fn.zTree.init($("#menuTree"), setting1, newTreeNodes);
        }
    });
    $("#budgetAccount").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#menuContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });
    $("body").unbind("click").click(function () {
        $("#menuContent").hide();
    });

    $("#menuContent").unbind("click").click(function (e) {
        e.stopPropagation();
    });
    getMainProject();
    $("#projectId").unbind("change").change(function () {
        getChildProject();
    })
})

function addCost() {
    $.ajax({
        url: "/set/budgetset/insertBudgetCost",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            title: $("#title").val(),
            projectId: $("#projectId").val(),
            childProjectId: $("#childProjectId").val(),
            runId: $("#runId").val(),
            happenTime: $("#happenTime").val(),
            totalCost: $("#totalCost").val(),
            budgetAccount: $("#budgetAccount").attr("data-value"),
            attach: $("#attach").attr("data_value"),
            remark: ue.getContent()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

var setting1 = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/budgetget/getBudgetAccountTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("menuTree"),
                nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.id;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].sortName + ",";
                vid += nodes[i].sortId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#budgetAccount");
            idem.attr("data-value", vid);
            idem.val(v);
        }
    }
};


function getMainProject() {
    $.ajax({
        url: "/ret/budgetget/getBudgetProjectListForProject",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var datalist = data.list;
                var html = "<option value=''>请选择</option>";
                for (var i = 0; i < datalist.length; i++) {
                    html += "<option value='" + datalist[i].projectId + "'>" + datalist[i].title + "</option>";
                }
                $("#projectId").html(html);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function getChildProject() {
    $.ajax({
        url: "/ret/budgetget/getBudgetChildProjectListForProject",
        type: "post",
        dataType: "json",
        data: {parentId: $("#projectId").val()},
        success: function (data) {
            if (data.status == "200") {
                var datalist = data.list;
                var html = "<option value=''>请选择</option>";
                for (var i = 0; i < datalist.length; i++) {
                    html += "<option value='" + datalist[i].projectId + "'>" + datalist[i].title + "</option>";
                }
                $("#childProjectId").html(html);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}
