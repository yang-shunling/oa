$(function () {
    jeDate("#birthday", {
        format: "YYYY-MM-DD",
        maxDate: getSysTime()
    });
    getUserinfo();
    $(".js-setuserinfo").unbind("click").click(function () {
        setUserInfo();
    })
})

function setUserInfo() {
    $.ajax({
        url: "/set/unitset/updateMyUserInfo",
        type: "post",
        dataType: "json",
        data: {
            nick: $("#nick").val(),
            sign: $("#sign").val(),
            mobileNo: $("#mobileNo").val(),
            wxNo: $("#wxNo").val(),
            eMail: $("#eMail").val(),
            birthday: $("#birthday").val(),
            msgTip: $("#msgTip").val(),
            msgAudio: $("#msgAudio").val(),
            theme: $("#theme").val()
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                top.msgTip = $("#msgTip").val();
                top.msgAudio = $("#msgAudio").val();
                layer.msg(sysmsg[data.msg] + "注销后重新登陆生效!");
            }
        }
    })
}

function getUserinfo() {
    $.ajax({
        url: "/ret/unitget/getMyUserInfo",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                $("#userName").html(data.list.userName);
                $("#nick").val(data.list.nick);
                $("#sign").val(data.list.sign);
                $("#mobileNo").val(data.list.mobileNo);
                $("#wxNo").val(data.list.wxNo);
                $("#eMail").val(data.list.eMail);
                $("#birthday").val(data.list.birthday);
                $("#msgTip").val(data.list.msgTip);
                $("#msgAudio").val(data.list.msgAudio);
                $("#theme").val(data.list.theme);

            }
        }
    })
}
