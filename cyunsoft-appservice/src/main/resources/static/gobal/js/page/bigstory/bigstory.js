$(function () {
    $.ajax({
        url: "/ret/oaget/getBigStoryList",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                let recordInfo = data.list;
                let yearArray = [];
                for (let i = 0; i < recordInfo.length; i++) {
                    yearArray.push(recordInfo[i].happenYear);
                }
                let newYearArray = $.unique(yearArray);
                for (let i = 0; i < newYearArray.length; i++) {
                    if (i == 0) {
                        $("#firstyear").html(newYearArray[i] + "年");
                        for (let s = 0; s < recordInfo.length; s++) {
                            if (recordInfo[s].happenYear == newYearArray[i]) {
                                let html = "<li class=\"green bounceInDown\" style=\"margin-top: 0px; animation-duration: 2s; animation-timing-function: ease; animation-fill-mode: both;\">\n" +
                                    "<h3 style=\"display: block;\">" + recordInfo[s].happenTime.substring(5, 10) + "<span>" + recordInfo[s].happenYear + "</span></h3>\n" +
                                    "<dl style=\"display: block;\">\n" +
                                    "<dt><a href=\"#\" onclick=\"details('" + recordInfo[s].recordId + "');\" style=\"cursor: pointer;\">" + recordInfo[s].title + "<span>" + recordInfo[s].subheading + "</span></a></dt>\n" +
                                    "</dl>\n" +
                                    "</li>";
                                $(".first").parent("ul").append(html);
                            }
                        }
                    } else {
                        var html = "<div class=\"history-date\"><ul class=\"js_" + newYearArray[i] + "\">\n" +
                            "<h2 class=\"date02 bounceInDown\" style=\"margin-top: 0px; position: relative; animation-duration: 2s; animation-timing-function: ease; animation-fill-mode: both;\">\n" +
                            "<a style=\"display: inline-block;\" href=\"#nogo\">" + newYearArray[i] + "年</a>\n" +
                            "</h2>\n" +
                            "</ul>\n" +
                            "</div>\n";
                        $(".history").append(html);
                    }
                }
                for (var s = 0; s < recordInfo.length; s++) {
                    var html = "<li class=\"green bounceInDown\" style=\"margin-top: 0px; animation-duration: 2s; animation-timing-function: ease; animation-fill-mode: both;\">\n" +
                        "<h3 style=\"display: block;\">" + recordInfo[s].happenTime.substring(5, 10) + "<span>" + recordInfo[s].happenYear + "</span></h3>\n" +
                        "<dl style=\"display: block;\">\n" +
                        "<dt><a href=\"#\" onclick=\"details('" + recordInfo[s].recordId + "');\" style=\"cursor: pointer;\">" + recordInfo[s].title + "<span>" + recordInfo[s].subheading + "</span></a></dt>\n" +
                        "</dl>\n" +
                        "</li>";
                    $(".js_" + recordInfo[s].happenYear).append(html);
                }
                systole();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
});

function systole() {
    if (!$(".history").length) {
        return;
    }
    var $warpEle = $(".history-date"),
        $targetA = $warpEle.find("h2 a,ul li dl dt a"),
        parentH,
        eleTop = [];

    parentH = $warpEle.parent().height();
    $warpEle.parent().css({"height": 59});

    setTimeout(function () {

        $warpEle.find("ul").children(":not('h2:first')").each(function (idx) {
            eleTop.push($(this).position().top);
            $(this).css({"margin-top": -eleTop[idx]}).children().hide();
        }).animate({"margin-top": 0}, 1600).children().fadeIn();

        $warpEle.parent().animate({"height": parentH}, 2600);

        $warpEle.find("ul").children(":not('h2:first')").addClass("bounceInDown").css({
            "-webkit-animation-duration": "2s",
            "-webkit-animation-delay": "0",
            "-webkit-animation-timing-function": "ease",
            "-webkit-animation-fill-mode": "both"
        }).end().children("h2").css({"position": "relative"});

    }, 600);

    $targetA.click(function () {
        $(this).parent().css({"position": "relative"});
        $(this).parent().siblings().slideToggle();
        $warpEle.parent().removeAttr("style");
        return false;
    });
};

function details(recordId) {
    window.open("/app/core/oa/bigstorydetails?recordId=" + recordId);
}
