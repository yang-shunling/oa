let ue = UE.getEditor("remark")
$(function () {
    jeDate("#payTime", {
        format: "YYYY-MM-DD",
    });
    $(".js-add-save").unbind("click").click(function () {
        addCostApply();
    });
    $.ajax({
        url: "/ret/budgetget/getBudgetAccountTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            var topNode = [{
                sortName: "TOP分类",
                isParent: "false",
                sortId: ""
            }];
            var newTreeNodes = topNode.concat(data);
            $.fn.zTree.init($("#menuTree"), setting1, newTreeNodes);
        }
    });
    $.ajax({
        url: "/ret/budgetget/getProjectTreeList",
        type: "post",
        dataType: "json",
        success: function (data) {
            var topNode = [{
                sortName: "全部",
                isParent: "false",
                sortId: ""
            }];
            var newTreeNodes = topNode.concat(data);
            $.fn.zTree.init($("#ztree1"), setting, newTreeNodes);
        }
    });
    getCostApprovalUser();
    $("#budgetAccount").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#menuContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });
    $("#projectId").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#menuContent1").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });
    $("body").unbind("click").click(function () {
        $(".menuContent").hide();
    });

    $(".menuContent").unbind("click").click(function (e) {
        e.stopPropagation();
    });
})

function getCostApprovalUser() {
    $.ajax({
        url: "/ret/budgetget/getCostApprovalUser",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var list = data.list;
                var html1 = "";
                for (var i = 0; i < list.length; i++) {
                    html1 += "<option value=\"" + list[i].accountId + "\">" + list[i].userName + "</option>"
                }
                $("#approvalUser").html(html1);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function addCostApply() {
    $.ajax({
        url: "/set/budgetset/insertBudgetCostApply",
        type: "post",
        dataType: "json",
        data: {
            title: $("#title").val(),
            totalCost: $("#totalCost").val(),
            payTime: $("#payTime").val(),
            projectId: $("#projectId").attr("data-value"),
            budgetAccount: $("#budgetAccount").attr("data-value"),
            applyUser: $("#applyUser").attr("data-value"),
            approvalUser: $("#approvalUser").val(),
            status: '0',
            attach: $("#attach").attr("data_value"),
            remark: ue.getContent()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

var setting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/budgetget/getProjectTreeList",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("ztree1"),
                nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.id;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].sortName + ",";
                vid += nodes[i].sortId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#projectId");
            idem.attr("data-value", vid);
            idem.val(v);
        }
    }
};


var setting1 = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/budgetget/getBudgetAccountTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("menuTree"),
                nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.id;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].sortName + ",";
                vid += nodes[i].sortId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#budgetAccount");
            idem.attr("data-value", vid);
            idem.val(v);
        }
    }
};
