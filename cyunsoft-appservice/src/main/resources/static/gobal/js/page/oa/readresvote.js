var isAnonymous = 0;
$(function () {
    getVoteInfo();
    $.ajax({
        url: "/ret/oaget/getVoteItemsList",
        type: "post",
        dataType: "json",
        async: false,
        data: {voteId: voteId},
        success: function (data) {
            if (data.status == "200") {
                var itemList = data.list;
                for (var i = 0; i < itemList.length; i++) {
                    var title = itemList[i].title;
                    if (itemList[i].remark) {
                        title += "<br/><span style='margin-left: 20px;'>说明：" + itemList[i].remark + "</span>";
                    }

                    var optType = itemList[i].optType;
                    var recordId = itemList[i].recordId;
                    var total = itemList[i].total;
                    var childItem = getChildItem(voteId, itemList[i].recordId);
                    if (optType == "1") {
                        getRadioItem(i, recordId, total, title, childItem)
                    } else if (optType == "2") {
                        getCheckBoxItem(i, recordId, total, title, childItem)
                    }
                }

            } else if (data.statu == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
    getVoteResult();
})


function getChildItem(voteId, recordId) {
    var jsonArr;
    $.ajax({
        url: "/ret/oaget/getVoteChildItemsList",
        type: "post",
        dataType: "json",
        async: false,
        data: {voteId: voteId, recordId: recordId},
        success: function (data) {
            if (data.status == "200") {
                jsonArr = data.list;
            } else if (data.statu == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
    return jsonArr;
}

function getVoteResult() {
    $.ajax({
        url: "/ret/oaget/getVoteResult",
        type: "post",
        dataType: "json",
        data: {
            voteId: voteId
        },
        success: function (data) {
            if (data.status == "200") {
                var infoList = data.list;
                for (var i = 0; i < infoList.length; i++) {
                    var itemId = infoList[i].itemId;
                    var zs = infoList[i].zs;
                    var total = $("#" + itemId).attr("aria-valuemax")
                    var withValue = zs / total * 100;
                    $("#" + itemId).css("width", withValue + "%").attr("aria-valuenow", zs);
                    $("#" + itemId).find("span").html("投票比例：" + withValue + "%");
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function getVoteInfo() {
    $.ajax({
        url: "/ret/oaget/getVoteById",
        type: "post",
        dataType: "json",
        async: false,
        data: {
            voteId: voteId
        },
        success: function (data) {
            if (data.status == "200") {
                $("#voteTitle").html(data.list.title);
                isAnonymous = data.list.isAnonymous;
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function getRadioItem(index, recordId, total, title, arr) {
    var html = "<tr  class='itemrow' data-id=\"" + recordId + "\" optType='1'><td>" + (index + 1) + ":" + title;
    if (isAnonymous == "1") {
        html += "<a style='float:right;cursor: pointer;' onclick=\"votedetailsforuser('" + recordId + "');\">投票详情</a>"
    }
    for (var i = 0; i < arr.length; i++) {
        html += "<div class=\"radio\">【" + arr[i].title + "】&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + arr[i].remark + "</span></div>" +
            "<div class=\"progress\"><div id=\"" + arr[i].recordId + "\" parent-id=\"" + recordId + "\" class=\"progress-bar progress-bar-magenta\" role=\"progressbar\" aria-valuenow=\"\" aria-valuemin=\"0\" aria-valuemax=\"" + total + "\">" +
            "<span></span></div></div>";
    }
    html += "</td></tr>"
    $("#child-item-table").append(html);
}

function getCheckBoxItem(index, recordId, total, title, arr) {
    var html = "<tr class='itemrow' data-id=\"" + recordId + "\" optType='2'><td>" + (index + 1) + ":" + title;
    if (isAnonymous == "1") {
        html += "<a style='float:right;cursor: pointer;' onclick=\"votedetailsforuser('" + recordId + "');\">投票详情</a>"
    }
    for (var i = 0; i < arr.length; i++) {
        html += "<div class=\"checkbox\">【" + arr[i].title + "】&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + arr[i].remark + "</span></div>" +
            "<div class=\"progress\"><div id=\"" + arr[i].recordId + "\" parent-id=\"" + recordId + "\" class=\"progress-bar progress-bar-magenta\" role=\"progressbar\" aria-valuenow=\"\" aria-valuemin=\"0\" aria-valuemax=\"" + total + "\">" +
            "<span></div></div>";
    }
    html += "</td></tr>"
    $("#child-item-table").append(html);
}

function votedetailsforuser(recordId) {
    $("#userlistmoadl").modal("show");
    $.ajax({
        url: "/ret/oaget/voteDetailsForUser",
        type: "post",
        dataType: "json",
        async: false,
        data: {
            voteId: voteId,
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "200") {
                var arr = [];
                for (var i = 0; i < data.list.length; i++) {
                    arr.push(data.list[i].userName);
                }
                $("#form1").html(arr.join(","));
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}
