var vm = new Vue({
    el: '#app',  //实例化对象
    data: {
        leftModule: [],  //要存放的数据
        rightModule: []
    },
    methods: {
        //存放实例方法
    }
})

$(function () {
    var date2 = getHMTime(new Date());
    $("#nowtime").html(date2);
    getMyDeskConfig();
    InitiateWidgets();
    $(".column").sortable({
        connectWith: ".column",
        handle: ".portlet-header",
        cancel: ".portlet-toggle",
        placeholder: "portlet-placeholder ui-corner-all",
        update: function (event, ui) { // 更新排序之后
            getDeskConfigJson();
        }
    });
    $(".portlet")
        .addClass(
            "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all")
        .find(".portlet-header")
        .addClass("ui-widget-header ui-corner-all")
        .prepend(
            "<span class='ui-icon ui-icon-minusthick portlet-toggle'></span>");
    $(".portlet-toggle").click(function () {
        var icon = $(this);
        icon.toggleClass("ui-icon-minusthick ui-icon-plusthick");
        icon.closest(".portlet").find(".portlet-content").toggle();
    });
    getMyEmail();
    if (msgtime > 0) {
        setInterval("getMyEmail()", msgtime);
    }
    getMyShortcutMenu();
    getAllAttendConfigList();
    $.ajax({
        url: "/ret/oaget/getMyPicNewsListForDesk",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                var newsInfo = data.list;
                for (var i = 0; i < newsInfo.length; i++) {
                    $("#pol")
                        .append(
                            " <img src="
                            + newsInfo[i].imgUrl
                            + " class=\"newsimg\" onclick=\"readNews('"
                            + newsInfo[i].newsId
                            + "')\">")
                    if (i == 0) {
                        $("#num")
                            .append(" <span class='cut'> 1</span>")
                    } else {
                        $("#num").append(
                            " <span class='cut'>" + (i + 1)
                            + "</span>")
                    }
                }
                var CRT = 0;
                var w = $(".newsimg").width(), pol = $("#pol"), spans = $("#num span");
                spans.hover(function () {
                    var me = $(this);
                    me.addClass("cut").siblings(".cut").removeClass(
                        "cut");
                    spans.eq(CRT).clearQueue();
                    pol.stop().animate({
                        left: "-" + w * (CRT = me.index()) + "px"
                    }, "slow");
                }, function () {
                    anony();
                });
                var anony = function () {
                    CRT++;
                    CRT = CRT > spans.length - 1 ? 0 : CRT;
                    spans.eq(CRT).clearQueue().delay(5000).queue(
                        function () {
                            spans.eq(CRT).triggerHandler(
                                "mouseover");
                            anony();
                        });
                };
                anony();
            }
        }
    })
});

function getAllAttendConfigList() {
    $.ajax({
        url: "/ret/oaget/getMyAttendConfigList",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                var time = getTime();
                if (data.list.length == 0) {
                    $("#hasConfig").html("异常");
                }
                for (var i = 0; i < data.list.length; i++) {
                    $("#configName").html(data.list[i].title);
                    $("#hasConfig").html("正常");
                    $("#beginTime").html(data.list[i].beginTime);
                    $("#endTime").html(data.list[i].endTime);
                    if (data.list[i].beginTimeStatus == "0") {
                        if (data.list[i].beginTime > time) {
                            $("#attendstatus").html(portalmsg['PORTAL_CLOCK_IN']);
                            $(".js-attend").unbind("click").click(function () {
                                addattend(1);
                            })
                        } else {
                            $("#attendstatus").html(portalmsg['PORTAL_IN_LATE']);
                            $(".js-attend").unbind("click").click(function () {
                                remarkattend(1);
                            })
                        }
                    } else {
                        $("#attendstatus").html(portalmsg['PORTAL_COMPLETE_CLOCK_OUT']);
                        if (data.list[i].endTimeStatus == "0") {
                            if (data.list[i].endTime < time) {
                                $("#attendstatus").html(portalmsg['PORTAL_CLOCK_OUT']);
                                $(".js-attend").unbind("click").click(
                                    function () {
                                        addattend(2);
                                    })
                            } else {
                                $("#attendstatus").html(portalmsg['PORTAL_CLOCK_OUT_EARLY']);
                                $(".js-attend").unbind("click").click(
                                    function () {
                                        remarkattend(2);
                                    })
                            }
                        } else {
                            $("#attendstatus").html(portalmsg['PORTAL_COMPLETE_CLOCK_OUT']);
                        }
                    }
                }
            }
        }
    });
}

function getTime() {
    var myDate = new Date();
    var h = myDate.getHours(); // 获取当前小时数(0-23)
    var m = myDate.getMinutes(); // 获取当前分钟数(0-59)
    var s = myDate.getSeconds();
    var now = getNow(h) + ':' + getNow(m) + ":" + getNow(s);
    return now;
}

function remarkattend(status) {
    $("#remarkmodal").modal("show");
    $(".js-save").unbind("click").click(function () {
        $.ajax({
            url: "/set/oaset/insertAttend",
            type: "post",
            dataType: "json",
            data: {
                status: status,
                time: getTime(),
                type: '1',
                remark: $("#remark").val()
            },
            success: function (data) {
                if (data.status == "500") {
                    console.log(data.msg);
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    layer.msg(sysmsg[data.msg]);
                    getAllAttendConfigList();
                    $("#remarkmodal").modal("hide");
                }
            }
        });
    })
}

function addattend(status) {
    $.ajax({
        url: "/set/oaset/insertAttend",
        type: "post",
        dataType: "json",
        data: {
            status: status,
            time: getTime()
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                layer.msg(sysmsg[data.msg]);
                window.location.reload();
            }
        }
    });
}

function getMyDeskConfig() {
    $.ajax({
        url: "/ret/unitget/getMyHomePage",
        type: "post",
        dataType: "json",
        async: false,
        success: function (data) {
            if (data.status == "200") {
                let leftJson = [];
                let rightJson = [];
                if (data.list) {
                    leftJson = data.list.left;
                }
                if (data.list) {
                    rightJson = data.list.right;
                }
                for (let i = 0; i < leftJson.length; i++) {
                    vm.leftModule.push(getDeskHtml(leftJson[i].module));
                }
                for (let i = 0; i < rightJson.length; i++) {
                    vm.rightModule.push(getDeskHtml(rightJson[i].module))
                }
                for (let i = 0; i < leftJson.length; i++) {
                    if (leftJson[i].module == "news") {
                        getMyNews();
                        if (msgtime > 0) {
                            setInterval("getMyNews()", msgtime);
                        }
                    } else if (leftJson[i].module == "notice") {
                        getMyNotice();
                        if (msgtime > 0) {
                            setInterval("getMyNotice()", msgtime);
                        }
                    } else if (leftJson[i].module == "bpm") {
                        getMyBpm();
                        if (msgtime > 0) {
                            setInterval("getMyBpm()", msgtime);
                        }
                    } else if (leftJson[i].module == "myTask") {
                        getMyTask();
                        if (msgtime > 0) {
                            setInterval("getMyTask()", msgtime);
                        }
                    } else if (leftJson[i].module == "myMeeting") {
                        getMyMeeting();
                        if (msgtime > 0) {
                            setInterval("getMyMeeting()", msgtime);
                        }
                    } else if (leftJson[i].module == "myFile") {
                        getMyFile();
                        if (msgtime > 0) {
                            setInterval("getMyFile()", msgtime);
                        }
                    } else if (leftJson[i].module == "document1") {
                        getMyDocument1();
                        if (msgtime > 0) {
                            setInterval("getMyDocument1()", msgtime);
                        }
                    } else if (leftJson[i].module == "document2") {
                        getMyDocument2();
                        if (msgtime > 0) {
                            setInterval("getMyDocument2()", msgtime);
                        }
                    } else if (leftJson[i].module == "superversion") {
                        getSuperversionListForDesk();
                        if (msgtime > 0) {
                            setInterval("getSuperversionListForDesk()", msgtime);
                        }
                    } else if (leftJson[i].module == "calendar") {
                        getMyCalendar();
                        if (msgtime > 0) {
                            setInterval("getMyCalendar()", msgtime);
                        }
                    } else if (leftJson[i].module == "leadactivity") {
                        getLeadActivity();
                        if (msgtime > 0) {
                            setInterval("getLeadActivity()", msgtime);
                        }
                    } else if (leftJson[i].module == "bpmworkretention") {
                        getBpmZl();
                        if (msgtime > 0) {
                            setInterval("getBpmZl()", msgtime);
                        }
                    }else if (leftJson[i].module == "fileMap") {
                        getMyFileMapForDesk('1');
                        if (msgtime > 0) {
                            setInterval("getMyFileMapForDesk('1')", msgtime);
                        }
                    }
                }
                for (let i = 0; i < rightJson.length; i++) {
                    if (rightJson[i].module == "news") {
                        getMyNews();
                        if (msgtime > 0) {
                            setInterval("getMyNews()", msgtime);
                        }
                    } else if (rightJson[i].module == "notice") {
                        getMyNotice();
                        if (msgtime > 0) {
                            setInterval("getMyNotice()", msgtime);
                        }
                    } else if (rightJson[i].module == "bpm") {
                        getMyBpm();
                        if (msgtime > 0) {
                            setInterval("getMyBpm()", msgtime);
                        }
                    } else if (rightJson[i].module == "myTask") {
                        getMyTask();
                        if (msgtime > 0) {
                            setInterval("getMyTask()", msgtime);
                        }
                    } else if (rightJson[i].module == "myMeeting") {
                        getMyMeeting();
                        if (msgtime > 0) {
                            setInterval("getMyMeeting()", msgtime);
                        }
                    } else if (rightJson[i].module == "myFile") {
                        getMyFile();
                        if (msgtime > 0) {
                            setInterval("getMyFile()", msgtime);
                        }
                    } else if (rightJson[i].module == "document1") {
                        getMyDocument1();
                        if (msgtime > 0) {
                            setInterval("getMyDocument1()", msgtime);
                        }
                    } else if (rightJson[i].module == "document2") {
                        getMyDocument2();
                        if (msgtime > 0) {
                            setInterval("getMyDocument2()", msgtime);
                        }
                    } else if (rightJson[i].module == "superversion") {
                        getSuperversionListForDesk();
                        if (msgtime > 0) {
                            setInterval("getSuperversionListForDesk()", msgtime);
                        }
                    } else if (rightJson[i].module == "calendar") {
                        getMyCalendar();
                        if (msgtime > 0) {
                            setInterval("getMyCalendar()", msgtime);
                        }
                    } else if (rightJson[i].module == "leadactivity") {
                        getLeadActivity();
                        if (msgtime > 0) {
                            setInterval("getLeadActivity()", msgtime);
                        }
                    } else if (rightJson[i].module == "bpmworkretention") {
                        getBpmZl();
                        if (msgtime > 0) {
                            setInterval("getBpmZl()", msgtime);
                        }
                    }else if (rightJson[i].module == "fileMap") {
                        getMyFileMapForDesk("1");
                        if (msgtime > 0) {
                            setInterval("getMyFileMapForDesk('1')", msgtime);
                        }
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    })
}

function getBpmZl() {
    $.ajax({
        url: "/ret/echartsbpmget/getUserWorkForBar",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var option = data.list;
                var myChart = echarts.init(document.getElementById('bpmzl'));
                myChart.setOption(option);
                window.addEventListener("resize", function () {
                    myChart.resize();
                });
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.list);
            }
        }
    })
}

function getLeadActivity() {
    $.ajax({
        url: "/ret/oaget/getLeadActivityLsitForDesk",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == 200) {
                var infoList = data.list;
                var html = "";
                for (var i = 0; i < infoList.length; i++) {
                    var title = infoList[i].title;
                    html += "<tr><td class='td_inline'><span style='position: absolute;display: block;width: 65%'><a href=\"javascript:void(0);readActivity('"
                        + infoList[i].recordId
                        + "')\">" + infoList[i].leaderUserName + "&nbsp;&nbsp;《" + title + "》</a>"
                        + "</span><span style='float:right;'>时间:"
                        + infoList[i].beginTime
                        + "--"
                        + infoList[i].endTime
                        + "</span></td></tr>";
                }
                $("#leadbody").html(html);
            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}


function readActivity(recordId) {
    window.open("/app/core/leadactivity/details?recordId=" + recordId);
}

function readMeeting(meetingId) {
    window.open("/app/core/meeting/meetingdetails?meetingId=" + meetingId);
}

function getMyMeeting() {
    $.ajax({
        url: "/ret/meetingget/getMyMeetingListForDesk",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == 200) {
                var meetingList = data.list;
                var html = "";
                for (var i = 0; i < meetingList.length; i++) {
                    var subject = meetingList[i].subject;
                    html += "<tr><td class='td_inline'><span title='" + meetingList[i].name + "' style='position: absolute;display: block;width: 65%'><a href=\"javascript:void(0);readMeeting('"
                        + meetingList[i].meetingId
                        + "')\">" + subject + "</a>"
                        + "</span><span style='float:right;'>开会时间:"
                        + meetingList[i].beginTime
                        + "--"
                        + meetingList[i].endTime
                        + "</span></td></tr>";
                }
                $("#meetingbody").html(html);

            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function getMyFile() {
    $
        .ajax({
            url: "/ret/fileget/getMyPublicFolderInPrivForDesk",
            type: "post",
            dataType: "json",
            success: function (data) {
                if (data.status == 200) {
                    var fileList = data.list;
                    var html = "";
                    for (var i = 0; i < fileList.length; i++) {
                        html += "<tr><td class='td_inline'><span><a href=\"javascript:void(0);gopublicfile('"
                            + fileList[i].folderId
                            + "')\">" + fileList[i].folderName + "</a></span></td></tr>";
                    }
                    $("#filebody").html(html);

                } else if (data.status == 100) {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
}

function gopublicfile(folderId) {
    var url = "/app/core/file/publicfile?folderId=" + folderId;
    parent.openNewTabs(url, "公共文件柜");
}

function getMyBpm() {
    $.ajax({
            url: "/ret/bpmget/getMyProcessListForDesk",
            type: "post",
            dataType: "json",
            success: function (data) {
                if (data.status == 200) {
                    var infoList = data.list;
                    var html = "";
                    for (var i = 0; i < infoList.length; i++) {
                        let newHtml="";
                        if (infoList[i].recTime == undefined||infoList[i].recTime == "") {
                            newHtml += "<li class=\"label label-darkorange\">new</li>&nbsp;&nbsp;";
                        }
                        var flowTitle = infoList[i].flowTitle;
                        html += "<tr><td class='td_inline'><span title=\"" + infoList[i].prcsName + "\" style='position: absolute;display: block;width: 65%'><a href=\"javascript:void(0);doBpm('"
                            + infoList[i].runId + "','"
                            + infoList[i].runProcessId + "','"
                            + infoList[i].flowTitle.replace("\'","")
                            + "')\">"+newHtml+"NO:"+infoList[i].id +"&nbsp;&nbsp;"+flowTitle + "</a>"
                            + "</span><span style='float:right'>"
                            + infoList[i].createTime + "&nbsp;&nbsp;"
                            + infoList[i].createUser
                            + "</span></td></tr>";
                    }
                    $("#bpmtbody").html(html);

                } else if (data.status == 100) {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
}

function getMyBpmSendToMeForDesk() {
    $.ajax({
        url: "/ret/bpmget/getMySendToListForDesk",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == 200) {
                var infoList = data.list;
                var html = "";
                for (var i = 0; i < infoList.length; i++) {
                    var flowTitle = infoList[i].flowTitle;
                    html += "<tr><td class='td_inline'><span title=\"" + infoList[i].flowName + "\" style='position: absolute;display: block;width: 65%'><a href=\"javascript:void(0);readsendtobpm('"
                        + infoList[i].runId + "','"
                        + infoList[i].flowId + "','"
                        + infoList[i].sendToId
                        + "')\">NO:"+infoList[i].id +"&nbsp;&nbsp;"+flowTitle + "</a>"
                        + "</span><span style='float:right;'>"
                        + infoList[i].createTime + "&nbsp;&nbsp;"
                        + infoList[i].createUserName
                        + "</span></td></tr>";
                }
                $("#bpmtbody").html(html);
            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function getDraftsBpmFlowListForDesk() {
    $
        .ajax({
            url: "/ret/bpmget/getDraftsBpmFlowListForDesk",
            type: "post",
            dataType: "json",
            success: function (data) {
                if (data.status == 200) {
                    var infoList = data.list;
                    var html = "";
                    for (var i = 0; i < infoList.length; i++) {
                        var flowTitle = infoList[i].flowTitle;
                        html += "<tr><td class='td_inline'><span title=\"" + infoList[i].flowName + "\" style='position: absolute;display: block;width: 65%'>" +
                            "<a href=\"javascript:void(0);window.open('/app/core/bpm/bpmread?runId=" + infoList[i].runId + "&flowId=" + infoList[i].flowId + "')\">NO:"+infoList[i].id +"&nbsp;&nbsp;"+flowTitle + "</a>"
                            + "</span><span style='float:right'>"
                            + infoList[i].createTime + "&nbsp;&nbsp;"
                            + "<a href=\"javascript:void(0);setBpmEmptyStatus('" + infoList[i].runId + "')\" class=\"btn btn-sky btn-xs\" >加入待办</a>"
                            + "</span></td></tr>";
                    }
                    $("#bpmtbody").html(html);
                } else if (data.status == 100) {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
}

function setBpmEmptyStatus(runId) {
    if (confirm("您确定将当前记录加入待办吗？")) {
        $.ajax({
            url: "/set/bpmset/setBpmEmptyStatus",
            type: "post",
            dataType: "json",
            data: {runId: runId},
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    getDraftsBpmFlowListForDesk();
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data);
                }
            }
        });
    }
}

function readsendtobpm(runId, flowId, sendToId) {
    $.ajax({
        url: "/set/bpmset/updateSendToStatus",
        type: "post",
        dataType: "json",
        data: {
            sendToId: sendToId
        },
        success: function (data) {
            if (data.status == "200") {
                window.open("/app/core/bpm/bpmread?runId=" + runId + "&flowId="
                    + flowId);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function getMyDocument1() {
    $.ajax({
        url: "/ret/documentget/getMyProcessListForDesk",
        type: "post",
        dataType: "json",
        data: {
            documentType: 1
        },
        success: function (data) {
            if (data.status == 200) {
                var infoList = data.list;
                var html = "";
                for (var i = 0; i < infoList.length; i++) {
                    let newHtml="";
                    if (infoList[i].recTime == undefined||infoList[i].recTime == "") {
                        newHtml += "<li class=\"label label-darkorange\">new</li>&nbsp;&nbsp;";
                    }
                    var flowTitle = infoList[i].flowTitle;
                    html += "<tr><td class='td_inline'><span title='" + infoList[i].prcsName + "' style='position: absolute;display: block;width: 65%'><a href=\"javascript:void(0);doDocument('"
                        + infoList[i].runId + "','"
                        + infoList[i].runProcessId + "','"
                        + infoList[i].flowTitle
                        + "')\">"+newHtml+"NO:"+infoList[i].id+"&nbsp;&nbsp;"+ flowTitle + "</a>"
                        + "</span><span style=\"float:right\">"
                        + infoList[i].createTime + "&nbsp;&nbsp;"
                        + infoList[i].createUser
                        + "<span></td></tr>";
                }
                $("#documentbody1").html(html);

            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}


function getDraftsDocumentFlowListForDesk1() {
    $.ajax({
        url: "/ret/documentget/getDraftsDocumentFlowListForDesk1",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == 200) {
                var infoList = data.list;
                var html = "";
                for (var i = 0; i < infoList.length; i++) {
                    var flowTitle = infoList[i].flowTitle;
                    html += "<tr><td class='td_inline'><span title='" + infoList[i].flowName + "' style='position: absolute;display: block;width: 65%'><a href=\"javascript:void(0);doDocument('"
                        + infoList[i].runId + "','"
                        + infoList[i].runProcessId + "','"
                        + infoList[i].flowTitle
                        + "')\">NO:"+infoList[i].id+"&nbsp;&nbsp;"+ flowTitle + "</a>"
                        + "</span><span style=\"float:right\">"
                        + infoList[i].createTime + "&nbsp;&nbsp;"
                        + infoList[i].createUser
                        + "<span></td></tr>";
                }
                $("#documentbody1").html(html);

            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function getDraftsDocumentFlowListForDesk2() {
    $.ajax({
        url: "/ret/documentget/getDraftsDocumentFlowListForDesk2",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == 200) {
                var infoList = data.list;
                var html = "";
                for (var i = 0; i < infoList.length; i++) {
                    var flowTitle = infoList[i].flowTitle;
                    html += "<tr><td class='td_inline'><span title='" + infoList[i].flowName + "' style='position: absolute;display: block;width: 65%'><a href=\"javascript:void(0);doDocument('"
                        + infoList[i].runId + "','"
                        + infoList[i].runProcessId + "','"
                        + infoList[i].flowTitle
                        + "')\">NO:"+infoList[i].id+"&nbsp;&nbsp;"+ flowTitle + "</a>"
                        + "</span><span style=\"float:right\">"
                        + infoList[i].createTime + "&nbsp;&nbsp;"
                        + infoList[i].createUser
                        + "<span></td></tr>";
                }
                $("#documentbody2").html(html);

            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function getMyDocument2() {
    $.ajax({
        url: "/ret/documentget/getMyProcessListForDesk",
        type: "post",
        dataType: "json",
        data: {
            documentType: 2
        },
        success: function (data) {
            if (data.status == 200) {
                var infoList = data.list;
                var html = "";
                for (var i = 0; i < infoList.length; i++) {
                    let newHtml="";
                    if (infoList[i].recTime == undefined||infoList[i].recTime == "") {
                        newHtml += "<li class=\"label label-darkorange\">new</li>&nbsp;&nbsp;";
                    }
                    var flowTitle = infoList[i].flowTitle;
                    html += "<tr><td class='td_inline'><span title='" + infoList[i].prcsName + "' style='position: absolute;display: block;width: 65%'><a href=\"javascript:void(0);doDocument('"
                        + infoList[i].runId + "','"
                        + infoList[i].runProcessId + "','"
                        + infoList[i].flowTitle
                        + "')\">"+newHtml+"NO:"+infoList[i].id+"&nbsp;&nbsp;"+ flowTitle + "</a>"
                        + "</span><span style=\"float:right\">"
                        + infoList[i].createTime
                        + "&nbsp;&nbsp;"
                        + infoList[i].createUser
                        + "</span></td></tr>";
                }
                $("#documentbody2").html(html);

            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function getMyDocumentSendTo1() {
    $
        .ajax({
            url: "/ret/documentget/getMySendToListForDesk",
            type: "post",
            dataType: "json",
            data: {
                documentType: 1
            },
            success: function (data) {
                if (data.status == 200) {
                    var infoList = data.list;
                    var html = "";
                    for (var i = 0; i < infoList.length; i++) {
                        var flowTitle = infoList[i].flowTitle;
                        html += "<tr><td class='td_inline'><span title='" + infoList[i].flowName + "' style='position: absolute;display: block;width: 65%'>"
                            + "<a href=\"javascript:void(0);readsendtodocument('"
                            + infoList[i].runId + "','"
                            + infoList[i].flowId + "','"
                            + infoList[i].sendToId
                            + "')\">NO:"+infoList[i].id+"&nbsp;&nbsp;"+ flowTitle + "</a>"
                            + "</span><span style='float:right;'>"
                            + infoList[i].createTime
                            + "&nbsp;&nbsp;"
                            + infoList[i].createUserName
                            + "</span></td></tr>";
                    }
                    $("#documentbody1").html(html);
                } else if (data.status == 100) {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
}

function getMyDocumentSendTo2() {
    $
        .ajax({
            url: "/ret/documentget/getMySendToListForDesk",
            type: "post",
            dataType: "json",
            data: {
                documentType: 2
            },
            success: function (data) {
                if (data.status == 200) {
                    var infoList = data.list;
                    var html = "";
                    for (var i = 0; i < infoList.length; i++) {
                        var flowTitle = infoList[i].flowTitle;
                        html += "<tr><td class='td_inline'><span title='" + infoList[i].flowName + "' style='position: absolute;display: block;width: 65%'>"
                            + "<a href=\"javascript:void(0);readsendtodocument('"
                            + infoList[i].runId + "','"
                            + infoList[i].flowId + "','"
                            + infoList[i].sendToId
                            + "')\">NO:"+infoList[i].id+"&nbsp;&nbsp;"+ flowTitle + "</a>"
                            + "</span><span style='float:right;'>"
                            + infoList[i].createTime
                            + "&nbsp;&nbsp;"
                            + infoList[i].createUserName
                            + "</span></td></tr>";
                    }
                    $("#documentbody2").html(html);
                } else if (data.status == 100) {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
}

function readsendtodocument(runId, flowId, sendToId) {
    $.ajax({
        url: "/set/documentset/updateSendToStatus",
        type: "post",
        dataType: "json",
        data: {
            sendToId: sendToId
        },
        success: function (data) {
            if (data.status == "200") {
                window.open("/app/core/document/documentread?runId=" + runId
                    + "&flowId=" + flowId);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function getMyEmail() {
 $.ajax({
            url: "/ret/oaget/getEmailListForDesk",
            type: "post",
            dataType: "json",
            success: function (data) {
                if (data.status == 200) {
                    var datalist = data.list;
                    var html = "";
                    for (var i = 0; i < datalist.length; i++) {
                        var subject = datalist[i].subject;
                        html += "<tr><td class='td_inline'><span style='position: absolute;display: block;width: 65%'>";
                        if (datalist[i].readTime == null) {
                            html += "<li class=\"label label-darkorange\">new</li>&nbsp;&nbsp;";
                        }
                        html +="<a href=\"javascript:void(0);readEmail('"+ datalist[i].emailId+ "')\">" + subject + "</a>";

                        html += "</span><span style='float:right;'>"
                            + datalist[i].sendTime
                            + "&nbsp;&nbsp;"
                            + datalist[i].fromUserName
                            + "</span></td></tr>";
                    }
                    $("#emailtbody").html(html);
                } else if (data.status == 100) {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
}

function getMyCalendar() {
    $.ajax({
        url: "/ret/oaget/getMyCalendarListForDesk",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == 200) {
                var datalist = data.list;
                var html = "";
                for (var i = 0; i < datalist.length; i++) {
                    html += "<tr><td>" + datalist[i].title;
                    html += "</td><td style='width:150px'>"
                        + datalist[i].calendarStart
                        + "</td><td style='width:80px'>"
                        + datalist[i].createUserName + "</td></tr>";
                }
                $("#calendartbody").html(html);
            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function getMyTask() {
    $.ajax({
            url: "/ret/taskget/getTaskListForDesk2",
            type: "post",
            dataType: "json",
            success: function (data) {
                if (data.status == 200) {
                    var taskList = data.list;
                    var html = "";
                    for (var i = 0; i < taskList.length; i++) {
                        var text1 = taskList[i].text1;
                        html += "<tr><td class='td_inline'><span title='" + taskList[i].taskName + "' style='position: absolute;display: block;width: 65%'><a href=\"javascript:void(0);doTask('"
                            + taskList[i].taskDataId
                            + "')\">" + text1 + "</a></span><span style='float:right;'>"
                            + taskList[i].startDate
                            + "&nbsp;&nbsp;进度："
                            + (taskList[i].progress)
                            + "%</span></td></tr>";
                    }
                    $("#mytaskbody").html(html);

                } else if (data.status == 100) {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
}

function getMyNews() {
    $
        .ajax({
            url: "/ret/oaget/getMyNewsListForDesk",
            type: "post",
            dataType: "json",
            success: function (data) {
                if (data.status == 200) {
                    var newsList = data.list;
                    var html = "";
                    for (var i = 0; i < newsList.length; i++) {
                        html += "<tr><td class='td_inline'><span>";
                        if (newsList[i].readStatus == "true") {
                            html += "<li class=\"label label-darkorange\">new</li>&nbsp;&nbsp;";
                        }
                        var newsTitle = newsList[i].newsTitle;
                        if (newsTitle.length > 20) {
                            newsTitle = newsTitle.substring(0, 20) + "...";
                        }
                        html += "<a href=\"javascript:void(0);readNews('" + newsList[i].newsId + "')\">" + newsTitle + "</a><span style=\"float:right;\">" + newsList[i].sendTime + "&nbsp;&nbsp;" + newsList[i].createUser + "</span>"
                            + "</td></tr>";
                    }
                    $("#newstbody").html(html);

                } else if (data.status == 100) {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
}

function getMyNotice() {
    $.ajax({
        url: "/ret/noticeget/getMyNoticeListForDesk",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == 200) {
                var noticeList = data.list;
                var html = "";
                for (var i = 0; i < noticeList.length; i++) {
                    var noticeTitle = noticeList[i].noticeTitle;
                    html += "<tr><td class='td_inline'>";
                    html += "<span style='position: absolute;display: block;width: 65%'>";
                    if (noticeList[i].readStatus == "true") {
                        html += "<li class=\"label label-darkorange\">new</li>&nbsp;&nbsp;";
                    }
                    html += "<a href=\"javascript:void(0);readNotice('"
                        + noticeList[i].noticeId
                        + "')\">" + noticeTitle + "</a>";

                    html += "</span><span style='float:right;'>"
                        + noticeList[i].sendTime
                        + "&nbsp;&nbsp;"
                        + noticeList[i].createUser
                        + "</span></td></tr>";
                }
                $("#noticetbody").html(html);

            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function readEmail(eamilId) {
    parent.openNewTabs("/app/core/oa/emaildetails?emailId=" + eamilId, "电子邮件");
}

function readNews(newsId) {
    window.open("/app/core/news/readnews?newsId=" + newsId);
}

function doBpm(runId, runProcessId, title) {
    parent.openNewTabs("/app/core/bpm/dowork?runId=" + runId + "&runProcessId="
        + runProcessId, title);
}

function doTask(taskDataId) {
    window.open("/app/core/task/taskdatadetails?taskDataId=" + taskDataId);
}

function doDocument(runId, runProcessId, title) {
    parent.openNewTabs("/app/core/document/dowork?runId=" + runId
        + "&runProcessId=" + runProcessId, title);
}

function readNotice(noticeId) {
    window.open("/app/core/notice/details?noticeId=" + noticeId);
}

function getDeskHtml(module) {
    if (module == "news") {
        return myNews;
    } else if (module == "notice") {
        return myNotice;
    } else if (module == "calendar") {
        return calendarHtml;
    } else if (module == "email") {
        return "";
    } else if (module == "superversion") {
        return supHtml;
    } else if (module == "sms") {
        return "";
    } else if (module == "bpm") {
        return bpmHtml;
    } else if (module == "myTask") {
        return myTask;
    } else if (module == "myMeeting") {
        return myMeeting;
    } else if (module == "myFile") {
        return myFile;
    } else if (module == "document1") {
        return documentHtml1;
    } else if (module == "document2") {
        return documentHtml2;
    } else if (module == "leadactivity") {
        return leadactivityHtml;
    } else if (module == "bpmworkretention") {
        return bpmzlHtml;
    }else if (module == "fileMap") {
        return fileMap;
    }
}

function getDeskConfigJson() {
    var lArr = [];
    $("#leftdiv").find(".js-module").each(function () {
        var json = {};
        json.module = $(this).attr("module");
        json.moduleName = $(this).attr("moduleName");
        lArr.push(json);
    });
    var rArr = [];
    $("#rightdiv").find(".js-module").each(function () {
        var json = {};
        json.module = $(this).attr("module");
        json.moduleName = $(this).attr("moduleName");
        rArr.push(json);
    });
    var json = {};
    json.left = lArr;
    json.right = rArr;
    $.ajax({
        url: "/set/unitset/setDeskConfig",
        type: "post",
        dataType: "json",
        data: {
            homePage: JSON.stringify(json)
        },
        success: function (data) {
            if (data.status == "200") {
                // layer.msg(sysmsg[data.msg]);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }

        }
    });
}

function getMyShortcutMenu() {
    $.ajax({
        url: "/ret/sysget/getMyShortcutMenu",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                if (data.list) {
                    var config = JSON.parse(data.list.config);
                    if (config) {
                        var html = "";
                        for (var i = 0; i < config.length; i++) {
                            if (config[i].url != ''
                                && config[i].name != ''
                                && config[i].status == "1") {
                                html += '<a onclick="parent.openNewTabs(\''
                                    + config[i].url
                                    + '\',\''
                                    + config[i].name
                                    + '\');" class="col-lg-4 col-sm-4 col-xs-4" style="height: auto; text-align: center;cursor: pointer;padding: 2px;"><img style="height: 48px" src="'
                                    + config[i].img
                                    + '" title="'
                                    + config[i].name + '"></a>';
                            }
                        }
                        if (html != "") {
                            $("#shortcut").empty();
                            $("#shortcut").append(html);
                        }
                        $("#shortcut").show();
                    } else {
                        $("#shortcut").show();
                    }

                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else if (data.status == "500") {
                    console.log(data.msg);
                }
            }
        }
    });
}

function refreshdocument1() {
    var flag = $("#documentdeskdiv1").attr("data-value");
    if (flag == "1") {
        getMyDocument1();
    } else if (flag == "2") {
        getMyDocumentSendTo1();
    } else if (flag == "3") {
        getDraftsDocumentFlowListForDesk1()
    }
}

function refreshdocument2() {
    var flag = $("#documentdeskdiv2").attr("data-value");
    if (flag == "1") {
        getMyDocument1();
    } else if (flag == "2") {
        getMyDocumentSendTo2();
    } else if (flag == "3") {
        getDraftsDocumentFlowListForDesk2();
    }
}

function getdocumentlist1(Obj, flag) {
    $("#documentdeskdiv1").attr("data-value", flag);
    $(".js-document1").each(function () {
        $(this).removeClass("onaction");
    })
    $(Obj).addClass("onaction");
    if (flag == "1") {
        $(".js-document1openmore").attr("data-value",
            "/app/core/document/receiptapproved");
        $(".js-document1openmore").attr("data-title", "公文传阅");
        getMyDocument1();
    } else if (flag == "2") {
        $(".js-document1openmore").attr("data-value",
            "/app/core/document/sendtome");
        $(".js-document1openmore").attr("data-title", "公文抄送");
        getMyDocumentSendTo1();
    } else if (flag == "3") {
        $(".js-document1openmore").attr("data-value",
            "/app/core/document/receiptapproved?view=draft");
        $(".js-document1openmore").attr("data-title", "草稿箱");
        getDraftsDocumentFlowListForDesk1();
    }
}

function getMyFileMap(Obj,flag)
{
    $("#fileMap").attr("data-value", flag);
    $(".js-filemap").each(function () {
        $(this).removeClass("onaction");
    })
    $(Obj).addClass("onaction");
    if (flag == "1") {
        $(".js-fileopenmore").attr("data-value",
            "/app/core/filemap/filemapsearch");
        $(".js-fileopenmore").attr("data-title", "待阅文件");
        getMyFileMapForDesk("1");
    } else if (flag == "2") {
        $(".js-fileopenmore").attr("data-value",
            "/app/core/filemap/filemapsearch");
        $(".js-fileopenmore").attr("data-title", "已阅文件");
        getMyFileMapForDesk("2");
    }
}

function refreshFileMap() {
    var flag = $("#fileMap").attr("data-value");
    if (flag == "1") {
        getMyFileMapForDesk("1");
    } else if (flag == "2") {
        getMyFileMapForDesk("2");
    }
}


function getMyFileMapForDesk(readStatus)
{
    $.ajax({
        url: "/ret/filemapget/getMyFileMapForDesk",
        type: "post",
        dataType: "json",
        data:{readStatus:readStatus},
        success: function (data) {
            if (data.status == "200") {
                let infoList = data.list;
                let html="";
                for(let i=0;i<infoList.length;i++)
                {
                    var fileName = infoList[i].fileName;
                    html += "<tr><td class='td_inline'>";
                    html += "<span style='position: absolute;display: block;width: 65%'>";
                    if(readStatus!="2")
                    {
                        html += "<li class=\"label label-darkorange\">new</li>&nbsp;&nbsp;";
                    }
                    html += "<a href=\"javascript:void(0);openFileMapOnLine('" + infoList[i].extName + "','" + infoList[i].attachId + "','1','','" + infoList[i].recordId + "')\">" + fileName + "</a>";
                    html += "</span><span style='float:right;'>"
                        + infoList[i].createTime
                        + "&nbsp;&nbsp;"
                        + infoList[i].createUserName
                        + "</span></td></tr>";
                }
                $("#fileMapRecord").html(html);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    });
}



function getdocumentlist2(Obj, flag) {
    $("#documentdeskdiv2").attr("data-value", flag);
    $(".js-document2").each(function () {
        $(this).removeClass("onaction");
    })
    $(Obj).addClass("onaction");
    if (flag == "1") {
        $(".js-document2openmore").attr("data-value",
            "/app/core/document/dispatchapproved");
        $(".js-document2openmore").attr("data-title", "发文审批");
        getMyDocument2();
    } else if (flag == "2") {
        $(".js-document2openmore").attr("data-value",
            "/app/core/document/sendtome");
        $(".js-document2openmore").attr("data-title", "公文抄送");
        getMyDocumentSendTo2();
    } else if (flag == "3") {
        $(".js-document2openmore").attr("data-value",
            "/app/core/document/dispatchapproved?view=draft");
        $(".js-document2openmore").attr("data-title", "草稿箱");
        getDraftsDocumentFlowListForDesk2();
    }
}

function getbpmlist(Obj, flag) {
    $("#bpmdeskdiv1").attr("data-value", flag);
    $(".js-bpm").each(function () {
        $(this).removeClass("onaction");
    })
    $(Obj).addClass("onaction");
    if (flag == "1") {
        $(".js-bpmopenmore").attr("data-value", "/app/core/bpm/doprocess");
        $(".js-bpmopenmore").attr("data-title", "流程审批");
        getMyBpm();
    } else if (flag == "2") {
        $(".js-bpmopenmore").attr("data-value",
            "/app/core/bpm/doprocess?view=sendtome");
        $(".js-bpmopenmore").attr("data-title", "流程审批");
        getMyBpmSendToMeForDesk();
    } else if (flag == "3") {
        $(".js-bpmopenmore").attr("data-value",
            "/app/core/bpm/doprocess?view=drafts");
        $(".js-bpmopenmore").attr("data-title", "草稿箱");
        getDraftsBpmFlowListForDesk();
    }
}

function refreshbpm() {
    var flag = $("#bpmdeskdiv1").attr("data-value");
    if (flag == "1") {
        getMyBpm();
    } else if (flag == "2") {
        getMyBpmSendToMeForDesk();
    } else if (flag == "3") {
        getDraftsBpmFlowListForDesk();
    }
}

function openMore(Obj) {
    var url = $(Obj).attr("data-value");
    var title = $(Obj).attr("data-title");
    parent.openNewTabs(url, title)
}

var bpmHtml = [
    '<div id="bpmdeskdiv1" class="portlet widget radius-bordered js-module" style="margin-bottom: 10px" module="bpm" moduleName="' + portalmsg['PORTAL_BPM_TO_WORK'] + '">',
    '						<div class="portlet-header widget-header">',
    '							<span class="portlet deskitem">' + portalmsg['PORTAL_BPM_TO_WORK'] + '</span>',
    '							<div style="font-size:14px;color:#777;display: inline;padding-top: 12px;margin-left: 20px;float:left;"><a class="js-bpm desktab onaction" onclick="getbpmlist(this,1);">' + portalmsg['PORTAL_MY_TASK'] + '</a>' +
    '<a class="js-bpm desktab" onclick="getbpmlist(this,2);" >' + portalmsg['PORTAL_SEND_TO_ME'] + '</a><a class="js-bpm desktab" onclick="getbpmlist(this,3);" >' + portalmsg['PORTAL_DRAFT'] + '</a></div>',
    '							<div class="portlet-content widget-buttons">',
    '								<a class="js-bpmopenmore" href="#" data-value="/app/core/bpm/doprocess" data-title="' + portalmsg['PORTAL_BPM_TO_WORK'] + '" onclick="openMore(this);" title="' + portalmsg['PORTAL_MORE'] + '"> <i class="fa fa-reorder blue"></i>',
    '								</a><a href="#" title="' + portalmsg['PORTAL_REFRESH'] + '" onclick="refreshbpm();"> <i class="fa fa-refresh blue"></i></a>',
    '							</div>',
    '						</div>',
    '						<div class="portlet-content widget-body" style="padding:0px;height: 190px;">',
    '							<table class="table table-hover">',
    '                                    <tbody id="bpmtbody">',
    '                                    </tbody>',
    '                                </table>',
    '						</div>',
    '					</div>'].join("");
var documentHtml1 = [
    '<div id="documentdeskdiv1" class="portlet widget radius-bordered js-module" style="margin-bottom: 10px" module="document1" data-value="1" moduleName="' + portalmsg['PORTAL_INCOMING_DOCUMENT'] + '">',
    '						<div class="portlet-header widget-header">',
    '							<span class="portlet deskitem">' + portalmsg['PORTAL_INCOMING_DOCUMENT'] + '</span>',
    '							<div style="font-size:14px;color:#777;display: inline;padding-top: 12px;margin-left: 20px;float:left;"><a class="js-document1 desktab onaction" onclick="getdocumentlist1(this,1);">' + portalmsg['PORTAL_MY_TASK'] + '</a>' +
    '							<a class="js-document1 desktab" onclick="getdocumentlist1(this,2);">' + portalmsg['PORTAL_SEND_TO_ME'] + '</a><a class="js-document1 desktab" onclick="getdocumentlist1(this,3);" >' + portalmsg['PORTAL_DRAFT'] + '</a></div>',
    '							<div class="portlet-content widget-buttons">',
    '								<a class="js-document1openmore" href="#" data-value="/app/core/document/receiptapproved" data-title="' + portalmsg['PORTAL_INCOMING_DOCUMENT'] + '" onclick="openMore(this);" title="' + portalmsg['PORTAL_MORE'] + '"> <i class="fa fa-reorder blue"></i>',
    '								</a><a href="#" onclick="refreshdocument1();" title="' + portalmsg['PORTAL_REFRESH'] + '"> <i class="fa fa-refresh blue"></i></a>',
    '							</div>',
    '						</div>',
    '						<div class="portlet-content widget-body" style="padding:0px;height: 190px;">',
    '							<table class="table table-hover">',
    '                                    <tbody id="documentbody1">',
    '                                    </tbody>',
    '                                </table>',
    '						</div>',
    '					</div>'].join("");
var documentHtml2 = [
    '<div id="documentdeskdiv2" class="portlet widget radius-bordered js-module" style="margin-bottom: 10px" module="document2" data-value="1" moduleName="' + portalmsg['PORTAL_DCOUMENT'] + '">',
    '						<div class="portlet-header widget-header">',
    '							<span class="portlet deskitem">' + portalmsg['PORTAL_DCOUMENT'] + '</span>',
    '							<div style="font-size:14px;color:#777;display: inline;padding-top: 12px;margin-left: 20px;float:left;"><a class="js-document2 desktab onaction" onclick="getdocumentlist2(this,1);">' + portalmsg['PORTAL_MY_TASK'] + '</a>' +
    '							<a class="js-document2 desktab" onclick="getdocumentlist2(this,2);">' + portalmsg['PORTAL_SEND_TO_ME'] + '</a><a class="js-document2 desktab" onclick="getdocumentlist2(this,3);" >' + portalmsg['PORTAL_DRAFT'] + '</a></div>',
    '							<div class="portlet-content widget-buttons">',
    '								<a class="js-document2openmore" href="#" data-value="/app/core/document/dispatchapproved" data-title="' + portalmsg['PORTAL_DCOUMENT'] + '" onclick="openMore(this);" title="' + portalmsg['PORTAL_MORE'] + '"> <i class="fa fa-reorder blue"></i>',
    '								</a><a href="#" onclick="refreshdocument2();" title="' + portalmsg['PORTAL_REFRESH'] + '"> <i class="fa fa-refresh blue"></i></a>',
    '							</div>',
    '						</div>',
    '						<div class="portlet-content widget-body" style="padding:0px;height: 190px;">',
    '							<table class="table table-hover">',
    '                                    <tbody id="documentbody2">',
    '                                    </tbody>',
    '                                </table>', '						</div>',
    '					</div>'].join("");

var myTask = [
    '<div class="portlet widget radius-bordered js-module" style="margin-bottom: 10px" module="myTask" moduleName="' + portalmsg['PORTAL_TASK'] + '">',
    '						<div class="portlet-header widget-header">',
    '							<span class="portlet deskitem">' + portalmsg['PORTAL_TASK'] + '</span>',
    '							<div class="portlet-content widget-buttons">',
    '								<a href="#" onclick="parent.openNewTabs(\'/app/core/task/mytask\',\'' + portalmsg['PORTAL_TASK'] + '\');" title="' + portalmsg['PORTAL_MORE'] + '"> <i class="fa fa-reorder blue"></i>',
    '								</a><a href="#" onclick="getMyTask();" title="' + portalmsg['PORTAL_REFRESH'] + '"> <i class="fa fa-refresh blue"></i></a>',
    '							</div>',
    '						</div>',
    '						<div class="portlet-content widget-body" style="padding:0px;height: 190px;">',
    '								<table class="table table-hover">',
    '                                    <tbody id="mytaskbody">',
    '                                    </tbody>',
    '                                </table>', '						</div>',
    '					</div>'].join("");
var myNews = [
    '<div class="portlet widget radius-bordered js-module" style="margin-bottom: 10px" module="news" moduleName="' + portalmsg['PORTAL_NEWS'] + '">',
    '						<div class="portlet-header widget-header" >',
    '							<span class="portlet deskitem">' + portalmsg['PORTAL_NEWS'] + '</span>',
    '							<div class="portlet-content widget-buttons">',
    '								<a href="#" onclick="parent.openNewTabs(\'/app/core/news/mynews\',\'' + portalmsg['PORTAL_NEWS'] + '\');" title="' + portalmsg['PORTAL_MORE'] + '"> <i class="fa fa-reorder blue"></i>',
    '								</a><a href="#" onclick="getMyNews();" title="' + portalmsg['PORTAL_REFRESH'] + '"> <i class="fa fa-refresh blue"></i></a>',
    '							</div>',
    '						</div>',
    '						<div class="portlet-content widget-body" style="padding:0px;height: 190px;">',
    '							<div style="width:40%;display: inline-block;height: 190px;">',
    '							<div id="pic">',
    '					        <div id="po">',
    '					            <div id="pol">',
    '					            </div>',
    '					            <div id="num">',
    '					            </div>',
    '					        </div>',
    '					    </div>',
    ' </div><div style="width:60%;float:right;"><table class="table table-hover">',
    '                                    <tbody id="newstbody">',
    '                                    </tbody>',
    '                                </table></div>', '						</div>',
    '					</div>'].join("");
var myNotice = [
    '<div class="portlet widget radius-bordered js-module" style="margin-bottom: 10px;" module="notice" moduleName="' + portalmsg['PORTAL_NOTICE'] + '">',
    '						<div class="portlet-header widget-header">',
    '							<span class="portlet deskitem">' + portalmsg['PORTAL_NOTICE'] + '</span>',
    '							<div class="portlet-content widget-buttons">',
    '								<a href="#" onclick="parent.openNewTabs(\'/app/core/notice/readnotice\',\'' + portalmsg['PORTAL_NOTICE'] + '\');" title="' + portalmsg['PORTAL_MORE'] + '"> <i class="fa fa-reorder blue"></i>',
    '								</a><a href="#" onclick="getMyNotice();" title="' + portalmsg['PORTAL_REFRESH'] + '"> <i class="fa fa-refresh blue"></i></a>',
    '							</div>',
    '						</div>',
    '						<div class="portlet-content widget-body" style="padding:0px;height: 190px;">',
    '							<table class="table table-hover">',
    '                                    <tbody id="noticetbody">',
    '                                    </tbody>',
    '                                </table>', '						</div>',
    '					</div>'].join("");
var myMeeting = [
    '<div class="portlet widget radius-bordered js-module" style="margin-bottom: 10px;" module="myMeeting" moduleName="' + portalmsg['PORTAL_MEETING'] + '">',
    '						<div class="portlet-header widget-header">',
    '							<span class="portlet deskitem">' + portalmsg['PORTAL_MEETING'] + '</span>',
    '							<div class="portlet-content widget-buttons">',
    '								<a href="#" onclick="parent.openNewTabs(\'/app/core/meeting/mymeeting\',\'' + portalmsg['PORTAL_MEETING'] + '\');" title="' + portalmsg['PORTAL_MORE'] + '"> <i class="fa fa-reorder blue"></i>',
    '								</a><a href="#" onclick="getMyMeeting();" title="' + portalmsg['PORTAL_REFRESH'] + '"> <i class="fa fa-refresh blue"></i></a>',
    '							</div>',
    '						</div>',
    '						<div class="portlet-content widget-body" style="padding:0px;height: 190px;">',
    '							<table class="table table-hover">',
    '                                    <tbody id="meetingbody">',
    '                                    </tbody>',
    '                                </table>', '						</div>',
    '					</div>'].join("");

var myFile = [
    '<div class="portlet widget radius-bordered js-module" style="margin-bottom: 10px;" module="myFile" moduleName="' + portalmsg['PORTAL_MY_FILE'] + '">',
    '						<div class="portlet-header widget-header">',
    '							<span class="portlet deskitem">' + portalmsg['PORTAL_MY_FILE'] + '</span>',
    '							<div class="portlet-content widget-buttons">',
    '								<a href="#" onclick="parent.openNewTabs(\'/app/core/file/publicfile\',\'' + portalmsg['PORTAL_MY_FILE'] + '\');" title="' + portalmsg['PORTAL_MORE'] + '"> <i class="fa fa-reorder blue"></i>',
    '								</a><a href="#" onclick="getMyFile();" title="' + portalmsg['PORTAL_REFRESH'] + '"> <i class="fa fa-refresh blue"></i></a>',
    '							</div>',
    '						</div>',
    '						<div class="portlet-content widget-body" style="padding:0px;height: 190px;">',
    '							<table class="table table-hover">',
    '                                    <tbody id="filebody">',
    '                                        ',
    '                                    </tbody>',
    '                                </table>', '						</div>',
    '					</div>'].join("");
var supHtml = [
    '<div id="supdeskdiv" class="portlet widget radius-bordered js-module" style="margin-bottom: 10px" module="superversion" data-value="1" moduleName="' + portalmsg['PORTAL_SUPERVERSION'] + '">',
    '						<div class="portlet-header widget-header">',
    '							<span class="portlet deskitem">' + portalmsg['PORTAL_SUPERVERSION'] + '</span>',
    '							<div style="font-size:14px;color:#777;display: inline;padding-top: 12px;margin-left: 20px;float:left;"><a class="js-sup desktab onaction" onclick="getsuperversion(this,1);">' + portalmsg['PORTAL_MY_JOB'] +
    '							</a><a class="js-sup desktab" onclick="getsuperversion(this,2);">' + portalmsg['PORTAL_MY_MANAGE'] + '</a></div>',
    '							<div class="portlet-content widget-buttons">',
    '								<a href="#" class="js-supopenmore" href="#" data-value="/app/core/superversion/process" data-title="' + portalmsg['PORTAL_SUPERVERSION'] + '" onclick="openMore(this);"  title="' + portalmsg['PORTAL_MORE'] + '"> <i class="fa fa-reorder blue"></i>',
    '								</a><a href="#" onclick="refreshsup();" title="' + portalmsg['PORTAL_REFRESH'] + '"> <i class="fa fa-refresh blue"></i></a>',
    '							</div>',
    '						</div>',
    '						<div class="portlet-content widget-body" style="padding:0px;height: 190px;">',
    '							<table class="table table-hover">',
    '                                    <tbody id="supbody">',
    '                                    </tbody>',
    '                                </table>', '						</div>',
    '					</div>'].join("");
var calendarHtml = [
    '<div id="supdeskdiv" class="portlet widget radius-bordered js-module" style="margin-bottom: 10px" module="calendar" data-value="1" moduleName="' + portalmsg['PORTAL_CALENDAR'] + '">',
    '						<div class="portlet-header widget-header">',
    '							<span class="portlet deskitem">' + portalmsg['PORTAL_CALENDAR'] + '</span>',
    '							<div class="portlet-content widget-buttons">',
    '								<a href="#" class="js-calopenmore" href="#" data-value="/app/core/oa/mycalendar" data-title="' + portalmsg['PORTAL_CALENDAR'] + '" onclick="openMore(this);"  title="' + portalmsg['PORTAL_MORE'] + '"> <i class="fa fa-reorder blue"></i>',
    '								</a><a href="#" onclick="getMyCalendar();" title="' + portalmsg['PORTAL_REFRESH'] + '"> <i class="fa fa-refresh blue"></i></a>',
    '							</div>',
    '						</div>',
    '						<div class="portlet-content widget-body" style="padding:0px;height: 190px;">',
    '							<table class="table table-hover">',
    '                                    <tbody id="calendartbody">',
    '                                    </tbody>',
    '                                </table>', '						</div>',
    '					</div>'].join("");

var leadactivityHtml = [
    '<div id="leaddeskdiv" class="portlet widget radius-bordered js-module" style="margin-bottom: 10px" module="leadactivity" data-value="1" moduleName="' + portalmsg['PORTAL_LEADERSHIP'] + '">',
    '						<div class="portlet-header widget-header">',
    '							<span class="portlet deskitem">' + portalmsg['PORTAL_LEADERSHIP'] + '</span>',
    '							<div class="portlet-content widget-buttons">',
    '								<a href="#" class="js-leadopenmore" href="#" data-value="/app/core/leadactivity/query" data-title="' + portalmsg['PORTAL_LEADERSHIP'] + '" onclick="openMore(this);"  title="' + portalmsg['PORTAL_MORE'] + '"> <i class="fa fa-reorder blue"></i>',
    '								</a><a href="#" onclick="getMyCalendar();" title="' + portalmsg['PORTAL_REFRESH'] + '"> <i class="fa fa-refresh blue"></i></a>',
    '							</div>',
    '						</div>',
    '						<div class="portlet-content widget-body" style="padding:0px;height: 190px;">',
    '							<table class="table table-hover">',
    '                                    <tbody id="leadbody">',
    '                                    </tbody>',
    '                                </table>', '						</div>',
    '					</div>'].join("");

let bpmzlHtml = [
    '<div id="bpmworkretention" class="portlet widget radius-bordered js-module" style="margin-bottom: 10px" module="bpmworkretention" data-value="1" moduleName="' + portalmsg['BPM_WORK_ZL'] + '">',
    '						<div class="portlet-header widget-header">',
    '							<span class="portlet deskitem">' + portalmsg['BPM_WORK_ZL'] + '</span>',
    '							<div class="portlet-content widget-buttons">',
    '								<a href="#" onclick="getBpmZl();" title="' + portalmsg['BPM_WORK_ZL'] + '"> <i class="fa fa-refresh blue"></i></a>',
    '							</div>',
    '						</div>',
    '						<div class="portlet-content widget-body" style="padding:0px;height: 190px;">',
    '                    <div id="bpmzl" style="height: 190px;" class="col-sm-12"></div>\n' +
    '						</div>',
    '            </div>'].join("");

var fileMap = [
    '<div id="fileMap" class="portlet widget radius-bordered js-module" style="margin-bottom: 10px" module="filemap" data-value="1" moduleName="文档地图">',
    '						<div class="portlet-header widget-header">',
    '							<span class="portlet deskitem">文档地图</span>',
    '							<div style="font-size:14px;color:#777;display: inline;padding-top: 12px;margin-left: 20px;float:left;"><a class="js-filemap desktab onaction" onclick="getMyFileMap(this,1);">待阅文件</a>' +
    '							<a class="js-filemap desktab" onclick="getMyFileMap(this,2);" >已阅文件</a></div>',
    '							<div class="portlet-content widget-buttons">',
    '								<a class="js-filemapopenmore" href="#" data-value="/app/core/filemap/filemapsearch" data-title="我的文档" onclick="openMore(this);" title="' + portalmsg['PORTAL_MORE'] + '"> <i class="fa fa-reorder blue"></i>',
    '								</a><a href="#" onclick="refreshFileMap();" title="' + portalmsg['PORTAL_REFRESH'] + '"> <i class="fa fa-refresh blue"></i></a>',
    '							</div>',
    '						</div>',
    '						<div class="portlet-content widget-body" style="padding:0px;height: 190px;">',
    '							<table class="table table-hover">',
    '                                    <tbody id="fileMapRecord">',
    '                                    </tbody>',
    '                                </table>',
    '						</div>',
    '					</div>'].join("");
function getsuperversion(Obj, flag) {
    $("#supdeskdiv").attr("data-value", flag);
    $(".js-sup").each(function () {
        $(this).removeClass("onaction");
    })
    $(Obj).addClass("onaction");
    if (flag == "1") {
        $(".js-supopenmore").attr("data-value",
            "/app/core/superversion/process");
        $(".js-supopenmore").attr("data-title", "督办处理");
        getSuperversionListForDesk();
    } else if (flag == "2") {
        $(".js-supopenmore").attr("data-value",
            "/app/core/superversion/leadmanage");
        $(".js-supopenmore").attr("data-title", "督查管控");
        getManageSuperversionListForDesk();
    }
}

function refreshsup() {
    var flag = $("#supdeskdiv").attr("data-value");
    if (flag == "1") {
        getSuperversionListForDesk();
    } else if (flag == "2") {
        getManageSuperversionListForDesk();
    }
}

function getSuperversionListForDesk() {
    $
        .ajax({
            url: "/ret/superversionget/getSupperversionPorcessListForDesk",
            type: "post",
            dataType: "json",
            success: function (data) {
                if (data.status == 200) {
                    var infoList = data.list;
                    var html = "";
                    for (var i = 0; i < infoList.length; i++) {
                        var title = infoList[i].title;
                        if (title.length > 22) {
                            title = title.substring(0, 22) + "...";
                        }
                        html += "<tr><td><span title=\"" + infoList[i].typeName + "\"><a href=\"javascript:void(0);readsuperversion('"
                            + infoList[i].superversionId
                            + "')\">" + title + "</a><span><span style=\"float:right;\">截止:" + infoList[i].endTime + "&nbsp;&nbsp;督查:" + infoList[i].leadUserName + "</span></td></tr>"
                    }
                    $("#supbody").html(html);
                } else if (data.status == 100) {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
}

function getManageSuperversionListForDesk() {
    $.ajax({
        url: "/ret/superversionget/getManageSuperversionListForDesk",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == 200) {
                var infoList = data.list;
                var html = "";
                for (var i = 0; i < infoList.length; i++) {
                    var title = infoList[i].title;
                    if (title.length > 22) {
                        title = title.substring(0, 22) + "...";
                    }
                    html += "<tr><td><span title=\"" + infoList[i].typeName + "\"><a href=\"javascript:void(0);readsuperversion('"
                        + infoList[i].superversionId
                        + "')\">" + title + "</a><span><span style=\"float:right;\">截止:" + infoList[i].endTime + "&nbsp;&nbsp;牵头:" + infoList[i].handedUserName + "</span></td></tr>"
                }
                $("#supbody").html(html);
            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function readsuperversion(supervresionId) {
    window.open("/app/core/superversion/superversiondetails?superversionId="
        + supervresionId);
}

setInterval(function () {
    var date2 = getHMTime(new Date());
    $("#nowtime").html(date2);
}, 1000);


function openFileMapOnLine(extName, attachId, priv, fileId, recordId) {
    $.ajax({
        url: "/set/filemapset/setReadStatus",
        type: "post",
        dataType: "json",
        data: {recordId: recordId},
        success: function (data) {
            if (data.status == "200") {
                //layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
    fileId = fileId || '';
    var extName = extName.toUpperCase();
    if (extName == ".TXT" || extName == ".HTML") {
        window.open("/sys/file/readFile?attachId=" + attachId, "_blank");
    } else if (extName == ".DOC" || extName == ".DOT" || extName == ".DOCX" || extName == ".DOTX" || extName == ".WPS" || extName == ".UOF")//
    {
        if (officetype == "0") {
            POBrowser.openWindowModeless("/office/openword?attachId=" + attachId + "&openModeType=" + priv, "width=1200px;height=800px;");
        } else if (officetype == "1") {
            window.open("/office/goldgrid/openword?attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "2") {
            window.open("/office/wps/openword?attachId=" + attachId + "&openModeType=" + priv + "&fileId=" + fileId);
        } else if (officetype == "3") {
            window.open("/v1/weboffice/index?attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "4") {
            window.open("/app/core/onlyoffice?attachId=" + attachId + "&openModeType=" + priv + "&fileId=" + fileId);
        }
    } else if (extName == ".XLS" || extName == ".XLSX" || extName == ".CSV" || extName == ".ET" || extName == ".ETT") {
        if (officetype == "0") {
            POBrowser.openWindowModeless("/office/openexcel?attachId=" + attachId + "&openModeType=" + priv, "width=1200px;height=800px;");
        } else if (officetype == "1") {
            window.open("/office/goldgrid/openexcel?attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "2") {
            window.open("/office/wps/openexcel?attachId=" + attachId + "&openModeType=" + priv + "&fileId=" + fileId);
        } else if (officetype == "3") {
            window.open("/v1/weboffice/index?attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "4") {
            window.open("/app/core/onlyoffice?attachId=" + attachId + "&openModeType=" + priv + "&fileId=" + fileId);
        }
    } else if (extName == ".PPT" || extName == ".PPTX" || extName == ".DPS" || extName == ".DPT") {
        if (officetype == "0") {
            POBrowser.openWindowModeless("/office/openppt?attachId=" + attachId + "&openModeType=" + priv, "width=1200px;height=800px;");
        } else if (officetype == "1") {
            window.open("/office/goldgrid/openword?attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "2") {
            window.open("/office/wps/openppt?attachId=" + attachId + "&openModeType=" + priv + "&fileId=" + fileId);
        } else if (officetype == "3") {
            window.open("/v1/weboffice/index?attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "4") {
            window.open("/app/core/onlyoffice?attachId=" + attachId + "&openModeType=" + priv + "&fileId=" + fileId);
        }
    } else if (extName == ".PDF") {
        if (officetype == "0") {
            POBrowser.openWindowModeless("/office/openpdf?attachId=" + attachId + "&openModeType=" + priv, "width=1200px;height=800px;");
        } else if (officetype == "1") {
            window.open("/office/goldgrid/openword?attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "2") {
            if (ofdtype == "1") {
                window.open("/office/ofd/openfxofd?attachId=" + attachId + "&openModeType=" + priv);
            } else {
                window.open("/module/pdfjs/web/viewer.html?file=" + encodeURIComponent("/sys/file/getFileDown?attachId=" + attachId), "_blank");
            }
        } else if (officetype == "3") {
            window.open("/v1/weboffice/index?attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "4") {
            window.open("/app/core/onlyoffice?attachId=" + attachId + "&openModeType=" + priv + "&fileId=" + fileId);
        }
    } else if (extName == '.JPG' || extName == '.PNG' || extName == '.JPEG' || extName == '.BMP' || extName == '.TIT' || extName == '.GIF') {
        window.open("/sys/file/getImage?attachId=" + attachId, "_blank", "location=no");
    } else if (extName == ".OFD") {
        if (ofdtype == "1") {
            window.open("/office/ofd/openfxofd?attachId=" + attachId + "&openModeType=" + priv, "_blank", "location=no");
        } else if (ofdtype == "2") {
            window.open("/office/ofd/openswofd?attachId=" + attachId + "&openModeType=" + priv, "_blank", "location=no");
        } else {
            window.open("/module/ofd/index.html?ofdFile=" + encodeURIComponent("/sys/file/getFileDown?attachId=" + attachId), "_blank", "location=no");
        }
    } else {
        layer.msg("文件格式不支持在线打开！");
    }
}
