$(function () {
    $('#template').summernote({height: 300});
    getDiaryPriv();
    $(".js-setbtn").unbind("click").click(function () {
        setDiaryPriv();
    });
})

function getDiaryPriv() {
    $.ajax({
        url: "/ret/oaget/getDiaryPriv",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status = "200") {
                if (data.list) {
                    $("#lockDay").val(data.list.lockDay);
                    $("input:radio[name='shareStatus'][value='" + data.list.shareStatus + "']").prop("checked", "checked");
                    $("input:radio[name='commStatus'][value='" + data.list.commStatus + "']").prop("checked", "checked");
                    $('#template').code(data.list.template);
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data);
            }
        }
    });
}

function setDiaryPriv() {
    $.ajax({
        url: "/set/oaset/setDiaryPriv",
        type: "post",
        dataType: "json",
        data: {
            lockDay: $("#lockDay").val(),
            shareStatus: getCheckBoxValue("shareStatus"),
            commStatus: getCheckBoxValue("commStatus"),
            template: $("#template").code()
        },
        success: function (data) {
            if (data.status = "200") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data);
            }


        }
    });
}
