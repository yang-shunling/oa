var setting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/biget/getBiSortTreeForParent",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    callback: {
        onClick: zTreeOnClick
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    }
};
$(function () {
    $.ajax({
        url: "/ret/biget/getBiSortTreeForParent",
        type: "post",
        dataType: "json",
        data: {sortId: "0"},
        success: function (data) {
            zTree = $.fn.zTree.init($("#tree"), setting, data);// 初始化树节点时，添加同步获取的数据
        }
    });
})


function zTreeOnClick(event, treeId, treeNode) {
    if (treeNode.isParent == false) {
        $("#reportiframe").attr("src", "/app/core/getreportpage?templateId=" + treeNode.sortId + "&r=" + Date.parse(new Date()) / 1000);
    } else {
    }
}
