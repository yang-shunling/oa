$(function () {
    jeDate("#cardTime", {
        format: "YYYY-MM-DD",
        maxDate: getSysDate()
    });
    $("#addBtn").unbind("click").click(function () {
        addVehicleOilCard();
    });
})

function addVehicleOilCard() {
    if ($("#cardCode").val() == "") {
        layer.msg("油卡编号不能为空！");
        return;
    }
    $.ajax({
        url: "/set/vehicleset/insertVehicleOilCard",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            oilType: $("#oilType").val(),
            oilComp: $("#oilComp").val(),
            cardCode: $("#cardCode").val(),
            cardNo: $("#cardNo").val(),
            passWord: $("#passWord").val(),
            status: $("#status").val(),
            balance: $("#balance").val(),
            cardTime: $("#cardTime").val(),
            cardUser: $("#cardUser").attr("data-value"),
            remark: $("#remark").val()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}
