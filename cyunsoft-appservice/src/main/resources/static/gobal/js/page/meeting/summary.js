let ue=UE.getEditor('remark');
$(function () {
    getSmsConfig("msgType", "meeting");
    getNotNotesMeetingList();
    $("#createbut").unbind("click").click(function () {
        createMeetingNotes();
    });
})

function getNotNotesMeetingList() {
    $.ajax({
        url: "/ret/meetingget/getNotNotesMeetingList",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                for (var i = 0; i < data.list.length; i++) {
                    $("#meetingId").append("<option value='" + data.list[i].meetingId + "'>" + data.list[i].subject + "</option>")
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}


function createMeetingNotes() {
    var meetingId = $("#meetingId").val();
    if (meetingId == "" || meetingId == null) {
        layer.msg("请先选择相关联的会议！");
    } else {
        if ($("#notesTitle").val() == "") {
            layer.msg("纪要标题不能为空");
            return;
        }
        $.ajax({
            url: "/set/meetingset/insertMeetingNotes",
            type: "post",
            dataType: "json",
            data: {
                meetingId: meetingId,
                notesTitle: $("#notesTitle").val(),
                userPriv: $("#userPriv").attr("data-value"),
                deptPriv: $("#deptPriv").attr("data-value"),
                levelPriv: $("#levelPriv").attr("data-value"),
                attach: $("#meetingattach").attr("data_value"),
                attachPriv: $("input:radio[name='attachPriv']:checked").val(),
                remark: ue.getContent(),
                msgType: getCheckBoxValue("msgType")
            },
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    window.location.reload();
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    }
}
