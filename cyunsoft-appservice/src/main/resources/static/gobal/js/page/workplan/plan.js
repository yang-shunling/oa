let ue = UE.getEditor("remark");
$(function () {
    getCodeClass("planType", "workplan");
    jeDate("#beginTime", {
        format: "YYYY-MM-DD",
        minDate: getSysDate(),
        isinitVal: true
    });
    jeDate("#endTime", {
        format: "YYYY-MM-DD",
        minDate: getSysDate(),
    });
    getSmsConfig("msgType", "workplan");
    $(".js-add-save").unbind("click").click(function () {
        addWorkPlan();
    })
})

function addWorkPlan() {
    if ($("#title").val() == "") {
        layer.msg("计划标题不能为空！");
        return;
    }
    $.ajax({
        url: "/set/workplanset/createWorkPlan",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            planType: $("#planType").val(),
            title: $("#title").val(),
            userPriv: $("#userPriv").attr("data-value"),
            deptPriv: $("#deptPriv").attr("data-value"),
            levelPriv: $("#levelPriv").attr("data-value"),
            joinUser: $("#joinUser").attr("data-value"),
            holdUser: $("#holdUser").attr("data-value"),
            supUser: $("#supUser").attr("data-value"),
            remark: ue.getContent(),
            beginTime: $("#beginTime").val(),
            endTime: $("#endTime").val(),
            attach: $("#workplanattach").attr("data_value"),
            msgType: getCheckBoxValue("msgType")
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                window.location.reload();
                layer.msg(sysmsg[data.msg]);
            }
        }
    })
}
