$(function () {
    $.ajax({
        url: "/ret/oaget/getVoteById",
        type: "post",
        dataType: "json",
        data: {
            voteId: voteId
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", recordInfo.attach);
                        createAttach("attach", 1);
                    } else if (id == "userPriv") {
                        $("#userPriv").html(getUserNameByStr(data.list[id]));
                    } else if (id == "deptPriv") {
                        $("#deptPriv").html(getDeptNameByDeptIds(data.list[id]));
                    } else if (id == "levelPriv") {
                        $("#levelPriv").html(getUserLevelStr(data.list[id]));
                    } else if (id == "isTop") {
                        if (recordInfo.isTop == "0") {
                            $("#isTop").html("不置顶");
                        } else if (recordInfo.isTop == "1") {
                            $("#isTop").html("置顶");
                        } else {
                            $("#isTop").html("未知");
                        }
                    } else if (id == "readRes") {
                        if (recordInfo.readRes == "0") {
                            $("#readRes").html("允许查看");
                        } else if (recordInfo.readRes == "1") {
                            $("#readRes").html("禁止查看");
                        } else {
                            $("#readRes").html("未知");
                        }
                    } else if (id == "voteType") {
                        $("#voteType").html(getCodeClassName(recordInfo.voteType, "vote"));
                    } else if (id == "isAnonymous") {
                        if (recordInfo.isAnonymous == "0") {
                            $("#isAnonymous").html("匿名");
                        } else if (recordInfo.isAnonymous == "1") {
                            $("#isAnonymous").html("实名");
                        } else {
                            $("#isAnonymous").html("未知");
                        }
                    } else {
                        $("#" + id).html(recordInfo[id]);
                    }
                }
            }
        }
    });
})
