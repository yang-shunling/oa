/*
//读写分离时打开
package com.core136.config;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Value;

@Aspect
@Component
@Lazy(false)
@Order(0) //Order设定AOP执行顺序 使之在数据库事务上先执行
public class SwitchDataSourceAOP {
@Value("${app.isdev}")
String isdev;
    //这里切到你的方法目录
	@Before("execution(* com.core136.service.*.*.*(..))")
    public void process(JoinPoint joinPoint) {
        String methodName=joinPoint.getSignature().getName();
        System.out.println(methodName);
        if (methodName.startsWith("get")
                ||methodName.startsWith("isExist")
                ||methodName.startsWith("find")
                ||methodName.startsWith("query")
                ||methodName.startsWith("select")
                ||methodName.startsWith("check")){
            DataSourceContextHolder.setDbType("selectDataSource");
            if(isdev.equals("1"))
            {
            	System.out.println("----切换到从库！-----");
            }
        }else {
            //切换dataSource
            DataSourceContextHolder.setDbType("updateDataSource");
            if(isdev.equals("1"))
            {
            	System.out.println("-----切换到主库！-----");
            }
        }
    }
}
*/