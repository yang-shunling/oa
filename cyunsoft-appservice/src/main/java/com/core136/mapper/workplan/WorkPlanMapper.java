package com.core136.mapper.workplan;

import com.core136.bean.workplan.WorkPlan;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface WorkPlanMapper extends MyMapper<WorkPlan> {

    /**
     * @param orgId
     * @param createUser
     * @param opFlag
     * @param beginTime
     * @param endTime
     * @param status
     * @param planType
     * @param search
     * @return List<Map < String, String>>
     * @Title: getManageWorkPlanList
     * @Description:  获取工作列表
     */
    public List<Map<String, String>> getManageWorkPlanList(
            @Param(value = "orgId") String orgId, @Param(value = "createUser") String createUser,
            @Param(value = "opFlag") String opFlag, @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime, @Param(value = "status") String status,
            @Param(value = "planType") String planType, @Param(value = "search") String search
    );

    /**
     * @param orgId
     * @param accountId
     * @param opFlag
     * @param beginTime
     * @param endTime
     * @param status
     * @param planType
     * @param search
     * @return List<Map < String, String>>
     * @Title: getHoldWorkPlanList
     * @Description:  我负责的计划表列
     */
    public List<Map<String, String>> getHoldWorkPlanList(
            @Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,
            @Param(value = "opFlag") String opFlag, @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime, @Param(value = "status") String status,
            @Param(value = "planType") String planType, @Param(value = "search") String search
    );

    /**
     * @param orgId
     * @param accountId
     * @param opFlag
     * @param beginTime
     * @param endTime
     * @param status
     * @param planType
     * @param search
     * @return List<Map < String, String>>
     * @Title: getSupWorkPlanList
     * @Description:  我督查的计划列表
     */
    public List<Map<String, String>> getSupWorkPlanList(
            @Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,
            @Param(value = "opFlag") String opFlag, @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime, @Param(value = "status") String status,
            @Param(value = "planType") String planType, @Param(value = "search") String search
    );

    /**
     * @param orgId
     * @param accountId
     * @param opFlag
     * @param beginTime
     * @param endTime
     * @param status
     * @param planType
     * @param search
     * @return List<Map < String, String>>
     * @Title: getMyWorkPlanList
     * @Description:  我参与的工作计划
     */
    public List<Map<String, String>> getMyWorkPlanList(
            @Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,
            @Param(value = "opFlag") String opFlag, @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime, @Param(value = "status") String status,
            @Param(value = "planType") String planType, @Param(value = "search") String search
    );

    /**
     * @param orgId
     * @param accountId
     * @param deptId
     * @param levelId
     * @param opFlag
     * @param beginTime
     * @param endTime
     * @param status
     * @param planType
     * @param search
     * @return List<Map < String, String>>
     * @Title: getShareWorkPlanList
     * @Description:  获取分享的工作计划
     */
    public List<Map<String, String>> getShareWorkPlanList(
            @Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,
            @Param(value = "deptId") String deptId, @Param(value = "levelId") String levelId,
            @Param(value = "opFlag") String opFlag, @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime, @Param(value = "status") String status,
            @Param(value = "planType") String planType, @Param(value = "search") String search
    );

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getTodayWorkPlanList
     * @Description:  获取今天的工作计划
     */
    public List<Map<String, String>> getTodayWorkPlanList(@Param(value = "orgId") String orgId, @Param(value = "search") String search);

    /**
     * @param orgId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getMonthWorkPlanList
     * @Description:  查询本月工作计划
     */
    public List<Map<String, String>> getMonthWorkPlanList(@Param(value = "orgId") String orgId, @Param(value = "search") String search);

    /**
     * @param orgId
     * @param beginTime
     * @param endTime
     * @param holdUser
     * @param supUser
     * @param status
     * @param planType
     * @param search
     * @return List<Map < String, String>>
     * @Title: queryWorkPlanList
     * @Description:  工作计划查询
     */
    public List<Map<String, String>> queryWorkPlanList(@Param(value = "orgId") String orgId, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime,
                                                       @Param(value = "holdUser") String holdUser, @Param(value = "supUser") String supUser, @Param(value = "status") String status,
                                                       @Param(value = "planType") String planType, @Param(value = "search") String search);


}
