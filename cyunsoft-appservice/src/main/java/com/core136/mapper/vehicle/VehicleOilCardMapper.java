package com.core136.mapper.vehicle;

import com.core136.bean.vehicle.VehicleOilCard;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface VehicleOilCardMapper extends MyMapper<VehicleOilCard> {

    /**
     * @param orgId
     * @param oilType
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getVehicleOilCardList
     * @Description:  获取油卡列表
     */
    public List<Map<String, String>> getVehicleOilCardList(@Param(value = "orgId") String orgId,
                                                           @Param(value = "oilType") String oilType,
                                                           @Param(value = "beginTime") String beginTime,
                                                           @Param(value = "endTime") String endTime,
                                                           @Param(value = "search") String search
    );

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getCanUsedOilCardList
     * @Description:  获取可表油卡列表
     */
    public List<Map<String, String>> getCanUsedOilCardList(@Param(value = "orgId") String orgId);
}
