package com.core136.mapper.echarts;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface EchartsSuperversionMapper {


    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getBiSuperversionByLeadPie
     * @Description:  前10位领导的人员工作量占比
     */
    public List<Map<String, String>> getBiSuperversionByLeadPie(@Param(value = "orgId") String orgId);

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getBiBpmFlowPie
     * @Description:  获取督查督办分类前10的占比
     */
    public List<Map<String, String>> getBiSuperversionTypePie(@Param(value = "orgId") String orgId);


    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getBiSuperversionStatusTypePie
     * @Description:  获取督查督办当前状态总数
     */
    public List<Map<String, String>> getBiSuperversionStatusTypePie(@Param(value = "orgId") String orgId);

    /**
     * @param orgId
     * @param beginTime
     * @param endTime
     * @return List<Map < String, String>>
     * @Title: getBiSuperversionByMonthLine
     * @Description:  按月份统计工作量
     */
    public List<Map<String, Object>> getBiSuperversionByMonthLine(@Param(value = "orgId") String orgId, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime);

}
