package com.core136.mapper.echarts;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface EchartsFinanceMapper {

    /**
     * @param orgId
     * @param beginTime
     * @param endTime
     * @return List<Map < String, Double>>
     * @Title: getPayableListData
     * @Description:  获取合同应付款列表
     */
    public List<Map<String, Double>> getPayableListData(@Param(value = "orgId") String orgId, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime);

    /**
     * @param orgId
     * @param beginTime
     * @param endTime
     * @return List<Map < String, Double>>
     * @Title: getReceviablesListData
     * @Description:  获取合同应收款
     */
    public List<Map<String, Double>> getReceviablesListData(@Param(value = "orgId") String orgId, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime);

    /**
     * @param orgId
     * @return Map<String, String>
     * @Title: getPayReceivTotalData
     * @Description:  获取应收应付总数
     */
    public Map<String, String> getPayReceivTotalData(@Param(value = "orgId") String orgId);
}
