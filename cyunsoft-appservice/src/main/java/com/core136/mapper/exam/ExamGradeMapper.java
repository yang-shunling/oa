package com.core136.mapper.exam;

import com.core136.bean.exam.ExamGrade;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ExamGradeMapper extends MyMapper<ExamGrade> {
    /**
     * 获取考试成绩列表
     *
     * @param orgId
     * @param examTestId
     * @param accountId
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String, String>> getExamGradeList(@Param(value = "orgId") String orgId, @Param(value = "examTestId") String examTestId,
                                                      @Param(value = "accountId") String accountId, @Param(value = "beginTime") String beginTime,
                                                      @Param(value = "endTime") String endTime, @Param(value = "search") String search);

    public List<Map<String, String>> getExamGradeOldList(@Param(value = "orgId") String orgId, @Param(value = "examTestId") String examTestId,
                                                         @Param(value = "accountId") String accountId, @Param(value = "beginTime") String beginTime,
                                                         @Param(value = "endTime") String endTime, @Param(value = "search") String search);
}
