package com.core136.mapper.oa;

import com.core136.bean.oa.LeaderMailbox;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface LeaderMailboxMapper extends MyMapper<LeaderMailbox> {

    public List<Map<String, String>> getMyLeaderMailBoxList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId, @Param(value = "search") String search);

    public List<Map<String, String>> getLeaderMailBoxList(@Param(value = "orgId") String orgId, @Param(value = "search") String search);

}
