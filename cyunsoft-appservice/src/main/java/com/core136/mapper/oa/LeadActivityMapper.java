package com.core136.mapper.oa;

import com.core136.bean.oa.LeadActivity;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface LeadActivityMapper extends MyMapper<LeadActivity> {

    /**
     * @param orgId
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getLeadActivityLsit
     * @Description:  获取领导行程列表
     */
    public List<Map<String, String>> getLeadActivityLsit(
            @Param(value = "orgId") String orgId, @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime, @Param(value = "leader") String leader, @Param(value = "search") String search);

    /**
     * @param orgId
     * @param beginTime
     * @param endTime
     * @param leader
     * @param accountId
     * @param deptId
     * @param levelId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getLeadActivityQueryLsit
     * @Description:  查询程领导日程
     */
    public List<Map<String, String>> getLeadActivityQueryLsit(
            @Param(value = "orgId") String orgId, @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime, @Param(value = "leader") String leader,
            @Param(value = "accountId") String accountId, @Param(value = "deptId") String deptId, @Param(value = "levelId") String levelId,
            @Param(value = "search") String search);

    /**
     * @param orgId
     * @param accountId
     * @param deptId
     * @param levelId
     * @return List<Map < String, String>>
     * @Title: getLeadActivityLsitForDesk
     * @Description:  领导日程forDESK
     */
    public List<Map<String, String>> getLeadActivityLsitForDesk(
            @Param(value = "orgId") String orgId, @Param(value = "nowTime") String nowTime, @Param(value = "accountId") String accountId, @Param(value = "deptId") String deptId, @Param(value = "levelId") String levelId);


    public List<Map<String, String>> getMobileMyLeadActivity(@Param(value = "orgId") String orgId, @Param(value = "nowTime") String nowTime, @Param(value = "accountId") String accountId, @Param(value = "deptId") String deptId, @Param(value = "levelId") String levelId, @Param(value = "page") Integer page);
}
