package com.core136.mapper.oa;

import com.core136.bean.oa.VoteItem;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface VoteItemMapper extends MyMapper<VoteItem> {

    /**
     * @param orgId
     * @param voteId
     * @return List<Map < String, String>>
     * @Title: getVoteItemListForManage
     * @Description:  获取指定投票的投票项列表
     */
    public List<Map<String, String>> getVoteItemListForManage(@Param(value = "orgId") String orgId, @Param(value = "voteId") String voteId);

    /**
     * @param orgId
     * @param voteId
     * @param recordId
     * @return int
     * @Title: deleteVoteItems
     * @Description:  删除投票项
     */
    public int deleteVoteItems(@Param(value = "orgId") String orgId, @Param(value = "voteId") String voteId, @Param(value = "recordId") String recordId);

    /**
     * @param orgId
     * @param nowTime
     * @param accountId
     * @param deptId
     * @param levelId
     * @param voteId
     * @return List<Map < String, String>>
     * @Title: getVoteItemsList
     * @Description:  获取投票子项列表
     */
    public List<Map<String, String>> getVoteItemsList(@Param(value = "orgId") String orgId, @Param(value = "nowTime") String nowTime,
                                                      @Param(value = "accountId") String accountId, @Param(value = "deptId") String deptId, @Param(value = "levelId") String levelId, @Param(value = "voteId") String voteId);

    /**
     * @param orgId
     * @param voteId
     * @param recordId
     * @return List<Map < String, String>>
     * @Title: getVoteChildItemsList
     * @Description:  获取投票明细项列表
     */
    public List<Map<String, String>> getVoteChildItemsList(@Param(value = "orgId") String orgId, @Param(value = "voteId") String voteId, @Param(value = "recordId") String recordId);

}
