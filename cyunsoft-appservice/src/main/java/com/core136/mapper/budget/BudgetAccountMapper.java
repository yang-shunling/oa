package com.core136.mapper.budget;

import com.core136.bean.budget.BudgetAccount;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface BudgetAccountMapper extends MyMapper<BudgetAccount> {
    /**
     * @param orgId
     * @param levelId
     * @return List<Map < String, String>>
     * @Title: getBudgetAccountTree
     * @Description:   获取预算科目树结构
     */
    public List<Map<String, String>> getBudgetAccountTree(@Param(value = "orgId") String orgId, @Param(value = "levelId") String levelId);

    /**
     * @param budgetAccountId
     * @param orgId
     * @return int
     * @Title: isExistChild
     * @Description:  判读预算科目是否存在
     */
    public int isExistChild(@Param(value = "budgetAccountId") String budgetAccountId, @Param(value = "orgId") String orgId);
}
