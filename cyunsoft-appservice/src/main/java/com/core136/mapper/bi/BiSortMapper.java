package com.core136.mapper.bi;

import com.core136.bean.bi.BiSort;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface BiSortMapper extends MyMapper<BiSort> {
    /**
     * @param levelId
     * @param orgId
     * @return List<Map < String, Object>>
     * @Title getBiSortTree
     * @Description  获取BI分类树结构
     */
    public List<Map<String, Object>> getBiSortTree(@Param(value = "levelId") String levelId, @Param(value = "orgId") String orgId);

    /**
     * @param levelId
     * @param orgId
     * @return List<Map < String, Object>>
     * @Title: getBiSortTreeForParent
     * @Description:  获取BI分类树结构
     */
    public List<Map<String, Object>> getBiSortTreeForParent(@Param(value = "levelId") String levelId, @Param(value = "orgId") String orgId);

}
