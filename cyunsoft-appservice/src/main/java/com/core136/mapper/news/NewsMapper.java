package com.core136.mapper.news;

import com.core136.bean.news.News;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface NewsMapper extends MyMapper<News> {
    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @return List<Map < String, Object>>
     * @Title getNewsManageList
     * @Description  获取新闻维护列表
     */
    public List<Map<String, Object>> getNewsManageList(@Param(value = "orgId") String orgId, @Param(value = "opFlag") String opFlag, @Param(value = "status") String status, @Param(value = "accountId") String accountId, @Param(value = "search") String search);

    /**
     * @param @param  orgId
     * @param @param  accountId
     * @param @param  deptId
     * @param @param  levelId
     * @param @param  newsType
     * @param @param  status
     * @param @param  beginTime
     * @param @param  endTime
     * @param @param  search
     * @param @return 设定文件
     * @return List<Map < String, Object>> 返回类型
     * @Title: getMyNewsList
     * @Description:  获取个人权限内的新闻
     */
    public List<Map<String, Object>> getMyNewsList(@Param(value = "orgId") String orgId,
                                                   @Param(value = "accountId") String accountId,
                                                   @Param(value = "deptId") String deptId,
                                                   @Param(value = "levelId") String levelId,
                                                   @Param(value = "newsType") String newsType,
                                                   @Param(value = "status") String status,
                                                   @Param(value = "beginTime") String beginTime,
                                                   @Param(value = "endTime") String endTime,
                                                   @Param(value = "search") String search);

    /**
     * @Title: getMyNewsListForDesk
     * @Description:  我的桌面上的新闻
     * @param: orgId
     * @param: accountId
     * @param: deptId
     * @param: levelId
     * @param: @return
     * @return: List<Map < String, Object>>
     */
    public List<Map<String, String>> getMyNewsListForDesk(
            @Param(value = "orgId") String orgId,
            @Param(value = "endTime") String endTime,
            @Param(value = "accountId") String accountId,
            @Param(value = "deptId") String deptId,
            @Param(value = "levelId") String levelId);

    public List<Map<String, String>> getMyPicNewsListForDesk(
            @Param(value = "orgId") String orgId,
            @Param(value = "endTime") String endTime,
            @Param(value = "accountId") String accountId,
            @Param(value = "deptId") String deptId,
            @Param(value = "levelId") String levelId);

    /**
     * @param @param  orgId
     * @param @param  accountId
     * @param @param  deptId
     * @param @param  levelId
     * @param @param  newsId
     * @param @return 设定文件
     * @return int 返回类型
     * @Title: updateReadStatus
     * @Description:  更新新闻点击次数
     */
    public int updateReadStatus(@Param(value = "orgId") String orgId,
                                @Param(value = "accountId") String accountId,
                                @Param(value = "deptId") String deptId,
                                @Param(value = "levelId") String levelId,
                                @Param(value = "newsId") String newsId,
                                @Param(value = "reader") String reader);

    /**
     * @param @param  orgId
     * @param @param  newsId
     * @param @return 设定文件
     * @return Map<String, Object> 返回类型
     * @Title: getReadNewsById
     * @Description:  获取阅读新闻详情
     */
    public Map<String, Object> getReadNewsById(@Param(value = "orgId") String orgId, @Param(value = "newsId") String newsId);

    /**
     * @Title: getMobileMyNewsList
     * @Description:  获取移动端新闻列表
     * @param: orgId
     * @param: accountId
     * @param: deptId
     * @param: levelId
     * @param: page
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> getMobileMyNewsList(@Param(value = "orgId") String orgId,
                                                         @Param(value = "accountId") String accountId,
                                                         @Param(value = "deptId") String deptId,
                                                         @Param(value = "levelId") String levelId,
                                                         @Param(value = "page") Integer page);

    /***
     * 获取新闻人员查看状态
     * @param orgId
     * @param newsId
     * @param list
     * @return
     */
    public List<Map<String, String>> getNewsReadStatus(@Param(value = "orgId") String orgId, @Param(value = "newsId") String newsId, @Param(value = "list") List<String> list);
}
