package com.core136.mapper.discuss;

import com.core136.bean.discuss.DiscussNotice;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface DiscussNoticeMapper extends MyMapper<DiscussNotice> {
    public List<Map<String, String>> getDiscussNoticeList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId);
}
