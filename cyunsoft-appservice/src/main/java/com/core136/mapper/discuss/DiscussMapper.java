package com.core136.mapper.discuss;

import com.core136.bean.discuss.Discuss;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface DiscussMapper extends MyMapper<Discuss> {

    /**
     * @param orgId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getDiscussList
     * @Description:  获取版块信息
     */
    public List<Map<String, String>> getDiscussList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId, @Param(value = "search") String search);

    /**
     * @param orgId
     * @param userPriv
     * @param deptPriv
     * @param levelPriv
     * @return List<Map < String, String>>
     * @Title: getMyDiscussList
     * @Description:  获取讨论区列表
     */
    public List<Map<String, String>> getMyDiscussList(@Param(value = "orgId") String orgId, @Param(value = "userPriv") String userPriv,
                                                      @Param(value = "deptPriv") String deptPriv, @Param(value = "levelPriv") String levelPriv);

}
