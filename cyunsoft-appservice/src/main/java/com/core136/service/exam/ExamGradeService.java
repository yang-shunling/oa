package com.core136.service.exam;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.core136.bean.exam.ExamGrade;
import com.core136.bean.exam.ExamQuestions;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.exam.ExamGradeMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
public class ExamGradeService {
    private ExamGradeMapper examGradeMapper;

    @Autowired
    public void setExamGradeMapper(ExamGradeMapper examGradeMapper) {
        this.examGradeMapper = examGradeMapper;
    }

    private ExamQuestionsService examQuestionsService;

    @Autowired
    public void setExamQuestionsService(ExamQuestionsService examQuestionsService) {
        this.examQuestionsService = examQuestionsService;
    }

    public int insertExamGrade(ExamGrade examGrade) {
        return examGradeMapper.insert(examGrade);
    }

    public int deleteExamGrade(ExamGrade examGrade) {
        return examGradeMapper.delete(examGrade);
    }

    public int updateExamGrade(Example example, ExamGrade examGrade) {
        return examGradeMapper.updateByExampleSelective(examGrade, example);
    }

    public ExamGrade selectOneExamGrade(ExamGrade examGrade) {
        return examGradeMapper.selectOne(examGrade);
    }

    /**
     * 获取考试成绩列表
     *
     * @param orgId
     * @param examTestId
     * @param accountId
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String, String>> getExamGradeList(String orgId, String examTestId, String accountId, String beginTime, String endTime, String search) {
        return examGradeMapper.getExamGradeList(orgId, examTestId, accountId, beginTime, endTime, "%" + search + "%");
    }

    /**
     * 获取考试成绩列表
     *
     * @param pageParam
     * @param examTestId
     * @param beginTime
     * @param endTime
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getExamGradeList(PageParam pageParam, String examTestId, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getExamGradeList(pageParam.getOrgId(), examTestId, pageParam.getAccountId(), beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    public List<Map<String, String>> getExamGradeOldList(String orgId, String examTestId, String accountId, String beginTime, String endTime, String search) {
        return examGradeMapper.getExamGradeOldList(orgId, examTestId, accountId, beginTime, endTime, "%" + search + "%");
    }

    public PageInfo<Map<String, String>> getExamGradeOldList(PageParam pageParam, String examTestId, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getExamGradeOldList(pageParam.getOrgId(), examTestId, pageParam.getAccountId(), beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    public Double addExamGread(ExamGrade examGrade) {
        examGrade.setGrade(getGrade(examGrade));
        examGradeMapper.insert(examGrade);
        return examGrade.getGrade();
    }

    public Double getGrade(ExamGrade examGrade) {
        Double grade = 0.0;
        String result = examGrade.getResult();
        if (StringUtils.isNotBlank(result)) {
            JSONArray jsonArray = JSON.parseArray(result);
            for (int i = 0; i < jsonArray.size(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                ExamQuestions examQuestions = getRealAnswer(examGrade.getOrgId(), jsonObject.getString("recordId"));
                if (examQuestions.getGrade() == null) {
                    examQuestions.setGrade(0.0);
                }
                if (jsonObject.getString("type").equals("checkbox")) {
                    String answerStr = jsonObject.getString("answer");
                    String answer = examQuestions.getAnswer();
                    String[] answerArr = answer.split(",");
                    String[] answerStrArr = answerStr.split(",");
                    Arrays.sort(answerArr);
                    Arrays.sort(answerStrArr);
                    String aStr = StringUtils.join(answerStrArr, ",");
                    String bStr = StringUtils.join(answerArr, ",");
                    if (aStr.equals(bStr)) {
                        grade += examQuestions.getGrade();
                    }
                } else if (jsonObject.getString("type").equals("radio")) {
                    if (examQuestions.getAnswer().equals(jsonObject.getString("answer"))) {
                        grade += examQuestions.getGrade();
                    }
                } else if (jsonObject.getString("type").equals("textarea")) {
                    grade += 0;
                }
            }
            return grade;
        } else {
            return 0.0;
        }
    }


    public ExamQuestions getRealAnswer(String orgId, String recordId) {
        ExamQuestions examQuestions = new ExamQuestions();
        examQuestions.setRecordId(recordId);
        examQuestions.setOrgId(orgId);
        examQuestions = examQuestionsService.selectOneExamQuestions(examQuestions);
        return examQuestions;
    }


}
