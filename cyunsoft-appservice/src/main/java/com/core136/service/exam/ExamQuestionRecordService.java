package com.core136.service.exam;

import com.core136.bean.exam.ExamQuestionRecord;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.exam.ExamQuestionRecordMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ExamQuestionRecordService {

    private ExamQuestionRecordMapper examQuestionRecordMapper;

    @Autowired
    public void setExamQuestionRecordMapper(ExamQuestionRecordMapper examQuestionRecordMapper) {
        this.examQuestionRecordMapper = examQuestionRecordMapper;
    }

    public int insertExamQuestionRecord(ExamQuestionRecord examQuestionRecord) {
        return examQuestionRecordMapper.insert(examQuestionRecord);
    }

    public int deleteExamQuestionRecord(ExamQuestionRecord examQuestionRecord) {
        return examQuestionRecordMapper.delete(examQuestionRecord);
    }

    public int updateExamQuestionRecord(Example example, ExamQuestionRecord examQuestionRecord) {
        return examQuestionRecordMapper.updateByExampleSelective(examQuestionRecord, example);
    }

    public ExamQuestionRecord selectOneExamQuestionRecord(ExamQuestionRecord examQuestionRecord) {
        return examQuestionRecordMapper.selectOne(examQuestionRecord);
    }

    /**
     * 获取试卷列表
     *
     * @param orgId
     * @param search
     * @return
     */
    public List<Map<String, String>> getExamQuestionsRecordList(String orgId, String search) {
        return examQuestionRecordMapper.getExamQuestionsRecordList(orgId, "%" + search + "%");
    }

    /**
     * 获取试卷列表
     *
     * @param pageParam
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getExamQuestionsRecordList(PageParam pageParam) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getExamQuestionsRecordList(pageParam.getOrgId(), pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * 获取试卷列表
     *
     * @param orgId
     * @return
     */
    public List<Map<String, String>> getExamQuestionRecordForSelect(String orgId) {
        return examQuestionRecordMapper.getExamQuestionRecordForSelect(orgId);
    }
}
