/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: AttendService.java
 * @Package com.core136.service.attend
 * @Description: 描述
 * @author: lsq
 * @date: 2019年11月15日 上午8:39:32
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.service.attend;

import com.alibaba.fastjson.JSONObject;
import com.core136.bean.account.Account;
import com.core136.bean.attend.Attend;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.attend.AttendMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 */
@Service
public class AttendService {
    private AttendMapper attendMapper;

    @Autowired
    public void setAttendMapper(AttendMapper attendMapper) {
        this.attendMapper = attendMapper;
    }

    public int insertAttend11(Attend attend) {
        return attendMapper.insert(attend);
    }

    /**
     * @param attend
     * @return int
     * @Title: addAttendRecord
     * @Description:  打卡时判断是否已打过卡
     */
    public int addAttendRecord(Attend attend) {
        Attend attend1 = new Attend();
        attend1.setStatus(attend.getStatus());
        attend1.setNowTime(attend.getNowTime());
        attend1.setCreateUser(attend.getCreateUser());
        attend1.setOrgId(attend.getOrgId());
        if (selectCountAttend(attend1) == 0) {
            return insertAttend11(attend);
        } else {
            Example example = new Example(Attend.class);
            example.createCriteria().andEqualTo("status", attend.getStatus()).andEqualTo("nowTime", attend.getNowTime()).andEqualTo("createUser", attend.getCreateUser()).andEqualTo("orgId", attend.getOrgId());
            attend.setAttendId(null);
            return updateAttend(example, attend);
        }
    }

    public int selectCountAttend(Attend attend) {
        return attendMapper.selectCount(attend);
    }

    public int deleteAttend(Attend attend) {
        return attendMapper.delete(attend);
    }

    public int updateAttend(Example example, Attend attend) {
        return attendMapper.updateByExampleSelective(attend, example);
    }

    public Attend selectOneAttend(Attend attend) {
        return attendMapper.selectOne(attend);
    }

    /**
     * @Title: getAttendYearList
     * @Description:  获取年份列表
     * @param: orgId
     * @param: accountId
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> getAttendYearList(String orgId, String accountId) {
        return attendMapper.getAttendYearList(orgId, accountId);
    }

    /**
     * @Title: getMonthList
     * @Description:  获取月份
     * @param: orgId
     * @param: year
     * @param: accountId
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> getMonthList(String orgId, String year, String accountId) {
        return attendMapper.getMonthList(orgId, year, accountId);
    }

    /**
     * @Title: getMyAttendList
     * @Description:  获取个人的考勤记录
     * @param: orgId
     * @param: year
     * @param: month
     * @param: accountId
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> getMyAttendList(String orgId, String year, String month, String accountId, String type) {
        return attendMapper.getMyAttendList(orgId, year, month, accountId, type);
    }

    /**
     * @Title: getTotalAttendList
     * @Description:  考勤统计查询
     * @param: orgId
     * @param: status
     * @param: beginTime
     * @param: endTime
     * @param: deptId
     * @param: createUser
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> getTotalAttendList(String orgId, String type, String beginTime, String endTime, String deptId, String createUser) {
        return attendMapper.getTotalAttendList(orgId, type, beginTime, endTime, deptId, createUser);
    }

    /**
     * @throws Exception
     * @Title: getTotalAttendList
     * @Description:  考勤统计查询
     * @param: pageParam
     * @param: status
     * @param: beginTime
     * @param: endTime
     * @param: deptId
     * @param: createUser
     * @param: @return
     * @return: PageInfo<Map < String, String>>
     */
    public PageInfo<Map<String, String>> getTotalAttendList(PageParam pageParam, String type, String beginTime, String endTime, String deptId, String createUser) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getTotalAttendList(pageParam.getOrgId(), type, beginTime, endTime, deptId, createUser);
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param accountId
     * @param beginTime
     * @param endTime
     * @return List<Map < String, String>>
     * @Title: getMyLevelList
     * @Description:  获取人员请假列表
     */
    public List<Map<String, String>> getMyLevelList(String orgId, String accountId, String beginTime, String endTime, String type) {
        return attendMapper.getMyLevelList(orgId, accountId, beginTime, endTime, type);
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getMyLevelList
     * @Description:  获取人员请假列表
     */
    public PageInfo<Map<String, String>> getMyLevelList(PageParam pageParam, String beginTime, String endTime, String type) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyLevelList(pageParam.getOrgId(), pageParam.getAccountId(), beginTime, endTime, type);
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param accountId
     * @param beginTime
     * @param endTime
     * @return List<Map < String, String>>
     * @Title: getMyTravelList
     * @Description:  获取出差列表
     */
    public List<Map<String, String>> getMyTravelList(String orgId, String accountId, String beginTime, String endTime) {
        return attendMapper.getMyTravelList(orgId, accountId, beginTime, endTime);
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getMyTravelList
     * @Description:  获取出差列表
     */
    public PageInfo<Map<String, String>> getMyTravelList(PageParam pageParam, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyTravelList(pageParam.getOrgId(), pageParam.getAccountId(), beginTime, endTime);
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param accountId
     * @param beginTime
     * @param endTime
     * @return List<Map < String, String>>
     * @Title: getMyOutattendList
     * @Description:  获取外出列表
     */
    public List<Map<String, String>> getMyOutattendList(String orgId, String accountId, String beginTime, String endTime) {
        return attendMapper.getMyOutattendList(orgId, accountId, beginTime, endTime);
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getMyOutattendList
     * @Description:  获取外出列表
     */
    public PageInfo<Map<String, String>> getMyOutattendList(PageParam pageParam, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyOutattendList(pageParam.getOrgId(), pageParam.getAccountId(), beginTime, endTime);
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param accountId
     * @param beginTime
     * @param endTime
     * @return List<Map < String, String>>
     * @Title: getMyOverTimeList
     * @Description:  获取加班列表
     */
    public List<Map<String, String>> getMyOverTimeList(String orgId, String accountId, String beginTime, String endTime) {
        return attendMapper.getMyOverTimeList(orgId, accountId, beginTime, endTime);
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getMyOverTimeList
     * @Description:  获取加班列表
     */
    public PageInfo<Map<String, String>> getMyOverTimeList(PageParam pageParam, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyOverTimeList(pageParam.getOrgId(), pageParam.getAccountId(), beginTime, endTime);
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param accountId
     * @param beginTime
     * @param endTime
     * @return List<Map < String, String>>
     * @Title: getMyDutyList
     * @Description: 获取值班列表
     */
    public List<Map<String, String>> getMyDutyList(String orgId, String accountId, String beginTime, String endTime) {
        return attendMapper.getMyDutyList(orgId, accountId, beginTime, endTime);
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getMyDutyList
     * @Description:  获取值班列表
     */
    public PageInfo<Map<String, String>> getMyDutyList(PageParam pageParam, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyDutyList(pageParam.getOrgId(), pageParam.getAccountId(), beginTime, endTime);
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @Title: getMyAllAttendList
     * @Description:  获取个人一年内的考勤记录
     * @param: orgId
     * @param: year
     * @param: accountId
     * @param: @return
     * @return: JSONObject
     */
    public JSONObject getMyAllAttendList(String orgId, String year, String accountId, String type) {
        JSONObject json = new JSONObject();
        for (int i = 1; i <= 12; i++) {
            if (i < 10) {
                json.put(i + "", getMyAttendList(orgId, year, "0" + i, accountId, type));
            } else {
                json.put(i + "", getMyAttendList(orgId, year, i + "", accountId, type));
            }
        }
        return json;
    }

    /**
     * @param account
     * @return int
     * @Title: getAttendStatusDay
     * @Description:  判断是否已打上班考勤
     */
    public int getAttendStatusDay(Account account) {
        Attend attend = new Attend();
        attend.setStatus("1");
        attend.setNowTime(SysTools.getTime("yyyy-MM-dd"));
        attend.setCreateUser(account.getAccountId());
        attend.setOrgId(account.getOrgId());
        return attendMapper.selectCount(attend);
    }

    /**
     * @param account
     * @return int
     * @Title: getOffWorkAttendStatusDay
     * @Description:  判读是否已打下班考勤
     */
    public int getOffWorkAttendStatusDay(Account account) {
        Attend attend = new Attend();
        attend.setStatus("2");
        attend.setNowTime(SysTools.getTime("yyyy-MM-dd"));
        attend.setCreateUser(account.getAccountId());
        attend.setOrgId(account.getOrgId());
        return attendMapper.selectCount(attend);
    }
}
