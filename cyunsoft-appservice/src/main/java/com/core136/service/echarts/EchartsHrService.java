package com.core136.service.echarts;

import com.core136.bean.account.Account;
import com.core136.bi.option.bean.OptionConfig;
import com.core136.bi.option.property.*;
import com.core136.bi.option.resdata.LegendData;
import com.core136.bi.option.resdata.SeriesData;
import com.core136.bi.option.style.*;
import com.core136.bi.option.units.BarOption;
import com.core136.bi.option.units.PieOption;
import com.core136.mapper.echarts.EchartsHrMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class EchartsHrService {
    private PieOption pieOption = new PieOption();
    private BarOption barOption = new BarOption();
    private EchartsHrMapper echartsHrMapper;

    @Autowired
    public void setEchartsHrMapper(EchartsHrMapper echartsHrMapper) {
        this.echartsHrMapper = echartsHrMapper;
    }

    /**
     * @param orgId
     * @param deptId
     * @param dataType
     * @return List<Map < String, String>>
     * @Title: getCareTableForAnalysis
     * @Description:  人员关怀类型分析tabale
     */
    public List<Map<String, String>> getCareTableForAnalysis(String orgId, String deptId, String dataType) {
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "U.CARE_TYPE";
            module = "careType";
        }
        return echartsHrMapper.getCareAnalysis(orgId, deptId, module, groupBy);
    }

    /**
     * @param orgId
     * @param deptId
     * @param dataType
     * @return OptionConfig
     * @Title: getCareBarForAnalysis
     * @Description:  人员关怀类型柱状图分析
     */
    public OptionConfig getCareBarForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "U.CARE_TYPE";
            module = "careType";
            optionSeriesName = "人员关怀类型分析";
        }
        List<Map<String, String>> resdataList = echartsHrMapper.getCareAnalysis(orgId, deptId, module, groupBy);
        return getBarForAnalysis(resdataList, optionSeriesName);
    }

    /**
     * @param orgId
     * @param deptId
     * @param dataType
     * @return OptionConfig
     * @Title: getCarePieForAnalysis
     * @Description: 人员关怀类型饼状图分析
     */
    public OptionConfig getCarePieForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String optionTitleText = "";
        String optionTitleSubtext = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "U.CARE_TYPE";
            module = "careType";
            optionSeriesName = "人员关怀类型分析";
            optionTitleText = "人员关怀类型统计";
            optionTitleSubtext = "人员关怀类型占比";
        }
        List<Map<String, String>> resdataList = echartsHrMapper.getCareAnalysis(orgId, deptId, module, groupBy);
        return getPieForAnalysis(resdataList, optionSeriesName, optionTitleText, optionTitleSubtext);
    }

    /**
     * @param orgId
     * @param deptId
     * @param dataType
     * @return List<Map < String, String>>
     * @Title: getEvaluationTableForAnalysis
     * @Description:  职称分析tabale
     */
    public List<Map<String, String>> getEvaluationTableForAnalysis(String orgId, String deptId, String dataType) {
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "u.get_type";
            module = "getType";
        } else if (dataType.equals("2")) {
            groupBy = "u.dept_id";
            module = "deptId";
        } else if (dataType.equals("3")) {
            groupBy = "u.post";
            module = "post";
        }
        return echartsHrMapper.getEvaluationAnalysis(orgId, deptId, module, groupBy);
    }

    /**
     * @param orgId
     * @param deptId
     * @param dataType
     * @return OptionConfig
     * @Title: getEvaluationBarForAnalysis
     * @Description:  称职评定柱状图分析
     */
    public OptionConfig getEvaluationBarForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "u.get_type";
            module = "getType";
            optionSeriesName = "称职评定分析";
        } else if (dataType.equals("2")) {
            groupBy = "u.dept_id";
            module = "deptId";
            optionSeriesName = "称职评定分析";
        } else if (dataType.equals("3")) {
            groupBy = "u.post";
            module = "post";
            optionSeriesName = "称职评定分析";
        }
        List<Map<String, String>> resdataList = echartsHrMapper.getEvaluationAnalysis(orgId, deptId, module, groupBy);
        return getBarForAnalysis(resdataList, optionSeriesName);
    }

    /**
     * @param orgId
     * @param deptId
     * @param dataType
     * @return OptionConfig
     * @Title: getEvaluationPieForAnalysis
     * @Description:  称职评定饼状图分析
     */
    public OptionConfig getEvaluationPieForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String optionTitleText = "";
        String optionTitleSubtext = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "u.get_type";
            module = "getType";
            optionSeriesName = "称职评定分析";
            optionTitleText = "称职评定统计";
            optionTitleSubtext = "称职评定占比";
        } else if (dataType.equals("2")) {
            groupBy = "u.dept_id";
            module = "deptId";
            optionSeriesName = "称职评定分析";
            optionTitleText = "称职评定统计";
            optionTitleSubtext = "称职评定占比";
        } else if (dataType.equals("3")) {
            groupBy = "u.post";
            module = "post";
            optionSeriesName = "称职评定分析";
            optionTitleText = "称职评定统计";
            optionTitleSubtext = "称职评定占比";
        }
        List<Map<String, String>> resdataList = echartsHrMapper.getEvaluationAnalysis(orgId, deptId, module, groupBy);
        return getPieForAnalysis(resdataList, optionSeriesName, optionTitleText, optionTitleSubtext);
    }

    /**
     * @param orgId
     * @param deptId
     * @param dataType
     * @return List<Map < String, String>>
     * @Title: getReinstatTableForAnalysis
     * @Description:  人员复职分析tabale
     */
    public List<Map<String, String>> getReinstatTableForAnalysis(String orgId, String deptId, String dataType) {
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "u.reinstatement_type";
            module = "reinstatementType";
        } else if (dataType.equals("2")) {
            groupBy = "u.dept_id";
            module = "deptId";
        } else if (dataType.equals("3")) {
            groupBy = "u.level_id";
            module = "levelId";
        }
        return echartsHrMapper.getReinstatAnalysis(orgId, deptId, module, groupBy);
    }

    /**
     * @param orgId
     * @param deptId
     * @param dataType
     * @return OptionConfig
     * @Title: getReinstatBarForAnalysis
     * @Description:  人员复职柱状图分析
     */
    public OptionConfig getReinstatBarForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "u.reinstatement_type";
            module = "reinstatementType";
            optionSeriesName = "人员复职分析";
        } else if (dataType.equals("2")) {
            groupBy = "u.dept_id";
            module = "deptId";
            optionSeriesName = "人员复职分析";
        } else if (dataType.equals("3")) {
            groupBy = "u.level_id";
            module = "levelId";
            optionSeriesName = "人员复职分析";
        }
        List<Map<String, String>> resdataList = echartsHrMapper.getReinstatAnalysis(orgId, deptId, module, groupBy);
        return getBarForAnalysis(resdataList, optionSeriesName);
    }

    /**
     * @param orgId
     * @param deptId
     * @param dataType
     * @return OptionConfig
     * @Title: getReinstatPieForAnalysis
     * @Description:  人员复职饼状图分析
     */
    public OptionConfig getReinstatPieForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String optionTitleText = "";
        String optionTitleSubtext = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "u.reinstatement_type";
            module = "reinstatementType";
            optionSeriesName = "人员复职分析";
            optionTitleText = "人员复职统计";
            optionTitleSubtext = "人员复职占比";
        } else if (dataType.equals("2")) {
            groupBy = "u.dept_id";
            module = "deptId";
            optionSeriesName = "人员复职分析";
            optionTitleText = "人员复职统计";
            optionTitleSubtext = "人员复职占比";
        } else if (dataType.equals("3")) {
            groupBy = "u.level_id";
            module = "levelId";
            optionSeriesName = "人员复职分析";
            optionTitleText = "人员复职统计";
            optionTitleSubtext = "人员复职占比";
        }
        List<Map<String, String>> resdataList = echartsHrMapper.getReinstatAnalysis(orgId, deptId, module, groupBy);
        return getPieForAnalysis(resdataList, optionSeriesName, optionTitleText, optionTitleSubtext);
    }


    /**
     * @param orgId
     * @param deptId
     * @param dataType
     * @return List<Map < String, String>>
     * @Title: getLevelTableForAnalysis
     * @Description:  人员离职分析tabale
     */
    public List<Map<String, String>> getLevelTableForAnalysis(String orgId, String deptId, String dataType) {
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "u.level_type";
            module = "levelType";
        } else if (dataType.equals("2")) {
            groupBy = "u.dept_id";
            module = "deptId";
        }
        return echartsHrMapper.getLevelAnalysis(orgId, deptId, module, groupBy);
    }

    /**
     * @param orgId
     * @param deptId
     * @param dataType
     * @return OptionConfig
     * @Title: getLevelBarForAnalysis
     * @Description:  人员离职柱状图分析
     */
    public OptionConfig getLevelBarForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "u.level_type";
            module = "levelType";
            optionSeriesName = "人员离职分析";
        } else if (dataType.equals("2")) {
            groupBy = "u.dept_id";
            module = "deptId";
            optionSeriesName = "人员离职分析";
        }
        List<Map<String, String>> resdataList = echartsHrMapper.getLevelAnalysis(orgId, deptId, module, groupBy);
        return getBarForAnalysis(resdataList, optionSeriesName);
    }

    /**
     * @param orgId
     * @param deptId
     * @param dataType
     * @return OptionConfig
     * @Title: getLevelPieForAnalysis
     * @Description:  人员离职饼状图分析
     */
    public OptionConfig getLevelPieForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String optionTitleText = "";
        String optionTitleSubtext = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "u.level_type";
            module = "levelType";
            optionSeriesName = "人员离职分析";
            optionTitleText = "人员离职统计";
            optionTitleSubtext = "人员离职占比";
        } else if (dataType.equals("2")) {
            groupBy = "u.dept_id";
            module = "deptId";
            optionSeriesName = "人员离职分析";
            optionTitleText = "人员离职统计";
            optionTitleSubtext = "人员离职占比";
        }
        List<Map<String, String>> resdataList = echartsHrMapper.getLevelAnalysis(orgId, deptId, module, groupBy);
        return getPieForAnalysis(resdataList, optionSeriesName, optionTitleText, optionTitleSubtext);
    }

    /**
     * @param orgId
     * @param deptId
     * @param dataType
     * @return List<Map < String, String>>
     * @Title: getTransferTableForAnalysis
     * @Description:  调动类型分析tabale
     */
    public List<Map<String, String>> getTransferTableForAnalysis(String orgId, String deptId, String dataType) {
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "U.TRANSFER_TYPE";
            module = "transferType";
        }
        return echartsHrMapper.getTransferAnalysis(orgId, deptId, module, groupBy);
    }

    /**
     * @param orgId
     * @param deptId
     * @param dataType
     * @return OptionConfig
     * @Title: getTransferBarForAnalysis
     * @Description:  调动类型柱状图分析
     */
    public OptionConfig getTransferBarForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "U.TRANSFER_TYPE";
            module = "transferType";
            optionSeriesName = "调动类型分析";
        }
        List<Map<String, String>> resdataList = echartsHrMapper.getTransferAnalysis(orgId, deptId, module, groupBy);
        return getBarForAnalysis(resdataList, optionSeriesName);
    }

    /**
     * @param orgId
     * @param deptId
     * @param dataType
     * @return OptionConfig
     * @Title: getTransferPieForAnalysis
     * @Description:  调动类型饼状图分析
     */
    public OptionConfig getTransferPieForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String optionTitleText = "";
        String optionTitleSubtext = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "U.TRANSFER_TYPE";
            module = "transferType";
            optionSeriesName = "调动类型分析";
            optionTitleText = "调动类型统计";
            optionTitleSubtext = "调动类型占比";
        }
        List<Map<String, String>> resdataList = echartsHrMapper.getTransferAnalysis(orgId, deptId, module, groupBy);
        return getPieForAnalysis(resdataList, optionSeriesName, optionTitleText, optionTitleSubtext);
    }

    /**
     * @param orgId
     * @param deptId
     * @param dataType
     * @return List<Map < String, String>>
     * @Title: getLearnTableForAnalysis
     * @Description:  劳动持能分析tabale
     */
    public List<Map<String, String>> getSkillsTableForAnalysis(String orgId, String deptId, String dataType) {
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "U.SKILLS_LEVEL";
            module = "skillsLevel";
        }
        return echartsHrMapper.getSkillsAnalysis(orgId, deptId, module, groupBy);
    }

    /**
     * @param orgId
     * @param deptId
     * @param dataType
     * @return OptionConfig
     * @Title: getSkillsBarForAnalysis
     * @Description:  劳动技能柱状图分析
     */
    public OptionConfig getSkillsBarForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "U.SKILLS_LEVEL";
            module = "skillsLevel";
            optionSeriesName = "劳动技能分析";
        }
        List<Map<String, String>> resdataList = echartsHrMapper.getSkillsAnalysis(orgId, deptId, module, groupBy);
        return getBarForAnalysis(resdataList, optionSeriesName);
    }

    /**
     * @param orgId
     * @param deptId
     * @param dataType
     * @return OptionConfig
     * @Title: getSkillsPieForAnalysis
     * @Description:  劳动技能饼状图分析
     */
    public OptionConfig getSkillsPieForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String optionTitleText = "";
        String optionTitleSubtext = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "U.SKILLS_LEVEL";
            module = "skillsLevel";
            optionSeriesName = "劳动技能分析";
            optionTitleText = "劳动技能统计";
            optionTitleSubtext = "劳动技能占比";
        }
        List<Map<String, String>> resdataList = echartsHrMapper.getSkillsAnalysis(orgId, deptId, module, groupBy);
        return getPieForAnalysis(resdataList, optionSeriesName, optionTitleText, optionTitleSubtext);
    }

    /**
     * @param orgId
     * @param deptId
     * @param dataType
     * @return List<Map < String, String>>
     * @Title: getLearnTableForAnalysis
     * @Description:  学习经历分析tabale
     */
    public List<Map<String, String>> getLearnTableForAnalysis(String orgId, String deptId, String dataType) {
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "U.HIGHSET_DEGREE";
            module = "highsetDegree";
        }
        return echartsHrMapper.getLearnAnalysis(orgId, deptId, module, groupBy);
    }

    /**
     * @param orgId
     * @param deptId
     * @param dataType
     * @return OptionConfig
     * @Title: getLearnBarForAnalysis
     * @Description:  学习经历柱状图分析
     */
    public OptionConfig getLearnBarForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "U.HIGHSET_DEGREE";
            module = "highsetDegree";
            optionSeriesName = "证照分析";
        }
        List<Map<String, String>> resdataList = echartsHrMapper.getLearnAnalysis(orgId, deptId, module, groupBy);
        return getBarForAnalysis(resdataList, optionSeriesName);
    }

    /**
     * @param orgId
     * @param deptId
     * @param dataType
     * @return OptionConfig
     * @Title: getLearnPieForAnalysis
     * @Description:  学习经历饼状图分析
     */
    public OptionConfig getLearnPieForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String optionTitleText = "";
        String optionTitleSubtext = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "U.HIGHSET_DEGREE";
            module = "highsetDegree";
            optionSeriesName = "证照分析";
            optionTitleText = "证照分析统计";
            optionTitleSubtext = "证照分析占比";
        }
        List<Map<String, String>> resdataList = echartsHrMapper.getLearnAnalysis(orgId, deptId, module, groupBy);
        return getPieForAnalysis(resdataList, optionSeriesName, optionTitleText, optionTitleSubtext);
    }

    /**
     * @param orgId
     * @param deptId
     * @param dataType
     * @return List<Map < String, String>>
     * @Title: getLicenceeTableForAnalysis
     * @Description:  学习经历 tabale
     */
    public List<Map<String, String>> getLicenceTableForAnalysis(String orgId, String deptId, String dataType) {
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "U.LICENCE_TYPE";
            module = "licenceType";
        }
        return echartsHrMapper.getLicenceAnalysis(orgId, deptId, module, groupBy);
    }

    /**
     * @param orgId
     * @param deptId
     * @param dataType
     * @return OptionConfig
     * @Title: getLicenceBarForAnalysis
     * @Description:  证照柱状图分析
     */
    public OptionConfig getLicenceBarForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "U.LICENCE_TYPE";
            module = "licenceType";
            optionSeriesName = "证照分析";
        }
        List<Map<String, String>> resdataList = echartsHrMapper.getLicenceAnalysis(orgId, deptId, module, groupBy);
        return getBarForAnalysis(resdataList, optionSeriesName);
    }

    /**
     * @param orgId
     * @param deptId
     * @param dataType
     * @return OptionConfig
     * @Title: getLicencePieForAnalysis
     * @Description:  证照饼状图分析
     */
    public OptionConfig getLicencePieForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String optionTitleText = "";
        String optionTitleSubtext = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "U.LICENCE_TYPE";
            module = "licenceType";
            optionSeriesName = "证照分析";
            optionTitleText = "证照分析统计";
            optionTitleSubtext = "证照分析占比";
        }
        List<Map<String, String>> resdataList = echartsHrMapper.getLicenceAnalysis(orgId, deptId, module, groupBy);
        return getPieForAnalysis(resdataList, optionSeriesName, optionTitleText, optionTitleSubtext);
    }

    /**
     * @param orgId
     * @param deptId
     * @param dataType
     * @return List<Map < String, String>>
     * @Title: getIncentiveTableForAnalysis
     * @Description:  奖惩分析tabale
     */
    public List<Map<String, String>> getIncentiveTableForAnalysis(String orgId, String deptId, String dataType) {
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "U.INCENTIVE_TYPE";
            module = "incentiveType";
        } else if (dataType.equals("2")) {
            groupBy = "U.INCENTIVE_ITEM";
            module = "incentiveItem";
        }
        return echartsHrMapper.getIncentiveAnalysis(orgId, deptId, module, groupBy);
    }

    /**
     * @param orgId
     * @param deptId
     * @param dataType
     * @return OptionConfig
     * @Title: getIncentiveBarForAnalysis
     * @Description:  奖惩分析柱状图
     */
    public OptionConfig getIncentiveBarForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "U.INCENTIVE_TYPE";
            module = "incentiveType";
            optionSeriesName = "奖惩类型";
        } else if (dataType.equals("2")) {
            groupBy = "U.INCENTIVE_ITEM";
            module = "incentiveItem";
            optionSeriesName = "处理事项";
        }
        List<Map<String, String>> resdataList = echartsHrMapper.getIncentiveAnalysis(orgId, deptId, module, groupBy);
        return getBarForAnalysis(resdataList, optionSeriesName);
    }

    /**
     * @param orgId
     * @param deptId
     * @param dataType
     * @return OptionConfig
     * @Title: getIncentivePieForAnalysis
     * @Description:  奖惩分析饼状图分析
     */
    public OptionConfig getIncentivePieForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String optionTitleText = "";
        String optionTitleSubtext = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "U.INCENTIVE_TYPE";
            module = "incentiveType";
            optionSeriesName = "奖惩类型";
            optionTitleText = "奖惩类型统计";
            optionTitleSubtext = "奖惩类型占比";
        } else if (dataType.equals("2")) {
            groupBy = "U.INCENTIVE_ITEM";
            module = "incentiveItem";
            optionSeriesName = "处理事项";
            optionTitleText = "处理事项统计";
            optionTitleSubtext = "处理事项占比";
        }
        List<Map<String, String>> resdataList = echartsHrMapper.getIncentiveAnalysis(orgId, deptId, module, groupBy);
        return getPieForAnalysis(resdataList, optionSeriesName, optionTitleText, optionTitleSubtext);
    }


    /**
     * @param orgId
     * @param deptId
     * @param dataType
     * @return List<Map < String, String>>
     * @Title: getContractTableForAnalysis
     * @Description:  合同分析tabale
     */
    public List<Map<String, String>> getContractTableForAnalysis(String orgId, String deptId, String dataType) {
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "U.CONTRACT_TYPE";
            module = "contractType";
        } else if (dataType.equals("2")) {
            groupBy = "U.ENTERPRIES";
            module = "enterpries";
        } else if (dataType.equals("3")) {
            groupBy = "U.SIGN_TYPE";
            module = "signType";
        } else if (dataType.equals("4")) {
            groupBy = "U.POOL_POSITION";
            module = "poolPosition";
        }
        return echartsHrMapper.getContractAnalysis(orgId, deptId, module, groupBy);
    }

    /**
     * @param orgId
     * @param deptId
     * @param dataType
     * @return OptionConfig
     * @Title: getContractBarForAnalysis
     * @Description:  合同柱状图
     */
    public OptionConfig getContractBarForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "U.CONTRACT_TYPE";
            module = "contractType";
            optionSeriesName = "合同类型";
        } else if (dataType.equals("2")) {
            groupBy = "U.ENTERPRIES";
            module = "enterpries";
            optionSeriesName = "签约公司";
        } else if (dataType.equals("3")) {
            groupBy = "U.SIGN_TYPE";
            module = "signType";
            optionSeriesName = "签约方式";
        } else if (dataType.equals("4")) {
            groupBy = "U.POOL_POSITION";
            module = "poolPosition";
            optionSeriesName = "应聘岗位";
        }
        List<Map<String, String>> resdataList = echartsHrMapper.getContractAnalysis(orgId, deptId, module, groupBy);
        return getBarForAnalysis(resdataList, optionSeriesName);
    }

    /**
     * @param orgId
     * @param deptId
     * @param dataType
     * @return OptionConfig
     * @Title: getContractPieForAnalysis
     * @Description:  合同饼状图分析
     */
    public OptionConfig getContractPieForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String optionTitleText = "";
        String optionTitleSubtext = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "U.CONTRACT_TYPE";
            module = "contractType";
            optionSeriesName = "合同类型";
            optionTitleText = "合同类型统计";
            optionTitleSubtext = "合同类型占比";
        } else if (dataType.equals("2")) {
            groupBy = "U.ENTERPRIES";
            module = "enterpries";
            optionSeriesName = "签约公司";
            optionTitleText = "签约公司统计";
            optionTitleSubtext = "签约公司占比";
        } else if (dataType.equals("3")) {
            groupBy = "U.SIGN_TYPE";
            module = "signType";
            optionSeriesName = "签约方式";
            optionTitleText = "签约方式统计";
            optionTitleSubtext = "签约方式占比";
        } else if (dataType.equals("4")) {
            groupBy = "U.POOL_POSITION";
            module = "poolPosition";
            optionSeriesName = "应聘岗位";
            optionTitleText = "应聘岗位统计";
            optionTitleSubtext = "应聘岗位占比";
        }
        List<Map<String, String>> resdataList = echartsHrMapper.getContractAnalysis(orgId, deptId, module, groupBy);
        return getPieForAnalysis(resdataList, optionSeriesName, optionTitleText, optionTitleSubtext);
    }

    /**
     * @param orgId
     * @param deptId
     * @param dataType
     * @return List<Map < String, String>>
     * @Title: getBaseInfoTableForAnalysis
     * @Description:  人事档案分析Table
     */
    public List<Map<String, String>> getBaseInfoTableForAnalysis(String orgId, String deptId, String dataType) {
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "U.WORK_TYPE";
            module = "workType";
        } else if (dataType.equals("2")) {
            groupBy = "U.HIGHSET_SHOOL";
            module = "highsetShool";
        } else if (dataType.equals("3")) {
            groupBy = "U.PERSENT_POSITION";
            module = "persentPosition";
        } else if (dataType.equals("4")) {
            groupBy = "U.NATIVE_PLACE";
            module = "nativePlace";
        } else if (dataType.equals("5")) {
            groupBy = "U.WAGES_LEVEL";
            module = "wagesLevel";
        } else if (dataType.equals("6")) {
            groupBy = "U.POLITICAL_STATUS";
            module = "politicalStatus";
        }
        return echartsHrMapper.getBaseInfoAnalysis(orgId, deptId, module, groupBy);
    }

    /**
     * @param orgId
     * @param deptId
     * @param dataType
     * @return OptionConfig
     * @Title: getBaseInfoBarForAnalysis
     * @Description:  人事档案柱状图
     */
    public OptionConfig getBaseInfoBarForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "U.WORK_TYPE";
            module = "workType";
            optionSeriesName = "工种";
        } else if (dataType.equals("2")) {
            groupBy = "U.HIGHSET_SHOOL";
            module = "highsetShool";
            optionSeriesName = "学历";
        } else if (dataType.equals("3")) {
            groupBy = "U.PERSENT_POSITION";
            module = "persentPosition";
            optionSeriesName = "职称";
        } else if (dataType.equals("4")) {
            groupBy = "U.NATIVE_PLACE";
            module = "nativePlace";
            optionSeriesName = "地区";
        } else if (dataType.equals("5")) {
            groupBy = "U.WAGES_LEVEL";
            module = "wagesLevel";
            optionSeriesName = "工资级别";
        } else if (dataType.equals("6")) {
            groupBy = "U.POLITICAL_STATUS";
            module = "politicalStatus";
            optionSeriesName = "政治面貌";
        }
        List<Map<String, String>> resdataList = echartsHrMapper.getBaseInfoAnalysis(orgId, deptId, module, groupBy);
        return getBarForAnalysis(resdataList, optionSeriesName);
    }

    /**
     * @param orgId
     * @param deptId
     * @param dataType
     * @return OptionConfig
     * @Title: getBaseInfoPieForAnalysis
     * @Description:  人事基本档案饼状图分析
     */
    public OptionConfig getBaseInfoPieForAnalysis(String orgId, String deptId, String dataType) {
        String optionSeriesName = "";
        String optionTitleText = "";
        String optionTitleSubtext = "";
        String module = "";
        String groupBy = "";
        if (dataType.equals("1")) {
            groupBy = "U.WORK_TYPE";
            module = "workType";
            optionSeriesName = "工种";
            optionTitleText = "人员工种统计";
            optionTitleSubtext = "人员工种占比";
        } else if (dataType.equals("2")) {
            groupBy = "U.HIGHSET_SHOOL";
            module = "highsetShool";
            optionSeriesName = "学历";
            optionTitleText = "人员学历统计";
            optionTitleSubtext = "人员学历占比";
        } else if (dataType.equals("3")) {
            groupBy = "U.PERSENT_POSITION";
            module = "persentPosition";
            optionSeriesName = "职称";
            optionTitleText = "人员职称统计";
            optionTitleSubtext = "人员职称占比";
        } else if (dataType.equals("4")) {
            groupBy = "U.NATIVE_PLACE";
            module = "nativePlace";
            optionSeriesName = "地区";
            optionTitleText = "人员籍贯统计";
            optionTitleSubtext = "人员籍贯占比";
        } else if (dataType.equals("5")) {
            groupBy = "U.WAGES_LEVEL";
            module = "wagesLevel";
            optionSeriesName = "工资级别";
            optionTitleText = "人员工资级别统计";
            optionTitleSubtext = "人员工资级别占比";
        } else if (dataType.equals("6")) {
            groupBy = "U.POLITICAL_STATUS";
            module = "politicalStatus";
            optionSeriesName = "政治面貌";
            optionTitleText = "人员政治面貌统计";
            optionTitleSubtext = "人员政治面貌占比";
        }
        List<Map<String, String>> resdataList = echartsHrMapper.getBaseInfoAnalysis(orgId, deptId, module, groupBy);
        return getPieForAnalysis(resdataList, optionSeriesName, optionTitleText, optionTitleSubtext);
    }

    /**
     * @param resdataList
     * @param seriesDataName
     * @return OptionConfig
     * @Title: getBaseInfoBarForAnalysis
     * @Description:  档案分析柱状图
     */
    public OptionConfig getBarForAnalysis(List<Map<String, String>> resdataList, String seriesDataName) {
        OptionConfig optionConfig = new OptionConfig();
        String[] xdata = new String[resdataList.size()];
        Double[] ydata = new Double[resdataList.size()];
        for (int i = 0; i < resdataList.size(); i++) {
            if (StringUtils.isNotBlank(resdataList.get(i).get("name"))) {
                xdata[i] = resdataList.get(i).get("name");
            } else {
                xdata[i] = "other" + i;
            }
            ydata[i] = Double.valueOf(String.valueOf(resdataList.get(i).get("value")));
        }
        OptionSeries optionSeries = new OptionSeries();
        optionSeries.setName(seriesDataName);
        optionSeries.setType("bar");
        optionSeries.setBarWidth("60%");
        optionSeries.setData(ydata);
        OptionXAxis xAxis = new OptionXAxis();
        xAxis.setType("category");
        xAxis.setData(xdata);
        AxisTick axisTick = new AxisTick();
        axisTick.setAlignWithLabel(true);
        xAxis.setAxisTick(axisTick);
        optionConfig = barOption.getBarTickAlignChartOption(new OptionXAxis[]{xAxis}, new OptionSeries[]{optionSeries});
        OptionTooltip optionTooltip = new OptionTooltip();
        optionTooltip.setTrigger("axis");
        AxisPointer axisPointer = new AxisPointer();
        axisPointer.setType("shadow");
        optionTooltip.setAxisPointer(axisPointer);
        optionConfig.setTooltip(optionTooltip);
        OptionGrid optionGrid = new OptionGrid();
        optionGrid.setLeft("3%");
        optionGrid.setRight("4%");
        optionGrid.setBottom("3%");
        optionGrid.setContainLabel(true);
        optionConfig.setGrid(optionGrid);
        return optionConfig;
    }


    /**
     * @param resdataList
     * @param optionSeriesName
     * @param optionTitleText
     * @param optionTitleSubtext
     * @return OptionConfig
     * @Title: getBaseInfoPieForAnalysisPlacePie
     * @Description:  人事档案的饼状图分析
     */
    public OptionConfig getPieForAnalysis(List<Map<String, String>> resdataList, String optionSeriesName, String optionTitleText, String optionTitleSubtext) {
        OptionConfig optionConfig = new OptionConfig();
        OptionSeries[] optionSeriesArr = new OptionSeries[1];
        SeriesData[] dataArr = new SeriesData[resdataList.size()];
        int selectedLeng = 0;
        if (dataArr.length >= 10) {
            selectedLeng = 10;
        } else {
            selectedLeng = dataArr.length;
        }
        String[] selected = new String[selectedLeng];
        LegendData[] legendDatas = new LegendData[dataArr.length];
        for (int i = 0; i < dataArr.length; i++) {
            if (StringUtils.isBlank(resdataList.get(i).get("name"))) {
                resdataList.get(i).put("name", "other" + i);
            }
            if (i < selectedLeng) {
                selected[i] = resdataList.get(i).get("name").toString();
            }
            LegendData legendData = new LegendData();
            legendData.setName(resdataList.get(i).get("name").toString());
            legendDatas[i] = legendData;
            SeriesData seriesData = new SeriesData();
            seriesData.setName(resdataList.get(i).get("name").toString());
            seriesData.setValue(Double.valueOf(String.valueOf(resdataList.get(i).get("value"))));
            dataArr[i] = seriesData;
        }
        OptionSeries optionSeries = new OptionSeries();
        optionSeries.setName(optionSeriesName);
        optionSeries.setType("pie");
        optionSeries.setRadius("55%");
        optionSeries.setCenter(new String[]{"40%", "50%"});
        Emphasis emphasis = new Emphasis();
        ItemStyle itemStyle = new ItemStyle();
        itemStyle.setShadowBlur(10);
        itemStyle.setShadowOffsetX(0);
        itemStyle.setShadowColor("rgba(0, 0, 0, 0.5)");
        emphasis.setItemStyle(itemStyle);
        optionSeries.setData(dataArr);
        optionSeriesArr[0] = optionSeries;
        optionConfig.setSeries(optionSeriesArr);
        optionConfig = pieOption.getPieLegendChartOption(legendDatas, selected, optionSeriesArr);
        OptionTitle optionTitle = new OptionTitle();
        optionTitle.setText(optionTitleText);
        optionTitle.setSubtext(optionTitleSubtext);
        optionTitle.setLeft("center");
        optionConfig.setTitle(optionTitle);
        return optionConfig;
    }


    /**
     * @param account
     * @return OptionConfig
     * @Title: getNativePlacePie
     * @Description:  HR人员籍贯占比
     */
    public OptionConfig getNativePlacePie(Account account) {
        OptionConfig optionConfig = new OptionConfig();
        List<Map<String, String>> resdataList = echartsHrMapper.getNativePlacePie(account.getOrgId());
        OptionSeries[] optionSeriesArr = new OptionSeries[1];
        SeriesData[] dataArr = new SeriesData[resdataList.size()];
        int selectedLeng = 0;
        if (dataArr.length >= 10) {
            selectedLeng = 10;
        } else {
            selectedLeng = dataArr.length;
        }
        String[] selected = new String[selectedLeng];
        LegendData[] legendDatas = new LegendData[dataArr.length];
        for (int i = 0; i < dataArr.length; i++) {
            if (StringUtils.isBlank(resdataList.get(i).get("name"))) {
                resdataList.get(i).put("name", "other" + i);
            }
            if (i < selectedLeng) {
                selected[i] = resdataList.get(i).get("name").toString();
            }
            LegendData legendData = new LegendData();
            legendData.setName(resdataList.get(i).get("name").toString());
            legendDatas[i] = legendData;
            SeriesData seriesData = new SeriesData();
            seriesData.setName(resdataList.get(i).get("name").toString());
            seriesData.setValue(Double.valueOf(String.valueOf(resdataList.get(i).get("value"))));
            dataArr[i] = seriesData;
        }
        OptionSeries optionSeries = new OptionSeries();
        optionSeries.setName("地区");
        optionSeries.setType("pie");
        optionSeries.setRadius("55%");
        optionSeries.setCenter(new String[]{"40%", "50%"});
        Emphasis emphasis = new Emphasis();
        ItemStyle itemStyle = new ItemStyle();
        itemStyle.setShadowBlur(10);
        itemStyle.setShadowOffsetX(0);
        itemStyle.setShadowColor("rgba(0, 0, 0, 0.5)");
        emphasis.setItemStyle(itemStyle);
        optionSeries.setData(dataArr);
        optionSeriesArr[0] = optionSeries;
        optionConfig.setSeries(optionSeriesArr);
        optionConfig = pieOption.getPieLegendChartOption(legendDatas, selected, optionSeriesArr);
        OptionTitle optionTitle = new OptionTitle();
        optionTitle.setText("人员籍贯统计");
        optionTitle.setSubtext("人员籍贯占比");
        optionTitle.setLeft("center");
        optionConfig.setTitle(optionTitle);
        return optionConfig;
    }

    /**
     * @param account
     * @return OptionConfig
     * @Title: getWorkTypeBar
     * @Description:  HR人员工种对比
     */
    public OptionConfig getWorkTypeBar(Account account) {
        List<Map<String, Object>> resList = echartsHrMapper.getWorkTypeBar(account.getOrgId());
        OptionConfig optionConfig = new OptionConfig();
        String[] xAxisData = new String[resList.size()];
        Object[] data = new Object[resList.size()];
        for (int i = 0; i < resList.size(); i++) {
            if (resList.get(i).get("name") == null) {
                xAxisData[i] = "其它";
            } else {
                xAxisData[i] = resList.get(i).get("name").toString();
            }
            data[i] = resList.get(i).get("value");
        }
        OptionSeries[] seriesDatas = new OptionSeries[1];
        OptionSeries optionSeries = new OptionSeries();
        optionSeries.setType("bar");
        optionSeries.setData(data);

        seriesDatas[0] = optionSeries;
        optionConfig = barOption.getBarSimpleChartOption(xAxisData, seriesDatas);
        return optionConfig;
    }


    /**
     * @param account
     * @return OptionConfig
     * @Title: getHighsetShoolPie
     * @Description:  获取HR学历占比
     */
    public OptionConfig getHighsetShoolPie(Account account) {
        List<Map<String, Object>> resList = echartsHrMapper.getHighsetShoolPie(account.getOrgId());
        OptionConfig optionConfig = new OptionConfig();
        OptionSeries[] optionSeriesArr = new OptionSeries[1];
        OptionSeries optionSeries = new OptionSeries();
        optionSeries.setName("学历占比");
        optionSeries.setType("pie");
        optionSeries.setRadius(new String[]{"50%", "70%"});
        optionSeries.setAvoidLabelOverlap(false);
        Label label = new Label();
        label.setShow(false);
        label.setPosition("center");
        optionSeries.setLabel(label);
        Emphasis emphasis = new Emphasis();
        Label label1 = new Label();
        label1.setShow(true);
        label1.setFontSize(20);
        label1.setFontWeight("bold");
        emphasis.setLabel(label1);
        optionSeries.setEmphasis(emphasis);
        LabelLine labelLine = new LabelLine();
        labelLine.setShow(false);
        optionSeries.setLabelLine(labelLine);
        LegendData[] legendDatas = new LegendData[resList.size()];
        for (int i = 0; i < resList.size(); i++) {
            LegendData legendData = new LegendData();
            legendData.setName(resList.get(i).get("name").toString());
            legendDatas[i] = legendData;
        }
        optionSeries.setData(resList.toArray());
        optionSeriesArr[0] = optionSeries;
        optionConfig = pieOption.getDoughnutChartOption(legendDatas, optionSeriesArr);
        return optionConfig;
    }

}
