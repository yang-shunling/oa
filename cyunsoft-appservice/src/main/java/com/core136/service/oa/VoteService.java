package com.core136.service.oa;

import com.core136.bean.account.Account;
import com.core136.bean.account.UserInfo;
import com.core136.bean.oa.Vote;
import com.core136.bean.sys.MsgBody;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.GobalConstant;
import com.core136.common.utils.SysTools;
import com.core136.mapper.oa.VoteMapper;
import com.core136.service.account.AccountService;
import com.core136.service.sys.MessageUnitService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class VoteService {
    private MessageUnitService messageUnitService;

    @Autowired
    public void setMessageUnitService(MessageUnitService messageUnitService) {
        this.messageUnitService = messageUnitService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private VoteMapper voteMapper;

    @Autowired
    public void setVoteMapper(VoteMapper voteMapper) {
        this.voteMapper = voteMapper;
    }

    public int insertVote(Vote vote) {
        return voteMapper.insert(vote);
    }

    public int deleteVote(Vote vote) {
        return voteMapper.delete(vote);
    }

    public int updateVote(Example example, Vote vote) {
        return voteMapper.updateByExampleSelective(vote, example);
    }

    /**
     * @param vote
     * @param userInfo void
     * @Title: sendMsgForUser
     * @Description:  消息提醒
     */
    public void sendMsgForUser(Vote vote, UserInfo userInfo) {
        vote = selectOneVote(vote);
        if (StringUtils.isNotBlank(vote.getMsgType())) {
            String sendTime = SysTools.getTime("yyyy-MM-dd HH:mm:ss");
            List<MsgBody> msgBodyList = new ArrayList<MsgBody>();
            List<Account> accountList = accountService.getAccountInPriv(vote.getOrgId(), vote.getUserPriv(), vote.getDeptPriv(), vote.getLevelPriv());
            for (int i = 0; i < accountList.size(); i++) {
                MsgBody msgBody = new MsgBody();
                msgBody.setFromAccountId(vote.getCreateUser());
                msgBody.setFormUserName(userInfo.getUserName());
                msgBody.setTitle(vote.getTitle());
                msgBody.setContent(vote.getRemark());
                msgBody.setSendTime(sendTime);
                msgBody.setAccount(accountList.get(i));
                msgBody.setRedirectUrl("/app/core/vote/myvote?view=dovote&voteId=" + vote.getVoteId());
                msgBody.setOrgId(vote.getOrgId());
                msgBodyList.add(msgBody);
            }
            messageUnitService.sendMessage(vote.getMsgType(), GobalConstant.MSG_TYPE_VOTE, msgBodyList);
        }
    }


    public Vote selectOneVote(Vote vote) {
        return voteMapper.selectOne(vote);
    }

    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @param voteType
     * @param beginTime
     * @param endTime
     * @param status
     * @param search
     * @return List<Map < String, String>>
     * @Title: getVoteListForManage
     * @Description:  获取投票管理列表
     */
    public List<Map<String, String>> getVoteListForManage(String orgId, String opFlag, String accountId, String voteType, String beginTime, String endTime, String status, String search) {
        return voteMapper.getVoteListForManage(orgId, opFlag, accountId, voteType, beginTime, endTime, status, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param voteType
     * @param beginTime
     * @param endTime
     * @param status
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getVoteListForManage
     * @Description:  获取投票管理列表
     */
    public PageInfo<Map<String, String>> getVoteListForManage(PageParam pageParam, String voteType, String beginTime, String endTime, String status) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getVoteListForManage(pageParam.getOrgId(), pageParam.getOpFlag(), pageParam.getAccountId(), voteType, beginTime, endTime, status, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @param voteType
     * @param beginTime
     * @param endTime
     * @param status
     * @param search
     * @return List<Map < String, String>>
     * @Title: getMyOldVoteListForVote
     * @Description:  获取历史投票列表
     */
    public List<Map<String, String>> getMyOldVoteListForVote(String orgId, String opFlag, String accountId, String voteType, String beginTime, String endTime, String status, String search) {
        return voteMapper.getMyOldVoteListForVote(orgId, opFlag, accountId, voteType, beginTime, endTime, status, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param voteType
     * @param beginTime
     * @param endTime
     * @param status
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getMyOldVoteListForVote
     * @Description:  获取历史投票列表
     */
    public PageInfo<Map<String, String>> getMyOldVoteListForVote(PageParam pageParam, String voteType, String beginTime, String endTime, String status) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyOldVoteListForVote(pageParam.getOrgId(), pageParam.getOpFlag(), pageParam.getAccountId(), voteType, beginTime, endTime, status, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param nowTime
     * @param accountId
     * @param deptId
     * @param levelId
     * @return List<Map < String, String>>
     * @Title: getMyVoteListForVote
     * @Description:  获取待我投票列表
     */
    public List<Map<String, String>> getMyVoteListForVote(String orgId, String nowTime, String accountId, String deptId, String levelId) {
        return voteMapper.getMyVoteListForVote(orgId, nowTime, accountId, deptId, levelId);
    }

    /**
     * @param pageParam
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getMyVoteListForVote
     * @Description:  获取待我投票列表
     */
    public PageInfo<Map<String, String>> getMyVoteListForVote(PageParam pageParam) throws Exception {
        String nowTime = SysTools.getTime("yyyy-MM-dd");
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyVoteListForVote(pageParam.getOrgId(), nowTime, pageParam.getAccountId(), pageParam.getDeptId(), pageParam.getLevelId());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
