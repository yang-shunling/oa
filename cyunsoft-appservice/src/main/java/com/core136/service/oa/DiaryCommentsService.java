package com.core136.service.oa;

import com.core136.bean.oa.DiaryComments;
import com.core136.mapper.oa.DiaryCommentsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class DiaryCommentsService {
    private DiaryCommentsMapper diaryCommentsMapper;

    @Autowired
    public void setDiaryCommentsMapper(DiaryCommentsMapper diaryCommentsMapper) {
        this.diaryCommentsMapper = diaryCommentsMapper;
    }

    public int insertDiaryComments(DiaryComments diaryComments) {
        return diaryCommentsMapper.insert(diaryComments);
    }

    public int deleteDiaryComments(DiaryComments diaryComments) {
        return diaryCommentsMapper.delete(diaryComments);
    }

    public DiaryComments selectOneDiaryComments(DiaryComments diaryComments) {
        return diaryCommentsMapper.selectOne(diaryComments);
    }

    public int updateDiaryComments(Example example, DiaryComments diaryComments) {
        return diaryCommentsMapper.updateByExampleSelective(diaryComments, example);
    }

    /**
     * @Title: getDiaryCommentsList
     * @Description:  获取评论列表
     * @param: orgId
     * @param: diaryId
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> getDiaryCommentsList(String orgId, String diaryId) {
        return diaryCommentsMapper.getDiaryCommentsList(orgId, diaryId);
    }

    /**
     * @Title: getMyDiaryCommentsCount
     * @Description:  获取评论数
     * @param: orgId
     * @param: accountId
     * @param: @return
     * @return: Integer
     */
    public Integer getMyDiaryCommentsCount(String orgId, String accountId) {
        return diaryCommentsMapper.getMyDiaryCommentsCount(orgId, accountId);
    }

}
