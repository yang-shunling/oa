package com.core136.service.oa;

import com.core136.bean.account.Account;
import com.core136.bean.oa.BigStory;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.ExcelUtil;
import com.core136.common.utils.SysTools;
import com.core136.mapper.oa.BigStoryMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class BigStoryService {
    private BigStoryMapper bigStoryMapper;

    @Autowired
    public void setBigStoryMapper(BigStoryMapper bigStoryMapper) {
        this.bigStoryMapper = bigStoryMapper;
    }

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public int insertBigStory(BigStory bigStory) {
        return bigStoryMapper.insert(bigStory);
    }

    public int deleteBigStory(BigStory bigStory) {
        return bigStoryMapper.delete(bigStory);
    }

    public int updateBigStory(Example example, BigStory bigStory) {
        return bigStoryMapper.updateByExampleSelective(bigStory, example);
    }

    public BigStory selectOneBigStory(BigStory bigStory) {
        return bigStoryMapper.selectOne(bigStory);
    }

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getBigStoryList
     * @Description:  获取所有大记事
     */
    public List<Map<String, String>> getBigStoryList(String orgId) {
        return bigStoryMapper.getBigStoryList(orgId);
    }

    /**
     * @param orgId
     * @param type
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getBigStoryListForManage
     * @Description:  获取大纪事管理列表
     */
    public List<Map<String, String>> getBigStoryListForManage(String orgId, String type, String beginTime, String endTime, String search) {
        return bigStoryMapper.getBigStoryListForManage(orgId, type, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param type
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getBigStoryListForManage
     * @Description:  获取大纪事管理列表
     */
    public PageInfo<Map<String, String>> getBigStoryListForManage(PageParam pageParam, String type, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getBigStoryListForManage(pageParam.getOrgId(), type, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }


    /**
     * @param account
     * @param file
     * @return
     * @throws IOException RetDataBean
     * @Title: importBigStory
     * @Description:  导入大记事
     */
    @Transactional(value = "generalTM")
    public RetDataBean importBigStory(Account account, MultipartFile file) throws IOException {
        Map<String, String> fieldMap = new HashMap<String, String>();
        fieldMap.put("事件标题", "TITLE");
        fieldMap.put("事件日期", "HAPPEN_TIME");
        fieldMap.put("大纪事类型", "TYPE");
        fieldMap.put("事件摘要", "REMARK");
        List<String> fieldList = new ArrayList<String>();
        List<String> titleList = new ArrayList<String>();
        for (Map.Entry<String, String> entry : fieldMap.entrySet()) {
            fieldList.add(entry.getValue());
            titleList.add(entry.getKey());
        }
        String[] fieldArr = new String[fieldList.size()];
        fieldList.toArray(fieldArr);
        String fieldString = StringUtils.join(fieldArr, ",");
        List<Map<String, String>> recordList = ExcelUtil.readExcel(file);
        for (int i = 0; i < recordList.size(); i++) {
            Map<String, String> tempMap = recordList.get(i);
            String valueString = "'" + SysTools.getGUID() + "',";
            String subheading = "";
            String happenYear = "";
            for (int k = 0; k < titleList.size(); k++) {
                if (titleList.get(k).equals("事件摘要")) {
                    if (StringUtils.isNotBlank(tempMap.get(titleList.get(k)))) {
                        Document htmlDoc = Jsoup.parse(tempMap.get(titleList.get(k)));
                        subheading = htmlDoc.text();
                        if (subheading.length() > 50) {
                            subheading = subheading.substring(0, 50) + "...";
                        }
                    }
                }
                if (titleList.get(k).equals("事件日期")) {
                    if (StringUtils.isNotBlank(tempMap.get(titleList.get(k)))) {
                        happenYear = tempMap.get(titleList.get(k)).substring(0, 4);
                    }
                }
                valueString += "'" + tempMap.get(titleList.get(k)) + "',";
            }
            valueString += "'" + happenYear + "',";
            valueString += "'" + subheading + "',";
            valueString += "'" + SysTools.getTime("yyyy-MM-dd HH:mm:ss") + "',";
            valueString += "'" + account.getAccountId() + "',";
            valueString += "'" + account.getOrgId() + "'";
            String insertSql = "insert into big_story(record_id," + fieldString + ",happen_year,subheading,create_time,create_user,org_id) values " + "(" + valueString + ")";
            jdbcTemplate.execute(insertSql);
        }
        return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
    }

}
