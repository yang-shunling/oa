package com.core136.service.budget;

import com.core136.bean.budget.BudgetAccount;
import com.core136.mapper.budget.BudgetAccountMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class BudgetAccountService {
    private BudgetAccountMapper budgetAccountMapper;

    @Autowired
    public void setBudgetAccountMapper(BudgetAccountMapper budgetAccountMapper) {
        this.budgetAccountMapper = budgetAccountMapper;
    }

    public int insertBudgetAccount(BudgetAccount budgetAccount) {
        return budgetAccountMapper.insert(budgetAccount);
    }

    public int deleteBudgetAccount(BudgetAccount budgetAccount) {
        return budgetAccountMapper.delete(budgetAccount);
    }

    public int updateBudgetAccount(Example example, BudgetAccount budgetAccount) {
        return budgetAccountMapper.updateByExampleSelective(budgetAccount, example);
    }

    public BudgetAccount selectOneBudgetAccount(BudgetAccount budgetAccount) {
        return budgetAccountMapper.selectOne(budgetAccount);
    }

    /**
     * @param orgId
     * @param levelId
     * @return List<Map < String, String>>
     * @Title: getBudgetAccountTree
     * @Description:  获取预算科目树结构
     */
    public List<Map<String, String>> getBudgetAccountTree(String orgId, String levelId) {
        return budgetAccountMapper.getBudgetAccountTree(orgId, levelId);
    }

    /**
     * @param budgetAccountId
     * @param orgId
     * @return int
     * @Title: isExistChild
     * @Description:  判断预算科目是否存在
     */
    public int isExistChild(String budgetAccountId, String orgId) {
        return budgetAccountMapper.isExistChild(budgetAccountId, orgId);
    }

}
