package com.core136.service.budget;

import com.core136.bean.budget.BudgetAdjustment;
import com.core136.bean.budget.BudgetProject;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.budget.BudgetAdjustmentMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class BudgetAdjustmentService {
    private BudgetAdjustmentMapper budgetAdjustmentMapper;

    @Autowired
    public void setBudgetAdjustmentMapper(BudgetAdjustmentMapper budgetAdjustmentMapper) {
        this.budgetAdjustmentMapper = budgetAdjustmentMapper;
    }

    private BudgetProjectService budgetProjectService;

    @Autowired
    public void setBudgetProjectService(BudgetProjectService budgetProjectService) {
        this.budgetProjectService = budgetProjectService;
    }

    public int insertBudgetAdjustment(BudgetAdjustment BudgetAdjustment) {
        return budgetAdjustmentMapper.insert(BudgetAdjustment);
    }

    public int deleteBudgetAdjustment(BudgetAdjustment budgetAdjustment) {
        return budgetAdjustmentMapper.delete(budgetAdjustment);
    }

    public int updateBudgetAdjustment(Example example, BudgetAdjustment budgetAdjustment) {
        return budgetAdjustmentMapper.updateByExampleSelective(budgetAdjustment, example);
    }


    public BudgetAdjustment selectOneBudgetAdjustment(BudgetAdjustment budgetAdjustment) {
        return budgetAdjustmentMapper.selectOne(budgetAdjustment);
    }

    /**
     * @param budgetAdjustment
     * @return int
     * @Title: approval
     * @Description:  审批时更新项目预算
     */
    @Transactional(value = "generalTM")
    public int setApprovalStatus(BudgetAdjustment budgetAdjustment) {
        if (budgetAdjustment.getStatus().equals("1")) {
            BudgetAdjustment budgetAdjustment1 = new BudgetAdjustment();
            budgetAdjustment1.setRecordId(budgetAdjustment.getRecordId());
            budgetAdjustment1.setOrgId(budgetAdjustment.getOrgId());
            budgetAdjustment1 = selectOneBudgetAdjustment(budgetAdjustment1);
            BudgetProject budgetProject = new BudgetProject();
            budgetProject.setOrgId(budgetAdjustment1.getOrgId());
            budgetProject.setProjectId(budgetAdjustment1.getProjectId());
            budgetProject = budgetProjectService.selectOneBudgetProject(budgetProject);
            if (budgetAdjustment1.getAdjustType().equals("1")) {
                Double res = budgetProject.getTotalCost() - budgetAdjustment1.getNewTotalCost();
                budgetProject.setTotalCost(res);
            } else if (budgetAdjustment1.getAdjustType().equals("2")) {
                Double res = budgetProject.getTotalCost() + budgetAdjustment1.getNewTotalCost();
                budgetProject.setTotalCost(res);
            }
            Example example1 = new Example(BudgetProject.class);
            example1.createCriteria().andEqualTo("orgId", budgetProject.getOrgId()).andEqualTo("projectId", budgetProject.getProjectId());
            budgetProjectService.updateBudgetProject(example1, budgetProject);
        }
        budgetAdjustment.setApprovalTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
        Example example = new Example(BudgetAdjustment.class);
        example.createCriteria().andEqualTo("orgId", budgetAdjustment.getOrgId()).andEqualTo("recordId", budgetAdjustment.getRecordId());
        return updateBudgetAdjustment(example, budgetAdjustment);
    }


    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @param status
     * @param projectId
     * @param approvalUser
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getAdjustmentApplayList
     * @Description:  获取申请列表
     */
    public List<Map<String, String>> getAdjustmentApplayList(String orgId, String opFlag, String accountId, String status, String projectId, String approvalUser, String beginTime, String endTime, String search) {
        return budgetAdjustmentMapper.getAdjustmentApplayList(orgId, opFlag, accountId, status, projectId, approvalUser, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param status
     * @param projectId
     * @param approvalUser
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getAdjustmentApplayList
     * @Description:  获取申请列表
     */
    public PageInfo<Map<String, String>> getAdjustmentApplayList(PageParam pageParam, String status, String projectId, String approvalUser, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getAdjustmentApplayList(pageParam.getOrgId(), pageParam.getOpFlag(), pageParam.getAccountId(), status, projectId, approvalUser, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }


    /**
     * @param orgId
     * @param accountId
     * @param projectId
     * @param applyUser
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getAdjustmentApprovalList
     * @Description:  获取等审批列表
     */
    public List<Map<String, String>> getAdjustmentApprovalList(String orgId, String accountId, String projectId, String applyUser, String beginTime, String endTime, String search) {
        return budgetAdjustmentMapper.getAdjustmentApprovalList(orgId, accountId, projectId, applyUser, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param projectId
     * @param applyUser
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getAdjustmentApprovalList
     * @Description:   获取等审批列表
     */
    public PageInfo<Map<String, String>> getAdjustmentApprovalList(PageParam pageParam, String projectId, String applyUser, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getAdjustmentApprovalList(pageParam.getOrgId(), pageParam.getAccountId(), projectId, applyUser, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param accountId
     * @param projectId
     * @param applyUser
     * @param beginTime
     * @param endTime
     * @param status
     * @param search
     * @return List<Map < String, String>>
     * @Title: getAdjustmentApprovalOldList
     * @Description:  获取等审批记录查询
     */
    public List<Map<String, String>> getAdjustmentApprovalOldList(String orgId, String accountId, String projectId, String applyUser, String beginTime, String endTime, String status, String search) {
        return budgetAdjustmentMapper.getAdjustmentApprovalOldList(orgId, accountId, projectId, applyUser, beginTime, endTime, status, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param projectId
     * @param applyUser
     * @param beginTime
     * @param endTime
     * @param status
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getAdjustmentApprovalOldList
     * @Description:  获取等审批记录查询
     */
    public PageInfo<Map<String, String>> getAdjustmentApprovalOldList(PageParam pageParam, String projectId, String applyUser, String beginTime, String endTime, String status) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getAdjustmentApprovalOldList(pageParam.getOrgId(), pageParam.getAccountId(), projectId, applyUser, beginTime, endTime, status, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
