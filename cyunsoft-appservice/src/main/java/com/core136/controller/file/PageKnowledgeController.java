/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: AppKnowledgePageController.java
 * @Package com.core136.controller.sys
 * @Description: (用一句话描述该文件做什么)
 * @author: 稠云信息
 * @date: 2019年9月10日 上午11:02:48
 * @version V1.0
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
package com.core136.controller.file;


import com.core136.bean.account.Account;
import com.core136.service.account.AccountService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * @author lsq
 * @ClassName: AppKnowledgePageController
 * @Description:(这里用一句话描述这个类的作用)
 * @author: 稠云信息
 * @date: 2019年9月10日 上午11:02:48
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Controller
@RequestMapping("/app/core")
public class PageKnowledgeController {
    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * @return ModelAndView
     * @Title: goKnowledgestudylist
     * @Description:  跳转到知识分类清单
     */
    @RequestMapping("/file/knowledgestudylist")
    @RequiresPermissions("/app/core/file/knowledgestudylist")
    public ModelAndView goKnowledgestudylist() {
        try {
            return new ModelAndView("app/core/knowledge/knowledgestudylist");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: goDetailKnowledge
     * @Description:  知识详情
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/file/detailknowledge")
    @RequiresPermissions("/app/core/file/detailknowledge")
    public ModelAndView goDetailKnowledge(HttpServletRequest request) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            ModelAndView mv = new ModelAndView("app/core/knowledge/knowledgedetail");
            mv.addObject("accountId", account.getAccountId());
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: goKnowledgeIntegral
     * @Description: 知识积分
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/file/knowledgeintegral")
    @RequiresPermissions("/app/core/file/knowledgeintegral")
    public ModelAndView goKnowledgeIntegral() {
        try {
            return new ModelAndView("app/core/knowledge/knowledgeintegral");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: goKnowledgestudy
     * @Description:  知识学习
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/file/knowledgestudy")
    @RequiresPermissions("/app/core/file/knowledgestudy")
    public ModelAndView goKnowledgestudy() {
        try {
            return new ModelAndView("app/core/knowledge/knowledgestudy");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: goIndexSearch
     * @Description:  全文检索页面
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/file/indexsearch")
    @RequiresPermissions("/app/core/file/indexsearch")
    public ModelAndView goIndexSearch() {
        try {
            ModelAndView mv = new ModelAndView("app/core/indexsearch");
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param @return 设定文件
     * @return ModelAndView 返回类型
     * @Title: goKnowledgesort
     * @Description:  设置知识分类
     */
    @RequestMapping("/file/knowledgesort")
    @RequiresPermissions("/app/core/file/knowledgesort")
    public ModelAndView goKnowledgesort() {
        try {
            return new ModelAndView("app/core/knowledge/sort");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: goKnowledgeManage
     * @Description:  知识记录管理
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/file/knowledgeinit")
    @RequiresPermissions("/app/core/file/knowledgeinit")
    public ModelAndView goKnowledgeManage(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/knowledge/knowledgeinit");
            } else {
                if (view.equals("init")) {
                    mv = new ModelAndView("app/core/knowledge/knowledgemaintain");
                } else if (view.equals("resetindex")) {
                    mv = new ModelAndView("app/core/knowledge/knowledgeresetindex");
                } else if (view.equals("search")) {
                    mv = new ModelAndView("app/core/knowledge/knowledgersearch");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: goKnowledgecreate
     * @Description:  添加知识
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/file/knowledgecreate")
    @RequiresPermissions("/app/core/file/knowledgecreate")
    public ModelAndView goKnowledgecreate(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/knowledge/knowledgecreate");
            } else {
                if (view.equals("manage")) {
                    mv = new ModelAndView("app/core/knowledge/knowledgemanage");
                } else if (view.equals("edit")) {
                    mv = new ModelAndView("app/core/knowledge/knowledgeedit");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }
}
