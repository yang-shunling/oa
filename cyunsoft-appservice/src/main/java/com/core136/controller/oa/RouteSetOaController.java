/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: RoutSetOaController.java
 * @Package com.core136.controller.oa
 * @Description: (用一句话描述该文件做什么)
 * @author: 刘绍全
 * @date: 2019年1月14日 下午7:55:31
 * @version V1.0
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术股份有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
package com.core136.controller.oa;


import com.core136.bean.account.Account;
import com.core136.bean.account.UserInfo;
import com.core136.bean.attend.Attend;
import com.core136.bean.attend.AttendConfig;
import com.core136.bean.oa.*;
import com.core136.bean.sys.Sms;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.attend.AttendConfigService;
import com.core136.service.attend.AttendService;
import com.core136.service.oa.*;
import com.core136.service.sys.MobileSmsService;
import com.core136.service.sys.SmsService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import tk.mybatis.mapper.entity.Example;

/**
 * @ClassName: RoutSetOaController
 * @Description:(这里用一句话描述这个类的作用)
 * @author: 刘绍全
 * @date: 2019年1月14日 下午7:55:31
 *
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云科信息技术股份有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@RestController
@RequestMapping("/set/oaset")
public class RouteSetOaController {
    private SmsService smsService;

    @Autowired
    public void setSmsService(SmsService smsService) {
        this.smsService = smsService;
    }

    private AttendConfigService attendConfigService;

    @Autowired
    public void setAttendConfigService(AttendConfigService attendConfigService) {
        this.attendConfigService = attendConfigService;
    }

    private AttendService attendService;

    @Autowired
    public void setAttendService(AttendService attendService) {
        this.attendService = attendService;
    }

    private MobileSmsService mobileSmsService;

    @Autowired
    public void setMobileSmsService(MobileSmsService mobileSmsService) {
        this.mobileSmsService = mobileSmsService;
    }

    private LeadActivityService leadActivityService;

    @Autowired
    public void setLeadActivityService(LeadActivityService leadActivityService) {
        this.leadActivityService = leadActivityService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private VoteService voteService;

    @Autowired
    public void setVoteService(VoteService voteService) {
        this.voteService = voteService;
    }

    private VoteItemService voteItemService;

    @Autowired
    public void setVoteItemService(VoteItemService voteItemService) {
        this.voteItemService = voteItemService;
    }

    private VoteResultService voteResultService;

    @Autowired
    public void setVoteResultService(VoteResultService voteResultService) {
        this.voteResultService = voteResultService;
    }

    private BigStoryService bigStoryService;

    @Autowired
    public void setBigStoryService(BigStoryService bigStoryService) {
        this.bigStoryService = bigStoryService;
    }

    private AddressBookService addressBookService;

    @Autowired
    public void setAddressBookService(AddressBookService addressBookService) {
        this.addressBookService = addressBookService;
    }

    private LeaderMailboxConfigService leaderMailboxConfigService;

    @Autowired
    public void setLeaderMailboxConfigService(LeaderMailboxConfigService leaderMailboxConfigService) {
        this.leaderMailboxConfigService = leaderMailboxConfigService;
    }

    private LeaderMailboxService leaderMailboxService;

    @Autowired
    public void setLeaderMailboxService(LeaderMailboxService leaderMailboxService) {
        this.leaderMailboxService = leaderMailboxService;
    }

    /**
     *
     * @Title: setLeaderMailBox
     * @Description:  发送领导信息
     * @param leaderMailbox
     * @return
     * RetDataBean
     */
    @RequestMapping(value = "/setLeaderMailBox", method = RequestMethod.POST)
    public RetDataBean setLeaderMailBox(LeaderMailbox leaderMailbox) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("leadermailbox:insert")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
            }
            UserInfo userinfo = accountService.getRedisAUserInfoToUserInfo();
            leaderMailbox.setRecordId(SysTools.getGUID());
            leaderMailbox.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            leaderMailbox.setStatus("0");
            leaderMailbox.setCreateUser(userinfo.getAccountId());
            leaderMailbox.setOrgId(userinfo.getOrgId());
            return leaderMailboxService.setLeaderMailBox(userinfo, leaderMailbox);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: deleteLeaderMailbox
     * @Description:  删除领导信箱信息
     * @param leaderMailbox
     * @return
     * RetDataBean
     */
    @RequestMapping(value = "/deleteLeaderMailbox", method = RequestMethod.POST)
    public RetDataBean deleteLeaderMailbox(LeaderMailbox leaderMailbox) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("leadermailbox:delete")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            leaderMailbox.setOrgId(account.getOrgId());
            if (StringUtils.isBlank(leaderMailbox.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            } else {
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, leaderMailboxService.deleteLeaderMailbox(leaderMailbox));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: updateLeaderMailbox
     * @Description:  更新领导信箱信息
     * @param leaderMailbox
     * @return
     * RetDataBean
     */
    @RequestMapping(value = "/updateLeaderMailbox", method = RequestMethod.POST)
    public RetDataBean updateLeaderMailbox(LeaderMailbox leaderMailbox) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("leadermailbox:update")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
            }
            if (StringUtils.isBlank(leaderMailbox.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            leaderMailbox.setOrgId(account.getOrgId());
            Example example = new Example(LeaderMailbox.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", leaderMailbox.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, leaderMailboxService.updateLeaderMailbox(example, leaderMailbox));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: setLeaderMailboxConfig
     * @Description:  更新领导信息配置
     * @param leaderMailboxConfig
     * @return
     * RetDataBean
     */
    @RequestMapping(value = "/setLeaderMailboxConfig", method = RequestMethod.POST)
    public RetDataBean setLeaderMailboxConfig(LeaderMailboxConfig leaderMailboxConfig) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(leaderMailboxConfig.getIsAnonymous())) {
                leaderMailboxConfig.setIsAnonymous("0");
            }
            leaderMailboxConfig.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            leaderMailboxConfig.setCreateUser(account.getAccountId());
            leaderMailboxConfig.setOrgId(account.getOrgId());
            return leaderMailboxConfigService.setLeaderMailboxConfig(leaderMailboxConfig);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     *
     * @Title: importUnitDept
     * @Description:  通讯录导入
     * @param file
     * @return
     * RetDataBean
     */
    @RequestMapping(value = "/importAddressBookForExcel", method = RequestMethod.POST)
    public RetDataBean importUnitDept(MultipartFile file) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return addressBookService.importAddressBookForExcel(account, file);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 创建通讯录人员
     * @param addressBook
     * @return
     */
    @RequestMapping(value = "/insertAddressBook", method = RequestMethod.POST)
    public RetDataBean insertAddressBook(AddressBook addressBook) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            addressBook.setRecordId(SysTools.getGUID());
            addressBook.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            String pinYin = StrTools.getPinYin(addressBook.getUserName());
            addressBook.setPinYin(pinYin);
            addressBook.setFirstPinYin(pinYin.substring(0, 1).toUpperCase());
            addressBook.setCreateUser(account.getAccountId());
            addressBook.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, addressBookService.insertAddressBook(addressBook));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除通讯录联系方式
     * @param addressBook
     * @return
     */
    @RequestMapping(value = "/deleteAddressBook", method = RequestMethod.POST)
    public RetDataBean deleteAddressBook(AddressBook addressBook) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            addressBook.setOrgId(account.getOrgId());
            if (StringUtils.isBlank(addressBook.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            } else {
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, addressBookService.deleteAddressBook(addressBook));
            }
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新通讯录
     * @param addressBook
     * @return
     */
    @RequestMapping(value = "/updateAddressBook", method = RequestMethod.POST)
    public RetDataBean updateAddressBook(AddressBook addressBook) {
        try {
            if (StringUtils.isBlank(addressBook.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            } else {
                Account account = accountService.getRedisAUserInfoToAccount();
                String pinYin = StrTools.getPinYin(addressBook.getUserName());
                addressBook.setPinYin(pinYin);
                addressBook.setFirstPinYin(pinYin.substring(0, 1).toUpperCase());
                Example example = new Example(AddressBook.class);
                example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", addressBook.getRecordId());
                return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, addressBookService.updateAddressBook(example, addressBook));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: importBigStory
     * @Description:  导入大记事
     * @param file
     * @return
     * ModelAndView
     */
    @RequestMapping(value = "/importBigStory", method = RequestMethod.POST)
    public ModelAndView importBigStory(MultipartFile file) {
        ModelAndView mv = null;
        try {
            RetDataBean retDataBean = null;
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("bigstory:insert")) {
                retDataBean = RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
            } else {
                Account account = accountService.getRedisAUserInfoToAccount();
                retDataBean = bigStoryService.importBigStory(account, file);
            }
            mv = new ModelAndView("/app/core/oa/bigstory?view=import");
            mv.addObject("retDataBean", retDataBean);
            return mv;
        } catch (Exception e) {

            mv.addObject("retDataBean", RetDataTools.NotOk(MessageCode.MESSAGE_FAILED));
            return mv;
        }
    }

    /**
     *
     * @Title: insertBigStory
     * @Description:  创建大纪事
     * @param bigStory
     * @return
     * RetDataBean
     */
    @RequestMapping(value = "/insertBigStory", method = RequestMethod.POST)
    public RetDataBean insertBigStory(BigStory bigStory) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("bigstory:insert")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            bigStory.setRecordId(SysTools.getGUID());
            Document htmlDoc = Jsoup.parse(bigStory.getRemark());
            String subheading = htmlDoc.text();
            if (subheading.length() > 50) {
                subheading = subheading.substring(0, 50) + "...";
            }
            bigStory.setSubheading(subheading);
            if (StringUtils.isNotBlank(bigStory.getHappenTime())) {
                bigStory.setHappenYear(bigStory.getHappenTime().substring(0, 4));
            }
            bigStory.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            bigStory.setCreateUser(account.getAccountId());
            bigStory.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, bigStoryService.insertBigStory(bigStory));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: deleteBigStory
     * @Description:  删除大纪事
     * @param bigStory
     * @return
     * RetDataBean
     */
    @RequestMapping(value = "/deleteBigStory", method = RequestMethod.POST)
    public RetDataBean deleteBigStory(BigStory bigStory) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("bigstory:delete")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            bigStory.setOrgId(account.getOrgId());
            if (StringUtils.isBlank(bigStory.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            } else {
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, bigStoryService.deleteBigStory(bigStory));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: updateBigStory
     * @Description:  大纪事更新
     * @param bigStory
     * @return
     * RetDataBean
     */
    @RequestMapping(value = "/updateBigStory", method = RequestMethod.POST)
    public RetDataBean updateBigStory(BigStory bigStory) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("bigstory:update")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
            }
            if (StringUtils.isBlank(bigStory.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            } else {
                Account account = accountService.getRedisAUserInfoToAccount();
                Document htmlDoc = Jsoup.parse(bigStory.getRemark());
                String subheading = htmlDoc.text();
                if (subheading.length() > 50) {
                    subheading = subheading.substring(0, 50) + "...";
                }
                bigStory.setSubheading(subheading);
                if (StringUtils.isNotBlank(bigStory.getHappenTime())) {
                    bigStory.setHappenYear(bigStory.getHappenTime().substring(0, 4));
                }
                Example example = new Example(BigStory.class);
                example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", bigStory.getRecordId());
                return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, bigStoryService.updateBigStory(example, bigStory));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: addVoteResuletRecord
     * @Description:  人员投票
     * @param result
     * @return
     * RetDataBean
     */
    @RequestMapping(value = "/addVoteResuletRecord", method = RequestMethod.POST)
    public RetDataBean addVoteResuletRecord(String voteId, String result) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return voteResultService.addVoteResuletRecord(account, voteId, result);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     *
     * @Title: deleteItems   删除投票项
     * @Description:
     * @param voteItem
     * @return
     * RetDataBean
     */
    @RequestMapping(value = "/deleteVoteItems", method = RequestMethod.POST)
    public RetDataBean deleteVoteItems(VoteItem voteItem) {
        try {
            if (StringUtils.isBlank(voteItem.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            voteItem.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, voteItemService.deleteVoteItems(voteItem));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: updateVoteItems
     * @Description:  更新投票子项
     * @param voteItem
     * @return
     * RetDataBean
     */
    @RequestMapping(value = "/updateVoteItems", method = RequestMethod.POST)
    public RetDataBean updateVoteItems(VoteItem voteItem) {
        try {
            if (StringUtils.isBlank(voteItem.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            voteItem.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, voteItemService.updateVoteItems(voteItem));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: insertVoteItem
     * @Description:  添加投票子项
     * @param voteItem
     * @return
     * RetDataBean
     */
    @RequestMapping(value = "/insertVoteItem", method = RequestMethod.POST)
    public RetDataBean insertVoteItem(VoteItem voteItem) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            voteItem.setRecordId(SysTools.getGUID());
            voteItem.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            voteItem.setCreateUser(account.getAccountId());
            voteItem.setOrgId(account.getOrgId());
            return voteItemService.addVoteItem(voteItem);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     *
     * @Title: insertVote
     * @Description:  添加投票记录
     * @param vote
     * @return
     * RetDataBean
     */
    @RequestMapping(value = "/insertVote", method = RequestMethod.POST)
    public RetDataBean insertVote(Vote vote) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("vote:insert")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            vote.setVoteId(SysTools.getGUID());
            vote.setStatus("0");
            vote.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            vote.setCreateUser(account.getAccountId());
            vote.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, voteService.insertVote(vote));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: deleteVote
     * @Description:  删除投票记录
     * @param vote
     * @return
     * RetDataBean
     */
    @RequestMapping(value = "/deleteVote", method = RequestMethod.POST)
    public RetDataBean deleteVote(Vote vote) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("vote:delete")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
            }
            if (StringUtils.isBlank(vote.getVoteId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            vote.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, voteService.deleteVote(vote));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: updateVote
     * @Description:  更新投票记录
     * @param vote
     * @return
     * RetDataBean
     */
    @RequestMapping(value = "/updateVote", method = RequestMethod.POST)
    public RetDataBean updateVote(Vote vote) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("vote:update")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
            }
            if (StringUtils.isBlank(vote.getVoteId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            if (StringUtils.isNotBlank(vote.getIsTop())) {
                if (vote.getIsTop().equals("1")) {
                    vote.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
                }
            }
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            Example example = new Example(Vote.class);
            example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("voteId", vote.getVoteId());
            if (voteService.updateVote(example, vote) > 0) {
                if (StringUtils.isNotBlank(vote.getStatus())) {
                    if (vote.getStatus().equals("1")) {
                        voteService.sendMsgForUser(vote, userInfo);
                    }
                }
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     *
     * @Title: addMoblieSms
     * @Description:   添加短信发送计划
     * @param accountIds
     * @param outUser
     * @param content
     * @param sendTime
     * @return
     * RetDataBean
     */
    @RequestMapping(value = "/addMoblieSms", method = RequestMethod.POST)
    public RetDataBean addMoblieSms(String accountIds, String outUser, String content, String sendTime) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return mobileSmsService.addMoblieSms(account, accountIds, outUser, content, sendTime);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: sendSms
     * @Description:  发送内部短信息
     * @param toUser
     * @param content
     * @return
     * RetDataBean

     */
    @RequestMapping(value = "/sendSms", method = RequestMethod.POST)
    public RetDataBean sendSms(String toUser, String content) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, smsService.sendSms(account, toUser, content));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: insertLeadActivity
     * @Description:  创建领导行程
     * @param leadActivity
     * @return
     * RetDataBean

     */
    @RequestMapping(value = "/insertLeadActivity", method = RequestMethod.POST)
    public RetDataBean insertLeadActivity(LeadActivity leadActivity) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("leadactivity:insert")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            leadActivity.setRecordId(SysTools.getGUID());
            leadActivity.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            leadActivity.setCreateUser(account.getAccountId());
            leadActivity.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, leadActivityService.insertLeadActivity(leadActivity));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: deleteLeadActivity
     * @Description:  删除领导行程
     * @param leadActivity
     * @return
     * RetDataBean

     */
    @RequestMapping(value = "/deleteLeadActivity", method = RequestMethod.POST)
    public RetDataBean deleteLeadActivity(LeadActivity leadActivity) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("leadactivity:delete")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
            }
            if (StringUtils.isBlank(leadActivity.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            leadActivity.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, leadActivityService.deleteLeadActivity(leadActivity));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: updateLeadActivity
     * @Description:  更新领导行程
     * @param leadActivity
     * @return
     * RetDataBean

     */
    @RequestMapping(value = "/updateLeadActivity", method = RequestMethod.POST)
    public RetDataBean updateLeadActivity(LeadActivity leadActivity) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("leadactivity:update")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
            }
            if (StringUtils.isBlank(leadActivity.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(LeadActivity.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", leadActivity.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, leadActivityService.updateLeadActivity(example, leadActivity));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: insertAttend
     * @Description:  添加考勤记录
     * @param: request
     * @param: attend
     * @param: @return
     * @return: RetDataBean

     */
    @RequestMapping(value = "/insertAttend", method = RequestMethod.POST)
    public RetDataBean insertAttend(Attend attend) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            attend.setAttendId(SysTools.getGUID());
            attend.setCreateUser(account.getAccountId());
            attend.setType("0");
            attend.setSource("pc");
            String nowTimeStr = SysTools.getTime("yyyy-MM-dd HH:mm:ss");
            attend.setCreateTime(nowTimeStr);
            attend.setYear(nowTimeStr.substring(0, 4));
            attend.setMonth(nowTimeStr.substring(5, 7));
            attend.setDay(nowTimeStr.substring(8, 10));
            attend.setNowTime(nowTimeStr.split(" ")[0]);
            attend.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, attendService.addAttendRecord(attend));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title delSms
     * @Description 删除消息
     * @param sms
     * @return
     * RetDataBean
     */
    @RequestMapping(value = "/delSms", method = RequestMethod.POST)
    public RetDataBean delSms(Sms sms) {
        try {
            if (StringUtils.isBlank(sms.getSmsId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            sms.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, smsService.deleteSms(sms));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: updateSms
     * @Description:  更新消息状态
     * @param sms
     * @return
     * RetDataBean

     */
    @RequestMapping(value = "/updateSms", method = RequestMethod.POST)
    public RetDataBean updateSms(Sms sms) {
        try {
            if (StringUtils.isBlank(sms.getSmsId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            sms.setOrgId(account.getOrgId());
            Example example = new Example(Sms.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("smsId", sms.getSmsId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, smsService.updateSms(sms, example));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: insertAttendConfig
     * @Description:  添加考勤规则
     * @param: request
     * @param: attendConfig
     * @param: @return
     * @return: RetDataBean

     */
    @RequestMapping(value = "/insertAttendConfig", method = RequestMethod.POST)
    public RetDataBean insertAttendConfig(AttendConfig attendConfig) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            attendConfig.setConfigId(SysTools.getGUID());
            ;
            attendConfig.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            attendConfig.setCreateUser(account.getAccountId());
            attendConfig.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, attendConfigService.insertAttendConfig(attendConfig));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: updateAttendConfig
     * @Description: 更新考勤规则
     * @param: request
     * @param: attendConfig
     * @param: @return
     * @return: RetDataBean

     */
    @RequestMapping(value = "/updateAttendConfig", method = RequestMethod.POST)
    public RetDataBean updateAttendConfig(AttendConfig attendConfig) {
        try {
            if (StringUtils.isBlank(attendConfig.getConfigId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            attendConfig.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            Example example = new Example(AttendConfig.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("configId", attendConfig.getConfigId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, attendConfigService.updateAttendConfig(attendConfig, example));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     *
     * @Title: deleteAttendConfig
     * @Description:  删除考勤规则
     * @param: request
     * @param: attendConfig
     * @param: @return
     * @return: RetDataBean

     */
    @RequestMapping(value = "/deleteAttendConfig", method = RequestMethod.POST)
    public RetDataBean deleteAttendConfig(AttendConfig attendConfig) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(attendConfig.getConfigId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            attendConfig.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, attendConfigService.deleteAttendConfig(attendConfig));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }
}
