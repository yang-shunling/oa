package com.core136.controller.echarts;


import com.core136.bean.account.Account;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.service.account.AccountService;
import com.core136.service.echarts.EchartsSuperversionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ret/echartssuperversionget")
public class RouteGetBiSuperversionController {
    private EchartsSuperversionService echartsSuperversionService;

    @Autowired
    public void setEchartsSuperversionService(EchartsSuperversionService echartsSuperversionService) {
        this.echartsSuperversionService = echartsSuperversionService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * @return RetDataBean
     * @Title: getBiSuperversionByLeadPie
     * @Description:  前10位领导的人员工作量占比
     */
    @RequestMapping(value = "/getBiSuperversionByLeadPie", method = RequestMethod.POST)
    public RetDataBean getBiSuperversionByLeadPie() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsSuperversionService.getBiSuperversionByLeadPie(account));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getBiBpmFlowByMonthLine
     * @Description:  按月份统计工作量
     */
    @RequestMapping(value = "/getBiSuperversionByMonthLine", method = RequestMethod.POST)
    public RetDataBean getBiSuperversionByMonthLine() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsSuperversionService.getBiSuperversionByMonthLine(account));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getBiSuperversionTypePie
     * @Description:  获取BPM使用分类前10的占比
     */
    @RequestMapping(value = "/getBiSuperversionTypePie", method = RequestMethod.POST)
    public RetDataBean getBiSuperversionTypePie() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsSuperversionService.getBiSuperversionTypePie(account));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getBiSuperversionStatusTypePie
     * @Description:  获取督查督办当前状态总数
     */
    @RequestMapping(value = "/getBiSuperversionStatusTypePie", method = RequestMethod.POST)
    public RetDataBean getBiSuperversionStatusTypePie() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsSuperversionService.getBiSuperversionStatusTypePie(account));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

}
