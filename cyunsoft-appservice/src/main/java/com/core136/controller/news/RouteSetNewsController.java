package com.core136.controller.news;

import com.core136.bean.account.Account;
import com.core136.bean.account.UserInfo;
import com.core136.bean.news.News;
import com.core136.bean.news.NewsComments;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.news.NewsCommentsService;
import com.core136.service.news.NewsService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

@RestController
@RequestMapping("/set/oaset")
public class RouteSetNewsController {
    private NewsService newsService;

    @Autowired
    public void setNewsService(NewsService newsService) {
        this.newsService = newsService;
    }

    private NewsCommentsService newsCommentsService;

    @Autowired
    public void setNewsCommentsService(NewsCommentsService newsCommentsService) {
        this.newsCommentsService = newsCommentsService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * @Title: insertNewsComments
     * @Description:  添加新闻评论
     * @param: request
     * @param: newsComments
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/insertNewsComments", method = RequestMethod.POST)
    public RetDataBean insertNewsComments(NewsComments newsComments) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            newsComments.setCommId(SysTools.getGUID());
            newsComments.setCreateUser(account.getAccountId());
            newsComments.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            newsComments.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, newsCommentsService.insertNewsComments(newsComments));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param news
     * @return RetDataBean
     * @Title sendNews
     * @Description  新闻发送
     */
    @RequestMapping(value = "/updateNews", method = RequestMethod.POST)
    public RetDataBean updateNews(News news) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("news:update")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
            }
            if (StringUtils.isBlank(news.getNewsId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            news.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            Document htmlDoc = Jsoup.parse(news.getContent());
            String subheading = htmlDoc.text();
            if (subheading.length() > 50) {
                subheading = subheading.substring(0, 50) + "...";
            }
            news.setSubheading(subheading);
            if (StringUtils.isNotBlank(news.getIsTop())) {
                if (news.getIsTop().equals("1")) {
                    news.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
                }
            }
            Example example = new Example(News.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("newsId", news.getNewsId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, newsService.updateNews(news, example));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  news
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: sendNews
     * @Description:  发布新闻
     */
    @RequestMapping(value = "/sendNews", method = RequestMethod.POST)
    public RetDataBean sendNews(News news) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("news:insert")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            news.setNewsId(SysTools.getGUID());
            String createTime = SysTools.getTime("yyyy-MM-dd HH:mm:ss");
            news.setCreateUser(account.getAccountId());
            news.setDelFlag("0");
            news.setStatus("0");
            news.setOnclickCount(0);
            if (StringUtils.isBlank(news.getSendTime())) {
                news.setSendTime(createTime.split(" ")[0]);
            }
            Document htmlDoc = Jsoup.parse(news.getContent());
            String subheading = htmlDoc.text();
            if (subheading.length() > 50) {
                subheading = subheading.substring(0, 50) + "...";
            }
            news.setSubheading(subheading);
            news.setCreateTime(createTime);
            news.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, newsService.sendNews(news, userInfo));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  news
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: delNews
     * @Description:  删除新闻
     */
    @RequestMapping(value = "/delNews", method = RequestMethod.POST)
    public RetDataBean delNews(News news) {
        Subject subject = SecurityUtils.getSubject();
        if (!subject.isPermitted("news:delete")) {
            return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
        }
        try {
            if (StringUtils.isBlank(news.getNewsId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            news.setOrgId(account.getOrgId());
            if (!account.getOpFlag().equals("1")) {
                news.setCreateUser(account.getAccountId());
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, newsService.deleteNews(news));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 生效新闻
     *
     * @param news
     * @return
     */
    @RequestMapping(value = "/startNews", method = RequestMethod.POST)
    public RetDataBean startNews(News news) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("news:update")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
            }
            if (StringUtils.isBlank(news.getNewsId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            news.setOrgId(account.getOrgId());
            news.setStatus("0");
            news.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            news.setEndTime("");
            Example example = new Example(News.class);
            Example.Criteria criteria = example.createCriteria();
            criteria.andEqualTo("orgId", account.getOrgId()).andEqualTo("newsId", news.getNewsId());
            if (!account.getOpFlag().equals("1")) {
                criteria.andEqualTo("createUser", account.getAccountId());

            }
            if (newsService.updateNews(news, example) > 0) {
                return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
            } else {
                return RetDataTools.Ok(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param @param  request
     * @param @param  news
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: stopNews
     * @Description: (这里用一句话描述这个方法的作用)
     */
    @RequestMapping(value = "/stopNews", method = RequestMethod.POST)
    public RetDataBean stopNews(News news) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("news:update")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
            }
            if (StringUtils.isBlank(news.getNewsId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            news.setOrgId(account.getOrgId());
            news.setStatus("1");
            news.setEndTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            Example example = new Example(News.class);
            Example.Criteria criteria = example.createCriteria();
            criteria.andEqualTo("orgId", account.getOrgId()).andEqualTo("newsId", news.getNewsId());
            if (!account.getOpFlag().equals("1")) {
                criteria.andEqualTo("createUser", account.getAccountId());

            }
            if (newsService.updateNews(news, example) > 0) {
                return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
            } else {
                return RetDataTools.Ok(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  news
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: sotpNews
     * @Description:  终止新闻状态
     */
    @RequestMapping(value = "/sotpNews", method = RequestMethod.POST)
    public RetDataBean sotpNews(News news) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("news:update")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
            }
            if (StringUtils.isBlank(news.getNewsId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            news.setDelFlag("1");
            Example example = new Example(News.class);
            if (!account.getOpFlag().equals("1")) {
                example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("newId", news.getNewsId()).andEqualTo("createUser", account.getAccountId());
            } else {
                example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("newId", news.getNewsId());
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, newsService.updateNews(news, example));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


}
