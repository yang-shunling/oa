package com.core136.controller.news;

import com.core136.bean.account.Account;
import com.core136.bean.account.UserInfo;
import com.core136.bean.news.News;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.account.UserInfoService;
import com.core136.service.news.NewsCommentsService;
import com.core136.service.news.NewsService;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/ret/oaget")
public class RouteGetNewsController {
    private NewsService newsService;

    @Autowired
    public void setNewsService(NewsService newsService) {
        this.newsService = newsService;
    }

    private NewsCommentsService newsCommentsService;

    @Autowired
    public void setNewsCommentsService(NewsCommentsService newsCommentsService) {
        this.newsCommentsService = newsCommentsService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private UserInfoService userInfoService;

    @Autowired
    public void setUserInfoService(UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }


    /**
     * 获取新闻人员查看状态
     *
     * @param news
     * @return
     */
    @RequestMapping(value = "/getNewsReadStatus", method = RequestMethod.POST)
    public RetDataBean getNewsReadStatus(News news) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            news.setOrgId(account.getOrgId());
            news = newsService.selectOneNews(news);
            List<Map<String, String>> accountIdList = userInfoService.getAccountIdInPriv(news.getOrgId(), news.getUserPriv(), news.getDeptPriv(), news.getLevelPriv());
            List<String> list = new ArrayList<String>();
            for (Map<String, String> map : accountIdList) {
                list.add(map.get("accountId"));
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, newsService.getNewsReadStatus(account.getOrgId(), news.getNewsId(), list));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @Title: getCommentsList
     * @Description:  获取新闻下的所有评论
     * @param: request
     * @param: newsId
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getCommentsList", method = RequestMethod.POST)
    public RetDataBean getCommentsList(String newsId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, newsCommentsService.getCommentsList(account.getOrgId(), newsId));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @return RetDataBean
     * @Title: getMyPicNewsListForDesk
     * @Description:  获取新闻图片
     */
    @RequestMapping(value = "/getMyPicNewsListForDesk", method = RequestMethod.POST)
    public RetDataBean getMyPicNewsListForDesk() {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("news:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, newsService.getMyPicNewsListForDesk(userInfo.getOrgId(), SysTools.getTime("yyyy-MM-dd"), userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getLeadLevel()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getMyNewsListForDesk
     * @Description: (这里用一句话描述这个方法的作用)
     * @param: request
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getMyNewsListForDesk", method = RequestMethod.POST)
    public RetDataBean getMyNewsListForDesk() {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("news:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, newsService.getMyNewsListForDesk(userInfo.getOrgId(), SysTools.getTime("yyyy-MM-dd"), userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getLeadLevel()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @return RetDataBean
     * @Title getNewsManageList
     * @Description  获取新闻维护列表
     */
    @RequestMapping(value = "/getNewsManageList", method = RequestMethod.POST)
    public RetDataBean getNewsManageList(PageParam pageParam, String status) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("news:manage")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_MANAGE_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("n.create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrgId(account.getOrgId());
            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            pageParam.setOrderBy(orderBy);
            PageInfo<Map<String, Object>> pageInfo = newsService.getNewsManageList(pageParam, status);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getMyNewsList
     * @Description:  获取个人新闻列表
     * @param: request
     * @param: pageParam
     * @param: newsType
     * @param: status
     * @param: beginTime
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getMyNewsList", method = RequestMethod.POST)
    public RetDataBean getMyNewsList(
            PageParam pageParam,
            String newsType,
            String status,
            String beginTime
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("news:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("n.create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            String endTime = SysTools.getTime("yyyy-MM-dd");
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            pageParam.setOrgId(userInfo.getOrgId());
            pageParam.setAccountId(userInfo.getAccountId());
            pageParam.setOrderBy(orderBy);
            PageInfo<Map<String, Object>> pageInfo = newsService.getMyNewsList(pageParam, userInfo.getDeptId(), userInfo.getLeadLevel(), newsType, status, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  news
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getReadNews
     * @Description:  获取查阅的新闻详情
     */
    @RequestMapping(value = "/getReadNews", method = RequestMethod.POST)
    public RetDataBean getReadNews(News news) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("news:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, newsService.getReadNews(userInfo.getOrgId(), userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getLeadLevel(), news.getNewsId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  news
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getNewsById
     * @Description:  按新闻ID获取新闻
     */
    @RequestMapping(value = "/getNewsById", method = RequestMethod.POST)
    public RetDataBean getNewsById(News news) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("news:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            news.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, newsService.selectOneNews(news));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getMobileNewsInfo
     * @Description:  移动端获取新闻详情
     * @param: request
     * @param: news
     * @param: orgId
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getMobileNewsInfo", method = RequestMethod.POST)
    public RetDataBean getMobileNewsInfo(News news) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("news:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            if (StringUtils.isBlank(news.getNewsId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            news.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, newsService.getReadNews(account.getOrgId(), userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getLeadLevel(), news.getNewsId()));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }
}
