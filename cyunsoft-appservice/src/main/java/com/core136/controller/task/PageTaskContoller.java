package com.core136.controller.task;

import com.core136.common.utils.SysTools;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/app/core/task")
public class PageTaskContoller {
    /**
     * @return ModelAndView
     * @Title: goTaskDataDetails
     * @Description:  任务处理详情
     */
    @RequestMapping("/taskdatadetails")
    @RequiresPermissions("/app/core/task/taskdatadetails")
    public ModelAndView goTaskDataDetails() {
        try {
            return new ModelAndView("app/core/task/taskdatadetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: goGanttDetails
     * @Description:  整体进度甘特图
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/ganttdetails")
    @RequiresPermissions("/app/core/task/ganttdetails")
    public ModelAndView goGanttDetails() {
        try {
            return new ModelAndView("app/core/task/ganttdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goProcessDetails
     * @Description:  处理事件详情
     */
    @RequestMapping("/processdetails")
    @RequiresPermissions("/app/core/task/processdetails")
    public ModelAndView goProcessDetails() {
        try {
            return new ModelAndView("app/core/task/taskprocessdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: goTaskMonitor
     * @Description:  领导管控
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/taskmonitor")
    @RequiresPermissions("/app/core/task/taskmonitor")
    public ModelAndView goTaskMonitor(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/task/taskmonitor");
            } else {
                if (view.equals("supervisor")) {
                    mv = new ModelAndView("app/core/task/taskmonitorsupervisor");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


    /**
     * @Title: goMyTask
     * @Description:  我的作务列表
     * @param: view
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/myTask")
    @RequiresPermissions("/app/core/task/myTask")
    public ModelAndView goMyTask(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/task/mytask");
            } else {
                if (view.equals("manage")) {
                    mv = new ModelAndView("app/core/task/managemytask");
                } else if (view.equals("edit")) {
                    mv = new ModelAndView("app/core/task/editmytask");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: goAddTask
     * @Description:  创建任务
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/createtask")
    @RequiresPermissions("/app/core/task/createtask")
    public ModelAndView goCreatetask(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/task/createtask");
            } else {
                if (view.equals("manage")) {
                    mv = new ModelAndView("app/core/task/managetask");
                } else if (view.equals("edit")) {
                    mv = new ModelAndView("app/core/task/edittask");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: goTaskDetails
     * @Description:  任务详情
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/taskdetails")
    @RequiresPermissions("/app/core/task/taskdetails")
    public ModelAndView goTaskDetails(HttpServletRequest request) {
        try {
            if (SysTools.isMobileDevice(request)) {
                return new ModelAndView("/app/mobile/main/task/details");
            } else {
                return new ModelAndView("app/core/task/taskdetails");
            }
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: goAssignmentTask
     * @Description:  任务分解
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/assignmentTask")
    @RequiresPermissions("/app/core/task/assignmentTask")
    public ModelAndView goAssignmentTask(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/task/assignmenttask");
            } else {
                if (view.equals("distribution")) {
                    mv = new ModelAndView("app/core/task/distribution");
                } else if (view.equals("editdistribution")) {
                    mv = new ModelAndView("app/core/task/editdistribution");
                } else if (view.equals("gantt")) {
                    mv = new ModelAndView("app/core/task/assigngantt");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }
}
