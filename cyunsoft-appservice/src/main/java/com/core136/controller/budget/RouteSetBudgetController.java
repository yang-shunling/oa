package com.core136.controller.budget;

import com.core136.bean.account.Account;
import com.core136.bean.budget.*;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.budget.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/set/budgetset")
public class RouteSetBudgetController {
    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private BudgetAccountService budgetAccountService;

    @Autowired
    public void setBudgetAccountService(BudgetAccountService budgetAccountService) {
        this.budgetAccountService = budgetAccountService;
    }

    private BudgetTypeService budgetTypeService;

    @Autowired
    public void setBudgetTypeService(BudgetTypeService budgetTypeService) {
        this.budgetTypeService = budgetTypeService;
    }

    private BudgetProjectService budgetProjectService;

    @Autowired
    public void setBudgetProjectService(BudgetProjectService budgetProjectService) {
        this.budgetProjectService = budgetProjectService;
    }

    private BudgetCostService budgetCostService;

    @Autowired
    public void setBudgetCostService(BudgetCostService budgetCostService) {
        this.budgetCostService = budgetCostService;
    }

    private BudgetConfigService budgetConfigService;

    @Autowired
    public void setBudgetConfigService(BudgetConfigService budgetConfigService) {
        this.budgetConfigService = budgetConfigService;
    }

    private BudgetAdjustmentService budgetAdjustmentService;

    @Autowired
    public void setBudgetAdjustmentService(BudgetAdjustmentService budgetAdjustmentService) {
        this.budgetAdjustmentService = budgetAdjustmentService;
    }

    private BudgetCostApplyService budgetCostApplyService;

    @Autowired
    public void setBudgetCostApplyService(BudgetCostApplyService budgetCostApplyService) {
        this.budgetCostApplyService = budgetCostApplyService;
    }


    /**
     * @param budgetCostApply
     * @return RetDataBean
     * @Title: insertBudgetCostApply
     * @Description:  申请项目预算费用
     */
    @RequestMapping(value = "/insertBudgetCostApply", method = RequestMethod.POST)
    public RetDataBean insertBudgetCostApply(BudgetCostApply budgetCostApply) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            budgetCostApply.setRecordId(SysTools.getGUID());
            budgetCostApply.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            budgetCostApply.setCreateUser(account.getAccountId());
            budgetCostApply.setOrgId(account.getOrgId());
            return budgetCostApplyService.addBudgetCostApply(budgetCostApply);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param budgetCostApply
     * @return RetDataBean
     * @Title: deleteBudgetCostApply
     * @Description:  删除预算费用申请
     */
    @RequestMapping(value = "/deleteBudgetCostApply", method = RequestMethod.POST)
    public RetDataBean deleteBudgetCostApply(BudgetCostApply budgetCostApply) {
        try {
            if (StringUtils.isBlank(budgetCostApply.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            budgetCostApply.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, budgetCostApplyService.deleteBudgetCostApply(budgetCostApply));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param budgetCostApply
     * @return RetDataBean
     * @Title: updateBudgetCostApply
     * @Description:  更新预算费用申请
     */
    @RequestMapping(value = "/updateBudgetCostApply", method = RequestMethod.POST)
    public RetDataBean updateBudgetCostApply(BudgetCostApply budgetCostApply) {
        try {
            if (StringUtils.isBlank(budgetCostApply.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            budgetCostApply.setOrgId(account.getOrgId());
            Example example = new Example(BudgetCostApply.class);
            example.createCriteria().andEqualTo("orgId", budgetCostApply.getOrgId()).andEqualTo("recordId", budgetCostApply.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, budgetCostApplyService.updateBudgetCostApply(example, budgetCostApply));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param budgetAdjustment
     * @return RetDataBean
     * @Title: insertBudgetAdjustment
     * @Description:  添加费用调整申请
     */
    @Transactional(value = "generalTM")
    @RequestMapping(value = "/insertBudgetAdjustment", method = RequestMethod.POST)
    public RetDataBean insertBudgetAdjustment(BudgetAdjustment budgetAdjustment) {
        try {
            String time = SysTools.getTime("yyyy-MM-dd HH:mm:ss");
            Account account = accountService.getRedisAUserInfoToAccount();
            BudgetConfig budgetConfig = new BudgetConfig();
            budgetConfig.setOrgId(account.getOrgId());
            budgetConfig = budgetConfigService.selectOneBudgetConfig(budgetConfig);
            budgetAdjustment.setRecordId(SysTools.getGUID());
            budgetAdjustment.setCreateTime(time);
            budgetAdjustment.setCreateUser(account.getAccountId());
            budgetAdjustment.setOrgId(account.getOrgId());
            if (budgetConfig != null) {
                if (StringUtils.isNotBlank(budgetConfig.getAdjustmentType())) {
                    if (budgetConfig.getAdjustmentType().equals("1")) {
                        budgetAdjustment.setStatus("1");
                        budgetAdjustment.setApprovalUser(account.getAccountId());
                        budgetAdjustment.setApprovalTime(time);
                        budgetAdjustmentService.insertBudgetAdjustment(budgetAdjustment);
                        return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, budgetAdjustmentService.setApprovalStatus(budgetAdjustment));
                    } else {
                        return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, budgetAdjustmentService.insertBudgetAdjustment(budgetAdjustment));
                    }
                } else {
                    return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, budgetAdjustmentService.insertBudgetAdjustment(budgetAdjustment));
                }
            } else {
                return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, budgetAdjustmentService.insertBudgetAdjustment(budgetAdjustment));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param budgetAdjustment
     * @return RetDataBean
     * @Title: deleteBudgetAdjustment
     * @Description:  删除预算调整申请
     */
    @RequestMapping(value = "/deleteBudgetAdjustment", method = RequestMethod.POST)
    public RetDataBean deleteBudgetAdjustment(BudgetAdjustment budgetAdjustment) {
        try {
            if (StringUtils.isBlank(budgetAdjustment.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            budgetAdjustment.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, budgetAdjustmentService.deleteBudgetAdjustment(budgetAdjustment));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param budgetAdjustment
     * @return RetDataBean
     * @Title: updateBudgetAdjustment
     * @Description:  费用调整申请
     */
    @RequestMapping(value = "/updateBudgetAdjustment", method = RequestMethod.POST)
    public RetDataBean updateBudgetAdjustment(BudgetAdjustment budgetAdjustment) {
        try {
            if (StringUtils.isBlank(budgetAdjustment.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            budgetAdjustment.setOrgId(account.getOrgId());
            Example example = new Example(BudgetAdjustment.class);
            example.createCriteria().andEqualTo("orgId", budgetAdjustment.getOrgId()).andEqualTo("recordId", budgetAdjustment.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, budgetAdjustmentService.updateBudgetAdjustment(example, budgetAdjustment));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param budgetAdjustment
     * @return RetDataBean
     * @Title: setApprovalStatus
     * @Description:  审批时更新项目预算
     */
    @RequestMapping(value = "/setApprovalStatus", method = RequestMethod.POST)
    public RetDataBean setApprovalStatus(BudgetAdjustment budgetAdjustment) {
        try {
            if (StringUtils.isBlank(budgetAdjustment.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            budgetAdjustment.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, budgetAdjustmentService.setApprovalStatus(budgetAdjustment));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param budgetCostApply
     * @return RetDataBean
     * @Title: setCostApprovalStatus
     * @Description:  预算费用申请审批
     */
    @RequestMapping(value = "/setCostApprovalStatus", method = RequestMethod.POST)
    public RetDataBean setCostApprovalStatus(BudgetCostApply budgetCostApply) {
        try {
            if (StringUtils.isBlank(budgetCostApply.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            budgetCostApply.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_APPROVAL_SUCCESS, budgetCostApplyService.setCostApprovalStatus(budgetCostApply));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param budgetConfig
     * @return RetDataBean
     * @Title: insertBudgetConfig
     * @Description:  创建配置
     */
    @RequestMapping(value = "/setBudgetConfig", method = RequestMethod.POST)
    public RetDataBean setBudgetConfig(BudgetConfig budgetConfig) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            budgetConfig.setConfigId(SysTools.getGUID());
            budgetConfig.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            budgetConfig.setCreateUser(account.getAccountId());
            budgetConfig.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, budgetConfigService.setBudgetConfig(budgetConfig));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param budgetConfig
     * @return RetDataBean
     * @Title: deleteBudgetConfig
     * @Description:  删除配置
     */
    @RequestMapping(value = "/deleteBudgetConfig", method = RequestMethod.POST)
    public RetDataBean deleteBudgetConfig(BudgetConfig budgetConfig) {
        try {
            if (StringUtils.isBlank(budgetConfig.getConfigId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            budgetConfig.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, budgetConfigService.deleteBudgetConfig(budgetConfig));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param budgetConfig
     * @return RetDataBean
     * @Title: updateBudgetConfig
     * @Description:  更新配置
     */
    @RequestMapping(value = "/updateBudgetConfig", method = RequestMethod.POST)
    public RetDataBean updateBudgetConfig(BudgetConfig budgetConfig) {
        try {
            if (StringUtils.isBlank(budgetConfig.getConfigId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            budgetConfig.setOrgId(account.getOrgId());
            Example example = new Example(BudgetConfig.class);
            example.createCriteria().andEqualTo("orgId", budgetConfig.getOrgId()).andEqualTo("configId", budgetConfig.getConfigId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, budgetConfigService.updateBudgetConfig(example, budgetConfig));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param budgetCost
     * @return RetDataBean
     * @Title: insertBudgetCost
     * @Description:  录入费用
     */
    @RequestMapping(value = "/insertBudgetCost", method = RequestMethod.POST)
    public RetDataBean insertBudgetCost(BudgetCost budgetCost) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            budgetCost.setRecordId(SysTools.getGUID());
            budgetCost.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            budgetCost.setCreateUser(account.getAccountId());
            budgetCost.setOrgId(account.getOrgId());
            return budgetCostService.addBudgetCost(budgetCost);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param budgetCost
     * @return RetDataBean
     * @Title: deleteBudgetCost
     * @Description:  删除费用
     */
    @RequestMapping(value = "/deleteBudgetCost", method = RequestMethod.POST)
    public RetDataBean deleteBudgetCost(BudgetCost budgetCost) {
        try {
            if (StringUtils.isBlank(budgetCost.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            budgetCost.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, budgetCostService.deleteBudgetCost(budgetCost));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param budgetCost
     * @return RetDataBean
     * @Title: updateBudgetCost
     * @Description:  更新费用
     */
    @RequestMapping(value = "/updateBudgetCost", method = RequestMethod.POST)
    public RetDataBean updateBudgetCost(BudgetCost budgetCost) {
        try {
            if (StringUtils.isBlank(budgetCost.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            budgetCost.setOrgId(account.getOrgId());
            Example example = new Example(BudgetCost.class);
            example.createCriteria().andEqualTo("orgId", budgetCost.getOrgId()).andEqualTo("recordId", budgetCost.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, budgetCostService.updateBudgetCost(example, budgetCost));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param budgetProject
     * @return RetDataBean
     * @Title: insertChildBudgetProject
     * @Description:  创建子项目
     */
    @RequestMapping(value = "/insertChildBudgetProject", method = RequestMethod.POST)
    public RetDataBean insertChildBudgetProject(BudgetProject budgetProject) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("budget:insert")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(budgetProject.getLevelId())) {
                budgetProject.setLevelId("0");
            }
            budgetProject.setProjectId(SysTools.getGUID());
            budgetProject.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            budgetProject.setCreateUser(account.getAccountId());
            budgetProject.setOrgId(account.getOrgId());
            budgetProjectService.addBudgetProject(budgetProject);
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, budgetProject);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param budgetProject
     * @return RetDataBean
     * @Title: insertBudgetProject
     * @Description:  创建预算项目
     */
    @RequestMapping(value = "/insertBudgetProject", method = RequestMethod.POST)
    public RetDataBean insertBudgetProject(BudgetProject budgetProject) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("budget:insert")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(budgetProject.getLevelId())) {
                budgetProject.setLevelId("0");
            }
            budgetProject.setProjectId(SysTools.getGUID());
            budgetProject.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            budgetProject.setCreateUser(account.getAccountId());
            budgetProject.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, budgetProjectService.addBudgetProject(budgetProject));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param budgetProject
     * @return RetDataBean
     * @Title: deleteBudgetProject
     * @Description:  删除预算项目
     */
    @RequestMapping(value = "/deleteBudgetProject", method = RequestMethod.POST)
    public RetDataBean deleteBudgetProject(BudgetProject budgetProject) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("budget:delete")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
            }
            if (StringUtils.isBlank(budgetProject.getProjectId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            budgetProject.setOrgId(account.getOrgId());
            return budgetProjectService.deleteAndCheckBudgetProject(budgetProject);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param budgetProject
     * @return RetDataBean
     * @Title: updateBudgetProject
     * @Description:  更新预算项目
     */
    @RequestMapping(value = "/updateBudgetProject", method = RequestMethod.POST)
    public RetDataBean updateBudgetProject(BudgetProject budgetProject) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("budget:update")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
            }
            if (StringUtils.isBlank(budgetProject.getProjectId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(BudgetProject.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("projectId", budgetProject.getProjectId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, budgetProjectService.updateBudgetProjectAndParentId(example, budgetProject));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param budgetTypeArr
     * @return RetDataBean
     * @Title: deleteBudgetTypeBatch
     * @Description:  批量删除预算分类
     */
    @RequestMapping(value = "/deleteBudgetTypeBatch", method = RequestMethod.POST)
    public RetDataBean deleteBudgetTypeBatch(@RequestParam(value = "budgetTypeArr[]") String[] budgetTypeArr) {
        try {
            if (budgetTypeArr.length == 0) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            }
            List<String> list = new ArrayList<String>(Arrays.asList(budgetTypeArr));
            Example example = new Example(BudgetType.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andIn("budgetTypeId", list);
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, budgetTypeService.deleteBudgetType(example));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param budgetType
     * @return RetDataBean
     * @Title: insertBudgetType
     * @Description:  添加预算分类
     */
    @RequestMapping(value = "/insertBudgetType", method = RequestMethod.POST)
    public RetDataBean insertBudgetType(BudgetType budgetType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            budgetType.setBudgetTypeId(SysTools.getGUID());
            budgetType.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            budgetType.setCreateUser(account.getAccountId());
            budgetType.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, budgetTypeService.insertBudgetType(budgetType));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param budgetType
     * @return RetDataBean
     * @Title: deleteBudgetType
     * @Description:  删除预算分类
     */
    @RequestMapping(value = "/deleteBudgetType", method = RequestMethod.POST)
    public RetDataBean deleteBudgetType(BudgetType budgetType) {
        try {
            if (StringUtils.isBlank(budgetType.getBudgetTypeId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            budgetType.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, budgetTypeService.deleteBudgetType(budgetType));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param budgetType
     * @return RetDataBean
     * @Title: updateBudgetType
     * @Description:  更新预算分类
     */
    @RequestMapping(value = "/updateBudgetType", method = RequestMethod.POST)
    public RetDataBean updateBudgetType(BudgetType budgetType) {
        try {
            if (StringUtils.isBlank(budgetType.getBudgetTypeId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(BudgetType.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("budgetTypeId", budgetType.getBudgetTypeId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, budgetTypeService.updateBudgetType(example, budgetType));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param budgetAccount
     * @return RetDataBean
     * @Title: insertContractSort
     * @Description:  添加预算科目
     */
    @RequestMapping(value = "/insertBudgetAccount", method = RequestMethod.POST)
    public RetDataBean insertContractSort(BudgetAccount budgetAccount) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            budgetAccount.setBudgetAccountId(SysTools.getGUID());
            budgetAccount.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            budgetAccount.setCreateUser(account.getAccountId());
            if (StringUtils.isBlank(budgetAccount.getLevelId())) {
                budgetAccount.setLevelId("0");
            }
            budgetAccount.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, budgetAccountService.insertBudgetAccount(budgetAccount));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param budgetAccount
     * @return RetDataBean
     * @Title: deleteBudgetAccount
     * @Description:  删除预算科目
     */
    @RequestMapping(value = "/deleteBudgetAccount", method = RequestMethod.POST)
    public RetDataBean deleteBudgetAccount(BudgetAccount budgetAccount) {
        try {
            if (StringUtils.isBlank(budgetAccount.getBudgetAccountId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            budgetAccount.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, budgetAccountService.deleteBudgetAccount(budgetAccount));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param budgetAccount
     * @return RetDataBean
     * @Title: updateContractSort
     * @Description:  更新预算科目
     */
    @RequestMapping(value = "/updateBudgetAccount", method = RequestMethod.POST)
    public RetDataBean updateBudgetAccount(BudgetAccount budgetAccount) {
        try {
            if (StringUtils.isBlank(budgetAccount.getBudgetAccountId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(BudgetAccount.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("budgetAccountId", budgetAccount.getBudgetAccountId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, budgetAccountService.updateBudgetAccount(example, budgetAccount));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

}
