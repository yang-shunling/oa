package com.core136.controller.archives;

import com.core136.bean.account.Account;
import com.core136.bean.archives.*;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.service.account.AccountService;
import com.core136.service.archives.*;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;


@RestController
@RequestMapping("/ret/archivesget")
public class RouteGetArchivesController {
    private ArchivesRepositoryService archivesRepositoryService;

    @Autowired
    public void setArchivesRepositoryService(ArchivesRepositoryService archivesRepositoryService) {
        this.archivesRepositoryService = archivesRepositoryService;
    }

    private ArchivesVolumeService archivesVolumeService;

    @Autowired
    public void setArchivesVolumeService(ArchivesVolumeService archivesVolumeService) {
        this.archivesVolumeService = archivesVolumeService;
    }

    private ArchivesFileService archivesFileService;

    @Autowired
    public void setArchivesFileService(ArchivesFileService archivesFileService) {
        this.archivesFileService = archivesFileService;
    }

    private ArchivesBorrowFileService archivesBorrowFileService;

    @Autowired
    public void setArchivesBorrowFileService(ArchivesBorrowFileService archivesBorrowFileService) {
        this.archivesBorrowFileService = archivesBorrowFileService;
    }

    private ArchivesBorrowVolumeService archivesBorrowVolumeService;

    @Autowired
    public void setArchivesBorrowVolumeService(ArchivesBorrowVolumeService archivesBorrowVolumeService) {
        this.archivesBorrowVolumeService = archivesBorrowVolumeService;
    }

    private ArchivesDestroyRecordService archivesDestroyRecordService;

    @Autowired
    public void setArchivesDestroyRecordService(ArchivesDestroyRecordService archivesDestroyRecordService) {
        this.archivesDestroyRecordService = archivesDestroyRecordService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * @param pageParam
     * @param approvalStatus
     * @param createUser
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getArchivesBorrowVolumeApprovalOldList
     * @Description:  获取历史审批记录
     */
    @RequestMapping(value = "/getArchivesBorrowVolumeApprovalOldList", method = RequestMethod.POST)
    public RetDataBean getArchivesBorrowVolumeApprovalOldList(PageParam pageParam, String approvalStatus, String createUser, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("b.create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = archivesBorrowVolumeService.getArchivesBorrowVolumeApprovalOldList(pageParam, approvalStatus, createUser, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @return RetDataBean
     * @Title: getArchivesBorrowFileApprovalOldList
     * @Description:  获取历史审批记录
     */
    @RequestMapping(value = "/getArchivesBorrowFileApprovalOldList", method = RequestMethod.POST)
    public RetDataBean getArchivesBorrowFileApprovalOldList(PageParam pageParam, String approvalStatus, String createUser, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("b.create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = archivesBorrowFileService.getArchivesBorrowFileApprovalOldList(pageParam, approvalStatus, createUser, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @param createUer
     * @return RetDataBean
     * @Title: getArchivesDestoryVolumeList
     * @Description:  档案销毁记录
     */
    @RequestMapping(value = "/getArchivesDestoryVolumeList", method = RequestMethod.POST)
    public RetDataBean getArchivesDestoryVolumeList(PageParam pageParam, String beginTime, String endTime, String createUer) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("d.create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(createUer);
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = archivesDestroyRecordService.getArchivesDestoryVolumeList(pageParam, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @param createUer
     * @return RetDataBean
     * @Title: getArchivesDestoryFileList
     * @Description:  档案销毁记录
     */
    @RequestMapping(value = "/getArchivesDestoryFileList", method = RequestMethod.POST)
    public RetDataBean getArchivesDestoryFileList(PageParam pageParam, String beginTime, String endTime, String createUer) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("d.create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(createUer);
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = archivesDestroyRecordService.getArchivesDestoryFileList(pageParam, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param archivesBorrowVolume
     * @param archivesFile
     * @return RetDataBean
     * @Title: getApprovalVolumeFile
     * @Description:  获取案卷借阅文件详情
     */
    @RequestMapping(value = "/getApprovalVolumeFile", method = RequestMethod.POST)
    public RetDataBean getApprovalVolumeFile(ArchivesBorrowVolume archivesBorrowVolume, ArchivesFile archivesFile) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            archivesBorrowVolume.setOrgId(account.getOrgId());
            archivesFile.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, archivesBorrowVolumeService.getApprovalVolumeFile(account, archivesBorrowVolume, archivesFile));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param volumeId
     * @return RetDataBean
     * @Title: getBorrowArchivesFileList
     * @Description:  获取借阅的文件列表
     */
    @RequestMapping(value = "/getBorrowArchivesFileList", method = RequestMethod.POST)
    public RetDataBean getBorrowArchivesFileList(PageParam pageParam, String volumeId) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("archives:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("f.sort_no");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = archivesFileService.getBorrowArchivesFileList(pageParam, volumeId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param archivesBorrowFile
     * @return RetDataBean
     * @Title: getApprovalFile
     * @Description:  获取借阅文件详情
     */
    @RequestMapping(value = "/getApprovalFile", method = RequestMethod.POST)
    public RetDataBean getApprovalFile(ArchivesBorrowFile archivesBorrowFile) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            archivesBorrowFile.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, archivesBorrowFileService.getApprovalFile(account, archivesBorrowFile));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @return RetDataBean
     * @Title: getArchivesBorrowVolumeApprovalList
     * @Description:  获取待审批记录
     */
    @RequestMapping(value = "/getArchivesBorrowVolumeApprovalList", method = RequestMethod.POST)
    public RetDataBean getArchivesBorrowVolumeApprovalList(PageParam pageParam) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("b.create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = archivesBorrowVolumeService.getArchivesBorrowVolumeApprovalList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @return RetDataBean
     * @Title: getArchivesBorrowFileApprovalList
     * @Description:  获取待审批记录
     */
    @RequestMapping(value = "/getArchivesBorrowFileApprovalList", method = RequestMethod.POST)
    public RetDataBean getArchivesBorrowFileApprovalList(PageParam pageParam) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("b.create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = archivesBorrowFileService.getArchivesBorrowFileApprovalList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param repositoryId
     * @param volumeId
     * @param fileType
     * @param secretLevel
     * @return RetDataBean
     * @Title: getArchivesFileQueryList
     * @Description:  档案查询列表
     */
    @RequestMapping(value = "/getArchivesFileQueryList", method = RequestMethod.POST)
    public RetDataBean getArchivesFileQueryList(PageParam pageParam, String repositoryId, String volumeId, String fileType, String secretLevel) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("archives:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("f.sort_no");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = archivesFileService.getArchivesFileQueryList(pageParam, repositoryId, volumeId, fileType, secretLevel);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param approvalStatus
     * @return RetDataBean
     * @Title: getArchivesBorrowVolumeList
     * @Description:  案卷借阅记录
     */
    @RequestMapping(value = "/getArchivesBorrowVolumeList", method = RequestMethod.POST)
    public RetDataBean getArchivesBorrowVolumeList(PageParam pageParam, String approvalStatus) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("b.create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = archivesBorrowVolumeService.getArchivesBorrowVolumeList(pageParam, approvalStatus);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param archivesBorrowVolume
     * @return RetDataBean
     * @Title: getArchivesBorrowVolumeById
     * @Description:  获取案卷详情
     */
    @RequestMapping(value = "/getArchivesBorrowVolumeById", method = RequestMethod.POST)
    public RetDataBean getArchivesBorrowVolumeById(ArchivesBorrowVolume archivesBorrowVolume) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            archivesBorrowVolume.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, archivesBorrowVolumeService.selectOneArchivesBorrowVolume(archivesBorrowVolume));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param approvalStatus
     * @return RetDataBean
     * @Title: getArchivesBorrowFileList
     * @Description:  我的借阅记录
     */
    @RequestMapping(value = "/getArchivesBorrowFileList", method = RequestMethod.POST)
    public RetDataBean getArchivesBorrowFileList(PageParam pageParam, String approvalStatus) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("b.create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = archivesBorrowFileService.getArchivesBorrowFileList(pageParam, approvalStatus);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param archivesBorrowFile
     * @return RetDataBean
     * @Title: getArchivesBorrowById
     * @Description:  获取借阅详情
     */
    @RequestMapping(value = "/getArchivesBorrowFileById", method = RequestMethod.POST)
    public RetDataBean getArchivesBorrowById(ArchivesBorrowFile archivesBorrowFile) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            archivesBorrowFile.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, archivesBorrowFileService.selectOneArchivesBorrowFile(archivesBorrowFile));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @return RetDataBean
     * @Title: getArchivesFileList
     * @Description:  获取案卷列表
     */
    @RequestMapping(value = "/getArchivesFileList", method = RequestMethod.POST)
    public RetDataBean getArchivesFileList(PageParam pageParam, String volumeId, String fileType, String secretLevel) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("archives:manage")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_MANAGE_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("f.sort_no");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = archivesFileService.getArchivesFileList(pageParam, volumeId, fileType, secretLevel);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getArchivesVolumeListForSelect
     * @Description:  获取案卷列表
     */
    @RequestMapping(value = "/getArchivesVolumeListForSelect", method = RequestMethod.POST)
    public RetDataBean getArchivesVolumeListForSelect() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, archivesVolumeService.getArchivesVolumeListForSelect(account.getOrgId(), account.getOpFlag(), account.getAccountId()));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param repositoryId
     * @return RetDataBean
     * @Title: getArchivesVolumeByRepositoryId
     * @Description:  获取卷库下的案卷列表
     */
    @RequestMapping(value = "/getArchivesVolumeByRepositoryId", method = RequestMethod.POST)
    public RetDataBean getArchivesVolumeByRepositoryId(String repositoryId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, archivesVolumeService.getArchivesVolumeByRepositoryId(account.getOrgId(), repositoryId));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param archivesFile
     * @return RetDataBean
     * @Title: getArchivesFileById
     * @Description:  获取文件详情
     */
    @RequestMapping(value = "/getArchivesFileById", method = RequestMethod.POST)
    public RetDataBean getArchivesFileById(ArchivesFile archivesFile) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("archives:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            archivesFile.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, archivesFileService.selectOneArchivesFile(archivesFile));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param archivesVolume
     * @return RetDataBean
     * @Title: getArchivesVolumeById
     * @Description:  获取案卷详情
     */
    @RequestMapping(value = "/getArchivesVolumeById", method = RequestMethod.POST)
    public RetDataBean getArchivesVolumeById(ArchivesVolume archivesVolume) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            archivesVolume.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, archivesVolumeService.selectOneArchivesVolume(archivesVolume));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @return RetDataBean
     * @Title: getArchivesVolumeList
     * @Description:  获取案卷列表
     */
    @RequestMapping(value = "/getArchivesVolumeList", method = RequestMethod.POST)
    public RetDataBean getArchivesVolumeList(PageParam pageParam) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("v.sort_no");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = archivesVolumeService.getArchivesVolumeList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param archivesRepository
     * @return RetDataBean
     * @Title: getArchivesRepositoryById
     * @Description:  获取卷库详情
     */
    @RequestMapping(value = "/getArchivesRepositoryById", method = RequestMethod.POST)
    public RetDataBean getArchivesRepositoryById(ArchivesRepository archivesRepository) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            archivesRepository.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, archivesRepositoryService.selectOneArchivesRepository(archivesRepository));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getArchivesRepositoryList
     * @Description: (这里用一句话描述这个方法的作用)
     */
    @RequestMapping(value = "/getArchivesRepositoryList", method = RequestMethod.POST)
    public RetDataBean getArchivesRepositoryList() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, archivesRepositoryService.getArchivesRepositoryList(account.getOrgId(), account.getOpFlag(), account.getAccountId()));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

}
