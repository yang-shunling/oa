package com.core136.controller.archives;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping("/app/core/archives")
public class PageArchivesController {
    /**
     * @return ModelAndView
     * @Title: goVolumeListDetails
     * @Description:  借阅案卷详情
     */
    @RequestMapping("/volumelistdetails")
    @RequiresPermissions("/app/core/archives/volumelistdetails")
    public ModelAndView goVolumeListDetails() {
        try {
            return new ModelAndView("app/core/archives/volumelistdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goApproval
     * @Description:  借阅审批
     */
    @RequestMapping("/approval")
    @RequiresPermissions("/app/core/archives/approval")
    public ModelAndView goApproval(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/archives/approvalmanage");
            } else {
                if (view.equals("old")) {
                    mv = new ModelAndView("app/core/archives/approval");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goDestroy
     * @Description:  档案销毁
     */
    @RequestMapping("/destroy")
    @RequiresPermissions("/app/core/archives/destroy")
    public ModelAndView goDestroy() {
        try {
            return new ModelAndView("app/core/archives/destroymanage");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goQuery
     * @Description:  档案查询
     */
    @RequestMapping("/query")
    @RequiresPermissions("/app/core/archives/query")
    public ModelAndView goQuery() {
        try {
            return new ModelAndView("app/core/archives/query");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goBorrow
     * @Description:  档案借阅管理
     */
    @RequestMapping("/borrow")
    @RequiresPermissions("/app/core/archives/borrow")
    public ModelAndView goBorrow(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/archives/borrowmanage");
            } else {
                if (view.equals("create")) {
                    mv = new ModelAndView("app/core/archives/borrow");
                } else if (view.equals("volume")) {
                    mv = new ModelAndView("app/core/archives/borrowvolume");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goFile
     * @Description:  文件管理
     */
    @RequestMapping("/files")
    @RequiresPermissions("/app/core/archives/files")
    public ModelAndView goFiles(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/archives/filemanage");
            } else {
                if (view.equals("create")) {
                    mv = new ModelAndView("app/core/archives/file");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goVolume
     * @Description:  案卷管理
     */
    @RequestMapping("/volume")
    @RequiresPermissions("/app/core/archives/volume")
    public ModelAndView goVolume(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/archives/volumemanage");
            } else {
                if (view.equals("create")) {
                    mv = new ModelAndView("app/core/archives/volume");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goRepository
     * @Description:  卷库管理
     */
    @RequestMapping("/repository")
    @RequiresPermissions("/app/core/archives/repository")
    public ModelAndView goRepository(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/archives/repositorymanage");
            } else {
                if (view.equals("create")) {
                    mv = new ModelAndView("app/core/archives/repository");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goRepositoryDetails
     * @Description:  卷库详情
     */
    @RequestMapping("/repositorydetails")
    @RequiresPermissions("/app/core/archives/repositorydetails")
    public ModelAndView goRepositoryDetails() {
        ModelAndView mv = null;
        try {
            return new ModelAndView("app/core/archives/repositorydetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goBorrowFileDetails
     * @Description:  档案查询详情
     */
    @RequestMapping("/borrowfiledetails")
    @RequiresPermissions("/app/core/archives/borrowfiledetails")
    public ModelAndView goBorrowFileDetails() {
        try {
            return new ModelAndView("app/core/archives/borrowfiledetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goFileDetails
     * @Description:  文件详情
     */
    @RequestMapping("/filedetails")
    @RequiresPermissions("/app/core/archives/filedetails")
    public ModelAndView goFileDetails() {
        try {
            return new ModelAndView("app/core/archives/filedetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goFileDetails2
     * @Description:  文件借阅详情
     */
    @RequestMapping("/filedetailsforborrow")
    @RequiresPermissions("/app/core/archives/filedetailsforborrow")
    public ModelAndView goFileDetails2() {
        try {
            return new ModelAndView("app/core/archives/filedetails2");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goVolumeFileDetails
     * @Description:  案卷文件借阅详情
     */
    @RequestMapping("/volumefiledetails")
    @RequiresPermissions("/app/core/archives/volumefiledetails")
    public ModelAndView goVolumeFileDetails() {
        try {
            return new ModelAndView("app/core/archives/borrowvolumefiledetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goVolumeDetails
     * @Description:  案卷详情
     */
    @RequestMapping("/volumedetails")
    @RequiresPermissions("/app/core/archives/volumedetails")
    public ModelAndView goVolumeDetails() {
        try {
            return new ModelAndView("app/core/archives/volumedetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }
}
