package com.core136.controller.workplan;

import com.core136.bean.account.Account;
import com.core136.bean.account.UserInfo;
import com.core136.bean.workplan.WorkPlan;
import com.core136.bean.workplan.WorkPlanProcess;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.workplan.WorkPlanProcessService;
import com.core136.service.workplan.WorkPlanService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

@RestController
@RequestMapping("/set/workplanset")
public class RouteSetWorkPlanController {
    private WorkPlanService workPlanService;

    @Autowired
    public void setWorkPlanService(WorkPlanService workPlanService) {
        this.workPlanService = workPlanService;
    }

    private WorkPlanProcessService workPlanProcessService;

    @Autowired
    public void setWorkPlanProcessService(WorkPlanProcessService workPlanProcessService) {
        this.workPlanProcessService = workPlanProcessService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * @param workPlanProcess
     * @return RetDataBean
     * @Title: insertWorkPlanProcess
     * @Description:  添加计划完成情况
     */
    @RequestMapping(value = "/insertWorkPlanProcess", method = RequestMethod.POST)
    public RetDataBean insertWorkPlanProcess(WorkPlanProcess workPlanProcess) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("workplan:insert")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            workPlanProcess.setProcessId(SysTools.getGUID());
            workPlanProcess.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            workPlanProcess.setCreateUser(account.getAccountId());
            workPlanProcess.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, workPlanProcessService.insertWorkPlanProcess(workPlanProcess));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param workPlanProcess
     * @return RetDataBean
     * @Title: deleteWorkPlanProcess
     * @Description:  删除工作计划处理结果
     */
    @RequestMapping(value = "/deleteWorkPlanProcess", method = RequestMethod.POST)
    public RetDataBean deleteWorkPlanProcess(WorkPlanProcess workPlanProcess) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("workplan:delete")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
            }
            if (StringUtils.isBlank(workPlanProcess.getProcessId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            workPlanProcess.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, workPlanProcessService.deleteWorkPlanProcess(workPlanProcess));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param workPlanProcess
     * @return RetDataBean
     * @Title: updateWorkPlanProcess
     * @Description:  更新工作计划处理结果
     */
    @RequestMapping(value = "/updateWorkPlanProcess", method = RequestMethod.POST)
    public RetDataBean updateWorkPlanProcess(WorkPlanProcess workPlanProcess) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("workplan:update")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
            }
            if (StringUtils.isBlank(workPlanProcess.getProcessId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(WorkPlanProcess.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("processId", workPlanProcess.getProcessId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, workPlanProcessService.updateWorkPlanProcess(example, workPlanProcess));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param workPlan
     * @return RetDataBean
     * @Title: insertWorkPlan
     * @Description:  创建工作计划
     */
    @RequestMapping(value = "/createWorkPlan", method = RequestMethod.POST)
    public RetDataBean insertWorkPlan(WorkPlan workPlan) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("workplan:insert")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            workPlan.setPlanId(SysTools.getGUID());
            workPlan.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            workPlan.setCreateUser(account.getAccountId());
            workPlan.setOrgId(account.getOrgId());
            workPlan.setStatus("0");
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, workPlanService.createWorkPlan(account, userInfo, workPlan));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param workPlan
     * @return RetDataBean
     * @Title: deleteWorkPlan
     * @Description:  删除工作计划
     */
    @RequestMapping(value = "/deleteWorkPlan", method = RequestMethod.POST)
    public RetDataBean deleteWorkPlan(WorkPlan workPlan) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("workplan:delete")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
            }
            if (StringUtils.isBlank(workPlan.getPlanId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            workPlan.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, workPlanService.deleteWorkPlan(workPlan));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param workPlan
     * @return RetDataBean
     * @Title: updateWorkPlan
     * @Description:  更新工作计划
     */
    @RequestMapping(value = "/updateWorkPlan", method = RequestMethod.POST)
    public RetDataBean updateWorkPlan(WorkPlan workPlan) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("workplan:update")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
            }
            if (StringUtils.isBlank(workPlan.getPlanId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(WorkPlan.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("planId", workPlan.getPlanId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, workPlanService.updateWorkPlan(example, workPlan));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

}
