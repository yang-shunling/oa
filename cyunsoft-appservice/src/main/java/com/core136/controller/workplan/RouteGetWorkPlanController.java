package com.core136.controller.workplan;

import com.core136.bean.account.Account;
import com.core136.bean.account.UserInfo;
import com.core136.bean.sys.PageParam;
import com.core136.bean.workplan.WorkPlan;
import com.core136.bean.workplan.WorkPlanProcess;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.service.account.AccountService;
import com.core136.service.workplan.WorkPlanProcessService;
import com.core136.service.workplan.WorkPlanService;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/ret/workplanget")
public class RouteGetWorkPlanController {
    private WorkPlanService workPlanService;

    @Autowired
    public void setWorkPlanService(WorkPlanService workPlanService) {
        this.workPlanService = workPlanService;
    }

    private WorkPlanProcessService workPlanProcessService;

    @Autowired
    public void setWorkPlanProcessService(WorkPlanProcessService workPlanProcessService) {
        this.workPlanProcessService = workPlanProcessService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }


    /**
     * @param workPlanProcess
     * @return RetDataBean
     * @Title: getWorkPlanProcessList
     * @Description:  获取工作计划处理反馈列表
     */
    @RequestMapping(value = "/getWorkPlanProcessList", method = RequestMethod.POST)
    public RetDataBean getWorkPlanProcessList(WorkPlanProcess workPlanProcess) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("workplan:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            workPlanProcess.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, workPlanProcessService.getWorkPlanProcessList(workPlanProcess));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param workPlanProcess
     * @return RetDataBean
     * @Title: getWorkPlanProcessById
     * @Description:  获取工作计划处理详情
     */
    @RequestMapping(value = "/getWorkPlanProcessById", method = RequestMethod.POST)
    public RetDataBean getWorkPlanProcessById(WorkPlanProcess workPlanProcess) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("workplan:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            workPlanProcess.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, workPlanProcessService.selectOneWorkPlanProcess(workPlanProcess));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param workPlan
     * @return RetDataBean
     * @Title: getWorkPlanById
     * @Description:  获取工作计划详情
     */
    @RequestMapping(value = "/getWorkPlanById", method = RequestMethod.POST)
    public RetDataBean getWorkPlanById(WorkPlan workPlan) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("workplan:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            workPlan.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, workPlanService.selectOneWorkPlan(workPlan));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param status
     * @param planType
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getManageWorkPlanList
     * @Description:  获取工作维护列表
     */
    @RequestMapping(value = "/getManageWorkPlanList", method = RequestMethod.POST)
    public RetDataBean getManageWorkPlanList(PageParam pageParam,
                                             String status, String planType, String beginTime, String endTime
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("workplan:manage")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_MANAGE_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("w.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = workPlanService.getManageWorkPlanList(pageParam, beginTime, endTime, status, planType);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param status
     * @param planType
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getHoldWorkPlanList
     * @Description:  我负责的计划列表
     */
    @RequestMapping(value = "/getHoldWorkPlanList", method = RequestMethod.POST)
    public RetDataBean getHoldWorkPlanList(PageParam pageParam,
                                           String status, String planType, String beginTime, String endTime
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("workplan:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("w.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = workPlanService.getHoldWorkPlanList(pageParam, beginTime, endTime, status, planType);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param status
     * @param planType
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getSupWorkPlanList
     * @Description:  我督查的计划列表
     */
    @RequestMapping(value = "/getSupWorkPlanList", method = RequestMethod.POST)
    public RetDataBean getSupWorkPlanList(PageParam pageParam,
                                          String status, String planType, String beginTime, String endTime
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("workplan:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("w.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = workPlanService.getSupWorkPlanList(pageParam, beginTime, endTime, status, planType);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param status
     * @param planType
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getMyWorkPlanList
     * @Description:  我参与的工作计划
     */
    @RequestMapping(value = "/getMyWorkPlanList", method = RequestMethod.POST)
    public RetDataBean getMyWorkPlanList(PageParam pageParam,
                                         String status, String planType, String beginTime, String endTime
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("workplan:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("w.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = workPlanService.getMyWorkPlanList(pageParam, beginTime, endTime, status, planType);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @return RetDataBean
     * @Title: getTodayWorkPlanList
     * @Description:  获取今天的工作计划
     */
    @RequestMapping(value = "/getTodayWorkPlanList", method = RequestMethod.POST)
    public RetDataBean getTodayWorkPlanList(PageParam pageParam
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("workplan:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("w.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = workPlanService.getTodayWorkPlanList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @return RetDataBean
     * @Title: getMonthWorkPlanList
     * @Description:  查询本月工作计划
     */
    @RequestMapping(value = "/getMonthWorkPlanList", method = RequestMethod.POST)
    public RetDataBean getMonthWorkPlanList(PageParam pageParam
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("workplan:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("w.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = workPlanService.getMonthWorkPlanList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param status
     * @param planType
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getShareWorkPlanList
     * @Description:  获取分享的工作计划
     */
    @RequestMapping(value = "/getShareWorkPlanList", method = RequestMethod.POST)
    public RetDataBean getShareWorkPlanList(PageParam pageParam,
                                            String status, String planType, String beginTime, String endTime
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("workplan:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("w.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(userInfo.getOrgId());
            pageParam.setAccountId(userInfo.getAccountId());
            pageParam.setLevelId(userInfo.getLeadLevel());
            pageParam.setDeptId(userInfo.getDeptId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = workPlanService.getShareWorkPlanList(pageParam, beginTime, endTime, status, planType);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param status
     * @param planType
     * @param beginTime
     * @param endTime
     * @param holdUser
     * @param supUser
     * @return RetDataBean
     * @Title: queryWorkPlanList
     * @Description:  获取工作列表
     */
    @RequestMapping(value = "/queryWorkPlanList", method = RequestMethod.POST)
    public RetDataBean queryWorkPlanList(PageParam pageParam,
                                         String status, String planType, String beginTime, String endTime, String holdUser, String supUser
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("workplan:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("w.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(userInfo.getOrgId());
            pageParam.setAccountId(userInfo.getAccountId());
            pageParam.setLevelId(userInfo.getLeadLevel());
            pageParam.setDeptId(userInfo.getDeptId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = workPlanService.queryWorkPlanList(pageParam, beginTime, endTime, holdUser, supUser, status, planType);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }
}
