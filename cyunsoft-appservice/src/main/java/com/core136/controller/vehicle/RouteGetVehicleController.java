package com.core136.controller.vehicle;

import com.core136.bean.account.Account;
import com.core136.bean.sys.PageParam;
import com.core136.bean.vehicle.*;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.service.account.AccountService;
import com.core136.service.vehicle.*;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/ret/vehicleget")
public class RouteGetVehicleController {
    private VehicleInfoService vehicleInfoService;

    @Autowired
    public void setVehicleInfoService(VehicleInfoService vehicleInfoService) {
        this.vehicleInfoService = vehicleInfoService;
    }

    private VehicleApplyService vehicleApplyService;

    @Autowired
    public void setVehicleApplyService(VehicleApplyService vehicleApplyService) {
        this.vehicleApplyService = vehicleApplyService;
    }

    private VehicleOperatorService vehicleOperatorService;

    @Autowired
    public void setVehicleOperatorService(VehicleOperatorService vehicleOperatorService) {
        this.vehicleOperatorService = vehicleOperatorService;
    }

    private VehicleOilCardService vehicleOilCardService;

    @Autowired
    public void setVehicleOilCardService(VehicleOilCardService vehicleOilCardService) {
        this.vehicleOilCardService = vehicleOilCardService;
    }

    private VehicleRepairRecordService vehicleRepairRecordService;

    @Autowired
    public void setVehicleRepairRecordService(VehicleRepairRecordService vehicleRepairRecordService) {
        this.vehicleRepairRecordService = vehicleRepairRecordService;
    }

    private VehicleDriverService vehicleDriverService;

    @Autowired
    public void setVehicleDriverService(VehicleDriverService vehicleDriverService) {
        this.vehicleDriverService = vehicleDriverService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * @return RetDataBean
     * @Title: getNextDayList
     * @Description:  获取未来半个月的日期与周几
     */
    @RequestMapping(value = "/getNextDayList", method = RequestMethod.POST)
    public RetDataBean getNextDayList() {
        try {
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, vehicleDriverService.getNextDayList());
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    @RequestMapping(value = "/getVehicleDriverList", method = RequestMethod.POST)
    public RetDataBean getVehicleDriverList(PageParam pageParam) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("d.sort_no");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = vehicleDriverService.getVehicleDriverList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    @RequestMapping(value = "/getVehicleDriverListForSelect", method = RequestMethod.POST)
    public RetDataBean getVehicleDriverListForSelect() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, vehicleDriverService.getVehicleDriverListForSelect(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param vehicleDriver
     * @return RetDataBean
     * @Title: getVehicleDriverById
     * @Description:  获取司机详情
     */
    @RequestMapping(value = "/getVehicleDriverById", method = RequestMethod.POST)
    public RetDataBean getVehicleDriverById(VehicleDriver vehicleDriver) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            vehicleDriver.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, vehicleDriverService.selectOneVehicleDriver(vehicleDriver));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param createUser
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getVehicleReturnOldList
     * @Description:  历史还车记录
     */
    @RequestMapping(value = "/getVehicleReturnOldList", method = RequestMethod.POST)
    public RetDataBean getVehicleReturnOldList(PageParam pageParam, String createUser, String beginTime, String endTime
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("a.create_time");
            } else {
                pageParam.setSort("a." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = vehicleApplyService.getVehicleReturnOldList(pageParam, createUser, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param createUser
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getVehicleReturnList
     * @Description:  获取待还车记录
     */
    @RequestMapping(value = "/getVehicleReturnList", method = RequestMethod.POST)
    public RetDataBean getVehicleReturnList(PageParam pageParam, String createUser, String beginTime, String endTime
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("a.create_time");
            } else {
                pageParam.setSort("a." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = vehicleApplyService.getVehicleReturnList(pageParam, createUser, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @return RetDataBean
     * @Title: getAllVehicleList
     * @Description:  获取所有车辆列表
     */
    @RequestMapping(value = "/getAllVehicleList", method = RequestMethod.POST)
    public RetDataBean getAllVehicleList() {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("vehicle:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            VehicleInfo vehicleInfo = new VehicleInfo();
            vehicleInfo.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, vehicleInfoService.getAllVehicleList(vehicleInfo));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getCanUsedVehicleList
     * @Description:  获取可调度车辆
     */
    @RequestMapping(value = "/getCanUsedVehicleList", method = RequestMethod.POST)
    public RetDataBean getCanUsedVehicleList() {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("vehicle:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, vehicleInfoService.getCanUsedVehicleList(account.getOrgId()));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param vehicleRepairRecord
     * @return RetDataBean
     * @Title: getVehicleRepairRecordById
     * @Description:  获取维修记录详情
     */
    @RequestMapping(value = "/getVehicleRepairRecordById", method = RequestMethod.POST)
    public RetDataBean getVehicleRepairRecordById(VehicleRepairRecord vehicleRepairRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            vehicleRepairRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, vehicleRepairRecordService.selectOneVehicleRepairRecord(vehicleRepairRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param vehicleOilCard
     * @return RetDataBean
     * @Title: getVehicleOilCardById
     * @Description:  获取油卡信息详情
     */
    @RequestMapping(value = "/getVehicleOilCardById", method = RequestMethod.POST)
    public RetDataBean getVehicleOilCardById(VehicleOilCard vehicleOilCard) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            vehicleOilCard.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, vehicleOilCardService.selectOneVehicleOilCard(vehicleOilCard));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getCanUsedOilCardList
     * @Description:  获取可通的油卡列表
     */
    @RequestMapping(value = "/getCanUsedOilCardList", method = RequestMethod.POST)
    public RetDataBean getCanUsedOilCardList() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, vehicleOilCardService.getCanUsedOilCardList(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getVehicleOperatorByOrgId
     * @Description:  获取车辆调度人员
     */
    @RequestMapping(value = "/getVehicleOperatorByOrgId", method = RequestMethod.POST)
    public RetDataBean getVehicleOperatorByOrgId() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            VehicleOperator vehicleOperator = new VehicleOperator();
            vehicleOperator.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, vehicleOperatorService.selectOneVehicleOperator(vehicleOperator));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param vehicleApply
     * @return RetDataBean
     * @Title: getVehicleApplyById
     * @Description:  获取车辆使用申请详情
     */
    @RequestMapping(value = "/getVehicleApplyById", method = RequestMethod.POST)
    public RetDataBean getVehicleApplyById(VehicleApply vehicleApply) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            vehicleApply.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, vehicleApplyService.selectOneVehicleApply(vehicleApply));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param vehicleInfo
     * @return RetDataBean
     * @Title: getVehicleInfoById
     * @Description:  获取车辆信息详情
     */
    @RequestMapping(value = "/getVehicleInfoById", method = RequestMethod.POST)
    public RetDataBean getVehicleInfoById(VehicleInfo vehicleInfo) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("vehicle:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            vehicleInfo.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, vehicleInfoService.selectOneVehicleInfo(vehicleInfo));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param onwer
     * @param type
     * @param nature
     * @param beginTime
     * @param endTime
     * @param beginTime1
     * @param endTime1
     * @return RetDataBean
     * @Title: getManageVehicleInfoList
     * @Description:  获取车辆列表
     */
    @RequestMapping(value = "/getManageVehicleInfoList", method = RequestMethod.POST)
    public RetDataBean getManageVehicleInfoList(PageParam pageParam,
                                                String onwer, String type, String nature, String beginTime, String endTime, String beginTime1, String endTime1
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("vehicle:manage")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_MANAGE_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = vehicleInfoService.getManageVehicleInfoList(pageParam, onwer, type, nature, beginTime, endTime, beginTime1, endTime1);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param repairType
     * @param repairUser
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getVehicleRepairRecordList
     * @Description:  获取维修列表
     */
    @RequestMapping(value = "/getVehicleRepairRecordList", method = RequestMethod.POST)
    public RetDataBean getVehicleRepairRecordList(PageParam pageParam,
                                                  String repairType, String repairUser, String beginTime, String endTime
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = vehicleRepairRecordService.getVehicleRepairRecordList(pageParam, repairUser, repairType, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param createUser
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getVehicleApplyList
     * @Description:  获取申请记录
     */
    @RequestMapping(value = "/getVehicleApplyList", method = RequestMethod.POST)
    public RetDataBean getVehicleApplyList(PageParam pageParam, String status,
                                           String createUser, String beginTime, String endTime
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("a.create_time");
            } else {
                pageParam.setSort("a." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = vehicleApplyService.getVehicleApplyList(pageParam, status, createUser, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param createUser
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getVehicleApprovedList
     * @Description:  获取待审批记录
     */
    @RequestMapping(value = "/getVehicleApprovedList", method = RequestMethod.POST)
    public RetDataBean getVehicleApprovedList(PageParam pageParam,
                                              String createUser, String beginTime, String endTime
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = vehicleApplyService.getVehicleApprovedList(pageParam, createUser, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param oilType
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getVehicleOilCardList
     * @Description:  获取油卡列表
     */
    @RequestMapping(value = "/getVehicleOilCardList", method = RequestMethod.POST)
    public RetDataBean getVehicleOilCardList(PageParam pageParam,String oilType, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("v.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = vehicleOilCardService.getVehicleOilCardList(pageParam, oilType, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }
}
