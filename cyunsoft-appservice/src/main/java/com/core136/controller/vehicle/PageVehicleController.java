package com.core136.controller.vehicle;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/app/core")
public class PageVehicleController {
    /**
     * @param view
     * @return ModelAndView
     * @Title: goDriver
     * @Description:  司机管理
     */
    @RequestMapping("/vehicle/driver")
    @RequiresPermissions("/app/core/vehicle/driver")
    public ModelAndView goDriver(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("/app/core/vehicle/drivermanage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("/app/core/vehicle/driver");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goReturnvehicle
     * @Description:  还车管理
     */
    @RequestMapping("/vehicle/returnvehicle")
    @RequiresPermissions("/app/core/vehicle/returnvehicle")
    public ModelAndView goReturnvehicle(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/vehicle/returnvehicle");
            } else {
                if (view.equals("manage")) {
                    mv = new ModelAndView("app/core/vehicle/returnvehiclemanage");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goRepari
     * @Description:  车辆维修管理
     */
    @RequestMapping("/vehicle/repair")
    @RequiresPermissions("/app/core/vehicle/repair")
    public ModelAndView goRepair(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/vehicle/vehiclerepairmange");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/vehicle/vehiclerepair");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


    /**
     * @param view
     * @return ModelAndView
     * @Title: goApproved
     * @Description:  车辆审批
     */
    @RequestMapping("/vehicle/approved")
    @RequiresPermissions("/app/core/vehicle/approved")
    public ModelAndView goApproved(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/vehicle/approved");
            } else {
                if (view.equals("manage")) {
                    mv = new ModelAndView("app/core/vehicle/approvedmanage");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goOilmange
     * @Description:  油卡管理
     */
    @RequestMapping("/vehicle/oilmange")
    @RequiresPermissions("/app/core/vehicle/oilmange")
    public ModelAndView goOilmange(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/vehicle/oilcardmange");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/vehicle/oilcard");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goApplyvehicle
     * @Description:  车辆使用申请
     */
    @RequestMapping("/vehicle/applyvehicle")
    public ModelAndView goApplyvehicle(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/vehicle/applyvehiclemanage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/vehicle/applyvehicle");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


    /**
     * @param view
     * @return ModelAndView
     * @Title: goVehicleinfo
     * @Description:  车辆信息管理
     */
    @RequestMapping("/vehicle/vehicleinfo")
    public ModelAndView goVehicleinfo(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/vehicle/managevehicleinfo");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/vehicle/addvehicleinfo");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goCareDetails
     * @Description:  车辆详情
     */
    @RequestMapping("/vehicle/vehicleinfodetails")
    @RequiresPermissions("/app/core/vehicle/vehicleinfodetails")
    public ModelAndView goVehicleInfoDetails() {
        try {
            return new ModelAndView("app/core/vehicle/vehicleinfodetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goApplyVehicleDetails
     * @Description:  申请记录详情
     */
    @RequestMapping("/vehicle/applyvehicledetails")
    @RequiresPermissions("/app/core/vehicle/applyvehicledetails")
    public ModelAndView goApplyVehicleDetails() {
        try {
            return new ModelAndView("app/core/vehicle/applyvehicledetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goVehicleRepairdetails
     * @Description:  维修详情
     */
    @RequestMapping("/vehicle/vehicleRepairdetails")
    @RequiresPermissions("/app/core/vehicle/vehicleRepairdetails")
    public ModelAndView goVehicleRepairdetails() {
        try {
            return new ModelAndView("app/core/vehicle/vehicleRepairdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goOilCardDetails
     * @Description: 油卡详情
     */
    @RequestMapping("/vehicle/oilcarddetails")
    @RequiresPermissions("/app/core/vehicle/oilcarddetails")
    public ModelAndView goOilCardDetails() {
        try {
            return new ModelAndView("app/core/vehicle/oilcarddetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goOperator
     * @Description:  调度人员设置
     */
    @RequestMapping("/vehicle/operator")
    @RequiresPermissions("/app/core/vehicle/operator")
    public ModelAndView goOperator() {
        try {
            return new ModelAndView("app/core/vehicle/operator");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }
}
