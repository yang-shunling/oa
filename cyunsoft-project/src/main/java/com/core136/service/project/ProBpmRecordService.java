package com.core136.service.project;

import com.core136.bean.project.ProBpmRecord;
import com.core136.mapper.project.ProBpmRecordMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

@Service
public class ProBpmRecordService {
    private ProBpmRecordMapper proBpmRecordMapper;

    @Autowired
    public void setProBpmRecordMapper(ProBpmRecordMapper proBpmRecordMapper) {
        this.proBpmRecordMapper = proBpmRecordMapper;
    }

    public int insertProBpmRecord(ProBpmRecord proBpmRecord) {
        return proBpmRecordMapper.insert(proBpmRecord);
    }

    public int deleteProBpmRecord(ProBpmRecord proBpmRecord) {
        return proBpmRecordMapper.delete(proBpmRecord);
    }

    public int updateProBpmRecord(Example example, ProBpmRecord proBpmRecord) {
        return proBpmRecordMapper.updateByExampleSelective(proBpmRecord, example);
    }

    public ProBpmRecord selectOneProBpmRecord(ProBpmRecord proBpmRecord) {
        return proBpmRecordMapper.selectOne(proBpmRecord);
    }

}
