package com.core136.service.project;

import com.core136.bean.project.ProProblem;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.project.ProProblemMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ProProblemService {
    private ProProblemMapper proProblemMapper;

    @Autowired
    public void setProProblemMapper(ProProblemMapper proProblemMapper) {
        this.proProblemMapper = proProblemMapper;
    }

    public int insertProProblem(ProProblem proProblem) {
        return proProblemMapper.insert(proProblem);
    }

    public int deleteProProblem(ProProblem proProblem) {
        return proProblemMapper.delete(proProblem);
    }

    public int updateProProblem(Example example, ProProblem proProblem) {
        return proProblemMapper.updateByExampleSelective(proProblem, example);
    }

    public ProProblem selectOneProProblem(ProProblem proProblem) {
        return proProblemMapper.selectOne(proProblem);
    }

    /**
     * 获取项目任务
     *
     * @param orgId
     * @param proId
     * @param taskId
     * @param search
     * @return
     */
    public List<Map<String, String>> getProProblemListByPro(String orgId, String proId, String taskId, String search) {
        return proProblemMapper.getProProblemListByPro(orgId, proId, taskId, "%" + search + "%");
    }

    /**
     * 获取项目任务
     *
     * @param pageParam
     * @param proId
     * @param taskId
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getProProblemListByPro(PageParam pageParam, String proId, String taskId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getProProblemListByPro(pageParam.getOrgId(), proId, taskId, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
