package com.core136.service.project;

import com.core136.bean.account.Account;
import com.core136.bi.option.bean.OptionConfig;
import com.core136.bi.option.property.*;
import com.core136.bi.option.resdata.LegendData;
import com.core136.bi.option.resdata.SeriesData;
import com.core136.bi.option.style.AxisPointer;
import com.core136.bi.option.style.AxisTick;
import com.core136.bi.option.style.Emphasis;
import com.core136.bi.option.style.ItemStyle;
import com.core136.bi.option.units.BarOption;
import com.core136.bi.option.units.PieOption;
import com.core136.mapper.project.EchartsProjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class EchartsProjectService {
    private PieOption pieOption = new PieOption();
    private BarOption barOption = new BarOption();
    private EchartsProjectMapper echartsProjectMapper;

    @Autowired
    public void setEchartsProjectMapper(EchartsProjectMapper echartsProjectMapper) {
        this.echartsProjectMapper = echartsProjectMapper;
    }

    public OptionConfig getProCostTypeBar(Account account, String proId) {
        OptionConfig optionConfig = new OptionConfig();
        List<Map<String, String>> resdataList = echartsProjectMapper.getProCostTypeBar(account.getOrgId(), proId);
        String[] xdata = new String[resdataList.size()];
        Double[] ydata = new Double[resdataList.size()];
        for (int i = 0; i < resdataList.size(); i++) {
            if (StringUtils.isNotBlank(resdataList.get(i).get("name"))) {
                xdata[i] = resdataList.get(i).get("name");
            } else {
                xdata[i] = "other" + i;
            }
            ydata[i] = Double.valueOf(String.valueOf(resdataList.get(i).get("value")));
        }
        OptionSeries optionSeries = new OptionSeries();
        optionSeries.setName("项目预算费用类型明细");
        optionSeries.setType("bar");
        optionSeries.setBarWidth("60%");
        optionSeries.setData(ydata);
        OptionXAxis xAxis = new OptionXAxis();
        xAxis.setType("category");
        xAxis.setData(xdata);
        AxisTick axisTick = new AxisTick();
        axisTick.setAlignWithLabel(true);
        xAxis.setAxisTick(axisTick);
        optionConfig = barOption.getBarTickAlignChartOption(new OptionXAxis[]{xAxis}, new OptionSeries[]{optionSeries});
        OptionTooltip optionTooltip = new OptionTooltip();
        optionTooltip.setTrigger("axis");
        AxisPointer axisPointer = new AxisPointer();
        axisPointer.setType("shadow");
        optionTooltip.setAxisPointer(axisPointer);
        optionConfig.setTooltip(optionTooltip);
        OptionGrid optionGrid = new OptionGrid();
        optionGrid.setLeft("3%");
        optionGrid.setRight("4%");
        optionGrid.setBottom("3%");
        optionGrid.setContainLabel(true);
        optionConfig.setGrid(optionGrid);
        return optionConfig;
    }

    /**
     * 项目费用类型统计
     *
     * @param account
     * @param proId
     * @return
     */
    public OptionConfig getProCostTypePie(Account account, String proId) {
        OptionConfig optionConfig = new OptionConfig();
        List<Map<String, String>> resdataList = echartsProjectMapper.getProCostTypePie(account.getOrgId(), proId);
        OptionSeries[] optionSeriesArr = new OptionSeries[1];
        SeriesData[] dataArr = new SeriesData[resdataList.size()];
        int selectedLeng = 0;
        if (dataArr.length >= 10) {
            selectedLeng = 10;
        } else {
            selectedLeng = dataArr.length;
        }
        String[] selected = new String[selectedLeng];
        LegendData[] legendDatas = new LegendData[dataArr.length];
        for (int i = 0; i < dataArr.length; i++) {
            if (StringUtils.isBlank(resdataList.get(i).get("name"))) {
                resdataList.get(i).put("name", "other" + i);
            }
            if (i < selectedLeng) {
                selected[i] = resdataList.get(i).get("name").toString();
            }
            LegendData legendData = new LegendData();
            legendData.setName(resdataList.get(i).get("name").toString());
            legendDatas[i] = legendData;
            SeriesData seriesData = new SeriesData();
            seriesData.setName(resdataList.get(i).get("name").toString());
            seriesData.setValue(Double.valueOf(String.valueOf(resdataList.get(i).get("value"))));
            dataArr[i] = seriesData;
        }
        OptionSeries optionSeries = new OptionSeries();
        optionSeries.setName("项目费用类型");
        optionSeries.setType("pie");
        optionSeries.setRadius("55%");
        optionSeries.setCenter(new String[]{"40%", "50%"});
        Emphasis emphasis = new Emphasis();
        ItemStyle itemStyle = new ItemStyle();
        itemStyle.setShadowBlur(10);
        itemStyle.setShadowOffsetX(0);
        itemStyle.setShadowColor("rgba(0, 0, 0, 0.5)");
        emphasis.setItemStyle(itemStyle);
        optionSeries.setData(dataArr);
        optionSeriesArr[0] = optionSeries;
        optionConfig.setSeries(optionSeriesArr);
        optionConfig = pieOption.getPieLegendChartOption(legendDatas, selected, optionSeriesArr);
        OptionTitle optionTitle = new OptionTitle();
        optionTitle.setText("项目费用类型统计");
        optionTitle.setSubtext("费用类型统占比");
        optionTitle.setLeft("center");
        optionConfig.setTitle(optionTitle);
        return optionConfig;
    }


}
