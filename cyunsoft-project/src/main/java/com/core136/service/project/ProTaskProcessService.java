package com.core136.service.project;

import com.core136.bean.project.ProTask;
import com.core136.bean.project.ProTaskProcess;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.project.ProTaskProcessMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ProTaskProcessService {
    private ProTaskProcessMapper proTaskProcessMapper;

    @Autowired
    public void setProTaskProcessMapper(ProTaskProcessMapper proTaskProcessMapper) {
        this.proTaskProcessMapper = proTaskProcessMapper;
    }

    private ProTaskService proTaskService;

    @Autowired
    public void setProTaskService(ProTaskService proTaskService) {
        this.proTaskService = proTaskService;
    }


    public int insertProTaskProcess(ProTaskProcess proTaskProcess) {
        return proTaskProcessMapper.insert(proTaskProcess);
    }

    public int deleteProTaskProcess(ProTaskProcess proTaskProcess) {
        return proTaskProcessMapper.delete(proTaskProcess);
    }

    public int updateProTaskProcess(Example example, ProTaskProcess proTaskProcess) {
        return proTaskProcessMapper.updateByExampleSelective(proTaskProcess, example);
    }

    public ProTaskProcess selectOneProTaskProcess(ProTaskProcess proTaskProcess) {
        return proTaskProcessMapper.selectOne(proTaskProcess);
    }

    /**
     * 添加项目任务处理结果的同时更新任务的进度
     *
     * @param proTaskProcess
     * @return
     */
    @Transactional(value = "generalTM")
    public RetDataBean createProcess(ProTaskProcess proTaskProcess) {
        ProTask proTask = new ProTask();
        if (proTaskProcess.getProcess() > 100) {
            proTaskProcess.setProcess(100);
            proTask.setStatus("2");
        }
        proTask.setProgress(proTaskProcess.getProcess());
        Example example = new Example(ProTask.class);
        example.createCriteria().andEqualTo("orgId", proTaskProcess.getOrgId()).andEqualTo("taskId", proTaskProcess.getTaskId());
        proTaskService.updateProTask(example, proTask);
        return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, insertProTaskProcess(proTaskProcess));

    }

    /**
     * 按任务ID获取处理过程列表
     *
     * @param orgId
     * @param taskId
     * @param search
     * @return
     */
    public List<Map<String, String>> getProTaskProcessList(String orgId, String taskId, String search) {
        return proTaskProcessMapper.getProTaskProcessList(orgId, taskId, "%" + search + "%");
    }

    /**
     * 按任务ID获取处理过程列表
     *
     * @param pageParam
     * @param taskId
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getProTaskProcessList(PageParam pageParam, String taskId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getProTaskProcessList(pageParam.getOrgId(), taskId, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
