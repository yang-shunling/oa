package com.core136.bean.project;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: ProRecord
 * @Description: 项目管理
 * @author: 稠云技术
 * @date: 2021年6月5日 下午12:41:00
 * @Copyright: 2021 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "pro_record")
public class ProRecord implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String proId;
    private Integer sortNo;
    private String proCode;
    private String title;
    private String proSort;
    private String approvalUser;
    private String proLevel;
    private String beginTime;
    private String endTime;
    private String manager;
    private Double budget;
    private String joinDept;
    private String taskUsers;
    private String remark;
    private String attach;
    private String isCreateFile;
    private String status;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getProId() {
        return proId;
    }

    public void setProId(String proId) {
        this.proId = proId;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getProCode() {
        return proCode;
    }

    public void setProCode(String proCode) {
        this.proCode = proCode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getProSort() {
        return proSort;
    }

    public void setProSort(String proSort) {
        this.proSort = proSort;
    }

    public String getApprovalUser() {
        return approvalUser;
    }

    public void setApprovalUser(String approvalUser) {
        this.approvalUser = approvalUser;
    }

    public String getProLevel() {
        return proLevel;
    }

    public void setProLevel(String proLevel) {
        this.proLevel = proLevel;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public Double getBudget() {
        return budget;
    }

    public void setBudget(Double budget) {
        this.budget = budget;
    }

    public String getJoinDept() {
        return joinDept;
    }

    public void setJoinDept(String joinDept) {
        this.joinDept = joinDept;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getIsCreateFile() {
        return isCreateFile;
    }

    public void setIsCreateFile(String isCreateFile) {
        this.isCreateFile = isCreateFile;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getTaskUsers() {
        return taskUsers;
    }

    public void setTaskUsers(String taskUsers) {
        this.taskUsers = taskUsers;
    }
}
