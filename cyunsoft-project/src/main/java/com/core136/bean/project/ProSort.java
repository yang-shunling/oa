package com.core136.bean.project;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * 项目分类
 */
@Table(name = "pro_sort")
public class ProSort implements Serializable {
    private static final long serialVersionUID = 1L;
    private String sortId;
    private String sortName;
    private Integer sortNo;
    private String remark;
    private String levelId;
    private String flowIds;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getSortId() {
        return sortId;
    }

    public void setSortId(String sortId) {
        this.sortId = sortId;
    }

    public String getSortName() {
        return sortName;
    }

    public void setSortName(String sortName) {
        this.sortName = sortName;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getLevelId() {
        return levelId;
    }

    public void setLevelId(String levelId) {
        this.levelId = levelId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getFlowIds() {
        return flowIds;
    }

    public void setFlowIds(String flowIds) {
        this.flowIds = flowIds;
    }
}
