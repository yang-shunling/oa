package com.core136.mapper.project;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface EchartsProjectMapper {
    /**
     * 项目费用类型统计
     *
     * @param orgId
     * @param proId
     * @return
     */
    public List<Map<String, String>> getProCostTypePie(@Param(value = "orgId") String orgId, @Param(value = "proId") String proId);


    public List<Map<String, String>> getProCostTypeBar(@Param(value = "orgId") String orgId, @Param(value = "proId") String proId);
}
