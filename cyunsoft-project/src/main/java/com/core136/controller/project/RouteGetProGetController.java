package com.core136.controller.project;

import com.core136.bean.account.Account;
import com.core136.bean.project.*;
import com.core136.bean.sys.CodeClass;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.service.account.AccountService;
import com.core136.service.account.UserInfoService;
import com.core136.service.project.*;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/ret/proget")
public class RouteGetProGetController {
    private UserInfoService userInfoService;

    @Autowired
    private void setUserInfoService(UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private ProPrivService proPrivService;

    @Autowired
    public void setProPrivService(ProPrivService proPrivService) {
        this.proPrivService = proPrivService;
    }

    private ProCostService proCostService;

    @Autowired
    public void setProCostService(ProCostService proCostService) {
        this.proCostService = proCostService;
    }

    private ProExpAccountService proExpAccountService;

    @Autowired
    public void setProExpAccountService(ProExpAccountService proExpAccountService) {
        this.proExpAccountService = proExpAccountService;
    }

    private ProModelService proModelService;

    @Autowired
    public void setProModelService(ProModelService proModelService) {
        this.proModelService = proModelService;
    }

    private ProRecordService proRecordService;

    @Autowired
    public void setProRecordService(ProRecordService proRecordService) {
        this.proRecordService = proRecordService;
    }

    private ProRoleService proRoleService;

    @Autowired
    public void setProRoleService(ProRoleService proRoleService) {
        this.proRoleService = proRoleService;
    }

    private ProSortService proSortService;

    @Autowired
    public void setProSortService(ProSortService proSortService) {
        this.proSortService = proSortService;
    }

    private ProFileService proFileService;

    @Autowired
    public void setProFileService(ProFileService proFileService) {
        this.proFileService = proFileService;
    }

    private ProProblemService proProblemService;

    @Autowired
    public void setProProblemService(ProProblemService proProblemService) {
        this.proProblemService = proProblemService;
    }

    private ProBpmRecordService proBpmRecordService;

    @Autowired
    public void setProBpmRecordService(ProBpmRecordService proBpmRecordService) {
        this.proBpmRecordService = proBpmRecordService;
    }

    private ProTaskService proTaskService;

    @Autowired
    public void setProTaskService(ProTaskService proTaskService) {
        this.proTaskService = proTaskService;
    }

    private ProTaskLinkService proTaskLinkService;

    @Autowired
    public void setProTaskLinkService(ProTaskLinkService proTaskLinkService) {
        this.proTaskLinkService = proTaskLinkService;
    }

    private ProApprovalService proApprovalService;

    @Autowired
    public void setProApprovalService(ProApprovalService proApprovalService) {
        this.proApprovalService = proApprovalService;
    }

    private ProTaskProcessService proTaskProcessService;

    @Autowired
    private void setProTaskProcessService(ProTaskProcessService proTaskProcessService) {
        this.proTaskProcessService = proTaskProcessService;
    }

    private ProProblemReplyService proProblemReplyService;

    @Autowired
    public void setProProblemReplyService(ProProblemReplyService proProblemReplyService) {
        this.proProblemReplyService = proProblemReplyService;
    }

    private ProFundsService proFundsService;

    @Autowired
    public void setProFundsService(ProFundsService proFundsService) {
        this.proFundsService = proFundsService;
    }

    /**
     * 获取费用类型列表
     *
     * @return
     */
    @RequestMapping(value = "/getProCostForSelect", method = RequestMethod.POST)
    public RetDataBean getProCostForSelect() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, proCostService.getProCostForSelect(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 项目预算列表
     *
     * @param pageParam
     * @param proId
     * @return
     */
    @RequestMapping(value = "/getProFundsListByProId", method = RequestMethod.POST)
    public RetDataBean getProFundsListByProId(PageParam pageParam, String proId) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("f.sort_no");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = proFundsService.getProFundsListByProId(pageParam, proId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取项目金费详情
     *
     * @param proFunds
     * @return
     */
    @RequestMapping(value = "/getProFundsById", method = RequestMethod.POST)
    public RetDataBean getProFundsById(ProFunds proFunds) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            proFunds.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, proFundsService.selectOneProFunds(proFunds));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取项目文档详情
     *
     * @param proFile
     * @return
     */
    @RequestMapping(value = "/getProFileById", method = RequestMethod.POST)
    public RetDataBean getProFileById(ProFile proFile) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            proFile.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, proFileService.selectOneProFile(proFile));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param proId
     * @return
     */
    @RequestMapping(value = "/getProFileListByProId", method = RequestMethod.POST)
    public RetDataBean getProFileListByProId(PageParam pageParam, String proId) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("f.sort_no");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = proFileService.getProFileListByProId(pageParam, proId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新任务问题列表
     *
     * @param problemId
     * @return
     */
    @RequestMapping(value = "/getProblemReplyList", method = RequestMethod.POST)
    public RetDataBean getProblemReplyList(String problemId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, proProblemReplyService.getProblemReplyList(account.getOrgId(), problemId));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取问题回复详情
     *
     * @param proProblemReply
     * @return
     */
    @RequestMapping(value = "/getProProblemReplyById", method = RequestMethod.POST)
    public RetDataBean getProProblemReplyById(ProProblemReply proProblemReply) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            proProblemReply.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, proProblemReplyService.selectOneProProblemReply(proProblemReply));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取项目任务
     *
     * @param pageParam
     * @param proId
     * @param taskId
     * @return
     */
    @RequestMapping(value = "/getProProblemListByPro", method = RequestMethod.POST)
    public RetDataBean getProProblemListByPro(PageParam pageParam, String proId, String taskId) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("p.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("DESC");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = proProblemService.getProProblemListByPro(pageParam, proId, taskId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 按项目获取项目列表
     *
     * @param proId
     * @return
     */
    @RequestMapping(value = "/getTaskByProForSelect", method = RequestMethod.POST)
    public RetDataBean getTaskByProForSelect(String proId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, proTaskService.getTaskByProForSelect(account.getOrgId(), proId));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取项目问题详情
     *
     * @param proProblem
     * @return
     */
    @RequestMapping(value = "/getProProblemById", method = RequestMethod.POST)
    public RetDataBean getProProblemById(ProProblem proProblem) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            proProblem.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, proProblemService.selectOneProProblem(proProblem));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 按任务ID获取处理过程列表
     *
     * @param pageParam
     * @param taskId
     * @return
     */
    @RequestMapping(value = "/getProTaskProcessList", method = RequestMethod.POST)
    public RetDataBean getProTaskProcessList(PageParam pageParam, String taskId) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("p.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("DESC");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = proTaskProcessService.getProTaskProcessList(pageParam, taskId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取项目任务处理详情
     *
     * @param proTaskProcess
     * @return
     */
    @RequestMapping(value = "/getProTaskProcessById", method = RequestMethod.POST)
    public RetDataBean getProTaskProcessById(ProTaskProcess proTaskProcess) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            proTaskProcess.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, proTaskProcessService.selectOneProTaskProcess(proTaskProcess));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取项目子任务列表
     *
     * @param pageParam
     * @param proSort
     * @param beginTime
     * @param endTime
     * @return
     */
    @RequestMapping(value = "/getMyTaskWorkList", method = RequestMethod.POST)
    public RetDataBean getMyTaskWorkList(PageParam pageParam, String proSort, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("T.START_DATE");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("DESC");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = proTaskService.getMyTaskWorkList(pageParam, proSort, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取子任甘特图信息
     *
     * @param proId
     * @return
     */
    @RequestMapping(value = "/getProTaskInfo", method = RequestMethod.POST)
    public RetDataBean getProTaskInfo(String proId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, proTaskService.getProTaskInfo(account.getOrgId(), proId));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取节点关联详情
     *
     * @param proTaskLink
     * @return
     */
    @RequestMapping(value = "/getProTaskLinkById", method = RequestMethod.POST)
    public RetDataBean getProTaskLinkById(ProTaskLink proTaskLink) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            proTaskLink.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, proTaskLinkService.selectOneProTaskLink(proTaskLink));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取子任务负责人列表
     *
     * @param proId
     * @return
     */
    @RequestMapping(value = "/getTaskUsersList", method = RequestMethod.POST)
    public RetDataBean getTaskUsersList(String proId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, proRecordService.getTaskUsersList(account.getOrgId(), proId));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取项目子任务列表
     *
     * @param proId
     * @return
     */
    @RequestMapping(value = "/getProTaskList", method = RequestMethod.POST)
    public RetDataBean getProTaskList(String proId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, proTaskService.getProTaskList(account.getOrgId(), proId));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取任务子任务详情
     *
     * @param proTask
     * @return
     */
    @RequestMapping(value = "/getProTaskById", method = RequestMethod.POST)
    public RetDataBean getProTaskById(ProTask proTask) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            proTask.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, proTaskService.selectOneProTask(proTask));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取项项目审批人员
     *
     * @return
     */
    @RequestMapping(value = "/getProApprovalUserList", method = RequestMethod.POST)
    public RetDataBean getProApprovalUserList() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            ProPriv proPriv = new ProPriv();
            proPriv.setOrgId(account.getOrgId());
            proPriv = proPrivService.selectOneProPriv(proPriv);
            if (proPriv == null) {
                return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, userInfoService.getAllUserInfoByAccountList(account.getOrgId(), ""));
            } else {
                return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, userInfoService.getAllUserInfoByAccountList(account.getOrgId(), proPriv.getApprovalUserPriv()));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 获取项目流程列表
     *
     * @param proBpmRecord
     * @return
     */
    @RequestMapping(value = "/getProBpmRecordById", method = RequestMethod.POST)
    public RetDataBean getProBpmRecordById(ProBpmRecord proBpmRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            proBpmRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, proBpmRecordService.selectOneProBpmRecord(proBpmRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取项目详情
     *
     * @param proRecord
     * @return
     */
    @RequestMapping(value = "/getProRecordById", method = RequestMethod.POST)
    public RetDataBean getProRecordById(ProRecord proRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            proRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, proRecordService.selectOneProRecord(proRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 按状态获取项目列表
     *
     * @param pageParam
     * @param status
     * @return
     */
    @RequestMapping(value = "/getProRecordListByStatus", method = RequestMethod.POST)
    public RetDataBean getProRecordListByStatus(PageParam pageParam, String status, String proLevel, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("r.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("DESC");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = proRecordService.getProRecordListByStatus(pageParam, proLevel, beginTime, endTime, status);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 项目记录查询
     *
     * @param pageParam
     * @param proSort
     * @param status
     * @param proLevel
     * @param beginTime
     * @param endTime
     * @return
     */
    @RequestMapping(value = "/getProRecordListByQuery", method = RequestMethod.POST)
    public RetDataBean getProRecordListByQuery(PageParam pageParam, String proSort, String status, String proLevel, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("r.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("DESC");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = proRecordService.getProRecordListByQuery(pageParam, proSort, proLevel, beginTime, endTime, status);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取待审批项目列表
     *
     * @param pageParam
     * @param proLevel
     * @param beginTime
     * @param endTime
     * @return
     */
    @RequestMapping(value = "/getProRecordListForApproval", method = RequestMethod.POST)
    public RetDataBean getProRecordListForApproval(PageParam pageParam, String proLevel, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("r.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("DESC");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            ProPriv proPriv = new ProPriv();
            proPriv.setOrgId(account.getOrgId());
            proPriv = proPrivService.selectOneProPriv(proPriv);
            String approvalUserPriv = "";
            if (proPriv != null) {
                approvalUserPriv = proPriv.getApprovalUserPriv();
            } else {
                return RetDataTools.Ok(MessageCode.MESSAGE_FAILED,new PageInfo<Map<String, String>>());
            }
            if (StringUtils.isNotBlank(approvalUserPriv)) {
                if (Arrays.asList(approvalUserPriv.split(",")).contains(account.getAccountId())) {
                    pageParam.setOrgId(account.getOrgId());
                    pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
                    PageInfo<Map<String, String>> pageInfo = proRecordService.getProRecordListForApproval(pageParam, proLevel, beginTime, endTime);
                    return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
                } else {
                    return RetDataTools.NotOk(MessageCode.MSG_00015);
                }
            } else {
                return RetDataTools.NotOk(MessageCode.MSG_00015);
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取项目历史审批记录
     *
     * @param pageParam
     * @param status
     * @param beginTime
     * @param endTime
     * @return
     */
    @RequestMapping(value = "/getOldProApprovalList", method = RequestMethod.POST)
    public RetDataBean getOldProApprovalList(PageParam pageParam, String status, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("a.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("DESC");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = proApprovalService.getOldProApprovalList(pageParam, status, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 获取当前所有状态的项目数
     *
     * @param proCost
     * @return
     */
    @RequestMapping(value = "/getProRecordStatusCountList", method = RequestMethod.POST)
    public RetDataBean getProRecordStatusCountList(ProCost proCost) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, proRecordService.getProRecordStatusCountList(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取项目费用详情
     *
     * @param proCost
     * @return
     */
    @RequestMapping(value = "/getProCostById", method = RequestMethod.POST)
    public RetDataBean getProCostById(ProCost proCost) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            proCost.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, proCostService.selectOneProCost(proCost));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取项目费用列表
     *
     * @param pageParam
     * @return
     */
    @RequestMapping(value = "/getProCostList", method = RequestMethod.POST)
    public RetDataBean getProCostList(PageParam pageParam) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("sort_no");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(CodeClass.class);
            example.setOrderByClause(orderBy);
            Example.Criteria criteria = example.createCriteria();
            criteria.andEqualTo("orgId", account.getOrgId());
            if (StringUtils.isNotEmpty(pageParam.getSearch())) {
                Example.Criteria criteria2 = example.createCriteria();
                criteria2.orLike("costName", "%" + pageParam.getSearch() + "%");
                example.and(criteria2);
            }
            PageInfo<ProCost> pageInfo = proCostService.getProCostList(example, pageParam.getPageNumber(), pageParam.getPageSize());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取项目角色列表
     *
     * @param pageParam
     * @return
     */
    @RequestMapping(value = "/getProRoleList", method = RequestMethod.POST)
    public RetDataBean getProRoleList(PageParam pageParam) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("sort_no");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(CodeClass.class);
            example.setOrderByClause(orderBy);
            Example.Criteria criteria = example.createCriteria();
            criteria.andEqualTo("orgId", account.getOrgId());
            if (StringUtils.isNotEmpty(pageParam.getSearch())) {
                Example.Criteria criteria2 = example.createCriteria();
                criteria2.orLike("roleName", "%" + pageParam.getSearch() + "%");
                example.and(criteria2);
            }
            PageInfo<ProRole> pageInfo = proRoleService.getProRoleList(example, pageParam.getPageNumber(), pageParam.getPageSize());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 项目人员角色详情
     *
     * @param proRole
     * @return
     */
    @RequestMapping(value = "/getProRoleById", method = RequestMethod.POST)
    public RetDataBean getProRoleById(ProRole proRole) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            proRole.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, proRoleService.selectOneProRole(proRole));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取项目费用科目树结构
     *
     * @param sortId
     * @return
     */
    @RequestMapping(value = "/getProExpAccountTree", method = RequestMethod.POST)
    public List<Map<String, String>> getProExpAccountTree(String sortId) {
        try {
            String levelId = "0";
            if (StringUtils.isNotBlank(sortId)) {
                levelId = sortId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return proExpAccountService.getProExpAccountTree(account.getOrgId(), levelId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 获取项目科目详情
     *
     * @param proExpAccount
     * @return
     */
    @RequestMapping(value = "/getProExpAccountById", method = RequestMethod.POST)
    public RetDataBean getProExpAccountById(ProExpAccount proExpAccount) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            proExpAccount.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, proExpAccountService.selectOneProExpAccount(proExpAccount));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取项目分类树结构
     *
     * @param sortId
     * @return
     */
    @RequestMapping(value = "/getProSortTree", method = RequestMethod.POST)
    public List<Map<String, String>> getProSortTree(String sortId) {
        try {
            String levelId = "0";
            if (StringUtils.isNotBlank(sortId)) {
                levelId = sortId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return proSortService.getProSortTree(account.getOrgId(), levelId);
        } catch (Exception e) {
            return null;
        }
    }

    @RequestMapping(value = "/getProReocrdTree", method = RequestMethod.POST)
    public List<Map<String, String>> getProReocrdTree(String sortId) {
        try {
            String levelId = "0";
            if (StringUtils.isNotBlank(sortId)) {
                levelId = sortId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return proRecordService.getProReocrdTree(account.getOrgId(), levelId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 获取项目分类树结构
     *
     * @param sortId
     * @return
     */
    @RequestMapping(value = "/getProSortForProTree", method = RequestMethod.POST)
    public List<Map<String, String>> getProSortForProTree(String sortId) {
        try {
            String levelId = "0";
            if (StringUtils.isNotBlank(sortId)) {
                levelId = sortId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return proSortService.getProSortForProTree(account.getOrgId(), levelId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 获取项目人员权限详情
     *
     * @param ProPriv
     * @return
     */
    @RequestMapping(value = "/getProPrivById", method = RequestMethod.POST)
    public RetDataBean getProPrivById(ProPriv ProPriv) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            ProPriv.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, proPrivService.selectOneProPriv(ProPriv));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取项目分类详情
     *
     * @param proSort
     * @return
     */
    @RequestMapping(value = "/getProSortById", method = RequestMethod.POST)
    public RetDataBean getProSortById(ProSort proSort) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            proSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, proSortService.selectOneProSort(proSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


}
