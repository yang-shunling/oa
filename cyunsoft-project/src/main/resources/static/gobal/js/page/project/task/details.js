$(function () {
    $.ajax({
        url: "/ret/proget/getProRecordById",
        type: "post",
        dataType: "json",
        data: {proId: proId},
        success: function (data) {
            if (data.status == "200") {
                let info = data.list;
                for (let id in info) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", info.attach);
                        createAttach("attach", 1);
                    } else if (id == "manager" || id == "approvalUser") {
                        $("#" + id).html(getUserNameByStr(info[id]));
                    } else if (id == "joinDept") {
                        $("#" + id).html(getDeptNameByDeptIds(info[id]));
                    } else if (id == "taskUsers") {
                        $("#" + id).html(getUserNameByStr(info[id]));
                    } else if (id == "isCreateFile") {
                        if (info[id] == "1") {
                            $("#" + id).html("创建项目文档目录");
                        } else {
                            $("#" + id).html("不创建项目文档目录");
                        }
                    } else if (id == "proSort") {
                        $.ajax({
                            url: "/ret/proget/getProSortById",
                            type: "post",
                            dataType: "json",
                            data: {sortId: info[id]},
                            success: function (data) {
                                if (data.status = "200") {
                                    $("#" + id).html(data.list.sortName);
                                } else if (data.status == "100") {
                                    layer.msg(sysmsg[data.msg]);
                                } else {
                                    console.log(data.msg);
                                }
                            }
                        });
                    } else {
                        $("#" + id).html(info[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }

        }
    });
    $.ajax({
        url: "/ret/proget/getProTaskById",
        type: "post",
        dataType: "json",
        data: {taskId: taskId},
        success: function (data) {
            if (data.status == "200") {
                let info = data.list;
                for (let id in info) {
                    if (id == "accountId") {
                        $("#taskUser").html(getUserNameByStr(info[id]));
                    } else if (id == "status") {
                        if (info.status == "0") {
                            $("#statusTask").html("进行中");
                        } else if (info.status == "1") {
                            $("#statusTask").html("已挂起");
                        } else if (info.status == "2") {
                            $("#statusTask").html("完成");
                        }
                    } else {
                        $("#" + id + "Task").html(info[id]);
                    }
                    $("#endTimeTask").html(addDay(info.duration, info.startDate));
                }
            }
        }
    });
    query();
})


function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/proget/getProTaskProcessList?taskId=' + taskId,
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        search: true,//启用搜索
        sortOrder: "asc",
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'taskId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',//标题  可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'title',
            width: '150px',
            title: '处理事件标题'
        }, {
            field: 'finishTime',
            width: '50px',
            title: '完成时间'
        }, {
            field: 'process',
            width: '50px',
            title: '完成进度',
            formatter: function (value, row, index) {
                return value + "%";
            }
        }, {
            field: 'attach',
            width: '150px',
            title: '相关附件',
            formatter: function (value, row, index) {
                return createTableAttach(value);
            }
        }, {
            field: 'resContent',
            width: '250px',
            title: '处理结果',
        }],
        onClickCell: function (field, value, row, $element) {
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order
    };
    return temp;
};
