var setting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/proget/getProReocrdTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    callback: {
        onClick: zTreeOnClick
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    }
};
$(function () {
    $.ajax({
        url: "/ret/proget/getProSortForProTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            zTree = $.fn.zTree.init($("#tree"), setting, data);// 初始化树节点时，添加同步获取的数据
        }
    });
    query("");
    $(".js-create-file").unbind("click").click(function () {
        layer.msg("请先选择对左则项目,后再创建预算！")
    })
    getProCostForSelect();
});

function zTreeOnClick(event, treeId, treeNode) {
    if (treeNode.isParent == false) {
        $(".js-create-file").unbind("click").click(function () {
            document.getElementById("form1").reset();
            $("#show_attach").empty();
            $("#attach").attr("data_value", "");
            $("#fundsModel").modal("show");
            $(".js-funds").unbind("click").click(function () {
                if ($("#title").val() == "") {
                    layer.msg("预算标题不能为空！");
                    return;
                }
                $.ajax({
                    url: "/set/proset/insertProFunds",
                    type: "post",
                    dataType: "json",
                    data: {
                        proId: treeNode.sortId,
                        sortNo: $("#sortNo").val(),
                        costId: $("#costId").val(),
                        title: $("#title").val(),
                        funds: $("#funds").val(),
                        remark: $("#remark").val(),
                        attach: $("#attach").attr("data_value")
                    },
                    success: function (data) {
                        if (data.status == "200") {
                            $("#fundsModel").modal("hide");
                            $("#myTable").bootstrapTable("refresh");
                        } else if (data.status == "100") {
                            layer.msg(sysmsg[data.msg]);
                        } else {
                            console.log(data.msg);
                        }
                    }
                });
            })
        })
        $("#myTable").bootstrapTable('destroy');
        query(treeNode.sortId);
    } else {
    }
}

function query(proId) {
    $("#myTable").bootstrapTable({
        url: '/ret/proget/getProFundsListByProId?proId=' + proId,
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        search: true,//启用搜索
        sortOrder: "asc",
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'recordId',//key值栏位
        clickToSelect: false,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            field: 'num',
            title: '序号',//标题  可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'title',
            width: '100px',
            title: '预算标题'
        }, {
            field: 'costName',
            width: '100px',
            title: '费用类型'
        }, {
            field: 'funds',
            width: '100px',
            title: '预算金额'
        }, {
            field: 'remark',
            width: '300px',
            title: '文档摘要'
        }, {
            field: 'attach',
            width: '100px',
            title: '相关附件',
            formatter: function (value, row, index) {
                return createTableAttach(value);
            }
        }, {
            field: 'createTime',
            width: '100px',
            title: '创建时间'
        }, {
            field: 'createUser',
            width: '100px',
            title: '创建人',
            formatter: function (value, row, index) {
                return getUserNameByStr(value);
            }
        }, {
            field: 'opt',
            title: '操作',
            align: 'center',
            width: '100px',
            formatter: function (value, row, index) {
                return createOptBtn(row.recordId);
            }
        }],
        onClickCell: function (field, value, row, $element) {
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list
                    //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order
    };
    return temp;
};

function createOptBtn(recordId) {
    let html = "<a href=\"javascript:void(0);editProFunds('" + recordId + "')\" class=\"btn btn-sky btn-xs\" >编辑</a>&nbsp;&nbsp;"
        + "<a href=\"javascript:void(0);deleteProFunds('" + recordId + "')\" class=\"btn btn-darkorange btn-xs\" >删除</a>"
    return html;
}

function editProFunds(recordId) {
    document.getElementById("form1").reset();
    $("#show_attach").empty();
    $("#attach").attr("data_value", "");
    $("#fundsModel").modal("show");
    $.ajax({
        url: "/ret/proget/getProFundsById",
        type: "post",
        dataType: "json",
        data: {recordId: recordId},
        success: function (data) {
            if (data.status == "200") {
                let info = data.list;
                for (let id in info) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", info.attach);
                        createAttach("attach", 4);
                    } else {
                        $("#" + id).val(info[id]);
                    }
                }
                $(".js-funds").unbind("click").click(function () {
                    if ($("#title").val() == "") {
                        layer.msg("预算标题不能为空！");
                        return;
                    }
                    $.ajax({
                        url: "/set/proset/updateProFunds",
                        type: "post",
                        dataType: "json",
                        data: {
                            recordId: recordId,
                            sortNo: $("#sortNo").val(),
                            costId: $("#costId").val(),
                            title: $("#title").val(),
                            funds: $("#funds").val(),
                            remark: $("#remark").val(),
                            attach: $("#attach").attr("data_value")
                        },
                        success: function (data) {
                            if (data.status == "200") {
                                $("#fundsModel").modal("hide");
                                $("#myTable").bootstrapTable("refresh");
                            } else if (data.status == "100") {
                                layer.msg(sysmsg[data.msg]);
                            } else {
                                console.log(data.msg);
                            }
                        }
                    });
                })
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function deleteProFunds(recordId) {
    if (confirm("确定删除当前预算记录吗？")) {
        $.ajax({
            url: "/set/proset/deleteProFunds",
            type: "post",
            dataType: "json",
            data: {
                recordId: recordId,
            },
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable("refresh");
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    }
}

function getProCostForSelect() {
    $.ajax({
        url: "/ret/proget/getProCostForSelect",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                let info = data.list;
                for (let i = 0; i < info.length; i++) {
                    $("#costId").append("<option value='" + info[i].costId + "'>" + info[i].costName + "</option>");
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}
