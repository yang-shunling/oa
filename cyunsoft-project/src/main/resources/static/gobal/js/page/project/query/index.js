$(function () {
    jeDate("#beginTimeQuery", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTimeQuery", {
        format: "YYYY-MM-DD"
    });
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })
    query();
    $.ajax({
        url: "/ret/proget/getProSortTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            var topNode = [{
                sortName: "全部",
                isParent: "false",
                sortId: ""
            }];
            var newTreeNodes = topNode.concat(data);
            $.fn.zTree.init($("#menuTree"), setting1, newTreeNodes);
        }
    });
    $("#proSortQuery").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#menuContent").css({
            "width": $(this).outerWidth() + "px",
            "left": ($(this).offset().left - 10) + "px"
        }).slideDown(200);
    });
    $("body").unbind("click").click(function () {
        $("#menuContent").hide();
    });
    $("#menuContent").unbind("click").click(function (e) {
        e.stopPropagation();
    });


})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/proget/getProRecordListByQuery',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        search: true,//启用搜索
        sortOrder: "asc",
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'proId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',//标题  可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'proCode',
            width: '50px',
            title: '项目编号'
        }, {
            field: 'title',
            width: '150px',
            title: '项目名称',
            formatter:function (value,row,index)
            {
                return "<a href=\"javascript:void(0);details('" + row.proId + "')\" style='cursor: pointer'>"+value+"</a>";
            }
        }, {
            field: 'sortName',
            width: '50px',
            title: '项目分类'
        }, {
            field: 'proLevel',
            width: '50px',
            title: '项目等级',
            formatter: function (value, row, index) {
                if (value == "0") {
                    return "一般";
                } else if (value == "1") {
                    return "普通";
                } else if (value == "2") {
                    return "重点";
                }
            }
        }, {
            field: 'beginTime',
            width: '50px',
            title: '开始时间'
        }, {
            field: 'endTime',
            width: '50px',
            title: '结束时间'
        }, {
            field: 'managerUserName',
            width: '50px',
            title: '项目经理'
        }, {
            field: 'status',
            width: '50px',
            title: '当前状态',
            formatter: function (value, row, index) {
                if (value == "0") {
                    return "立项中";
                } else if (value == "1") {
                    return "已驳回";
                } else if (value == "2") {
                    return "审批中";
                } else if (value == "3") {
                    return "处理中";
                } else if (value == "4") {
                    return "已挂起";
                } else if (value == "5") {
                    return "已完成";
                }
            }
        }],
        onClickCell: function (field, value, row, $element) {
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list
                    //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        proSort: $("#proSortQuery").attr("data-value"),
        status: $("#statusQuery").val(),
        proLevel: $("#proLevelQuery").val(),
        endTime: $("#endTimeQuery").val(),
        beginTime: $("#beginTimeQuery").val()
    };
    return temp;
};

function details(proId) {
    window.open("/app/core/project/projectdetails?proId=" + proId);
}

var setting1 = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/proget/getProSortTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("menuTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.id;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].sortName + ",";
                vid += nodes[i].sortId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#proSortQuery");
            idem.val(v);
            idem.attr("data-value", vid);
        }
    }
};
