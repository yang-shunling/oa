package com.core136.mapper.crm;

import com.core136.bean.crm.CrmPriv;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CrmPrivMapper extends MyMapper<CrmPriv> {

}
