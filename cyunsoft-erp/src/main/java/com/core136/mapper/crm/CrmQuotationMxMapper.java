package com.core136.mapper.crm;

import com.core136.bean.crm.CrmQuotationMx;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface CrmQuotationMxMapper extends MyMapper<CrmQuotationMx> {

    /**
     * @param orgId
     * @param inquiryId
     * @param quotationId
     * @return List<Map < String, String>>
     * @Title: getCrmInquiryDetailListForQuotation
     * @Description:  获取报价单明细
     */
    public List<Map<String, String>> getCrmInquiryDetailListForQuotation(@Param(value = "orgId") String orgId, @Param(value = "inquiryId") String inquiryId, @Param("quotationId") String quotationId);
}
