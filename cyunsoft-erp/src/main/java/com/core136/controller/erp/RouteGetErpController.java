package com.core136.controller.erp;

import com.core136.bean.account.Account;
import com.core136.bean.erp.*;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.service.account.AccountService;
import com.core136.service.erp.*;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

import java.util.List;
import java.util.Map;

/**
 * @ClassName: RoutGetErpController
 * @Description ERP数据获取接口
 * @author: 稠云信息
 * @date: 2018年12月7日 下午1:36:14
 * @Copyright: 2018 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@RestController
@RequestMapping("/ret/erpget")
public class RouteGetErpController {

    private ErpMaterielSortService erpMaterielSortService;

    @Autowired
    public void setErpMaterielSortService(ErpMaterielSortService erpMaterielSortService) {
        this.erpMaterielSortService = erpMaterielSortService;
    }

    private ErpProductSortService erpProductSortService;

    @Autowired
    public void setErpProductSortService(ErpProductSortService erpProductSortService) {
        this.erpProductSortService = erpProductSortService;
    }

    private ErpUnitService erpUnitService;

    @Autowired
    public void setErpUnitService(ErpUnitService erpUnitService) {
        this.erpUnitService = erpUnitService;
    }

    private ErpMaterielService erpMaterielService;

    @Autowired
    public void setErpMaterielService(ErpMaterielService erpMaterielService) {
        this.erpMaterielService = erpMaterielService;
    }

    private ErpBomSortService erpBomSortService;

    @Autowired
    public void setErpBomSortService(ErpBomSortService erpBomSortService) {
        this.erpBomSortService = erpBomSortService;
    }

    private ErpBomDetailService erpBomDetailService;

    @Autowired
    public void setErpBomDetailService(ErpBomDetailService erpBomDetailService) {
        this.erpBomDetailService = erpBomDetailService;
    }

    private ErpBomService erpBomService;

    @Autowired
    public void setErpBomService(ErpBomService erpBomService) {
        this.erpBomService = erpBomService;
    }

    private ErpProductService erpProductService;

    @Autowired
    public void setErpProductService(ErpProductService erpProductService) {
        this.erpProductService = erpProductService;
    }

    private ErpOrderService erpOrderService;

    @Autowired
    public void setErpOrderService(ErpOrderService erpOrderService) {
        this.erpOrderService = erpOrderService;
    }

    private ErpOrderDetailService erpOrderDetailService;

    @Autowired
    public void setErpOrderDetailService(ErpOrderDetailService erpOrderDetailService) {
        this.erpOrderDetailService = erpOrderDetailService;
    }

    private DoCostService doCostService;

    @Autowired
    public void setDoCostService(DoCostService doCostService) {
        this.doCostService = doCostService;
    }

    private ErpEquipmentService erpEquipmentService;

    @Autowired
    public void setErpEquipmentService(ErpEquipmentService erpEquipmentService) {
        this.erpEquipmentService = erpEquipmentService;
    }

    private ErpEquipmentSortService erpEquipmentSortService;

    @Autowired
    public void setErpEquipmentSortService(ErpEquipmentSortService erpEquipmentSortService) {
        this.erpEquipmentSortService = erpEquipmentSortService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * @param erpMaterielSort
     * @return RetDataBean
     * @Title getErpMaterielSortById
     * @Description  按条件查询一个分类信息
     */
    @RequestMapping(value = "/getErpMaterielSortById", method = RequestMethod.POST)
    public RetDataBean getErpMaterielSortById(ErpMaterielSort erpMaterielSort) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            erpMaterielSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, erpMaterielSortService.selectOne(erpMaterielSort));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param sortId
     * @return List<Map < String, Object>>
     * @Title getErpMaterielSortTree
     * @Description  获取物料的分类树
     */
    @RequestMapping(value = "/getErpMaterielSortTree", method = RequestMethod.POST)
    public List<Map<String, Object>> getErpMaterielSortTree(String sortId) {
        try {
            String sortLevel = "0";
            if (StringUtils.isNotBlank(sortId)) {
                sortLevel = sortId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return erpMaterielSortService.getErpMaterielSortTree(sortLevel, account.getOrgId());
        } catch (Exception e) {

            return null;
        }
    }

    /**
     * @param sortId
     * @return List<Map < String, Object>>
     * @Title getErpProductSortTree
     * @Description  获取产品分类的树
     */
    @RequestMapping(value = "/getErpProductSortTree", method = RequestMethod.POST)
    public List<Map<String, Object>> getErpProductSortTree(String sortId) {
        try {
            String sortLevel = "0";
            if (StringUtils.isNotBlank(sortId)) {
                sortLevel = sortId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return erpProductSortService.getErpProductSortTree(sortLevel, account.getOrgId());
        } catch (Exception e) {

            return null;
        }
    }

    /**
     * @param erpProductSort
     * @return RetDataBean
     * @Title getErpProductSortById
     * @Description  按条件查询一条产品分类
     */
    @RequestMapping(value = "/getErpProductSortById", method = RequestMethod.POST)
    public RetDataBean getErpProductSortById(ErpProductSort erpProductSort) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            erpProductSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, erpProductSortService.selectOne(erpProductSort));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @return RetDataBean
     * @Title getAllErpUnit
     * @Description  获取所有计量单位
     */
    @RequestMapping(value = "/getAllErpUnit", method = RequestMethod.POST)
    public RetDataBean getAllErpUnit() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, erpUnitService.getAllUnit(account.getOrgId()));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param sortLevel
     * @param pageNumber
     * @param pageSize
     * @param search
     * @param sort
     * @param sortOrder
     * @return RetDataBean
     * @Title getErpMaterielBySort
     * @Description  获取物料清单
     */
    @RequestMapping(value = "/getErpMaterielBySort", method = RequestMethod.POST)
    public RetDataBean getErpMaterielBySort(
            String sortLevel,
            Integer pageNumber,
            Integer pageSize,
            String search,
            String sort,
            String sortOrder
    ) {
        try {
            if (StringUtils.isBlank(sort)) {
                sort = "materiel_code";
            } else {
                sort = StrTools.lowerCharToUnderLine(sort);
            }
            if (StringUtils.isBlank(sortOrder)) {
                sortOrder = "asc";
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(ErpMateriel.class);
            example.setOrderByClause(sort + " " + sortOrder);
            Criteria criteria = example.createCriteria();
            Criteria criteria2 = example.createCriteria();
            criteria.andEqualTo("orgId", account.getOrgId());

            if (!StringUtils.isBlank(search)) {
                criteria2.orLike("materielCode", "%" + search + "%").orLike("materielName", "%" + search + "%");
                example.and(criteria2);
            } else {
                criteria.andEqualTo("sortLevel", sortLevel);
            }
            PageInfo<ErpMateriel> pageInfo = erpMaterielService.getErpMaterielBySort(example, pageNumber, pageSize);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param sortId
     * @return List<Map < String, Object>>
     * @Title getErpBomSortTree
     * @Description  获取BOM 分类树结构
     */
    @RequestMapping(value = "/getErpBomSortTree", method = RequestMethod.POST)
    public List<Map<String, Object>> getErpBomSortTree(String sortId) {
        try {
            String sortLevel = "0";
            if (StringUtils.isNotBlank(sortId)) {
                sortLevel = sortId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return erpBomSortService.getErpBomSortTree(sortLevel, account.getOrgId());
        } catch (Exception e) {

            return null;
        }
    }

    /**
     * @param erpBomSort
     * @return RetDataBean
     * @Title getErpBomSortById
     * @Description   按分类ID获取分类信息
     */
    @RequestMapping(value = "/getErpBomSortById", method = RequestMethod.POST)
    public RetDataBean getErpBomSortById(ErpBomSort erpBomSort) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            erpBomSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, erpBomSortService.selectOne(erpBomSort));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param sortId
     * @param pageNumber
     * @param pageSize
     * @param search
     * @param sort
     * @param sortOrder
     * @return RetDataBean
     * @Title getErpBomBySort
     * @Description  按分类获取BOM列表
     */
    @RequestMapping(value = "/getErpBomBySort", method = RequestMethod.POST)
    public RetDataBean getErpBomBySort(
            String sortId,
            Integer pageNumber,
            Integer pageSize,
            String search,
            String sort,
            String sortOrder
    ) {
        try {
            if (StringUtils.isBlank(sort)) {
                sort = "sort_no";
            } else {
                sort = StrTools.lowerCharToUnderLine(sort);
            }
            if (StringUtils.isBlank(sortOrder)) {
                sortOrder = "asc";
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(ErpBom.class);
            example.setOrderByClause(sort + " " + sortOrder);
            Criteria criteria = example.createCriteria();
            Criteria criteria2 = example.createCriteria();
            criteria.andEqualTo("orgId", account.getOrgId());
            if (!StringUtils.isBlank(search)) {
                criteria2.orLike("bomName", "%" + search + "%").orLike("version", "%" + search + "%");
                example.and(criteria2);
            } else {
                criteria.andEqualTo("sortId", sortId);
            }
            PageInfo<ErpBom> pageInfo = erpBomService.getErpBomBySort(example, pageNumber, pageSize);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param erpBom
     * @return RetDataBean
     * @Title getErpBomById
     * @Description  查询一条BOM
     */
    @RequestMapping(value = "/getErpBomById", method = RequestMethod.POST)
    public RetDataBean getErpBomById(ErpBom erpBom) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            erpBom.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, erpBomService.selectOne(erpBom));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param erpMateriel
     * @return RetDataBean
     * @Title getErpMaterielById
     * @Description (这里用一句话描述这个方法的作用)
     */
    @RequestMapping(value = "/getErpMaterielById", method = RequestMethod.POST)
    public RetDataBean getErpMaterielById(ErpMateriel erpMateriel) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            erpMateriel.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, erpMaterielService.selectOne(erpMateriel));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param materielCode
     * @return RetDataBean
     * @Title selectMateriel2ById
     * @Description  按物料ID的模糊查询，用于SELECT2插件的选择
     */
    @RequestMapping(value = "/selectMateriel2ById", method = RequestMethod.POST)
    public RetDataBean selectMateriel2ById(String materielCode) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, erpMaterielService.selectMateriel2ById(materielCode + "%", account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageNumber
     * @param pageSize
     * @param search
     * @param sort
     * @param sortOrder
     * @return RetDataBean
     * @Title getErpBomDetailById
     * @Description  按BOMID获取BOM清单详情
     */
    @RequestMapping(value = "/getErpBomDetailById", method = RequestMethod.POST)
    public RetDataBean getErpBomDetailById(
            String bomId,
            Integer pageNumber,
            Integer pageSize,
            String search,
            String sort,
            String sortOrder
    ) {
        try {
            if (StringUtils.isBlank(sort)) {
                sort = "bom_id";
            } else {
                sort = StrTools.lowerCharToUnderLine(sort);
            }
            if (StringUtils.isBlank(sortOrder)) {
                sortOrder = "asc";
            }
            String orderBy = sort + " " + sortOrder;
            Account account = accountService.getRedisAUserInfoToAccount();
            PageInfo<Map<String, Object>> pageInfo = erpBomDetailService.getBomDetailList(pageNumber, pageSize, orderBy, bomId, "%" + search + "%", account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param erpBomDetail
     * @return RetDataBean
     * @Title getErpBomDetailByDetailId
     * @Description  获取一条BOMDETAIL物料
     */
    @RequestMapping(value = "/getErpBomDetailByDetailId", method = RequestMethod.POST)
    public RetDataBean getErpBomDetailByDetailId(ErpBomDetail erpBomDetail) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, erpBomDetailService.getBomDetailByDetailId(erpBomDetail.getBomId(), erpBomDetail.getBomDetailId(), account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    @RequestMapping(value = "/getErpProductBySort", method = RequestMethod.POST)
    public RetDataBean getErpProductBySort(
            String sortLevel,
            Integer pageNumber,
            Integer pageSize,
            String search,
            String sort,
            String sortOrder
    ) {
        try {
            if (StringUtils.isBlank(sort)) {
                sort = "sort_no";
            } else {
                sort = StrTools.lowerCharToUnderLine(sort);
            }
            if (StringUtils.isBlank(sortOrder)) {
                sortOrder = "asc";
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(ErpProduct.class);
            example.setOrderByClause(sort + " " + sortOrder);
            Criteria criteria = example.createCriteria();
            criteria.andEqualTo("orgId", account.getOrgId());
            if (StringUtils.isNotEmpty(search)) {
                Criteria criteria2 = example.createCriteria();
                criteria2.orLike("productName", "%" + search + "%").orLike("model", "%" + search + "%");
                example.and(criteria2);
            } else {
                criteria.andEqualTo("sortLevel", sortLevel);
            }
            PageInfo<ErpProduct> pageInfo = erpProductService.getErpProductBySort(example, pageNumber, pageSize);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param productName
     * @return RetDataBean
     * @Title selectProductByName
     * @Description  按名称模糊查询产品
     */
    @RequestMapping(value = "/selectProductByName", method = RequestMethod.POST)
    public RetDataBean selectProductByName(String productName) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, erpProductService.selectProductByName("%" + productName + "%", account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param erpProduct
     * @return RetDataBean
     * @Title getProductById
     * @Description  获取产品信息
     */
    @RequestMapping(value = "/getProductById", method = RequestMethod.POST)
    public RetDataBean getProductById(ErpProduct erpProduct) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            erpProduct.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, erpProductService.selectOne(erpProduct));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param sortId
     * @return RetDataBean
     * @Title getErpBomTreeBySortId
     * @Description  按分类获取BOM树结构
     */
    @RequestMapping(value = "/getErpBomTreeBySortId", method = RequestMethod.POST)
    public RetDataBean getErpBomTreeBySortId(String sortId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, erpBomService.getErpBomTreeBySortId(sortId, account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageNumber
     * @param pageSize
     * @param search
     * @param sort
     * @param sortOrder
     * @return RetDataBean
     * @Title getErpOrderList
     * @Description  获取订单列表
     */
    @RequestMapping(value = "/getErpOrderList", method = RequestMethod.POST)
    public RetDataBean getErpOrderList(
            Integer pageNumber,
            Integer pageSize,
            String search,
            String sort,
            String sortOrder
    ) {
        try {
            if (StringUtils.isBlank(sort)) {
                sort = "create_time";
            } else {
                sort = StrTools.lowerCharToUnderLine(sort);
            }
            if (StringUtils.isBlank(sortOrder)) {
                sortOrder = "asc";
            }
            String orderBy = sort + " " + sortOrder;
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(ErpOrder.class);
            example.setOrderByClause(orderBy);
            Criteria criteria = example.createCriteria();
            if (!account.getOpFlag().equals("1")) {
                criteria.andEqualTo("createUser", account.getAccountId());
            }
            criteria.andEqualTo("orgId", account.getOrgId());
            if (StringUtils.isNotEmpty(search)) {
                Criteria criteria2 = example.createCriteria();
                criteria2.orLike("orderTitle", "%" + search + "%").orLike("orderId", "%" + search + "%").orLike("customer", "%" + search + "%");
                example.and(criteria2);
            }
            PageInfo<ErpOrder> pageInfo = erpOrderService.getErpOrderList(example, pageNumber, pageSize);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageNumber
     * @param pageSize
     * @param search
     * @param sort
     * @param sortOrder
     * @return RetDataBean
     * @Title getErpOrderDetail
     * @Description  获取订单详情产品列表
     */
    @RequestMapping(value = "/getErpOrderDetail", method = RequestMethod.POST)
    public RetDataBean getErpOrderDetail(
            String orderId,
            Integer pageNumber,
            Integer pageSize,
            String search,
            String sort,
            String sortOrder
    ) {
        try {
            if (StringUtils.isBlank(sort)) {
                sort = "create_time";
            } else {
                sort = StrTools.lowerCharToUnderLine(sort);
            }
            if (StringUtils.isBlank(sortOrder)) {
                sortOrder = "asc";
            }
            String orderBy = sort + " " + sortOrder;
            Account account = accountService.getRedisAUserInfoToAccount();
            PageInfo<Map<String, Object>> pageInfo = erpOrderDetailService.getErpOrderDetailList(orderId, "%" + search + "%", account.getOrgId(), pageNumber, pageSize, orderBy);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {


            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param erpOrder
     * @return RetDataBean
     * @Title doCostByOrder
     * @Description  计算订单产品总价
     */
    @RequestMapping(value = "/doCostByOrder", method = RequestMethod.POST)
    public RetDataBean doCostByOrder(ErpOrder erpOrder) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            erpOrder.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, doCostService.doCost(erpOrder));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param bomId
     * @param pageNumber
     * @param pageSize
     * @param search
     * @param sort
     * @param sortOrder
     * @return RetDataBean
     * @Title getErpBomByBomIdList
     * @Description  获取BOM清单中的子BOM清单
     */
    @RequestMapping(value = "/getErpBomByBomIdList", method = RequestMethod.POST)
    public RetDataBean getErpBomByBomIdList(
            String bomId,
            Integer pageNumber,
            Integer pageSize,
            String search,
            String sort,
            String sortOrder
    ) {
        try {
            if (StringUtils.isBlank(sort)) {
                sort = "b.bom_id";
            } else {
                sort = "b." + StrTools.lowerCharToUnderLine(sort);
            }
            if (StringUtils.isBlank(sortOrder)) {
                sortOrder = "asc";
            }
            String orderBy = sort + " " + sortOrder;
            Account account = accountService.getRedisAUserInfoToAccount();
            PageInfo<Map<String, Object>> pageInfo = erpBomDetailService.getErpBomByBomIdList(pageNumber, pageSize, orderBy, bomId, "%" + search + "%", account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param bomName
     * @return RetDataBean
     * @Title selectBomList2ById
     * @Description  获取BOM清单用于SELECT2插件
     */
    @RequestMapping(value = "/selectBomList2ById", method = RequestMethod.POST)
    public RetDataBean selectBomList2ById(String bomName) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, erpBomService.selectBomList2ById("%" + bomName + "%", account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param productId
     * @return RetDataBean
     * @Title getProuctAndBomInfoByProductId
     * @Description  获取产品与BOM的对应信息
     */
    @RequestMapping(value = "/getProuctAndBomInfoByProductId", method = RequestMethod.POST)
    public RetDataBean getProuctAndBomInfoByProductId(String productId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, erpProductService.getProuctAndBomInfoByProductId(productId, account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param bomId
     * @param pageNumber
     * @param pageSize
     * @param sort
     * @param sortOrder
     * @return RetDataBean
     * @Title getProductMaterielListByBomId
     * @Description  产品物料详情
     */
    @RequestMapping(value = "/getProductMaterielListByBomId", method = RequestMethod.POST)
    public RetDataBean getProductMaterielListByBomId(
            String bomId,
            Integer pageNumber,
            Integer pageSize,
            String sort,
            String sortOrder
    ) {
        try {
            if (StringUtils.isBlank(sort)) {
                sort = "id";
            } else {
                sort = StrTools.lowerCharToUnderLine(sort);
            }
            if (StringUtils.isBlank(sortOrder)) {
                sortOrder = "asc";
            }
            String orderBy = sort + " " + sortOrder;
            Account account = accountService.getRedisAUserInfoToAccount();
            PageInfo<Map<String, Object>> pageInfo = erpBomDetailService.getProductMaterielListByBomId(pageNumber, pageSize, orderBy, bomId, account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param erpProduct
     * @return RetDataBean
     * @Title doCostByProduct
     * @Description  计算产品单价
     */
    @RequestMapping(value = "/doCostByProduct", method = RequestMethod.POST)
    public RetDataBean doCostByProduct(ErpProduct erpProduct) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            erpProduct.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, doCostService.doCostByProduct(erpProduct));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param erpBom
     * @return RetDataBean
     * @Title doCostByBom
     * @Description  按BOMID获取所有物料清单与用量
     */
    @RequestMapping(value = "/doCostByBom", method = RequestMethod.POST)
    public RetDataBean doCostByBom(ErpBom erpBom) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            erpBom.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, doCostService.doCostByBom(erpBom));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param sortId
     * @return List<Map < String, Object>>
     * @Title getErpEquipmentSortTree
     * @Description  获取设备分类树结构
     */
    @RequestMapping(value = "/getErpEquipmentSortTree", method = RequestMethod.POST)
    public List<Map<String, Object>> getErpEquipmentSortTree(String sortId) {
        try {
            String sortLevel = "0";
            if (StringUtils.isNotBlank(sortId)) {
                sortLevel = sortId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return erpEquipmentSortService.getErpEquipmentSortTree(sortLevel, account.getOrgId());
        } catch (Exception e) {

            return null;
        }
    }

    /**
     * @param erpEquipmentSort
     * @return RetDataBean
     * @Title getErpEquipmentSortById
     * @Description  获取设备分类
     */
    @RequestMapping(value = "/getErpEquipmentSortById", method = RequestMethod.POST)
    public RetDataBean getErpEquipmentSortById(ErpEquipmentSort erpEquipmentSort) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            erpEquipmentSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, erpEquipmentSortService.selectOne(erpEquipmentSort));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  search
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getProductSelect2
     * @Description: 获取产品的select2列表
     */
    @RequestMapping(value = "/getProductSelect2", method = RequestMethod.POST)
    public RetDataBean getProductSelect2(String search) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, erpProductService.getProductSelect2(account.getOrgId(), "%" + search + "%"));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

}
