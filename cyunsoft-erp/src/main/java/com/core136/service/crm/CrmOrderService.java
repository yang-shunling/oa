package com.core136.service.crm;

import com.core136.bean.crm.CrmOrder;
import com.core136.mapper.crm.CrmOrderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

/**
 * @ClassName: CrmOrderService
 * @Description: 客户订单管理
 * @author: 稠云信息
 * @date: 2019年2月12日 下午5:01:58
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Service
public class CrmOrderService {
    private CrmOrderMapper crmOrderMapper;

    @Autowired
    public void setCrmOrderMapper(CrmOrderMapper crmOrderMapper) {
        this.crmOrderMapper = crmOrderMapper;
    }

    /**
     * @param crmOrder
     * @return CrmOrder
     * @Title selectOne
     * @Description  获取订单详情
     */
    public CrmOrder selectOne(CrmOrder crmOrder) {
        return crmOrderMapper.selectOne(crmOrder);
    }

    /**
     * @param crmOrder
     * @param example
     * @return int
     * @Title updateCrmOrder
     * @Description  修改订单
     */
    public int updateCrmOrder(CrmOrder crmOrder, Example example) {
        return crmOrderMapper.updateByExampleSelective(crmOrder, example);
    }

    /**
     * @param crmOrder
     * @return int
     * @Title deleteCrmOrder
     * @Description  删除订单
     */
    public int deleteCrmOrder(CrmOrder crmOrder) {
        return crmOrderMapper.delete(crmOrder);
    }

    /**
     * @param crmOrder
     * @return int
     * @Title insertCrmOrder
     * @Description  创建订单
     */
    public int insertCrmOrder(CrmOrder crmOrder) {
        return crmOrderMapper.insert(crmOrder);
    }
}
