package com.core136.service.erp;

import com.core136.bean.erp.ErpBomSort;
import com.core136.mapper.erp.ErpBomSortMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * @ClassName: ErpBomSortService
 * @Description: BOM分类操作服务类
 * @author: 稠云信息
 * @date: 2018年12月14日 上午11:05:41
 * @Copyright: 2018 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Service
public class ErpBomSortService {
    private ErpBomSortMapper erpBomSortMapper;

    @Autowired
    public void setErpBomSortMapper(ErpBomSortMapper erpBomSortMapper) {
        this.erpBomSortMapper = erpBomSortMapper;
    }

    /**
     * 获取BOM的父级分类
     */

    public List<Map<String, Object>> getErpBomSortTree(String sortLevel, String orgId) {
        //  Auto-generated method stub
        return erpBomSortMapper.getErpBomSortTree(sortLevel, orgId);
    }

    /**
     * 判断分类下是否还有子分类
     */

    public int isExistChild(String sortId, String orgId) {
        //  Auto-generated method stub
        return erpBomSortMapper.isExistChild(sortId, orgId);
    }

    /**
     * @param erpBomSort
     * @return ErpBomSort
     * @Title selectOne
     * @Description  按条Id获取Bom分类
     */
    public ErpBomSort selectOne(ErpBomSort erpBomSort) {
        return erpBomSortMapper.selectOne(erpBomSort);
    }

    /**
     * @param erpBomSort
     * @return int
     * @Title insertErpBomSort
     * @Description  创建新的BOM分类
     */
    public int insertErpBomSort(ErpBomSort erpBomSort) {
        return erpBomSortMapper.insert(erpBomSort);
    }

    /**
     * @param erpBomSort
     * @return int
     * @Title delErpBomSort
     * @Description  删除BOM分类
     */
    public int delErpBomSort(ErpBomSort erpBomSort) {
        return erpBomSortMapper.delete(erpBomSort);
    }

    /**
     * @param erpBomSort
     * @param example
     * @return int
     * @Title updateErpBomSort
     * @Description  按条件更新BOM分类
     */
    public int updateErpBomSort(ErpBomSort erpBomSort, Example example) {
        return erpBomSortMapper.updateByExample(erpBomSort, example);
    }
}
