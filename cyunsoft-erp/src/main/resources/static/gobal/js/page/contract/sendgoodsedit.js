let ue = UE.getEditor("remark");
$(function () {
    jeDate("#sendDate", {
        format: "YYYY-MM-DD"
    });
    $("#createbut").unbind("click").click(function () {
        updateContractSendgoods();
    })
    ue.addListener("ready", function () {
        $.ajax({
            type: "post",
            url: "/ret/contractget/getContractSendgoodsById",
            data: {
                recordId: recordId
            },
            success: function (data) {
                if (data.status == 200) {
                    for (var id in data.list) {
                        if (id == "attach") {
                            $("#contractattach").attr("data_value", data.list[id]);
                            createAttach("contractattach", 4);
                        } else if (id == "contractId") {
                            $("#" + id).attr("data-value", data.list[id]);
                            $.ajax({
                                type: "post",
                                url: "/ret/contractget/getContractById",
                                data: {contractId: data.list[id]},
                                success: function (res) {
                                    if (res.status == 200) {
                                        $("#contractId").html(res.list.title);
                                    } else if (res.status == "100") {
                                        layer.msg(sysmsg[res.msg]);
                                    } else {
                                        console.log(res.msg);
                                    }
                                }
                            });
                        } else if (id == "remark") {
                            ue.setContent(data.list[id]);
                        } else {
                            $("#" + id).val(data.list[id]);
                        }
                    }
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            },
            error: function (e) {
            }
        })
    });
});

function updateContractSendgoods() {
    $.ajax({
        type: "post",
        dataType: "json",
        url: "/set/contractset/updateContractSendgoods",
        data: {
            recordId: recordId,
            sendDate: $("#sendDate").val(),
            trackingComp: $("#trackingComp").val(),
            trackingNumber: $("#trackingNumber").val(),
            consignee: $("#consignee").val(),
            address: $("#address").val(),
            iphone: $("#iphone").val(),
            remark: ue.getContent(),
            userPriv: $("#userPriv").attr("data-value"),
            attach: $("#contractattach").attr("data_value")
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                window.location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    });
}
