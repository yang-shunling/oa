$(function () {
    createSupTypeList();
    $(".js-add-bpm").unbind("click").click(function () {
        $("#setBpmConfigModal").modal("show");
    })
    $.ajax({
        url: "/ret/bpmget/getAllBpmFlowListByManage",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var html = "";
                for (var i = 0; i < data.list.length; i++) {
                    html += "<option value='" + data.list[i].flowId + "'>" + data.list[i].flowName + "</option>"
                }
                $("#bpmlist").html(html);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
    $(".js-save").unbind("click").click(function () {
        var bpmConfigArr = [];
        $(".js-config").each(function () {
            let flowIds = $(this).attr("data-value");
            let configId = $(this).attr("data-flag");
            let json = {};
            json.configId = configId;
            json.flowIds = flowIds;
            bpmConfigArr.push(json);
        })
        let configStr = JSON.stringify(bpmConfigArr);
        setBpmConfig(configStr);
    })
    query();
})


function createFlowDiv(configId) {
    $.ajax({
        url: "/ret/superversionget/getSuperversionBpmConfigById",
        type: "post",
        dataType: "json",
        async: false,
        data: {configId: configId},
        success: function (data) {
            if (data.status == "200") {
                $(".js-" + configId).attr("data-value", data.list.flowIds);
                $.ajax({
                    url: "/ret/bpmget/getFlowNameByFlowIds",
                    type: "post",
                    dataType: "json",
                    async: false,
                    data: {flowIds: data.list.flowIds},
                    success: function (data) {
                        if (data.status == "200") {
                            for (let i = 0; i < data.list.length; i++) {
                                $(".js-" + configId).append("<div class='js-" + configId + "-" + data.list[i].flowId + "'>" + data.list[i].flowName + "<a onclick='clearBpmOpt(\"" + configId + "\",\"" + data.list[i].flowId + "\");' style='float: right;cursor: pointer'>×</a></div>");
                            }
                        } else if (data.status == "100") {
                            layer.msg(sysmsg[data.msg]);
                        } else {
                            console.log(data.list);
                        }
                    }
                })
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.list);
            }
        }
    })
}

function createSupTypeList() {
    $.ajax({
        url: "/ret/superversionget/getAllSuperversionConfigList",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                for (let i = 0; i < data.list.length; i++) {
                    $("#configlist").append(
                        "<tr>" +
                        "<td>" + (i + 1) + "</td><td>" + data.list[i].typeName + "</td>" +
                        "<td><div class='js-config js-" + data.list[i].configId + "' data-flag='" + data.list[i].configId + "'></div></td>" +
                        "<td><a href=\"javascript:void(0);setBpm('" + data.list[i].configId + "');\" class=\"btn btn-purple btn-xs\">设置</a></td>" +
                        "</tr>"
                    );
                }
                $(".js-config").each(function () {
                    let configId = $(this).attr("data-flag");
                    createFlowDiv(configId);
                })
            }
        }
    })
}

function setBpm(configId) {
    $("#bpmModal").modal("show");
    $(".js-add-bpm").unbind("click").click(function () {
        let flowId = $("#bpmlist").val();
        let flowName = $("#bpmlist").find("option:selected").text();
        $(".js-" + configId).append("<div class='js-" + configId + "-" + flowId + "'>" + flowName + "<a onclick='clearBpmOpt(\"" + configId + "\",\"" + flowId + "\");' style='float: right;cursor: pointer'>×</a></div>");
        let tmpValue = $(".js-" + configId).attr("data-value");
        let flowArr = [];
        if (tmpValue) {
            flowArr = tmpValue.split(",");
        }
        flowArr.push(flowId);
        $(".js-" + configId).attr("data-value", flowArr.join(","));
        $("#bpmModal").modal("hide");
    })
}

function clearBpmOpt(configId, flowId) {
    let tmpValue = $(".js-" + configId).attr("data-value");
    tmpValue = tmpValue.replaceAll(flowId + ",", "");
    tmpValue = tmpValue.replaceAll(flowId, "");
    let flowArr = [];
    if (tmpValue) {
        flowArr = tmpValue.split(",");
    }
    $(".js-" + configId).attr("data-value", flowArr.join(","));
    $(".js-" + configId + "-" + flowId).remove();
}

function setBpmConfig(configStr) {
    $.ajax({
        url: "/set/superversionset/setBpmConfig",
        type: "post",
        dataType: "json",
        data: {configStr: configStr},
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                layer.msg(sysmsg[data.msg]);
                $("#setBpmConfigModal").modal("hide");
            }
        }
    })
}

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/superversionget/getAllSuperversionBpmConfigList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: false,//是否显示 数据条数选择框
        sortable: false,//排序
        search: false,//启用搜索
        showColumns: false,//是否显示 内容列下拉框
        showRefresh: false,//显示刷新按钮
        idField: 'configId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [
            {
                field: 'num',
                title: '序号',//标题  可不加
                width: '50px',
                formatter: function (value, row, index) {
                    return index + 1;
                }
            },
            {
                field: 'configId',
                width: '200px',
                title: '唯一标识'
            },
            {
                field: 'typeName',
                width: '100px',
                title: '类型名称'
            },
            {
                field: 'flowIds',
                title: '关联流程',
                width: '100px',
                formatter: function (value, row, index) {
                    return getFlowNameByFlowIds(value);
                }
            },
            {
                field: 'createUserName',
                title: '创建人',
                width: '100px'
            },
            {
                field: 'createTime',
                title: '创建时间',
                width: '100px'
            }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
                layer.msg(sysmsg[res.msg]);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order
    };
    return temp;
};


function getFlowNameByFlowIds(flowIds) {
    var returnStr;
    $.ajax({
        url: "/ret/bpmget/getFlowNameByFlowIds",
        type: "post",
        dataType: "json",
        async: false,
        data: {flowIds: flowIds},
        success: function (data) {
            if (data.status == "200") {
                let arr = [];
                for (let i = 0; i < data.list.length; i++) {
                    arr.push(data.list[i].flowName);
                }
                returnStr = arr.join(",");
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.list);
            }
        }
    })
    return returnStr;
}
