package com.core136.service.superversion;

import com.core136.bean.account.Account;
import com.core136.bean.superversion.SuperversionIntegral;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.superversion.SuperversionIntegralMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

@Service
public class SuperversionIntegralService {
    private SuperversionIntegralMapper superversionIntegralMapper;

    @Autowired
    public void setSuperversionIntegralMapper(SuperversionIntegralMapper superversionIntegralMapper) {
        this.superversionIntegralMapper = superversionIntegralMapper;
    }

    public int insertSuperversionIntegral(SuperversionIntegral superversionIntegral) {
        return superversionIntegralMapper.insert(superversionIntegral);
    }

    public int deleteSuperversionIntegral(SuperversionIntegral superversionIntegral) {
        return superversionIntegralMapper.delete(superversionIntegral);
    }

    public int updateSuperversionIntegral(Example example, SuperversionIntegral superversionIntegral) {
        return superversionIntegralMapper.updateByExampleSelective(superversionIntegral, example);
    }

    public SuperversionIntegral selectOneSuperversionIntegral(SuperversionIntegral superversionIntegral) {
        return superversionIntegralMapper.selectOne(superversionIntegral);
    }

    public RetDataBean setIntegralRule(Account account, SuperversionIntegral superversionIntegral) {
        SuperversionIntegral tempSuperversionIntegral = new SuperversionIntegral();
        tempSuperversionIntegral.setOrgId(superversionIntegral.getOrgId());
        tempSuperversionIntegral = selectOneSuperversionIntegral(tempSuperversionIntegral);
        if (tempSuperversionIntegral == null) {
            superversionIntegral.setConfigId(SysTools.getGUID());
            superversionIntegral.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            superversionIntegral.setCreateUser(account.getAccountId());
            superversionIntegral.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, insertSuperversionIntegral(superversionIntegral));
        } else {
            Example example = new Example(SuperversionIntegral.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, updateSuperversionIntegral(example, superversionIntegral));
        }
    }

}
