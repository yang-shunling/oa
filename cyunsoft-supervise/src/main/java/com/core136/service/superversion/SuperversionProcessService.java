/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: SuperversionProcessService.java
 * @Package com.core136.service.superversion
 * @Description: 描述
 * @author: lsq
 * @date: 2019年11月18日 上午9:18:42
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.service.superversion;

import com.core136.bean.superversion.SuperversionProcess;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.superversion.SuperversionProcessMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 */
@Service
public class SuperversionProcessService {
    private SuperversionProcessMapper superversionProcessMapper;

    @Autowired
    public void setSuperversionProcessMapper(SuperversionProcessMapper superversionProcessMapper) {
        this.superversionProcessMapper = superversionProcessMapper;
    }

    public int insertSuperversionProcess(SuperversionProcess superversionProcess) {
        return superversionProcessMapper.insert(superversionProcess);
    }

    public int deleteSuperversionProcess(SuperversionProcess superversionProcess) {
        return superversionProcessMapper.delete(superversionProcess);
    }

    public int updateSuperversionProcess(Example example, SuperversionProcess superversionProcess) {
        return superversionProcessMapper.updateByExampleSelective(superversionProcess, example);
    }

    public SuperversionProcess selectOneSuperversionProcess(SuperversionProcess superversionProcess) {
        return superversionProcessMapper.selectOne(superversionProcess);
    }

    /**
     * @Title: getOldProcessList
     * @Description:  获取事件处理过程列表
     * @param: orgId
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> getOldProcessList(String orgId, String accountId, String beginTime, String endTime, String type, String search) {
        return superversionProcessMapper.getOldProcessList(orgId, accountId, beginTime, endTime, type, "%" + search + "%");
    }

    /**
     * @Title: getControlProcessList
     * @Description:  获取我所管控的任务列表
     * @param: orgId
     * @param: accountId
     * @param: beginTime
     * @param: endTime
     * @param: type
     * @param: search
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> getControlProcessList(String orgId, String accountId, String beginTime, String endTime, String superversionId, String search) {
        return superversionProcessMapper.getControlProcessList(orgId, accountId, beginTime, endTime, superversionId, "%" + search + "%");
    }


    /**
     * @throws Exception
     * @Title: getOldProcessList
     * @Description: 获取事件处理过程列表
     * @param: pageParam
     * @param: @return
     * @return: PageInfo<Map < String, String>>
     */
    public PageInfo<Map<String, String>> getOldProcessList(PageParam pageParam, String beginTime, String endTime, String superversionId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getOldProcessList(pageParam.getOrgId(), pageParam.getAccountId(), beginTime, endTime, superversionId, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @throws Exception
     * @Title: getControlProcessList
     * @Description:  获取我所管控的任务列表
     * @param: pageParam
     * @param: beginTime
     * @param: endTime
     * @param: type
     * @param: @return
     * @return: PageInfo<Map < String, String>>
     */
    public PageInfo<Map<String, String>> getControlProcessList(PageParam pageParam, String beginTime, String endTime, String type) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getControlProcessList(pageParam.getOrgId(), pageParam.getAccountId(), beginTime, endTime, type, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @Title: getSupperversionPorcessList
     * @Description:  获取待处理的督查列表
     * @param: orgId
     * @param: accountId
     * @param: type
     * @param: beginTime
     * @param: endTime
     * @param: search
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> getSupperversionPorcessList(String orgId, String accountId, String type, String beginTime, String endTime, String search) {
        return superversionProcessMapper.getSupperversionPorcessList(orgId, accountId, type, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @throws Exception
     * @Title: getSupperversionPorcessList
     * @Description:  获取待处理的督查列表
     * @param: pageParam
     * @param: type
     * @param: beginTime
     * @param: endTime
     * @param: @return
     * @return: PageInfo<Map < String, String>>
     */
    public PageInfo<Map<String, String>> getSupperversionPorcessList(PageParam pageParam, String type, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getSupperversionPorcessList(pageParam.getOrgId(), pageParam.getAccountId(), type, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * 获取转办给我的列表
     *
     * @param orgId
     * @param accountId
     * @param type
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String, String>> getSupperversionPorcessForChangeList(String orgId, String accountId, String type, String beginTime, String endTime, String search) {
        return superversionProcessMapper.getSupperversionPorcessForChangeList(orgId, accountId, type, beginTime, endTime, "%" + search + "%");
    }

    /**
     * 获取转办给我的列表
     *
     * @param pageParam
     * @param type
     * @param beginTime
     * @param endTime
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getSupperversionPorcessForChangeList(PageParam pageParam, String type, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getSupperversionPorcessForChangeList(pageParam.getOrgId(), pageParam.getAccountId(), type, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @Title: getQuerySuperversionForDept
     * @Description: 按部门汇总
     * @param: orgId
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> getQuerySuperversionForDept(String orgId) {
        return superversionProcessMapper.getQuerySuperversionForDept(orgId);
    }

    /**
     * @param orgId
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getSupperversionPorcessListForDesk
     * @Description:  获取督查督办待办事件
     */
    public List<Map<String, String>> getSupperversionPorcessListForDesk(String orgId, String accountId) {
        return superversionProcessMapper.getSupperversionPorcessListForDesk(orgId, accountId);
    }


    public List<Map<String, String>> getSuperversionProcessListForSelect(String orgId, String accountId) {
        return superversionProcessMapper.getSuperversionProcessListForSelect(orgId, accountId);
    }

    /**
     * 获取延时任务列表
     *
     * @param orgId
     * @return
     */
    public List<Map<String, String>> getAllSuperversionProcessForDesk(String orgId) {
        return superversionProcessMapper.getAllSuperversionProcessForDesk(orgId);
    }

}
