/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: SuperversionDelay.java
 * @Package com.core136.bean.superversion
 * @Description: 描述
 * @author: lsq
 * @date: 2019年11月19日 下午5:32:36
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.bean.superversion;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * 事件延期
 * @author lsq
 *
 */
@Table(name = "superversion_delay")
public class SuperversionDelay implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String delayId;
    private String processId;
    /**
     * 延期原因
     */
    private String content;
    /**
     * 相关附件
     */
    private String attach;
    /**
     * 延期到什么时间
     */
    private String delayTime;
    /**
     * 审批时间
     */
    private String applyTime;
    /**
     * 审批领导
     */
    private String leadId;
    /**
     * 审批意见
     */
    private String ideaText;
    /**
     * 客观意见
     */
    private String passStatus;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getDelayId() {
        return delayId;
    }

    public void setDelayId(String delayId) {
        this.delayId = delayId;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getDelayTime() {
        return delayTime;
    }

    public void setDelayTime(String delayTime) {
        this.delayTime = delayTime;
    }

    public String getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(String applyTime) {
        this.applyTime = applyTime;
    }

    public String getLeadId() {
        return leadId;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }

    public String getIdeaText() {
        return ideaText;
    }

    public void setIdeaText(String ideaText) {
        this.ideaText = ideaText;
    }

    public String getPassStatus() {
        return passStatus;
    }

    public void setPassStatus(String passStatus) {
        this.passStatus = passStatus;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }
}
