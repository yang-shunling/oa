package com.core136.mapper.superversion;

import com.core136.bean.superversion.SuperversionResult;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface SuperversionResultMapper extends MyMapper<SuperversionResult> {
    /**
     * 获取任务处理过程列表
     *
     * @param orgId
     * @param processId
     * @return
     */
    public List<Map<String, String>> getSuperversionResultList(@Param(value = "orgId") String orgId, @Param(value = "processId") String processId);
}
