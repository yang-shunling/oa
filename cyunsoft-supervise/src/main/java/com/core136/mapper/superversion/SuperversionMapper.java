/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: SuperversionMapper.java
 * @Package com.core136.mapper.superversion
 * @Description: 描述
 * @author: lsq
 * @date: 2019年10月28日 下午4:27:45
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.mapper.superversion;

import com.core136.bean.superversion.Superversion;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 *
 */
@Mapper
public interface SuperversionMapper extends MyMapper<Superversion> {

    public List<Map<String, String>> getSupserversionForTree(@Param(value = "orgId") String orgId, @Param(value = "type") String type);


    /**
     *
     * @Title: getSupperversionList
     * @Description:  我的历史记录
     * @param: orgId
     * @param: accountId
     * @param: type
     * @param: handedUser
     * @param: beginTime
     * @param: endTime
     * @param: search
     * @param: @return
     * @return: List<Map < String, String>>

     */
    public List<Map<String, String>> getSupperversionList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,
                                                          @Param(value = "type") String type, @Param(value = "handedUser") String handedUser, @Param(value = "beginTime") String beginTime,
                                                          @Param(value = "endTime") String endTime, @Param(value = "status") String status, @Param(value = "search") String search);

    /**
     * 获取完成督查督办列表
     * @param orgId
     * @param accountId
     * @param type
     * @param handedUser
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String, String>> getSupperversionFinishList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,
                                                                @Param(value = "type") String type, @Param(value = "handedUser") String handedUser, @Param(value = "beginTime") String beginTime,
                                                                @Param(value = "endTime") String endTime, @Param(value = "search") String search);

    /**
     *
     * @Title: getLeadManageSupperversionList
     * @Description:  获取当前用户管控的事件列表
     * @param: orgId
     * @param: accountId
     * @param: type
     * @param: handedUser
     * @param: beginTime
     * @param: endTime
     * @param: status
     * @param: search
     * @param: @return
     * @return: List<Map < String, String>>

     */
    public List<Map<String, String>> getLeadManageSupperversionList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,
                                                                    @Param(value = "type") String type, @Param(value = "handedUser") String handedUser, @Param(value = "beginTime") String beginTime,
                                                                    @Param(value = "endTime") String endTime, @Param(value = "status") String status, @Param(value = "search") String search);

    /**
     * 获取督查督办事件列表
     * @param orgId
     * @param accountId
     * @param type
     * @param handedUser
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String, String>> getQueryOldSupperversionList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,
                                                                  @Param(value = "type") String type, @Param(value = "handedUser") String handedUser, @Param(value = "beginTime") String beginTime,
                                                                  @Param(value = "endTime") String endTime, @Param(value = "search") String search);

    public List<Map<String, String>> getManageSuperversionListForDesk(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId);

    /**
     * 移动端督查督办列表
     * @param orgId
     * @param accountId
     * @param page
     * @return
     */
    public List<Map<String, String>> getMobileMySuperversionList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId, @Param(value = "page") Integer page);

    /**
     * 获取所有督查事件
     * @param orgId
     * @return
     */
    public List<Map<String, String>> getAllSuperversionForDesk(@Param(value = "orgId") String orgId);


    public List<Map<String, String>> getAllFinishSuperversionForDesk(@Param(value = "orgId") String orgId);
}


