/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: RoutSetSuperversionController.java
 * @Package com.core136.controller.superversion
 * @Description: 描述
 * @author: lsq
 * @date: 2019年11月14日 下午1:18:27
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.controller.superversion;

import com.core136.bean.account.Account;
import com.core136.bean.account.UserInfo;
import com.core136.bean.superversion.*;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.superversion.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author lsq
 */
@RestController
@RequestMapping("/set/superversionset")
public class RouteSetSuperversionController {
    private SuperversionService superversionService;

    @Autowired
    public void setSuperversionService(SuperversionService superversionService) {
        this.superversionService = superversionService;
    }

    private SuperversionConfigService superversionConfigService;

    @Autowired
    public void setSuperversionConfigService(SuperversionConfigService superversionConfigService) {
        this.superversionConfigService = superversionConfigService;
    }

    private SuperversionDelayService superversionDelayService;

    @Autowired
    public void setSuperversionDelayService(SuperversionDelayService superversionDelayService) {
        this.superversionDelayService = superversionDelayService;
    }

    private SuperversionProcessService superversionProcessService;

    @Autowired
    public void setSuperversionProcessService(SuperversionProcessService superversionProcessService) {
        this.superversionProcessService = superversionProcessService;
    }

    private SuperversionScoreService superversionScoreService;

    @Autowired
    public void setSuperversionScoreService(SuperversionScoreService superversionScoreService) {
        this.superversionScoreService = superversionScoreService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private SuperversionBpmConfigService superversionBpmConfigService;

    @Autowired
    public void setSuperversionBpmConfigService(SuperversionBpmConfigService superversionBpmConfigService) {
        this.superversionBpmConfigService = superversionBpmConfigService;
    }

    private SuperversionIntegralService superversionIntegralService;

    @Autowired
    public void setSuperversionIntegralService(SuperversionIntegralService superversionIntegralService) {
        this.superversionIntegralService = superversionIntegralService;
    }

    private SuperversionResultService superversionResultService;

    @Autowired
    public void setSuperversionResultService(SuperversionResultService superversionResultService) {
        this.superversionResultService = superversionResultService;
    }

    private SuperversionProblemService superversionProblemService;

    @Autowired
    public void setSuperversionProblemService(SuperversionProblemService superversionProblemService) {
        this.superversionProblemService = superversionProblemService;
    }

    private SuperversionBpmListService superversionBpmListService;

    @Autowired
    public void setSuperversionBpmListServer(SuperversionBpmListService superversionBpmListService) {
        this.superversionBpmListService = superversionBpmListService;
    }

    /**
     * 设置积分规则
     *
     * @param superversionIntegral
     * @return
     */
    @RequestMapping(value = "/setIntegralRule", method = RequestMethod.POST)
    public RetDataBean setIntegralRule(SuperversionIntegral superversionIntegral) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return superversionIntegralService.setIntegralRule(account, superversionIntegral);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 督查督办发起流程
     *
     * @param superversionBpmList
     * @return
     */
    @RequestMapping(value = "/startSuperversionBpm", method = RequestMethod.POST)
    public RetDataBean startSuperversionBpm(SuperversionBpmList superversionBpmList) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            superversionBpmList.setRecordId(SysTools.getGUID());
            superversionBpmList.setRunId(SysTools.getGUID());
            superversionBpmList.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            superversionBpmList.setCreateUser(account.getAccountId());
            superversionBpmList.setOrgId(account.getOrgId());
            superversionBpmListService.insertSuperversionBpmList(superversionBpmList);
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, "/app/core/apibpmstart?isEmpty=1&runId=" + superversionBpmList.getRunId() + "&flowId=" + superversionBpmList.getFlowId() + "&title=" + superversionBpmList.getTitle());
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 发起流程审批
     *
     * @param superversionBpmList
     * @return
     */
    @RequestMapping(value = "/insertSuperversionBpmList", method = RequestMethod.POST)
    public RetDataBean insertSuperversionBpmList(SuperversionBpmList superversionBpmList) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            superversionBpmList.setRecordId(SysTools.getGUID());
            superversionBpmList.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            superversionBpmList.setCreateUser(account.getAccountId());
            superversionBpmList.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, superversionBpmListService.insertSuperversionBpmList(superversionBpmList));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除流程
     *
     * @param superversionBpmList
     * @return
     */
    @RequestMapping(value = "/deleteSuperversionBpmList", method = RequestMethod.POST)
    public RetDataBean deleteSuperversionBpmList(SuperversionBpmList superversionBpmList) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(superversionBpmList.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            superversionBpmList.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, superversionBpmListService.deleteSuperversionBpmList(superversionBpmList));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 流程更新
     *
     * @param superversionBpmList
     * @return
     */
    @RequestMapping(value = "/updateSuperversionBpmList", method = RequestMethod.POST)
    public RetDataBean updateSuperversionBpmList(SuperversionBpmList superversionBpmList) {
        try {
            if (StringUtils.isBlank(superversionBpmList.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(SuperversionBpmList.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", superversionBpmList.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, superversionBpmListService.updateSuperversionBpmList(example, superversionBpmList));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 反馈问题
     *
     * @param superversionProblem
     * @return
     */
    @RequestMapping(value = "/insertSuperversionProblem", method = RequestMethod.POST)
    public RetDataBean insertSuperversionProblem(SuperversionProblem superversionProblem) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            superversionProblem.setRecordId(SysTools.getGUID());
            if (StringUtils.isBlank(superversionProblem.getParentId())) {
                superversionProblem.setParentId("0");
            }
            superversionProblem.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            superversionProblem.setCreateUser(account.getAccountId());
            superversionProblem.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, superversionProblemService.insertSuperversionProblem(superversionProblem));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除反馈问题
     *
     * @return
     */
    @RequestMapping(value = "/deleteSuperversionProblem", method = RequestMethod.POST)
    public RetDataBean deleteSuperversionProblem(SuperversionProblem superversionProblem) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(superversionProblem.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            superversionProblem.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, superversionProblemService.deleteSuperversionProblem(superversionProblem));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新反馈问题
     *
     * @return
     */
    @RequestMapping(value = "/updateSuperversionProblem", method = RequestMethod.POST)
    public RetDataBean updateSuperversionProblem(SuperversionProblem superversionProblem) {
        try {
            if (StringUtils.isBlank(superversionProblem.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(SuperversionProblem.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", superversionProblem.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, superversionProblemService.updateSuperversionProblem(example, superversionProblem));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 接收转办任务
     *
     * @param processId
     * @param passStatus
     * @return
     */
    @RequestMapping(value = "/revChangeUserProcess", method = RequestMethod.POST)
    public RetDataBean revChangeUserProcess(String processId, String passStatus) {
        try {
            if (StringUtils.isBlank(processId) || StringUtils.isBlank(passStatus)) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (passStatus.equals("1")) {
                SuperversionProcess superversionProcess = new SuperversionProcess();
                superversionProcess.setOrgId(account.getOrgId());
                superversionProcess.setProcessId(processId);
                superversionProcess = superversionProcessService.selectOneSuperversionProcess(superversionProcess);
                superversionProcess.setHolder(superversionProcess.getChangeUser());
                superversionProcess.setChangeStatus("1");
                Example example = new Example(SuperversionProcess.class);
                example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("processId", processId);
                return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, superversionProcessService.updateSuperversionProcess(example, superversionProcess));
            } else {
                SuperversionProcess superversionProcess = new SuperversionProcess();
                superversionProcess.setChangeStatus("2");
                Example example = new Example(SuperversionProcess.class);
                example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("processId", processId);
                return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, superversionProcessService.updateSuperversionProcess(example, superversionProcess));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 任务转办
     *
     * @param processId
     * @param changeUser
     * @return
     */
    @RequestMapping(value = "/setChangeUserByProcess", method = RequestMethod.POST)
    public RetDataBean setChangeUserByProcess(String processId, String changeUser) {
        try {
            if (StringUtils.isBlank(changeUser) || StringUtils.isBlank(processId)) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            SuperversionProcess superversionProcess = new SuperversionProcess();
            superversionProcess.setChangeUser(changeUser);
            superversionProcess.setChangeStatus("0");
            superversionProcess.setChangeTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            Example example = new Example(SuperversionProcess.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("processId", processId);
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, superversionProcessService.updateSuperversionProcess(example, superversionProcess));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 设置督查督办流程
     *
     * @param configStr
     * @return
     */
    @RequestMapping(value = "/setBpmConfig", method = RequestMethod.POST)
    public RetDataBean setBpmConfig(String configStr) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return superversionBpmConfigService.setBpmConfig(account, configStr);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 添加任务处理结果
     *
     * @param superversionResult
     * @return
     */
    @Transactional(value = "generalTM")
    @RequestMapping(value = "/insertSuperversionResult", method = RequestMethod.POST)
    public RetDataBean insertSuperversionResult(SuperversionResult superversionResult) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            superversionResult.setRecordId(SysTools.getGUID());
            superversionResult.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            if (superversionResult.getPrcsValue() == null) {
                superversionResult.setPrcsValue(0);
            } else {
                if (superversionResult.getPrcsValue() >= 100) {
                    SuperversionProcess superversionProcess = new SuperversionProcess();
                    superversionProcess.setStatus("2");
                    superversionProcess.setPrcsValue(100);
                    superversionProcess.setFinishTime(superversionResult.getFinishTime());
                    Example example = new Example(SuperversionProcess.class);
                    example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("processId", superversionResult.getProcessId());
                    superversionProcessService.updateSuperversionProcess(example, superversionProcess);
                }
            }
            superversionResult.setCreateUser(account.getAccountId());
            superversionResult.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, superversionResultService.insertSuperversionResult(superversionResult));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 删除任务处理结果
     *
     * @param superversionResult
     * @return
     */
    @RequestMapping(value = "/deleteSuperversionResult", method = RequestMethod.POST)
    public RetDataBean deleteSuperversionResult(SuperversionResult superversionResult) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(superversionResult.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            superversionResult.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, superversionResultService.deleteSuperversionResult(superversionResult));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新任务处理结果
     *
     * @param superversionResult
     * @return
     */
    @RequestMapping(value = "/updateSuperversionResult", method = RequestMethod.POST)
    public RetDataBean updateSuperversionResult(SuperversionResult superversionResult) {
        try {
            if (StringUtils.isBlank(superversionResult.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            if (superversionResult.getPrcsValue() == null) {
                superversionResult.setPrcsValue(0);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(SuperversionResult.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", superversionResult.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, superversionResultService.updateSuperversionResult(example, superversionResult));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param superversionBpmConfig
     * @return RetDataBean
     * @Title: insertSuperversionBpmConfig
     * @Description:  添加流程配置
     */
    @RequestMapping(value = "/insertSuperversionBpmConfig", method = RequestMethod.POST)
    public RetDataBean insertSuperversionBpmConfig(SuperversionBpmConfig superversionBpmConfig) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            superversionBpmConfig.setConfigId(SysTools.getGUID());
            superversionBpmConfig.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            superversionBpmConfig.setCreateUser(account.getAccountId());
            superversionBpmConfig.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, superversionBpmConfigService.insertSuperversionBpmConfig(superversionBpmConfig));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param superversionBpmConfig
     * @return RetDataBean
     * @Title: deleteSuperversionBpmConfig
     * @Description:  删除流程设置
     */
    @RequestMapping(value = "/deleteSuperversionBpmConfig", method = RequestMethod.POST)
    public RetDataBean deleteSuperversionBpmConfig(SuperversionBpmConfig superversionBpmConfig) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(superversionBpmConfig.getConfigId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            superversionBpmConfig.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, superversionBpmConfigService.deleteSuperversionBpmConfig(superversionBpmConfig));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param superversionBpmConfig
     * @return RetDataBean
     * @Title: updateSuperversionBpmConfig
     * @Description:  更新流程设置
     */
    @RequestMapping(value = "/updateSuperversionBpmConfig", method = RequestMethod.POST)
    public RetDataBean updateSuperversionBpmConfig(SuperversionBpmConfig superversionBpmConfig) {
        try {
            if (StringUtils.isBlank(superversionBpmConfig.getConfigId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(SuperversionBpmConfig.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("configId", superversionBpmConfig.getConfigId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, superversionBpmConfigService.updateSuperversionBpmConfig(example, superversionBpmConfig));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param superversionScore
     * @return RetDataBean
     * @Title: insertSuperversionScore
     * @Description:  添加
     */
    @RequestMapping(value = "/insertSuperversionScore", method = RequestMethod.POST)
    public RetDataBean insertSuperversionScore(SuperversionScore superversionScore) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            superversionScore.setScoreId(SysTools.getGUID());
            if (superversionScore.getUserScore() > 100) {
                superversionScore.setUserScore(100.00);
            }
            superversionScore.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            superversionScore.setCreateUser(account.getAccountId());
            superversionScore.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, superversionScoreService.insertSuperversionScore(superversionScore));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 评分
     *
     * @param superversionScore
     * @return
     */
    @RequestMapping(value = "/setSuperversionScore", method = RequestMethod.POST)
    public RetDataBean setSuperversionScore(SuperversionScore superversionScore) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (superversionScore.getUserScore() > 100) {
                superversionScore.setUserScore(100.00);
            }
            return superversionScoreService.setSuperversionScore(account, superversionScore);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    @RequestMapping(value = "/deleteSuperversionScore", method = RequestMethod.POST)
    public RetDataBean deleteSuperversionScore(SuperversionScore superversionScore) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(superversionScore.getScoreId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            superversionScore.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, superversionScoreService.deleteSuperversionScore(superversionScore));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    @RequestMapping(value = "/updateSuperversionScore", method = RequestMethod.POST)
    public RetDataBean updateSuperversionScore(SuperversionScore superversionScore) {
        try {
            if (StringUtils.isBlank(superversionScore.getScoreId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(SuperversionScore.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("scoreId", superversionScore.getScoreId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, superversionScoreService.updateSuperversionScore(example, superversionScore));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 任务全配成功
     *
     * @param superversionProcess
     * @return
     */
    @RequestMapping(value = "/insertSuperversionProcess", method = RequestMethod.POST)
    public RetDataBean insertSuperversionProcess(SuperversionProcess superversionProcess) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            superversionProcess.setProcessId(SysTools.getGUID());
            superversionProcess.setStatus("0");
            superversionProcess.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            superversionProcess.setCreateUser(account.getAccountId());
            superversionProcess.setOrgId(account.getOrgId());
            superversionProcess.setChangeStatus("0");
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, superversionProcessService.insertSuperversionProcess(superversionProcess));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 任务删除成功
     *
     * @param superversionProcess
     * @return
     */
    @RequestMapping(value = "/deleteSuperversionProcess", method = RequestMethod.POST)
    public RetDataBean deleteSuperversionProcess(SuperversionProcess superversionProcess) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(superversionProcess.getProcessId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            superversionProcess.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, superversionProcessService.deleteSuperversionProcess(superversionProcess));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 任务更新成功
     *
     * @param superversionProcess
     * @return
     */
    @RequestMapping(value = "/updateSuperversionProcess", method = RequestMethod.POST)
    public RetDataBean updateSuperversionProcess(SuperversionProcess superversionProcess) {
        try {
            if (StringUtils.isBlank(superversionProcess.getProcessId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(SuperversionProcess.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("processId", superversionProcess.getProcessId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, superversionProcessService.updateSuperversionProcess(example, superversionProcess));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: insertSuperversionDelay
     * @Description:  发起延期审批申请
     * @param: request
     * @param: superversionDelay
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/insertSuperversionDelay", method = RequestMethod.POST)
    public RetDataBean insertSuperversionDelay(SuperversionDelay superversionDelay, String superversionId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            Superversion superversion = new Superversion();
            superversion.setOrgId(account.getOrgId());
            superversion.setSuperversionId(superversionId);
            superversion = superversionService.selectOneSuperversion(superversion);
            superversionDelay.setDelayId(SysTools.getGUID());
            superversionDelay.setLeadId(superversion.getLeadId());
            superversionDelay.setPassStatus("0");
            superversionDelay.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            superversionDelay.setCreateUser(account.getAccountId());
            superversionDelay.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_APPROVAL_SUCCESS, superversionDelayService.insertSuperversionDelay(superversionDelay));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    @RequestMapping(value = "/deleteSuperversionDelay", method = RequestMethod.POST)
    public RetDataBean deleteSuperversionDelay(SuperversionDelay superversionDelay) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(superversionDelay.getDelayId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            superversionDelay.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, superversionDelayService.deleteSuperversionDelay(superversionDelay));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 延期更改
     *
     * @param superversionDelay
     * @return
     */
    @Transactional(value = "generalTM")
    @RequestMapping(value = "/updateSuperversionDelay", method = RequestMethod.POST)
    public RetDataBean updateSuperversionDelay(SuperversionDelay superversionDelay) {
        try {
            if (StringUtils.isBlank(superversionDelay.getDelayId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            superversionDelay.setApplyTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            Example example = new Example(SuperversionDelay.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("delayId", superversionDelay.getDelayId());
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, superversionDelayService.updateSuperversionDelay(example, superversionDelay));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 延期审批
     *
     * @param superversionDelay
     * @return
     */
    @Transactional(value = "generalTM")
    @RequestMapping(value = "/updateSuperversionDelayApply", method = RequestMethod.POST)
    public RetDataBean updateSuperversionDelayApply(SuperversionDelay superversionDelay) {
        try {
            if (StringUtils.isBlank(superversionDelay.getDelayId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (superversionDelay.getPassStatus().equals("1")) {
                SuperversionDelay superversionDelay2 = new SuperversionDelay();
                superversionDelay2.setOrgId(account.getOrgId());
                superversionDelay2.setDelayId(superversionDelay.getDelayId());
                superversionDelay2 = superversionDelayService.selectOneSuperversionDelay(superversionDelay2);
                SuperversionProcess superversionProcess = new SuperversionProcess();
                superversionProcess.setEndTime(superversionDelay2.getDelayTime());
                superversionProcess.setStatus("1");
                Example example1 = new Example(SuperversionProcess.class);
                example1.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("processId", superversionDelay2.getProcessId());
                superversionProcessService.updateSuperversionProcess(example1, superversionProcess);
                //处理积分
                SuperversionIntegral superversionIntegral = new SuperversionIntegral();
                superversionIntegral.setOrgId(account.getOrgId());
                superversionIntegral = superversionIntegralService.selectOneSuperversionIntegral(superversionIntegral);
                if (superversionIntegral != null) {
                    SuperversionProcess tmpSuperversionProcess = new SuperversionProcess();
                    tmpSuperversionProcess.setProcessId(superversionDelay2.getProcessId());
                    tmpSuperversionProcess.setOrgId(account.getOrgId());
                    tmpSuperversionProcess = superversionProcessService.selectOneSuperversionProcess(tmpSuperversionProcess);
                    String time1 = tmpSuperversionProcess.getEndTime();
                    String time2 = superversionDelay.getDelayTime();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date date1 = sdf.parse(time1);
                    Date date2 = sdf.parse(time2);
                    long between_days = (date2.getTime() - date1.getTime()) / (1000 * 3600 * 24);
                    DecimalFormat df = new DecimalFormat("#.##");
                    Double resDouble = Double.parseDouble(df.format(between_days));
                    if (resDouble > 0) {
                        superversionScoreService.setDelayScore(account, tmpSuperversionProcess.getSuperversionId(), superversionIntegral, resDouble / superversionIntegral.getDelay());
                    }
                }
            }
            superversionDelay.setApplyTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            Example example = new Example(SuperversionDelay.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("delayId", superversionDelay.getDelayId());
            return RetDataTools.Ok(MessageCode.MESSAGE_APPROVAL_SUCCESS, superversionDelayService.updateSuperversionDelay(example, superversionDelay));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: insertProjectBuildDetails
     * @Description:  创建类型
     * @param: request
     * @param: superversionConfig
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/insertSuperversionConfig", method = RequestMethod.POST)
    public RetDataBean insertSuperversionConfig(SuperversionConfig superversionConfig) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            superversionConfig.setConfigId(SysTools.getGUID());
            superversionConfig.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            superversionConfig.setCreateUser(account.getAccountId());
            superversionConfig.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, superversionConfigService.insertSuperversionConfig(superversionConfig));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: deleteSuperversionConfig
     * @Description:  删除类型
     * @param: request
     * @param: superversionConfig
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/deleteSuperversionConfig", method = RequestMethod.POST)
    public RetDataBean deleteSuperversionConfig(SuperversionConfig superversionConfig) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(superversionConfig.getConfigId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            superversionConfig.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, superversionConfigService.deleteSuperversionConfig(superversionConfig));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: updateSuperversionConfig
     * @Description:  更新分类
     * @param: request
     * @param: superversionConfig
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/updateSuperversionConfig", method = RequestMethod.POST)
    public RetDataBean updateSuperversionConfig(SuperversionConfig superversionConfig) {
        try {
            if (StringUtils.isBlank(superversionConfig.getConfigId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(SuperversionConfig.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("configId", superversionConfig.getConfigId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, superversionConfigService.updateSuperversionConfig(example, superversionConfig));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @Title: insertSuperversion
     * @Description:  创建督查项目
     * @param: request
     * @param: superversion
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/insertSuperversion", method = RequestMethod.POST)
    public RetDataBean insertSuperversion(Superversion superversion) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("superverion:insert")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
            }
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            superversion.setSuperversionId(SysTools.getGUID());
            superversion.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            superversion.setStatus("0");
            superversion.setCreateUser(userInfo.getAccountId());
            superversion.setOrgId(userInfo.getOrgId());
            Document htmlDoc = Jsoup.parse(superversion.getContent());
            String subheading = htmlDoc.text();
            if (subheading.length() > 50) {
                subheading = subheading.substring(0, 50) + "...";
            }
            superversion.setSubheading(subheading);
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, superversionService.createSuperversion(userInfo, superversion));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: deleteSuperversion
     * @Description:  删除督查项目
     * @param: request
     * @param: superversion
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/deleteSuperversion", method = RequestMethod.POST)
    public RetDataBean deleteSuperversion(Superversion superversion) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("superverion:delete")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
            }
            if (StringUtils.isBlank(superversion.getSuperversionId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            superversion.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, superversionService.deleteSuperversion(superversion));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: updateSuperversion
     * @Description:  更新督查项目
     * @param: request
     * @param: superversion
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/updateSuperversion", method = RequestMethod.POST)
    public RetDataBean updateSuperversion(Superversion superversion) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("superverion:update")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
            }
            if (StringUtils.isBlank(superversion.getSuperversionId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Document htmlDoc = Jsoup.parse(superversion.getContent());
            String subheading = htmlDoc.text();
            if (subheading.length() > 50) {
                subheading = subheading.substring(0, 50) + "...";
            }
            superversion.setSubheading(subheading);
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            Example example = new Example(Superversion.class);
            example.createCriteria().andEqualTo("orgId", userInfo.getOrgId()).andEqualTo("superversionId", superversion.getSuperversionId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, superversionService.updateSuperversion(userInfo, example, superversion));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新督查督办状态
     *
     * @param superversion
     * @return
     */
    @RequestMapping(value = "/updateSuperversionStatus", method = RequestMethod.POST)
    public RetDataBean updateSuperversionStatus(Superversion superversion) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("superverion:update")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
            }
            if (StringUtils.isBlank(superversion.getSuperversionId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(Superversion.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("superversionId", superversion.getSuperversionId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, superversionService.updateSuperversion(example, superversion));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 设置督查督办为完成状态
     *
     * @param superversion
     * @return
     */
    @Transactional(value = "generalTM")
    @RequestMapping(value = "/setFinishStatus", method = RequestMethod.POST)
    public RetDataBean setFinishStatus(Superversion superversion) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("superverion:update")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
            }
            if (StringUtils.isBlank(superversion.getSuperversionId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            superversion.setStatus("2");
            Account account = accountService.getRedisAUserInfoToAccount();

            //处理积分
            SuperversionIntegral superversionIntegral = new SuperversionIntegral();
            superversionIntegral.setOrgId(account.getOrgId());
            superversionIntegral = superversionIntegralService.selectOneSuperversionIntegral(superversionIntegral);
            if (superversionIntegral != null) {
                Superversion tmpSuperversion = new Superversion();
                tmpSuperversion.setSuperversionId(superversion.getSuperversionId());
                tmpSuperversion.setOrgId(account.getOrgId());
                tmpSuperversion = superversionService.selectOneSuperversion(tmpSuperversion);
                String time1 = tmpSuperversion.getEndTime();
                String time2 = superversion.getFinishTime();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date date1 = sdf.parse(time1);
                Date date2 = sdf.parse(time2);
                long between_days = (date2.getTime() - date1.getTime()) / (1000 * 3600 * 24);
                DecimalFormat df = new DecimalFormat("#.##");
                Double resDouble = Double.parseDouble(df.format(between_days));
                if (resDouble > 0) {
                    superversionScoreService.setPassTimeScore(account, superversion.getSuperversionId(), superversionIntegral, resDouble / superversionIntegral.getPassTime());
                }
            }
            Example example = new Example(Superversion.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("superversionId", superversion.getSuperversionId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, superversionService.updateSuperversion(example, superversion));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


}
