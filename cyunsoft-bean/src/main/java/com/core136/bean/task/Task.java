package com.core136.bean.task;

import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "task")
public class Task implements Serializable {
    /**
     * Time : 2017 2017年10月17日 下午2:38:29
     * Author : LSQ
     *
     * @param request
     * @param response
     * Exception
     */
    private static final long serialVersionUID = 1L;
    private String taskId;
    private String taskName;
    private String taskType;
    private String deptPriv;
    private String userPriv;
    private String levelPriv;
    private String chargeAccountId;
    private String participantAccountId;
    private String supervisorAccountId;
    private String attach;
    private String attachPriv;
    private String beginTime;
    private String isTop;
    private String endTime;
    private String msgType;
    private String remark;
    private String createTime;
    private String createUser;
    private String status;
    private String duration;
    private String orgId;

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getIsTop() {
        return isTop;
    }

    public void setIsTop(String isTop) {
        this.isTop = isTop;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskType() {
        return taskType;
    }

    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }

    public String getDeptPriv() {
        return deptPriv;
    }

    public void setDeptPriv(String deptPriv) {
        this.deptPriv = deptPriv;
    }

    public String getChargeAccountId() {
        return chargeAccountId;
    }

    public void setChargeAccountId(String chargeAccountId) {
        this.chargeAccountId = chargeAccountId;
    }

    public String getParticipantAccountId() {
        return participantAccountId;
    }

    public void setParticipantAccountId(String participantAccountId) {
        this.participantAccountId = participantAccountId;
    }

    public String getSupervisorAccountId() {
        return supervisorAccountId;
    }

    public void setSupervisorAccountId(String supervisorAccountId) {
        this.supervisorAccountId = supervisorAccountId;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getUserPriv() {
        return userPriv;
    }

    public void setUserPriv(String userPriv) {
        this.userPriv = userPriv;
    }

    public String getLevelPriv() {
        return levelPriv;
    }

    public void setLevelPriv(String levelPriv) {
        this.levelPriv = levelPriv;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAttachPriv() {
        return attachPriv;
    }

    public void setAttachPriv(String attachPriv) {
        this.attachPriv = attachPriv;
    }

}
