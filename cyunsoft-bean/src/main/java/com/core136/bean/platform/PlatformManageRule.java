package com.core136.bean.platform;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: PlatformManageRule
 * @Description: 智能管理平台数据管理业务规则
 * @author: 稠云技术
 * @date: 2020年12月31日 下午3:39:11
 * @Copyright: 2020 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "platform_manage_rule")
public class PlatformManageRule implements Serializable {


    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String recordId;
    private String sysMenuId;
    private String queryFields;
    private String checkedFlag;
    private Integer pageSize;
    private String dataRange;
    private String bootstrapTableColumns;
    private String querySql;
    private String tableField;
    private Boolean search;
    private Boolean clickToSelect;
    private Boolean showRefresh;
    private Boolean showColumns;
    private String idField;
    private String optFun;
    private String formatter;
    private String createUser;
    private String createTime;
    private String orgId;

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getSysMenuId() {
        return sysMenuId;
    }

    public void setSysMenuId(String sysMenuId) {
        this.sysMenuId = sysMenuId;
    }

    public String getQueryFields() {
        return queryFields;
    }

    public void setQueryFields(String queryFields) {
        this.queryFields = queryFields;
    }

    public String getBootstrapTableColumns() {
        return bootstrapTableColumns;
    }

    public void setBootstrapTableColumns(String bootstrapTableColumns) {
        this.bootstrapTableColumns = bootstrapTableColumns;
    }

    public String getDataRange() {
        return dataRange;
    }

    public void setDataRange(String dataRange) {
        this.dataRange = dataRange;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getQuerySql() {
        return querySql;
    }

    public void setQuerySql(String querySql) {
        this.querySql = querySql;
    }

    public Boolean getSearch() {
        return search;
    }

    public void setSearch(Boolean search) {
        this.search = search;
    }

    public Boolean getClickToSelect() {
        return clickToSelect;
    }

    public void setClickToSelect(Boolean clickToSelect) {
        this.clickToSelect = clickToSelect;
    }

    public Boolean getShowRefresh() {
        return showRefresh;
    }

    public void setShowRefresh(Boolean showRefresh) {
        this.showRefresh = showRefresh;
    }

    public Boolean getShowColumns() {
        return showColumns;
    }

    public void setShowColumns(Boolean showColumns) {
        this.showColumns = showColumns;
    }

    public String getIdField() {
        return idField;
    }

    public void setIdField(String idField) {
        this.idField = idField;
    }

    public String getFormatter() {
        return formatter;
    }

    public void setFormatter(String formatter) {
        this.formatter = formatter;
    }

    public String getOptFun() {
        return optFun;
    }

    public void setOptFun(String optFun) {
        this.optFun = optFun;
    }

    public String getCheckedFlag() {
        return checkedFlag;
    }

    public void setCheckedFlag(String checkedFlag) {
        this.checkedFlag = checkedFlag;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getTableField() {
        return tableField;
    }

    public void setTableField(String tableField) {
        this.tableField = tableField;
    }
}
