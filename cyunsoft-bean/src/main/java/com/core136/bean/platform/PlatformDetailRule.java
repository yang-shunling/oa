package com.core136.bean.platform;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * 详情设置
 */
@Table(name = "platform_detail_rule")
public class PlatformDetailRule implements Serializable {
    private String recordId;
    private String hideFields;
    private String printFlag;
    private String docFlag;
    private String pdfFlag;
    private String title;
    private String formId;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getHideFields() {
        return hideFields;
    }

    public void setHideFields(String hideFields) {
        this.hideFields = hideFields;
    }

    public String getPrintFlag() {
        return printFlag;
    }

    public void setPrintFlag(String printFlag) {
        this.printFlag = printFlag;
    }

    public String getDocFlag() {
        return docFlag;
    }

    public void setDocFlag(String docFlag) {
        this.docFlag = docFlag;
    }

    public String getPdfFlag() {
        return pdfFlag;
    }

    public void setPdfFlag(String pdfFlag) {
        this.pdfFlag = pdfFlag;
    }

    public String getFormId() {
        return formId;
    }

    public void setFormId(String formId) {
        this.formId = formId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
