/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: BpmChildProcess.java
 * @Package com.core136.bean.bpm
 * @Description: 描述
 * @author: lsq
 * @date: 2020年1月13日 下午3:46:53
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.bean.bpm;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author lsq
 *子流程步骤配置
 */
@Table(name = "bpm_child_process")
public class BpmChildProcess implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String childProcessId;
    private String processId;
    private String childFlowId;
    private String dataMapping;
    private String endOpt;
    private String copyPublicFile;
    private String createUser;
    private String createTime;
    private String orgId;

    /**
     * @return the childProcessId
     */
    public String getChildProcessId() {
        return childProcessId;
    }

    /**
     * @param childProcessId the childProcessId to set
     */
    public void setChildProcessId(String childProcessId) {
        this.childProcessId = childProcessId;
    }

    public String getCopyPublicFile() {
        return copyPublicFile;
    }

    public void setCopyPublicFile(String copyPublicFile) {
        this.copyPublicFile = copyPublicFile;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    /**
     * @return the processId
     */
    public String getProcessId() {
        return processId;
    }

    /**
     * @param processId the processId to set
     */
    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getChildFlowId() {
        return childFlowId;
    }

    public void setChildFlowId(String childFlowId) {
        this.childFlowId = childFlowId;
    }

    /**
     * @return the dataMapping
     */
    public String getDataMapping() {
        return dataMapping;
    }

    /**
     * @param dataMapping the dataMapping to set
     */
    public void setDataMapping(String dataMapping) {
        this.dataMapping = dataMapping;
    }

    /**
     * @return the createUser
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * @param createUser the createUser to set
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * @return the createTime
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime the createTime to set
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    /**
     * @return the orgId
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * @param orgId the orgId to set
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getEndOpt() {
        return endOpt;
    }

    public void setEndOpt(String endOpt) {
        this.endOpt = endOpt;
    }

}
