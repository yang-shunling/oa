/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: OfficeSuppliesApply.java
 * @Package com.core136.bean.officesupplies
 * @Description: 描述
 * @author: lsq
 * @date: 2019年11月25日 上午8:56:15
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.bean.officesupplies;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author lsq
 *
 */
@Table(name = "office_supplies_apply")
public class OfficeSuppliesApply implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String applyId;
    private String title;
    private String suppliesId;
    private String remark;
    private Integer quantity;
    private String usedUser;
    private String status;
    private String applyUser;
    private String createUser;
    private String createTime;
    private String orgId;

    /**
     * @return the applyId
     */
    public String getApplyId() {
        return applyId;
    }

    /**
     * @param applyId the applyId to set
     */
    public void setApplyId(String applyId) {
        this.applyId = applyId;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the suppliesId
     */
    public String getSuppliesId() {
        return suppliesId;
    }

    /**
     * @param suppliesId the suppliesId to set
     */
    public void setSuppliesId(String suppliesId) {
        this.suppliesId = suppliesId;
    }

    /**
     * @return the remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark the remark to set
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * @return the usedUser
     */
    public String getUsedUser() {
        return usedUser;
    }

    /**
     * @param usedUser the usedUser to set
     */
    public void setUsedUser(String usedUser) {
        this.usedUser = usedUser;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the applyUser
     */
    public String getApplyUser() {
        return applyUser;
    }

    /**
     * @param applyUser the applyUser to set
     */
    public void setApplyUser(String applyUser) {
        this.applyUser = applyUser;
    }

    /**
     * @return the createTime
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime the createTime to set
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    /**
     * @return the orgId
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * @param orgId the orgId to set
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    /**
     * @return the createUser
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * @param createUser the createUser to set
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

}
