/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: OfficeSuppliesApproval.java
 * @Package com.core136.bean.officesupplies
 * @Description: 描述
 * @author: lsq
 * @date: 2019年11月25日 上午9:51:24
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.bean.officesupplies;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author lsq
 *
 */
@Table(name = "office_supplies_approval")
public class OfficeSuppliesApproval implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String approvalId;
    private String applyId;
    private String status;
    private String ideaText;
    private String createUser;
    private String createTime;
    private String orgId;

    /**
     * @return the approvalId
     */
    public String getApprovalId() {
        return approvalId;
    }

    /**
     * @param approvalId the approvalId to set
     */
    public void setApprovalId(String approvalId) {
        this.approvalId = approvalId;
    }

    /**
     * @return the applyId
     */
    public String getApplyId() {
        return applyId;
    }

    /**
     * @param applyId the applyId to set
     */
    public void setApplyId(String applyId) {
        this.applyId = applyId;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the ideaText
     */
    public String getIdeaText() {
        return ideaText;
    }

    /**
     * @param ideaText the ideaText to set
     */
    public void setIdeaText(String ideaText) {
        this.ideaText = ideaText;
    }

    /**
     * @return the createUser
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * @param createUser the createUser to set
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * @return the createTime
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime the createTime to set
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    /**
     * @return the orgId
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * @param orgId the orgId to set
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }


}
