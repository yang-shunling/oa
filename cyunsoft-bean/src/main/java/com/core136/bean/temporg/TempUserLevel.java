/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: UserLevel.java
 * @Package com.core136.bean.account
 * @Description: (用一句话描述该文件做什么)
 * @author: 刘绍全
 * @date: 2019年1月24日 下午8:04:22
 * @version V1.0
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术股份有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
package com.core136.bean.temporg;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: UserLevel
 * @Description: 用户行政级别
 * @author: 刘绍全
 * @date: 2019年1月24日 下午8:04:22
 *
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云科信息技术股份有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "temp_user_level")
public class TempUserLevel implements Serializable {
    private static final long serialVersionUID = 1L;

    private String levelId;
    private String levelName;
    private String levelNoId;
    private String superior;
    private String tempOrgId;
    private String createUser;
    private String createTime;
    private String orgId;

    public String getLevelId() {
        return levelId;
    }

    public void setLevelId(String levelId) {
        this.levelId = levelId;
    }

    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

    public String getLevelNoId() {
        return levelNoId;
    }

    public void setLevelNoId(String levelNoId) {
        this.levelNoId = levelNoId;
    }

    public String getSuperior() {
        return superior;
    }

    public void setSuperior(String superior) {
        this.superior = superior;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getTempOrgId() {
        return tempOrgId;
    }

    public void setTempOrgId(String tempOrgId) {
        this.tempOrgId = tempOrgId;
    }


}
