package com.core136.bean.archives;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: ArchivesRepository
 * @Description: 档案库管理
 * @author: 稠云技术
 * @date: 2020年7月3日 下午3:54:53
 * @Copyright: 2020 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "archives_repository")
public class ArchivesRepository implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Integer sortNo;
    private String repositoryId;
    private String title;
    private String userPriv;
    private String deptPriv;
    private String levelPriv;
    private String manageUser;
    private String remark;
    private String createTime;
    private String createUser;
    private String orgId;

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getRepositoryId() {
        return repositoryId;
    }

    public void setRepositoryId(String repositoryId) {
        this.repositoryId = repositoryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUserPriv() {
        return userPriv;
    }

    public void setUserPriv(String userPriv) {
        this.userPriv = userPriv;
    }

    public String getDeptPriv() {
        return deptPriv;
    }

    public void setDeptPriv(String deptPriv) {
        this.deptPriv = deptPriv;
    }

    public String getLevelPriv() {
        return levelPriv;
    }

    public void setLevelPriv(String levelPriv) {
        this.levelPriv = levelPriv;
    }

    public String getManageUser() {
        return manageUser;
    }

    public void setManageUser(String manageUser) {
        this.manageUser = manageUser;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }


}
