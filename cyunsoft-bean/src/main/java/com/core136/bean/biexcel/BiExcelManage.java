package com.core136.bean.biexcel;

import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "bi_excel_manage")
public class BiExcelManage implements Serializable {
    private String manageId;
    private String title;
    private String status;
    private String beginTime;
    private String endTime;
    private String biExcelSort;
    private Integer sortNo;
    private String userPriv;
    private String deptPriv;
    private String levelPriv;
    private String excelTemplate;
    private String remark;
    private String createUser;
    private String createTime;
    private String orgId;

    public String getManageId() {
        return manageId;
    }

    public void setManageId(String manageId) {
        this.manageId = manageId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getBiExcelSort() {
        return biExcelSort;
    }

    public void setBiExcelSort(String biExcelSort) {
        this.biExcelSort = biExcelSort;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getUserPriv() {
        return userPriv;
    }

    public void setUserPriv(String userPriv) {
        this.userPriv = userPriv;
    }

    public String getDeptPriv() {
        return deptPriv;
    }

    public void setDeptPriv(String deptPriv) {
        this.deptPriv = deptPriv;
    }

    public String getLevelPriv() {
        return levelPriv;
    }

    public void setLevelPriv(String levelPriv) {
        this.levelPriv = levelPriv;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getExcelTemplate() {
        return excelTemplate;
    }

    public void setExcelTemplate(String excelTemplate) {
        this.excelTemplate = excelTemplate;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }
}
