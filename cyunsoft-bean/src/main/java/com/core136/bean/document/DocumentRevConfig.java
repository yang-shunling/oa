package com.core136.bean.document;

import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "document_rev_config")
public class DocumentRevConfig implements Serializable {
    private String configId;
    private String flowId;
    private String createTime;
    private String createUser;
    private String orgId;

}
