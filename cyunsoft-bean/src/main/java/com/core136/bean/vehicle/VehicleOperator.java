package com.core136.bean.vehicle;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: VehicleOperator
 * @Description: 车辆调度员设置
 * @author: 稠云技术
 * @date: 2020年5月8日 下午8:17:23
 * @Copyright: 2020 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "vehicle_operator")
public class VehicleOperator implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String configId;
    private String optUser;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getConfigId() {
        return configId;
    }

    public void setConfigId(String configId) {
        this.configId = configId;
    }

    public String getOptUser() {
        return optUser;
    }

    public void setOptUser(String optUser) {
        this.optUser = optUser;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

}
