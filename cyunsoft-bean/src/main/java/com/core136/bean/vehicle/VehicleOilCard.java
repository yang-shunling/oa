package com.core136.bean.vehicle;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: VehicleOilCard
 * @Description: 油卡管理
 * @author: 稠云技术
 * @date: 2020年5月9日 下午3:16:37
 * @Copyright: 2020 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "vehicle_oil_card")
public class VehicleOilCard implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String cardId;
    private Integer sortNo;
    private String cardNo;
    private String cardCode;
    private String passWord;
    private String oilType;
    private String oilComp;
    private String status;
    private Double balance;
    private String cardTime;
    private String cardUser;
    private String remark;
    private String createUser;
    private String createTime;
    private String orgId;

    public String getCardId() {
        return cardId;
    }

    public String getCardCode() {
        return cardCode;
    }

    public void setCardCode(String cardCode) {
        this.cardCode = cardCode;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getOilType() {
        return oilType;
    }

    public void setOilType(String oilType) {
        this.oilType = oilType;
    }

    public String getOilComp() {
        return oilComp;
    }

    public void setOilComp(String oilComp) {
        this.oilComp = oilComp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getCardTime() {
        return cardTime;
    }

    public void setCardTime(String cardTime) {
        this.cardTime = cardTime;
    }

    public String getCardUser() {
        return cardUser;
    }

    public void setCardUser(String cardUser) {
        this.cardUser = cardUser;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

}
