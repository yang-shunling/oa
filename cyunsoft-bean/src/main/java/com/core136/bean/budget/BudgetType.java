package com.core136.bean.budget;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: BudgetType
 * @Description: 预算类型分类
 * @author: 稠云技术
 * @date: 2020年9月21日 下午11:32:03
 * @Copyright: 2020 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "budget_type")
public class BudgetType implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String budgetTypeId;
    private Integer sortNo;
    private String title;
    private String remark;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getBudgetTypeId() {
        return budgetTypeId;
    }

    public void setBudgetTypeId(String budgetTypeId) {
        this.budgetTypeId = budgetTypeId;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

}
