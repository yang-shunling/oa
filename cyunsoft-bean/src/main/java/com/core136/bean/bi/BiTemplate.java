package com.core136.bean.bi;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: BiTemplate
 * @Description: 报表模版
 * @author: 稠云技术
 * @date: 2020年9月10日 下午3:10:47
 * @Copyright: 2020 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "bi_template")
public class BiTemplate implements Serializable {
    private static final long serialVersionUID = 1L;
    private int sortNo;
    private String templateId;
    private String templateName;
    private String version;
    private String jasTemplate;
    private String pagingFlag;
    private String showType;
    private String levelId;
    private String userPriv;
    private String dbSource;
    private String createUser;
    private String createTime;
    private String orgId;

    public int getSortNo() {
        return sortNo;
    }

    public void setSortNo(int sortNo) {
        this.sortNo = sortNo;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getLevelId() {
        return levelId;
    }

    public void setLevelId(String levelId) {
        this.levelId = levelId;
    }

    public String getUserPriv() {
        return userPriv;
    }

    public void setUserPriv(String userPriv) {
        this.userPriv = userPriv;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getJasTemplate() {
        return jasTemplate;
    }

    public void setJasTemplate(String jasTemplate) {
        this.jasTemplate = jasTemplate;
    }

    public String getPagingFlag() {
        return pagingFlag;
    }

    public void setPagingFlag(String pagingFlag) {
        this.pagingFlag = pagingFlag;
    }

    public String getShowType() {
        return showType;
    }

    public void setShowType(String showType) {
        this.showType = showType;
    }

    public String getDbSource() {
        return dbSource;
    }

    public void setDbSource(String dbSource) {
        this.dbSource = dbSource;
    }

}
