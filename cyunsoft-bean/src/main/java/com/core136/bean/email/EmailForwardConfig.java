package com.core136.bean.email;

import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "email_forward_config")
public class EmailForwardConfig implements Serializable {
    private String configId;
    private String optType;
    private String optAccountId;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getConfigId() {
        return configId;
    }

    public void setConfigId(String configId) {
        this.configId = configId;
    }

    public String getOptType() {
        return optType;
    }

    public void setOptType(String optType) {
        this.optType = optType;
    }

    public String getOptAccountId() {
        return optAccountId;
    }

    public void setOptAccountId(String optAccountId) {
        this.optAccountId = optAccountId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }
}
