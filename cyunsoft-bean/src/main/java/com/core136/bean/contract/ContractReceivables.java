package com.core136.bean.contract;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: ContractReceivables
 * @Description: 合同应收款
 * @author: 稠云技术
 * @date: 2020年3月7日 上午11:15:12
 * @Copyright: 2020 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "contract_receivables")
public class ContractReceivables implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String receivablesId;
    private String contractId;
    private Double UnReceived;
    private String userPriv;
    /**
     * 应收款日期
     */
    private String receivablesTime;
    private String attach;
    private String remark;

    private Double received;
    /**
     * 真正收款日期
     */
    private String receivedTime;
    private String createTime;
    private String createUser;
    private String orgId;

    public Double getReceived() {
        return received;
    }

    public void setReceived(Double received) {
        this.received = received;
    }

    public Double getUnReceived() {
        return UnReceived;
    }

    public void setUnReceived(Double unReceived) {
        UnReceived = unReceived;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getReceivablesId() {
        return receivablesId;
    }

    public void setReceivablesId(String receivablesId) {
        this.receivablesId = receivablesId;
    }

    public String getReceivablesTime() {
        return receivablesTime;
    }

    public void setReceivablesTime(String receivablesTime) {
        this.receivablesTime = receivablesTime;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getReceivedTime() {
        return receivedTime;
    }

    public void setReceivedTime(String receivedTime) {
        this.receivedTime = receivedTime;
    }

    public String getUserPriv() {
        return userPriv;
    }

    public void setUserPriv(String userPriv) {
        this.userPriv = userPriv;
    }


}
