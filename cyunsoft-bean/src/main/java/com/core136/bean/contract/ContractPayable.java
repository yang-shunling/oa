package com.core136.bean.contract;

import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "contract_payable")
public class ContractPayable implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String payableId;
    private String contractId;
    private Double payabled;
    private Double unPayabled;
    /**
     * 需付款时间
     */
    private String payableTime;
    /**
     * 真正付款时间
     */
    private String payabledTime;
    private String remark;
    /**
     * 付款凭证
     */
    private String attach;
    private String userPriv;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getPayableId() {
        return payableId;
    }

    public void setPayableId(String payableId) {
        this.payableId = payableId;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getPayableTime() {
        return payableTime;
    }

    public void setPayableTime(String payableTime) {
        this.payableTime = payableTime;
    }

    public String getPayabledTime() {
        return payabledTime;
    }

    public void setPayabledTime(String payabledTime) {
        this.payabledTime = payabledTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public Double getPayabled() {
        return payabled;
    }

    public void setPayabled(Double payabled) {
        this.payabled = payabled;
    }

    public Double getUnPayabled() {
        return unPayabled;
    }

    public void setUnPayabled(Double unPayabled) {
        this.unPayabled = unPayabled;
    }

    public String getUserPriv() {
        return userPriv;
    }

    public void setUserPriv(String userPriv) {
        this.userPriv = userPriv;
    }

}
