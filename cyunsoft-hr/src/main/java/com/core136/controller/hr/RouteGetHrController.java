/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: RoutGetHrController.java
 * @Package com.core136.controller.hr
 * @Description: 描述
 * @author: lsq
 * @date: 2019年11月14日 下午1:21:05
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.controller.hr;

import com.core136.bean.account.Account;
import com.core136.bean.hr.*;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.service.account.AccountService;
import com.core136.service.hr.*;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 */
@RestController
@RequestMapping("/ret/hrget")
public class RouteGetHrController {
    private HrDepartmentService hrDepartmentService;

    @Autowired
    public void setHrDepartmentService(HrDepartmentService hrDepartmentService) {
        this.hrDepartmentService = hrDepartmentService;
    }

    private HrUserInfoService hrUserInfoService;

    @Autowired
    public void setHrUserInfoService(HrUserInfoService hrUserInfoService) {
        this.hrUserInfoService = hrUserInfoService;
    }

    private HrUserLevelService hrUserLevelService;

    @Autowired
    public void setHrUserLevelService(HrUserLevelService hrUserLevelService) {
        this.hrUserLevelService = hrUserLevelService;
    }

    private HrWagesLevelService hrWagesLevelService;

    @Autowired
    public void setHrWagesLevelService(HrWagesLevelService hrWagesLevelService) {
        this.hrWagesLevelService = hrWagesLevelService;
    }

    private HrClassCodeService hrClassCodeService;

    @Autowired
    public void setHrClassCodeService(HrClassCodeService hrClassCodeService) {
        this.hrClassCodeService = hrClassCodeService;
    }

    private HrContractService hrContractService;

    @Autowired
    public void setHrContractService(HrContractService hrContractService) {
        this.hrContractService = hrContractService;
    }

    private HrIncentiveService hrIncentiveService;

    @Autowired
    public void setHrIncentiveService(HrIncentiveService hrIncentiveService) {
        this.hrIncentiveService = hrIncentiveService;
    }

    private HrLicenceService hrLicenceService;

    @Autowired
    public void setHrLicenceService(HrLicenceService hrLicenceService) {
        this.hrLicenceService = hrLicenceService;
    }

    private HrLearnRecordService hrLearnRecordService;

    @Autowired
    public void setHrLearnRecordService(HrLearnRecordService hrLearnRecordService) {
        this.hrLearnRecordService = hrLearnRecordService;
    }

    private HrWorkRecordService hrWorkRecordService;

    @Autowired
    public void setHrWorkRecordService(HrWorkRecordService hrWorkRecordService) {
        this.hrWorkRecordService = hrWorkRecordService;
    }

    private HrWorkSkillsService hrWorkSkillsService;

    @Autowired
    public void setHrWorkSkillsService(HrWorkSkillsService hrWorkSkillsService) {
        this.hrWorkSkillsService = hrWorkSkillsService;
    }

    private HrPersonnelTransferService hrPersonnelTransferService;

    @Autowired
    public void setHrPersonnelTransferService(HrPersonnelTransferService hrPersonnelTransferService) {
        this.hrPersonnelTransferService = hrPersonnelTransferService;
    }

    private HrLeaveRecordService hrLeaveRecordService;

    @Autowired
    public void setHrLeaveRecordService(HrLeaveRecordService hrLeaveRecordService) {
        this.hrLeaveRecordService = hrLeaveRecordService;
    }

    private HrReinstatementService hrReinstatementService;

    @Autowired
    public void setHrReinstatementService(HrReinstatementService hrReinstatementService) {
        this.hrReinstatementService = hrReinstatementService;
    }

    private HrTitleEvaluationService hrTitleEvaluationService;

    @Autowired
    public void setHrTitleEvaluationService(HrTitleEvaluationService hrTitleEvaluationService) {
        this.hrTitleEvaluationService = hrTitleEvaluationService;
    }

    private HrCareRecordService hrCareRecordService;

    @Autowired
    public void setHrCareRecordService(HrCareRecordService hrCareRecordService) {
        this.hrCareRecordService = hrCareRecordService;
    }

    private HrTrainRecordService hrTrainRecordService;

    @Autowired
    public void setHrTrainRecordService(HrTrainRecordService hrTrainRecordService) {
        this.hrTrainRecordService = hrTrainRecordService;
    }

    private HrRecruitNeedsService hrRecruitNeedsService;

    @Autowired
    public void setHrRecruitNeedsService(HrRecruitNeedsService hrRecruitNeedsService) {
        this.hrRecruitNeedsService = hrRecruitNeedsService;
    }

    private HrKpiItemService hrKpiItemService;

    @Autowired
    public void setHrKpiItemService(HrKpiItemService hrKpiItemService) {
        this.hrKpiItemService = hrKpiItemService;
    }

    private HrRecruitPlanService hrRecruitPlanService;

    @Autowired
    public void setHrRecruitPlanService(HrRecruitPlanService hrRecruitPlanService) {
        this.hrRecruitPlanService = hrRecruitPlanService;
    }

    private HrSalaryRecordService hrSalaryRecordService;

    @Autowired
    public void setHrSalaryRecordService(HrSalaryRecordService hrSalaryRecordService) {
        this.hrSalaryRecordService = hrSalaryRecordService;
    }

    private HrWelfareRecordService hrWelfareRecordService;

    @Autowired
    public void setHrWelfareRecordService(HrWelfareRecordService hrWelfareRecordService) {
        this.hrWelfareRecordService = hrWelfareRecordService;
    }

    private HrEvaluateService hrEvaluateService;

    @Autowired
    public void setHrEvaluateService(HrEvaluateService hrEvaluateService) {
        this.hrEvaluateService = hrEvaluateService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private HrKpiPlanService hrKpiPlanService;

    @Autowired
    public void setHrKpiPlanService(HrKpiPlanService hrKpiPlanService) {
        this.hrKpiPlanService = hrKpiPlanService;
    }

    private HrKpiPlanItemService hrKpiPlanItemService;

    @Autowired
    public void setHrKpiPlanItemService(HrKpiPlanItemService hrKpiPlanItemService) {
        this.hrKpiPlanItemService = hrKpiPlanItemService;
    }

    private HrKpiPlanRecordService hrKpiPlanRecordService;

    @Autowired
    public void setHrKpiPlanRecordService(HrKpiPlanRecordService hrKpiPlanRecordService) {
        this.hrKpiPlanRecordService = hrKpiPlanRecordService;
    }

    private HrRecruitTaskService hrRecruitTaskService;

    @Autowired
    public void setHrRecruitTaskService(HrRecruitTaskService hrRecruitTaskService) {
        this.hrRecruitTaskService = hrRecruitTaskService;
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getHrRecruitTaskList
     * @Description:  获取招聘任务列表
     */
    @RequestMapping(value = "/getHrRecruitTaskList", method = RequestMethod.POST)
    public RetDataBean getHrRecruitTaskList(PageParam pageParam, String beginTime, String endTime
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("t.create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = hrRecruitTaskService.getHrRecruitTaskList(pageParam, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrRecruitTask
     * @return RetDataBean
     * @Title: getHrRecruitTaskById
     * @Description:  获取任务详情
     */
    @RequestMapping(value = "/getHrRecruitTaskById", method = RequestMethod.POST)
    public RetDataBean getHrRecruitTaskById(HrRecruitTask hrRecruitTask) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrRecruitTask.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrRecruitTaskService.selectOneHrRecruitTask(hrRecruitTask));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @return RetDataBean
     * @Title: getMyKpiPlanForList
     * @Description:  获取个人考核列表
     */
    @RequestMapping(value = "/getMyKpiPlanForList", method = RequestMethod.POST)
    public RetDataBean getMyKpiPlanForList(PageParam pageParam) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("l.create_time");
            } else {
                pageParam.setSort("l." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = hrKpiPlanService.getMyKpiPlanForList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param planId
     * @param accountId
     * @return RetDataBean
     * @Title: getMyHrKpiScoreList
     * @Description:  获取个人考核分数明细
     */
    @RequestMapping(value = "/getMyHrKpiScoreList", method = RequestMethod.POST)
    public RetDataBean getMyHrKpiScoreList(String planId, String accountId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrKpiPlanRecordService.getMyHrKpiScoreList(account.getOrgId(), planId, accountId));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getKpiPlanForListForSelect
     * @Description:  获取考核计划列表
     */
    @RequestMapping(value = "/getKpiPlanForListForSelect", method = RequestMethod.POST)
    public RetDataBean getKpiPlanForListForSelect() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrKpiPlanService.getKpiPlanForListForSelect(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param planId
     * @param chargeUser
     * @param accountId
     * @return RetDataBean
     * @Title: getKpiPlanForUserQueryList
     * @Description:  查询获取考核列表
     */
    @RequestMapping(value = "/getKpiPlanForUserQueryList", method = RequestMethod.POST)
    public RetDataBean getKpiPlanForUserQueryList(PageParam pageParam,
                                                  String planId, String chargeUser, String accountId) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("createTime");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setAccountId(accountId);
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = hrKpiPlanService.getKpiPlanForUserQueryList(pageParam, planId, chargeUser);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @return RetDataBean
     * @Title: getKpiPlanForUserList
     * @Description:  获取人员考核列表
     */
    @RequestMapping(value = "/getKpiPlanForUserList", method = RequestMethod.POST)
    public RetDataBean getKpiPlanForUserList(PageParam pageParam) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("l.sort_no");
            } else {
                pageParam.setSort("l." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = hrKpiPlanService.getKpiPlanForUserList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param planId
     * @param accountId
     * @return RetDataBean
     * @Title: getKipItemForUserById
     * @Description:  获取人员考核指标集
     */
    @RequestMapping(value = "/getKipItemForUserById", method = RequestMethod.POST)
    public RetDataBean getKipItemForUserById(String planId, String accountId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrKpiPlanService.getKipItemForUserById(account.getOrgId(), planId, accountId, account.getAccountId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param planId
     * @return RetDataBean
     * @Title: getHrKpiPlanItemList
     * @Description:  获取考核计划的指标集
     */
    @RequestMapping(value = "/getHrKpiPlanItemList", method = RequestMethod.POST)
    public RetDataBean getHrKpiPlanItemList(String planId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrKpiPlanItemService.getHrKpiPlanItemList(account.getOrgId(), planId));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param status
     * @param kpiRule
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getMyHrKpiPlanList
     * @Description:  获取考核管理列表
     */
    @RequestMapping(value = "/getMyHrKpiPlanList", method = RequestMethod.POST)
    public RetDataBean getMyHrKpiPlanList(PageParam pageParam, String status, String kpiRule,
                                          String beginTime, String endTime
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("sort_no");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = hrKpiPlanService.getMyHrKpiPlanList(pageParam, status, kpiRule, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param kpiType
     * @return RetDataBean
     * @Title: getHrKpiItemListForSelect
     * @Description:  获取考核指标集
     */
    @RequestMapping(value = "/getHrKpiItemListForSelect", method = RequestMethod.POST)
    public RetDataBean getHrKpiItemListForSelect(String kpiType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrKpiItemService.getHrKpiItemListForSelect(account.getOrgId(), kpiType));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @return RetDataBean
     * @Title: getMyHrSalaryRecordList
     * @Description:  获取人员薪资列表
     */
    @RequestMapping(value = "/getMyHrSalaryRecordList", method = RequestMethod.POST)
    public RetDataBean getMyHrSalaryRecordList(PageParam pageParam) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("s.create_time");
            } else {
                pageParam.setSort("s." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = hrSalaryRecordService.getMyHrSalaryRecordList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @return RetDataBean
     * @Title: getMyHrPersonnelTransferList
     * @Description:  人个工作调动记录
     */
    @RequestMapping(value = "/getMyHrPersonnelTransferList", method = RequestMethod.POST)
    public RetDataBean getMyHrPersonnelTransferList(PageParam pageParam) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("p.create_time");
            } else {
                pageParam.setSort("p." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = hrPersonnelTransferService.getMyHrPersonnelTransferList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @return RetDataBean
     * @Title: getMyHrWorkRecordList
     * @Description:  查询个人工作经历
     */
    @RequestMapping(value = "/getMyHrWorkRecordList", method = RequestMethod.POST)
    public RetDataBean getMyHrWorkRecordList(PageParam pageParam) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("w.create_time");
            } else {
                pageParam.setSort("w." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = hrWorkRecordService.getMyHrWorkRecordList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @return RetDataBean
     * @Title: getMyHrLearnRecordList
     * @Description:  个人查询学习记录
     */
    @RequestMapping(value = "/getMyHrLearnRecordList", method = RequestMethod.POST)
    public RetDataBean getMyHrLearnRecordList(PageParam pageParam) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("l.create_time");
            } else {
                pageParam.setSort("l." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = hrLearnRecordService.getMyHrLearnRecordList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @return RetDataBean
     * @Title: getMyHrLicenceList
     * @Description:  查询个人证照信息
     */
    @RequestMapping(value = "/getMyHrLicenceList", method = RequestMethod.POST)
    public RetDataBean getMyHrLicenceList(PageParam pageParam) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("l.create_time");
            } else {
                pageParam.setSort("l." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = hrLicenceService.getMyHrLicenceList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @return RetDataBean
     * @Title: getMyHrIncentiveList
     * @Description:  个人查询奖惩记录
     */
    @RequestMapping(value = "/getMyHrIncentiveList", method = RequestMethod.POST)
    public RetDataBean getMyHrIncentiveList(PageParam pageParam
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("i.create_time");
            } else {
                pageParam.setSort("i." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = hrIncentiveService.getMyHrIncentiveList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @return RetDataBean
     * @Title: getMyHrContractList
     * @Description:  查询自己的合同列表
     */
    @RequestMapping(value = "/getMyHrContractList", method = RequestMethod.POST)
    public RetDataBean getMyHrContractList(PageParam pageParam
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("c.create_time");
            } else {
                pageParam.setSort("c." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = hrContractService.getMyHrContractList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getDeskHrContractList
     * @Description:  获取快到期的合同列表
     */
    @RequestMapping(value = "/getDeskHrContractList", method = RequestMethod.POST)
    public RetDataBean getDeskHrContractList() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrContractService.getDeskHrContractList(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getDeskHrUserInfo
     * @Description:  获取人力资源门户人员信息
     */
    @RequestMapping(value = "/getDeskHrUserInfo", method = RequestMethod.POST)
    public RetDataBean getDeskHrUserInfo() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrUserInfoService.getDeskHrUserInfo(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrKpiPlan
     * @return RetDataBean
     * @Title: getHrKpiPlanById
     * @Description:  获取考核计划详情
     */
    @RequestMapping(value = "/getHrKpiPlanById", method = RequestMethod.POST)
    public RetDataBean getHrKpiPlanById(HrKpiPlan hrKpiPlan) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrKpiPlan.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrKpiPlanService.selectOneHrKpiPlan(hrKpiPlan));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param hrEvaluate
     * @return RetDataBean
     * @Title: getHrEvaluateById
     * @Description:  获取人员评价详情
     */
    @RequestMapping(value = "/getHrEvaluateById", method = RequestMethod.POST)
    public RetDataBean getHrEvaluateById(HrEvaluate hrEvaluate) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrEvaluate.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrEvaluateService.selectOneHrEvaluate(hrEvaluate));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrWelfareRecord
     * @return RetDataBean
     * @Title: getHrWelfareRecordById
     * @Description:  人员福利详情
     */
    @RequestMapping(value = "/getHrWelfareRecordById", method = RequestMethod.POST)
    public RetDataBean getHrWelfareRecordById(HrWelfareRecord hrWelfareRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrWelfareRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrWelfareRecordService.selectOneHrWelfareRecord(hrWelfareRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrSalaryRecord
     * @return RetDataBean
     * @Title: getHrSalaryRecordById
     * @Description:  获取人员薪资详情
     */
    @RequestMapping(value = "/getHrSalaryRecordById", method = RequestMethod.POST)
    public RetDataBean getHrSalaryRecordById(HrSalaryRecord hrSalaryRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrSalaryRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrSalaryRecordService.selectOneHrSalaryRecord(hrSalaryRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrRecruitPlan
     * @return RetDataBean
     * @Title: getHrRecruitPlanById
     * @Description:  获取招聘计划详情
     */
    @RequestMapping(value = "/getHrRecruitPlanById", method = RequestMethod.POST)
    public RetDataBean getHrRecruitPlanById(HrRecruitPlan hrRecruitPlan) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrRecruitPlan.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrRecruitPlanService.selectOneHrRecruitPlan(hrRecruitPlan));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrRecruitPlan
     * @return RetDataBean
     * @Title: getHrRecruitPlanForSelect
     * @Description:  获取当前可填报的招聘计划
     */
    @RequestMapping(value = "/getHrRecruitPlanForSelect", method = RequestMethod.POST)
    public RetDataBean getHrRecruitPlanForSelect(HrRecruitPlan hrRecruitPlan) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrRecruitPlanService.getHrRecruitPlanForSelect(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrKpiItem
     * @return RetDataBean
     * @Title: getHrKpiItemById
     * @Description:  获取考核指标详情
     */
    @RequestMapping(value = "/getHrKpiItemById", method = RequestMethod.POST)
    public RetDataBean getHrKpiItemById(HrKpiItem hrKpiItem) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrKpiItem.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrKpiItemService.selectOneHrKpiItem(hrKpiItem));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrRecruitNeeds
     * @return RetDataBean
     * @Title: getHrRecruitNeedsById
     * @Description:  获取招聘需求详情
     */
    @RequestMapping(value = "/getHrRecruitNeedsById", method = RequestMethod.POST)
    public RetDataBean getHrRecruitNeedsById(HrRecruitNeeds hrRecruitNeeds) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrRecruitNeeds.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrRecruitNeedsService.selectOneHrRecruitNeeds(hrRecruitNeeds));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrTrainRecord
     * @return RetDataBean
     * @Title: getHrTrainRecordById
     * @Description:  获取人员培训详情
     */
    @RequestMapping(value = "/getHrTrainRecordById", method = RequestMethod.POST)
    public RetDataBean getHrTrainRecordById(HrTrainRecord hrTrainRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrTrainRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrTrainRecordService.selectOneHrTrainRecord(hrTrainRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrCareRecord
     * @return RetDataBean
     * @Title: getHrCareRecordById
     * @Description:  获取员工关怀详情
     */
    @RequestMapping(value = "/getHrCareRecordById", method = RequestMethod.POST)
    public RetDataBean getHrCareRecordById(HrCareRecord hrCareRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrCareRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrCareRecordService.selectOneHrCareRecord(hrCareRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrTitleEvaluation
     * @return RetDataBean
     * @Title: getHrTitleEvaluationById
     * @Description:  获取职称评定详情
     */
    @RequestMapping(value = "/getHrTitleEvaluationById", method = RequestMethod.POST)
    public RetDataBean getHrTitleEvaluationById(HrTitleEvaluation hrTitleEvaluation) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrTitleEvaluation.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrTitleEvaluationService.selectOneHrTitleEvaluation(hrTitleEvaluation));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param hrReinstatement
     * @return RetDataBean
     * @Title: getHrReinstatementById
     * @Description:  获取复职详情
     */
    @RequestMapping(value = "/getHrReinstatementById", method = RequestMethod.POST)
    public RetDataBean getHrReinstatementById(HrReinstatement hrReinstatement) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrReinstatement.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrReinstatementService.selectOneHrReinstatement(hrReinstatement));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrLeaveRecord
     * @return RetDataBean
     * @Title: getHrLeaveRecordById
     * @Description:  获取离职记录详情
     */
    @RequestMapping(value = "/getHrLeaveRecordById", method = RequestMethod.POST)
    public RetDataBean getHrLeaveRecordById(HrLeaveRecord hrLeaveRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrLeaveRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrLeaveRecordService.selectOneHrLeaveRecord(hrLeaveRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrPersonnelTransfer
     * @return RetDataBean
     * @Title: getHrPersonnelTransferById
     * @Description:  获取人事调动详情
     */
    @RequestMapping(value = "/getHrPersonnelTransferById", method = RequestMethod.POST)
    public RetDataBean getHrPersonnelTransferById(HrPersonnelTransfer hrPersonnelTransfer) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrPersonnelTransfer.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrPersonnelTransferService.selectOneHrPersonnelTransfer(hrPersonnelTransfer));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrWorkSkills
     * @return RetDataBean
     * @Title: getHrWorkSkillsById
     * @Description:  获取工作特长详情
     */
    @RequestMapping(value = "/getHrWorkSkillsById", method = RequestMethod.POST)
    public RetDataBean getHrWorkSkillsById(HrWorkSkills hrWorkSkills) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrWorkSkills.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrWorkSkillsService.selectOneHrWorkSkills(hrWorkSkills));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrWorkRecord
     * @return RetDataBean
     * @Title: getHrWorkRecordById
     * @Description:  获取工作记录详情
     */
    @RequestMapping(value = "/getHrWorkRecordById", method = RequestMethod.POST)
    public RetDataBean getHrWorkRecordById(HrWorkRecord hrWorkRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrWorkRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrWorkRecordService.selectOneHrWorkRecord(hrWorkRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrLearnRecord
     * @return RetDataBean
     * @Title: getHrLearnRecordById
     * @Description:  获取学习记录详情
     */
    @RequestMapping(value = "/getHrLearnRecordById", method = RequestMethod.POST)
    public RetDataBean getHrLearnRecordById(HrLearnRecord hrLearnRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrLearnRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrLearnRecordService.selectOneHrLearnRecord(hrLearnRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrLicence
     * @return RetDataBean
     * @Title: getHrLicenceById
     * @Description:  获取证照详情
     */
    @RequestMapping(value = "/getHrLicenceById", method = RequestMethod.POST)
    public RetDataBean getHrLicenceById(HrLicence hrLicence) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrLicence.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrLicenceService.selectOneHrLicence(hrLicence));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param hrIncentive
     * @return RetDataBean
     * @Title: getHrIncentiveById
     * @Description:  获取奖惩记录详情
     */
    @RequestMapping(value = "/getHrIncentiveById", method = RequestMethod.POST)
    public RetDataBean getHrIncentiveById(HrIncentive hrIncentive) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrIncentive.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrIncentiveService.selectOneHrIncentive(hrIncentive));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrContract
     * @return RetDataBean
     * @Title: getHrContractById
     * @Description:  获取合同详情
     */
    @RequestMapping(value = "/getHrContractById", method = RequestMethod.POST)
    public RetDataBean getHrContractById(HrContract hrContract) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrContract.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrContractService.selectOneHrContract(hrContract));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param searchuser
     * @return RetDataBean
     * @Title: getHrUserInfoBySearchuser
     * @Description:  查询HR人员
     */
    @RequestMapping(value = "/getHrUserInfoBySearchuser", method = RequestMethod.POST)
    public RetDataBean getHrUserInfoBySearchuser(String searchuser) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrUserInfoService.getHrUserInfoBySearchuser(account.getOrgId(), searchuser));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param userIds
     * @return RetDataBean
     * @Title: getUserNamesByUserIds
     * @Description:  获取HR人员列表
     */
    @RequestMapping(value = "/getUserNamesByUserIds", method = RequestMethod.POST)
    public RetDataBean getUserNamesByUserIds(String userIds) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrUserInfoService.getUserNamesByUserIds(account.getOrgId(), userIds));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getWagesLevelListForSelect
     * @Description:  获取工资级别列表
     */
    @RequestMapping(value = "/getWagesLevelListForSelect", method = RequestMethod.POST)
    public RetDataBean getWagesLevelListForSelect() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrWagesLevelService.getWagesLevelListForSelect(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @Title: getWagesLevelList
     * @Description:  获取工资级别列表
     * @param: request
     * @param: pageParam
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getWagesLevelList", method = RequestMethod.POST)
    public RetDataBean getWagesLevelList(PageParam pageParam) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("h.sort_no");
            } else {
                pageParam.setSort("h."+StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = hrWagesLevelService.getWagesLevelList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param deptId
     * @return RetDataBean
     * @Title: getHrUserInfoByBeptIdInWorkList
     * @Description:  获取部门下的人员列表
     */
    @RequestMapping(value = "/getHrUserInfoByBeptIdInWorkList", method = RequestMethod.POST)
    public RetDataBean getHrUserInfoByBeptIdInWorkList(PageParam pageParam, String deptId,
                                                       String workStatus, String employedTime, String staffCardNo) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("u.sort_no");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setDeptId(deptId);
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = hrUserInfoService.getHrUserInfoByBeptIdInWorkList(pageParam, workStatus, employedTime, staffCardNo);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getHrWagesLevelById
     * @Description:  获取工资级别详情
     * @param: request
     * @param: hrWorkType
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getHrWagesLevelById", method = RequestMethod.POST)
    public RetDataBean getHrWagesLevelById(HrWagesLevel hrWagesLevel) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrWagesLevel.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrWagesLevelService.selectOneHrWagesLevel(hrWagesLevel));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getHrUserInfoByDeptId
     * @Description:  获取部门下的人员列表
     * @param: request
     * @param: pageParam
     * @param: deptId
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getHrUserInfoByDeptId", method = RequestMethod.POST)
    public RetDataBean getHrUserInfoByDeptId(String deptId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrUserInfoService.getHrUserInfoByDeptId(account.getOrgId(), deptId));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getHrDepartmentTree
     * @Description:  人员基本信息树结构
     * @param: request
     * @param: deptId
     * @param: @return
     * @return: List<Map < String, String>>
     */
    @RequestMapping(value = "/getHrUserInfoDepartmentTree", method = RequestMethod.POST)
    public List<Map<String, String>> getHrUserInfoDepartmentTree(String deptId) {
        try {
            String orgLevelId = "0";
            if (StringUtils.isNotBlank(deptId)) {
                orgLevelId = deptId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return hrDepartmentService.getHrUserInfoDepartmentTree(account.getOrgId(), orgLevelId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * @Title: getHrDepartmentTree
     * @Description: 获取HR部门树
     * @param: request
     * @param: deptId
     * @param: @return
     * @return: List<Map < String, String>>
     */
    @RequestMapping(value = "/getHrDepartmentTree", method = RequestMethod.POST)
    public List<Map<String, String>> getHrDepartmentTree(String deptId) {
        try {
            String orgLevelId = "0";
            if (StringUtils.isNotBlank(deptId)) {
                orgLevelId = deptId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return hrDepartmentService.getHrDepartmentTree(account.getOrgId(), orgLevelId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * @Title: getHrDepartmentById
     * @Description:  获取部门详情
     * @param: request
     * @param: hrDepartment
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getHrDepartmentById", method = RequestMethod.POST)
    public RetDataBean getHrDepartmentById(HrDepartment hrDepartment) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrDepartment.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrDepartmentService.selectOneHrDepartment(hrDepartment));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getHrUserInfoById
     * @Description:  获取HR的用户详情
     * @param: request
     * @param: hrUserInfo
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getHrUserInfoById", method = RequestMethod.POST)
    public RetDataBean getHrUserInfoById(HrUserInfo hrUserInfo) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrUserInfo.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrUserInfoService.selectOneHrUserInfo(hrUserInfo));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getMyHrUserInfo
     * @Description:  获取个人人事信息详情
     */
    @RequestMapping(value = "/getMyHrUserInfo", method = RequestMethod.POST)
    public RetDataBean getMyHrUserInfo() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            HrUserInfo hrUserInfo = new HrUserInfo();
            hrUserInfo.setAccountId(account.getAccountId());
            hrUserInfo.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrUserInfoService.selectOneHrUserInfo(hrUserInfo));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @Title: getHrUserInfoForTree
     * @Description:  人员基本信息树结构
     * @param: request
     * @param: deptId
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getHrUserInfoForTree", method = RequestMethod.POST)
    public RetDataBean getHrUserInfoForTree(String deptId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrUserInfoService.getHrUserInfoForTree(account.getOrgId(), deptId));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @Title: getHrUserLevelById
     * @Description:  获取行政级别详情
     * @param: request
     * @param: hrUserLevel
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getHrUserLevelById", method = RequestMethod.POST)
    public RetDataBean getHrUserLevelById(HrUserLevel hrUserLevel) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrUserLevel.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrUserLevelService.selectOneHrUserLevel(hrUserLevel));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getHrUserLevelChart
     * @Description:  获取行政级别CHART数据
     * @param: request
     * @param: levelId
     * @param: @return
     * @return: Object
     */
    @RequestMapping(value = "/getHrUserLevelChart", method = RequestMethod.POST)
    public Object getHrUserLevelChart(String levelId) {
        try {
            if (StringUtils.isBlank(levelId)) {
                levelId = "0";
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            List<Map<String, Object>> listMap = hrUserLevelService.getAllHrUserLevelChart(account.getOrgId(), levelId);
            if (listMap.size() > 0) {
                return listMap.get(0);
            } else {
                return null;
            }

        } catch (Exception e) {


            return null;
        }
    }

    /**
     * @Title: getHrUserLevelList
     * @Description:  获取所有行政级别
     * @param: request
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getHrUserLevelList", method = RequestMethod.POST)
    public RetDataBean getHrUserLevelList() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(HrUserLevel.class);
            example.setOrderByClause("level_no_id asc");
            example.createCriteria().andEqualTo("orgId", account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrUserLevelService.selectByExample(example));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getAllParentCodeList
     * @Description: 获取所有的主分类
     */
    @RequestMapping(value = "/getAllParentCodeList", method = RequestMethod.POST)
    public RetDataBean getAllParentCodeList() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrClassCodeService.getAllParentCodeList(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param module
     * @return RetDataBean
     * @Title: getCodeListByModule
     * @Description:  获取子
     */
    @RequestMapping(value = "/getCodeListByModule", method = RequestMethod.POST)
    public RetDataBean getCodeListByModule(String module) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrClassCodeService.getCodeListByModule(account.getOrgId(), module));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrClassCode
     * @return RetDataBean
     * @Title: getHrClassCodeById
     * @Description:  获取分类详情
     */
    @RequestMapping(value = "/getHrClassCodeById", method = RequestMethod.POST)
    public RetDataBean getHrClassCodeById(HrClassCode hrClassCode) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrClassCode.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrClassCodeService.selectOneHrClassCode(hrClassCode));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrDepartment
     * @return RetDataBean
     * @Title: getHrDeptList
     * @Description:  获取子部门列表
     */
    @RequestMapping(value = "/getHrDeptList", method = RequestMethod.POST)
    public RetDataBean getHrDeptList(HrDepartment hrDepartment) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(HrDepartment.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("orgLevelId", hrDepartment.getOrgLevelId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrDepartmentService.getHrDeptList(example));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param deptIds
     * @return RetDataBean
     * @Title: getHrDeptNameByStr
     * @Description:  获取部门名称
     */
    @RequestMapping(value = "/getHrDeptNameByStr", method = RequestMethod.POST)
    public RetDataBean getHrDeptNameByStr(String deptIds) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrDepartmentService.getHrDeptNameByStr(account.getOrgId(), deptIds));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param levelIds
     * @return RetDataBean
     * @Title: getHrUserLevelByStr
     * @Description:  获取HR的行政级别名称
     */
    @RequestMapping(value = "/getHrUserLevelByStr", method = RequestMethod.POST)
    public RetDataBean getHrUserLevelByStr(String levelIds) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrUserLevelService.getHrUserLevelByStr(account.getOrgId(), levelIds));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param searchdept
     * @return RetDataBean
     * @Title: getHrDeptBySearchdept
     * @Description:  查询HR部门
     */
    @RequestMapping(value = "/getHrDeptBySearchdept", method = RequestMethod.POST)
    public RetDataBean getHrDeptBySearchdept(String searchdept) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrDepartmentService.getHrDeptBySearchdept(account.getOrgId(), searchdept));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param userId
     * @param beginTime
     * @param endTime
     * @param enterpries
     * @param contractType
     * @return RetDataBean
     * @Title: getHrContractList
     * @Description:  获取合同列表
     */
    @RequestMapping(value = "/getHrContractList", method = RequestMethod.POST)
    public RetDataBean getHrContractList(PageParam pageParam, String userId, String beginTime, String endTime,
                                         String enterpries, String contractType
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("sort_no");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = hrContractService.getHrContractList(pageParam, userId, beginTime, endTime, enterpries, contractType);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param userId
     * @param beginTime
     * @param endTime
     * @param incentiveType
     * @param incentiveItem
     * @return RetDataBean
     * @Title: getHrIncentiveList
     * @Description:  获取奖惩记录列表
     */
    @RequestMapping(value = "/getHrIncentiveList", method = RequestMethod.POST)
    public RetDataBean getHrIncentiveList(PageParam pageParam, String userId, String beginTime, String endTime,
                                          String incentiveType, String incentiveItem
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("sort_no");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = hrIncentiveService.getHrIncentiveList(pageParam, userId, beginTime, endTime, incentiveType, incentiveItem);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param module
     * @param codeValue
     * @return RetDataBean
     * @Title: getHrClassCodeName
     * @Description:  获取分类码名称
     */
    @RequestMapping(value = "/getHrClassCodeName", method = RequestMethod.POST)
    public RetDataBean getHrClassCodeName(String module, String codeValue) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, hrClassCodeService.getHrClassCodeName(account.getOrgId(), module, codeValue));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param userId
     * @param beginTime
     * @param endTime
     * @param licenceType
     * @return RetDataBean
     * @Title: getHrLicenceList
     * @Description:  获取证照列表
     */
    @RequestMapping(value = "/getHrLicenceList", method = RequestMethod.POST)
    public RetDataBean getHrLicenceList(PageParam pageParam, String userId, String beginTime, String endTime,
                                        String licenceType
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("sort_no");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = hrLicenceService.getHrLicenceList(pageParam, userId, beginTime, endTime, licenceType);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param userId
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getHrLearnRecordList
     * @Description:  获取教育经历列表
     */
    @RequestMapping(value = "/getHrLearnRecordList", method = RequestMethod.POST)
    public RetDataBean getHrLearnRecordList(PageParam pageParam, String userId, String beginTime, String endTime
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("sort_no");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = hrLearnRecordService.getHrLearnRecordList(pageParam, userId, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param userId
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getHrWorkRecordList
     * @Description:  获取工作记录
     */
    @RequestMapping(value = "/getHrWorkRecordList", method = RequestMethod.POST)
    public RetDataBean getHrWorkRecordList(PageParam pageParam, String userId, String beginTime,
                                           String endTime, String nature
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("sort_no");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = hrWorkRecordService.getHrWorkRecordList(pageParam, userId, beginTime, endTime, nature);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param userId
     * @param beginTime
     * @param endTime
     * @param reinstatementType
     * @return RetDataBean
     * @Title: getHrReinstatementList
     * @Description:  获取复值列表
     */
    @RequestMapping(value = "/getHrReinstatementList", method = RequestMethod.POST)
    public RetDataBean getHrReinstatementList(PageParam pageParam, String userId, String beginTime,
                                              String endTime, String reinstatementType
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("sort_no");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = hrReinstatementService.getHrReinstatementList(pageParam, userId, beginTime, endTime, reinstatementType);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param userId
     * @param beginTime
     * @param endTime
     * @param skillsLevel
     * @return RetDataBean
     * @Title: getHrWorkSkillsList
     * @Description:  工作特长列表
     */
    @RequestMapping(value = "/getHrWorkSkillsList", method = RequestMethod.POST)
    public RetDataBean getHrWorkSkillsList(PageParam pageParam, String userId, String beginTime, String endTime,
                                           String skillsLevel
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("sort_no");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = hrWorkSkillsService.getHrWorkSkillsList(pageParam, userId, beginTime, endTime, skillsLevel);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param userId
     * @param beginTime
     * @param endTime
     * @param transferType
     * @return RetDataBean
     * @Title: getHrPersonnelTransferList
     * @Description:   获取人员调动列表
     */
    @RequestMapping(value = "/getHrPersonnelTransferList", method = RequestMethod.POST)
    public RetDataBean getHrPersonnelTransferList(PageParam pageParam, String userId, String beginTime, String endTime,
                                                  String transferType
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("sort_no");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = hrPersonnelTransferService.getHrPersonnelTransferList(pageParam, userId, beginTime, endTime, transferType);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param pageParam
     * @param userId
     * @param year
     * @param month
     * @return RetDataBean
     * @Title: getHrSalaryRecordList
     * @Description:  获取人员薪资列表
     */
    @RequestMapping(value = "/getHrSalaryRecordList", method = RequestMethod.POST)
    public RetDataBean getHrSalaryRecordList(PageParam pageParam, String userId, String year, String month
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = hrSalaryRecordService.getHrSalaryRecordList(pageParam, userId, year, month);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param userId
     * @param beginTime
     * @param endTime
     * @param type
     * @return RetDataBean
     * @Title: getHrWelfareRecordList
     * @Description:  获取福利列表
     */
    @RequestMapping(value = "/getHrWelfareRecordList", method = RequestMethod.POST)
    public RetDataBean getHrWelfareRecordList(PageParam pageParam, String userId, String beginTime,
                                              String endTime, String type
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("h.create_time");
            } else {
                pageParam.setSort("h." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = hrWelfareRecordService.getHrWelfareRecordList(pageParam, beginTime, endTime, userId, type);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param userId
     * @param beginTime
     * @param endTime
     * @param levelType
     * @return RetDataBean
     * @Title: getHrLeaveRecordList
     * @Description:  获取离职人员列表
     */
    @RequestMapping(value = "/getHrLeaveRecordList", method = RequestMethod.POST)
    public RetDataBean getHrLeaveRecordList(PageParam pageParam, String userId, String beginTime,
                                            String endTime, String levelType
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("sort_no");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = hrLeaveRecordService.getHrLeaveRecordList(pageParam, userId, beginTime, endTime, levelType);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param userId
     * @param beginTime
     * @param endTime
     * @param getType
     * @return RetDataBean
     * @Title: getHrTitleEvaluationList
     * @Description:  获取人员职称评定列表
     */
    @RequestMapping(value = "/getHrTitleEvaluationList", method = RequestMethod.POST)
    public RetDataBean getHrTitleEvaluationList(PageParam pageParam, String userId, String beginTime,
                                                String endTime, String getType
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("sort_no");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = hrTitleEvaluationService.getHrTitleEvaluationList(pageParam, userId, beginTime, endTime, getType);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param userId
     * @param beginTime
     * @param endTime
     * @param careType
     * @return RetDataBean
     * @Title: getHrCareRecordList
     * @Description:  getHrCareRecordList
     */
    @RequestMapping(value = "/getHrCareRecordList", method = RequestMethod.POST)
    public RetDataBean getHrCareRecordList(PageParam pageParam, String userId, String beginTime, String endTime,
                                           String careType
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("sort_no");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = hrCareRecordService.getHrCareRecordList(pageParam, userId, beginTime, endTime, careType);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param channel
     * @param courseType
     * @param status
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getHrTrainRecordList
     * @Description:  获取培训列表
     */
    @RequestMapping(value = "/getHrTrainRecordList", method = RequestMethod.POST)
    public RetDataBean getHrTrainRecordList(PageParam pageParam,
                                            String channel, String courseType, String status, String beginTime, String endTime
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = hrTrainRecordService.getHrTrainRecordList(pageParam, channel, courseType, status, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param channel
     * @param courseType
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getHrTrainRecordApprovedList
     * @Description: (这里用一句话描述这个方法的作用)
     */
    @RequestMapping(value = "/getHrTrainRecordApprovedList", method = RequestMethod.POST)
    public RetDataBean getHrTrainRecordApprovedList(PageParam pageParam,
                                                    String channel, String courseType, String beginTime, String endTime
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = hrTrainRecordService.getHrTrainRecordApprovedList(pageParam, channel, courseType, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param channel
     * @param courseType
     * @param status
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getHrTrainRecordApprovedOldList
     * @Description:  获取历史审批记录
     */
    @RequestMapping(value = "/getHrTrainRecordApprovedOldList", method = RequestMethod.POST)
    public RetDataBean getHrTrainRecordApprovedOldList(PageParam pageParam,
                                                       String channel, String courseType, String status, String beginTime, String endTime
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = hrTrainRecordService.getHrTrainRecordApprovedOldList(pageParam, channel, courseType, status, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param highsetShool
     * @param occupation
     * @param status
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getHrRecruitNeedsList
     * @Description:  获取招聘需求列表
     */
    @RequestMapping(value = "/getHrRecruitNeedsList", method = RequestMethod.POST)
    public RetDataBean getHrRecruitNeedsList(PageParam pageParam,
                                             String highsetShool, String occupation, String status, String beginTime, String endTime
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = hrRecruitNeedsService.getHrRecruitNeedsList(pageParam, occupation, highsetShool, status, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param highsetShool
     * @param occupation
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getApprovedHrRecruitNeedsList
     * @Description:  获取待审批需求列表
     */
    @RequestMapping(value = "/getApprovedHrRecruitNeedsList", method = RequestMethod.POST)
    public RetDataBean getApprovedHrRecruitNeedsList(PageParam pageParam,
                                                     String highsetShool, String occupation, String beginTime, String endTime
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("n.create_time");
            } else {
                pageParam.setSort("n." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = hrRecruitNeedsService.getApprovedHrRecruitNeedsList(pageParam, occupation, highsetShool, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param status
     * @param highsetShool
     * @param occupation
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getOldApprovedHrRecruitNeedsList
     * @Description:   获取历史审批记录
     */
    @RequestMapping(value = "/getOldApprovedHrRecruitNeedsList", method = RequestMethod.POST)
    public RetDataBean getOldApprovedHrRecruitNeedsList(PageParam pageParam, String status,
                                                        String highsetShool, String occupation, String beginTime, String endTime
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("n.create_time");
            } else {
                pageParam.setSort("n." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = hrRecruitNeedsService.getOldApprovedHrRecruitNeedsList(pageParam, status, occupation, highsetShool, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getHrRecruitPlanList
     * @Description:  获取招聘计划列表
     */
    @RequestMapping(value = "/getHrRecruitPlanList", method = RequestMethod.POST)
    public RetDataBean getHrRecruitPlanList(PageParam pageParam, String beginTime, String endTime
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("h.create_time");
            } else {
                pageParam.setSort("h." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = hrRecruitPlanService.getHrRecruitPlanList(pageParam, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param createUser
     * @param kpiType
     * @return RetDataBean
     * @Title: getHrKpiItemList
     * @Description:  获取考核指标列表
     */
    @RequestMapping(value = "/getHrKpiItemList", method = RequestMethod.POST)
    public RetDataBean getHrKpiItemList(PageParam pageParam, String createUser, String kpiType
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("h.sort_no");
            } else {
                pageParam.setSort("h." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = hrKpiItemService.getHrKpiItemList(pageParam, createUser, kpiType);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param deptId
     * @return RetDataBean
     * @Title: getHrUserInfoListByDeptId
     * @Description:  获取部门下的人员列表
     */
    @RequestMapping(value = "/getHrUserInfoListByDeptId", method = RequestMethod.POST)
    public RetDataBean getHrUserInfoListByDeptId(PageParam pageParam, String deptId) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("u.sort_no");
            } else {
                pageParam.setSort("u." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setDeptId(deptId);
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = hrUserInfoService.getHrUserInfoListByDeptId(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param userId
     * @return RetDataBean
     * @Title: getHrEvaluateByUserIdList
     * @Description:  获取人员评价列表
     */
    @RequestMapping(value = "/getHrEvaluateByUserIdList", method = RequestMethod.POST)
    public RetDataBean getHrEvaluateByUserIdList(PageParam pageParam, String userId) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("e.create_time");
            } else {
                pageParam.setSort("e." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = hrEvaluateService.getHrEvaluateByUserIdList(pageParam, userId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param userId
     * @param beginTime
     * @param endTime
     * @param status
     * @return RetDataBean
     * @Title: getHrEvaluateQueryList
     * @Description:  getHrEvaluateQueryList
     */
    @RequestMapping(value = "/getHrEvaluateQueryList", method = RequestMethod.POST)
    public RetDataBean getHrEvaluateQueryList(PageParam pageParam, String userId, String beginTime, String endTime, String status) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("e.create_time");
            } else {
                pageParam.setSort("e." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = hrEvaluateService.getHrEvaluateQueryList(pageParam, userId, beginTime, endTime, status);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

}
