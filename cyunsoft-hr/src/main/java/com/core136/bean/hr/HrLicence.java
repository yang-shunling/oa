/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: HrLicence.java
 * @Package com.core136.bean.hr
 * @Description: 描述
 * @author: lsq
 * @date: 2019年10月31日 下午2:13:48
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.bean.hr;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author lsq
 * HR人员证照管理
 */
@Table(name = "hr_licence")
public class HrLicence implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String licenceId;
    private Integer sortNo;
    private String licenceCode;
    private String name;
    private String userId;
    private String beginTime;
    private String endTime;
    private String licenceType;
    private String attach;
    private String remark;
    private String notifiedBody;
    private String sendToUser;
    private String msgType;
    private String reminder;
    private String createTime;
    private String createUser;
    private String orgId;

    /**
     * @return the licenceId
     */
    public String getLicenceId() {
        return licenceId;
    }

    /**
     * @param licenceId the licenceId to set
     */
    public void setLicenceId(String licenceId) {
        this.licenceId = licenceId;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    /**
     * @return the endTime
     */
    public String getEndTime() {
        return endTime;
    }

    /**
     * @param endTime the endTime to set
     */
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    /**
     * @return the attach
     */
    public String getAttach() {
        return attach;
    }

    /**
     * @param attach the attach to set
     */
    public void setAttach(String attach) {
        this.attach = attach;
    }

    /**
     * @return the createTime
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime the createTime to set
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    /**
     * @return the createUser
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * @param createUser the createUser to set
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * @return the orgId
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * @param orgId the orgId to set
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getLicenceType() {
        return licenceType;
    }

    public void setLicenceType(String licenceType) {
        this.licenceType = licenceType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getNotifiedBody() {
        return notifiedBody;
    }

    public void setNotifiedBody(String notifiedBody) {
        this.notifiedBody = notifiedBody;
    }

    public String getSendToUser() {
        return sendToUser;
    }

    public void setSendToUser(String sendToUser) {
        this.sendToUser = sendToUser;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public String getReminder() {
        return reminder;
    }

    public void setReminder(String reminder) {
        this.reminder = reminder;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getLicenceCode() {
        return licenceCode;
    }

    public void setLicenceCode(String licenceCode) {
        this.licenceCode = licenceCode;
    }

}
