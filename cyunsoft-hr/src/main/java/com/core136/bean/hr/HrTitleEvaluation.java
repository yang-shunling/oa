package com.core136.bean.hr;

import javax.persistence.Table;

/**
 * @ClassName: HrTitleEvaluation
 * @Description: 人力资次职称评定
 * @author: 稠云技术
 * @date: 2020年4月30日 下午8:30:06
 * @Copyright: 2020 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "hr_title_evaluation")
public class HrTitleEvaluation {
    private String recordId;
    private Integer sortNo;
    private String deptId;
    private String post;
    private String userId;
    private String postName;
    private String getType;
    private String applyTime;
    private String receiveTime;
    private String nextPostName;
    private String nextApplyTime;
    private String employPost;
    private String employComp;
    private String employBeginTime;
    private String employEndTime;
    private String remark;
    private String attach;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPostName() {
        return postName;
    }

    public void setPostName(String postName) {
        this.postName = postName;
    }

    public String getGetType() {
        return getType;
    }

    public void setGetType(String getType) {
        this.getType = getType;
    }

    public String getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(String applyTime) {
        this.applyTime = applyTime;
    }

    public String getReceiveTime() {
        return receiveTime;
    }

    public void setReceiveTime(String receiveTime) {
        this.receiveTime = receiveTime;
    }

    public String getNextPostName() {
        return nextPostName;
    }

    public void setNextPostName(String nextPostName) {
        this.nextPostName = nextPostName;
    }

    public String getNextApplyTime() {
        return nextApplyTime;
    }

    public void setNextApplyTime(String nextApplyTime) {
        this.nextApplyTime = nextApplyTime;
    }

    public String getEmployPost() {
        return employPost;
    }

    public void setEmployPost(String employPost) {
        this.employPost = employPost;
    }

    public String getEmployComp() {
        return employComp;
    }

    public void setEmployComp(String employComp) {
        this.employComp = employComp;
    }

    public String getEmployBeginTime() {
        return employBeginTime;
    }

    public void setEmployBeginTime(String employBeginTime) {
        this.employBeginTime = employBeginTime;
    }

    public String getEmployEndTime() {
        return employEndTime;
    }

    public void setEmployEndTime(String employEndTime) {
        this.employEndTime = employEndTime;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

}
