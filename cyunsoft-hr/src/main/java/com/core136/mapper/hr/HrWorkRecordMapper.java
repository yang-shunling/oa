package com.core136.mapper.hr;

import com.core136.bean.hr.HrWorkRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface HrWorkRecordMapper extends MyMapper<HrWorkRecord> {
    /**
     * @param orgId
     * @param userId
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getHrWorkRecordList
     * @Description:  获取工作记录
     */
    public List<Map<String, String>> getHrWorkRecordList(@Param(value = "orgId") String orgId, @Param(value = "userId") String userId,
                                                         @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "nature") String nature,
                                                         @Param(value = "search") String search);

    /**
     * @param orgId
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getMyHrWorkRecordList
     * @Description:  查询个人工作经历
     */
    public List<Map<String, String>> getMyHrWorkRecordList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId);

}
