package com.core136.mapper.hr;

import com.core136.bean.hr.HrKpiPlanRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface HrKpiPlanRecordMapper extends MyMapper<HrKpiPlanRecord> {

    /**
     * @param orgId
     * @param planId
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getMyHrKpiScoreList
     * @Description:  获取个人考核分数明细
     */
    public List<Map<String, String>> getMyHrKpiScoreList(@Param(value = "orgId") String orgId, @Param(value = "planId") String planId, @Param(value = "accountId") String accountId);

}
