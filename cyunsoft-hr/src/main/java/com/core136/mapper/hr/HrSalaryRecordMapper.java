package com.core136.mapper.hr;

import com.core136.bean.hr.HrSalaryRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface HrSalaryRecordMapper extends MyMapper<HrSalaryRecord> {

    /**
     * @param orgId
     * @param userId
     * @param year
     * @param month
     * @return List<Map < String, String>>
     * @Title: getHrSalaryRecordList
     * @Description:  获取人员薪资列表
     */
    public List<Map<String, String>> getHrSalaryRecordList(
            @Param(value = "orgId") String orgId,
            @Param(value = "userId") String userId,
            @Param(value = "year") String year,
            @Param(value = "month") String month
    );

    /**
     * @param orgId
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getMyHrSalaryRecordList
     * @Description:  个人薪资查询
     */
    public List<Map<String, String>> getMyHrSalaryRecordList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId);
}
