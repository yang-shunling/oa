package com.core136.mapper.hr;

import com.core136.bean.hr.HrKpiPlan;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface HrKpiPlanMapper extends MyMapper<HrKpiPlan> {

    /**
     * @param orgId
     * @param status
     * @param kpiRule
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getMyHrKpiPlanList
     * @Description:  获取考核管理列表
     */
    public List<Map<String, String>> getMyHrKpiPlanList(@Param(value = "orgId") String orgId, @Param(value = "status") String status,
                                                        @Param(value = "kpiRule") String kpiRule, @Param(value = "beginTime") String beginTime,
                                                        @Param(value = "endTime") String endTime, @Param(value = "search") String search);

    /**
     * @param orgId
     * @param accountId
     * @param nowTime
     * @return List<Map < String, String>>
     * @Title: getKpiPlanForUserList
     * @Description:  获取人员考核列表
     */
    public List<Map<String, String>> getKpiPlanForUserList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,
                                                           @Param(value = "nowTime") String nowTime, @Param(value = "search") String search);

    /**
     * @param orgId
     * @param planId
     * @param accountId
     * @param chargeUser
     * @return List<Map < String, String>>
     * @Title: getKipItemForUserById
     * @Description:  获取人员考核指标集
     */
    public List<Map<String, String>> getKipItemForUserById(@Param(value = "orgId") String orgId, @Param(value = "planId") String planId,
                                                           @Param(value = "accountId") String accountId, @Param(value = "chargeUser") String chargeUser);


    /**
     * @param orgId
     * @param planId
     * @param chargeUser
     * @param accountId
     * @param deptId
     * @param levelId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getKpiPlanForUserQueryList
     * @Description:  查询获取考核列表
     */
    public List<Map<String, String>> getKpiPlanForUserQueryList(@Param(value = "orgId") String orgId,
                                                                @Param(value = "planId") String planId, @Param(value = "chargeUser") String chargeUser,
                                                                @Param(value = "accountId") String accountId, @Param(value = "search") String search);

    /**
     * @param orgId
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getMyKpiPlanForList
     * @Description:  获取个人考核列表
     */
    public List<Map<String, String>> getMyKpiPlanForList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId);


    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getKpiPlanForListForSelect
     * @Description:  获取考核计划列表
     */
    public List<Map<String, String>> getKpiPlanForListForSelect(@Param(value = "orgId") String orgId);

}
