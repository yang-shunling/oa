package com.core136.service.hr;

import com.core136.bean.account.Account;
import com.core136.bean.hr.HrLearnRecord;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.ExcelUtil;
import com.core136.common.utils.SysTools;
import com.core136.mapper.hr.HrLearnRecordMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class HrLearnRecordService {
    private HrLearnRecordMapper hrLearnRecordMapper;

    @Autowired
    public void setHrLearnRecordMapper(HrLearnRecordMapper hrLearnRecordMapper) {
        this.hrLearnRecordMapper = hrLearnRecordMapper;
    }

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public int insertHrLearnRecord(HrLearnRecord hrLearnRecord) {
        return hrLearnRecordMapper.insert(hrLearnRecord);
    }

    public int deleteHrLearnRecord(HrLearnRecord hrLearnRecord) {
        return hrLearnRecordMapper.delete(hrLearnRecord);
    }

    public int updateHrLearnRecord(Example example, HrLearnRecord hrLearnRecord) {
        return hrLearnRecordMapper.updateByExampleSelective(hrLearnRecord, example);
    }

    public HrLearnRecord selectOneHrLearnRecord(HrLearnRecord hrLearnRecord) {
        return hrLearnRecordMapper.selectOne(hrLearnRecord);
    }

    /**
     * @param orgId
     * @param userId
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getHrLearnRecordList
     * @Description:  获取教育经历列表
     */
    public List<Map<String, String>> getHrLearnRecordList(String orgId, String userId, String beginTime, String endTime, String search) {
        return hrLearnRecordMapper.getHrLearnRecordList(orgId, userId, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param orgId
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getMyHrLearnRecordList
     * @Description:  个人查询学习记录
     */
    public List<Map<String, String>> getMyHrLearnRecordList(String orgId, String accountId) {
        return hrLearnRecordMapper.getMyHrLearnRecordList(orgId, accountId);
    }

    /**
     * @param pageParam
     * @param userId
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getHrLearnRecordList
     * @Description:  获取教育经历列表
     */
    public PageInfo<Map<String, String>> getHrLearnRecordList(PageParam pageParam, String userId, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHrLearnRecordList(pageParam.getOrgId(), userId, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param pageParam
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getMyHrLearnRecordList
     * @Description:  个人查询学习记录
     */
    public PageInfo<Map<String, String>> getMyHrLearnRecordList(PageParam pageParam) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyHrLearnRecordList(pageParam.getOrgId(), pageParam.getAccountId());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param account
     * @param file
     * @return IOException
     * RetDataBean
     * @Title: importHrLearnRecord
     * @Description:  学习记录导入
     */
    @Transactional(value = "generalTM")
    public RetDataBean importHrLearnRecord(Account account, MultipartFile file) throws IOException {
        Map<String, String> fieldMap = new HashMap<String, String>();
        fieldMap.put("排序号", "sort_no");
        fieldMap.put("关联档案人员", "user_id");
        fieldMap.put("学校名称", "shool_name");
        fieldMap.put("所学专业", "major");
        fieldMap.put("入学日期", "begin_time");
        fieldMap.put("毕业日期", "end_time");
        fieldMap.put("所获学位", "highset_degree");
        fieldMap.put("证明人", "cerifier");
        fieldMap.put("获取证书", "cerificate");
        fieldMap.put("获奖情况", "honor");
        fieldMap.put("备注", "remark");
        List<String> fieldList = new ArrayList<String>();
        List<String> titleList = new ArrayList<String>();
        for (Map.Entry<String, String> entry : fieldMap.entrySet()) {
            fieldList.add(entry.getValue());
            titleList.add(entry.getKey());
        }
        String[] fieldArr = new String[fieldList.size()];
        fieldList.toArray(fieldArr);
        String fieldString = StringUtils.join(fieldArr, ",");
        List<Map<String, String>> recordList = ExcelUtil.readExcel(file);
        for (int i = 0; i < recordList.size(); i++) {
            Map<String, String> tempMap = recordList.get(i);
            String valueString = "'" + SysTools.getGUID() + "',";
            for (int k = 0; k < titleList.size(); k++) {
                if (titleList.get(k).equals("关联档案人员")) {
                    if (StringUtils.isNotBlank(tempMap.get(titleList.get(k)))) {
                        String sql1 = "select user_id from hr_user_info WHERE user_name ='" + tempMap.get(titleList.get(k)) + "' and org_id='" + account.getOrgId() + "'";
                        String userId = jdbcTemplate.queryForObject(sql1, String.class);
                        valueString += "'" + userId + "',";
                    } else {
                        valueString += "'',";
                    }
                } else {
                    valueString += "'" + tempMap.get(titleList.get(k)) + "',";
                }
            }
            valueString += "'" + SysTools.getTime("yyyy-MM-dd HH:mm:ss") + "',";
            valueString += "'" + account.getAccountId() + "',";
            valueString += "'" + account.getOrgId() + "'";
            String insertSql = "insert into hr_learn_record(record_id," + fieldString + ",create_time,create_user,org_id) values" + "(" + valueString + ")";
            jdbcTemplate.execute(insertSql);
        }
        return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
    }
}
