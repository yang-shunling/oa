package com.core136.service.hr;

import com.core136.bean.account.Account;
import com.core136.bean.hr.HrWelfareRecord;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.ExcelUtil;
import com.core136.common.utils.SysTools;
import com.core136.mapper.hr.HrWelfareRecordMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class HrWelfareRecordService {
    private HrWelfareRecordMapper hrWelfareRecordMapper;

    @Autowired
    public void setHrWelfareRecordMapper(HrWelfareRecordMapper hrWelfareRecordMapper) {
        this.hrWelfareRecordMapper = hrWelfareRecordMapper;
    }

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public int insertHrWelfareRecord(HrWelfareRecord hrWelfareRecord) {
        return hrWelfareRecordMapper.insert(hrWelfareRecord);
    }

    public int deleteHrWelfareRecord(HrWelfareRecord hrWelfareRecord) {
        return hrWelfareRecordMapper.delete(hrWelfareRecord);
    }

    public int updateHrWelfareRecord(Example example, HrWelfareRecord hrWelfareRecord) {
        return hrWelfareRecordMapper.updateByExampleSelective(hrWelfareRecord, example);
    }

    public HrWelfareRecord selectOneHrWelfareRecord(HrWelfareRecord hrWelfareRecord) {
        return hrWelfareRecordMapper.selectOne(hrWelfareRecord);
    }

    /**
     * @param orgId
     * @param beginTime
     * @param endTime
     * @param userId
     * @param type
     * @param search
     * @return List<Map < String, String>>
     * @Title: getHrWelfareRecordList
     * @Description:  获取福利列表
     */
    public List<Map<String, String>> getHrWelfareRecordList(String orgId, String beginTime, String endTime, String userId, String type, String search) {
        return hrWelfareRecordMapper.getHrWelfareRecordList(orgId, beginTime, endTime, type, userId, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @param userId
     * @param type
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getHrWelfareRecordList
     * @Description:  获取福利列表
     */
    public PageInfo<Map<String, String>> getHrWelfareRecordList(PageParam pageParam, String beginTime, String endTime, String userId, String type) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHrWelfareRecordList(pageParam.getOrgId(), beginTime, endTime, userId, type, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param account
     * @param file
     * @return IOException
     * RetDataBean
     * @Title: importHrWelfareRecord
     * @Description:  批量导入人员福利
     */
    @Transactional(value = "generalTM")
    public RetDataBean importHrWelfareRecord(Account account, MultipartFile file) throws IOException {
        Map<String, String> fieldMap = new HashMap<String, String>();
        fieldMap.put("排序号", "sort_no");
        fieldMap.put("福利标题", "title");
        fieldMap.put("福利年度", "year");
        fieldMap.put("福利月份", "month");
        fieldMap.put("人员姓名", "user_id");
        fieldMap.put("福利类型", "type");
        fieldMap.put("福利金额", "amount");
        fieldMap.put("备注", "remark");
        List<String> fieldList = new ArrayList<String>();
        List<String> titleList = new ArrayList<String>();
        for (Map.Entry<String, String> entry : fieldMap.entrySet()) {
            fieldList.add(entry.getValue());
            titleList.add(entry.getKey());
        }
        String[] fieldArr = new String[fieldList.size()];
        fieldList.toArray(fieldArr);
        String fieldString = StringUtils.join(fieldArr, ",");
        List<Map<String, String>> recordList = ExcelUtil.readExcel(file);
        for (int i = 0; i < recordList.size(); i++) {
            Map<String, String> tempMap = recordList.get(i);
            String valueString = "'" + SysTools.getGUID() + "',";
            for (int k = 0; k < titleList.size(); k++) {
                if (titleList.get(k).equals("人员姓名")) {
                    if (StringUtils.isNotBlank(tempMap.get(titleList.get(k)))) {
                        String sql1 = "select user_id from hr_user_info where user_name ='" + tempMap.get(titleList.get(k)) + "' and org_id='" + account.getOrgId() + "'";
                        String userId = jdbcTemplate.queryForObject(sql1, String.class);
                        valueString += "'" + userId + "',";
                    } else {
                        valueString += "'',";
                    }
                } else {
                    valueString += "'" + tempMap.get(titleList.get(k)) + "',";
                }
            }
            valueString += "'" + SysTools.getTime("yyyy-MM-dd HH:mm:ss") + "',";
            valueString += "'" + account.getAccountId() + "',";
            valueString += "'" + account.getOrgId() + "'";
            String insertSql = "insert into hr_welfare_record(record_id," + fieldString + ",create_time,create_user,org_id) values" + "(" + valueString + ")";
            jdbcTemplate.execute(insertSql);
        }
        return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
    }
}
