package com.core136.service.hr;

import com.core136.bean.account.Account;
import com.core136.bean.hr.HrSalaryRecord;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.ExcelUtil;
import com.core136.common.utils.SysTools;
import com.core136.mapper.hr.HrSalaryRecordMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class HrSalaryRecordService {
    private HrSalaryRecordMapper hrSalaryRecordMapper;

    @Autowired
    public void setHrSalaryRecordMapper(HrSalaryRecordMapper hrSalaryRecordMapper) {
        this.hrSalaryRecordMapper = hrSalaryRecordMapper;
    }

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public int insertHrSalaryRecord(HrSalaryRecord hrSalaryRecord) {
        return hrSalaryRecordMapper.insert(hrSalaryRecord);
    }

    public int deleteHrSalaryRecord(HrSalaryRecord hrSalaryRecord) {
        return hrSalaryRecordMapper.delete(hrSalaryRecord);
    }

    public int updateHrSalaryRecord(Example example, HrSalaryRecord hrSalaryRecord) {
        return hrSalaryRecordMapper.updateByExampleSelective(hrSalaryRecord, example);
    }

    public HrSalaryRecord selectOneHrSalaryRecord(HrSalaryRecord hrSalaryRecord) {
        return hrSalaryRecordMapper.selectOne(hrSalaryRecord);
    }

    /**
     * @param orgId
     * @param userId
     * @param year
     * @param month
     * @return List<Map < String, String>>
     * @Title: getHrSalaryRecordList
     * @Description:  获取人员薪资列表
     */
    public List<Map<String, String>> getHrSalaryRecordList(String orgId, String userId, String year, String month) {
        return hrSalaryRecordMapper.getHrSalaryRecordList(orgId, userId, year, month);
    }

    /**
     * @param orgId
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getMyHrSalaryRecordList
     * @Description:  个人薪资查询
     */
    public List<Map<String, String>> getMyHrSalaryRecordList(String orgId, String accountId) {
        return hrSalaryRecordMapper.getMyHrSalaryRecordList(orgId, accountId);
    }

    /**
     * @param pageParam
     * @param userId
     * @param year
     * @param month
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getHrSalaryRecordList
     * @Description:  获取人员薪资列表
     */
    public PageInfo<Map<String, String>> getHrSalaryRecordList(PageParam pageParam, String userId, String year, String month) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHrSalaryRecordList(pageParam.getOrgId(), userId, year, month);
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param pageParam
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getMyHrSalaryRecordList
     * @Description:  获取人员薪资列表
     */
    public PageInfo<Map<String, String>> getMyHrSalaryRecordList(PageParam pageParam) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyHrSalaryRecordList(pageParam.getOrgId(), pageParam.getAccountId());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param account
     * @param file
     * @return IOException
     * RetDataBean
     * @Title: importHrSalaryRecord
     * @Description:  导入人员薪资
     */
    @Transactional(value = "generalTM")
    public RetDataBean importHrSalaryRecord(Account account, MultipartFile file) throws IOException {
        Map<String, String> fieldMap = new HashMap<String, String>();
        fieldMap.put("排序号", "sort_no");
        fieldMap.put("薪资年度", "year");
        fieldMap.put("薪资月份", "month");
        fieldMap.put("人员姓名", "user_id");
        fieldMap.put("岗位工资", "post_salary");
        fieldMap.put("薪级工资", "level_salary");
        fieldMap.put("粮油补贴", "food_salary");
        fieldMap.put("特殊岗位津贴", "other_post_salary");
        fieldMap.put("交通补贴", "transport_salary");
        fieldMap.put("岗位津贴", "post_allowance");
        fieldMap.put("应发合计数", "sum_amount");
        fieldMap.put("退休金", "pensoin");
        fieldMap.put("失业保险", "unemployment");
        fieldMap.put("医保", "medical");
        fieldMap.put("公积金", "accumulation_fund");
        fieldMap.put("个人所得税", "tax");
        fieldMap.put("其它费用", "cost_other");
        fieldMap.put("实扣合计数", "real_cost");
        fieldMap.put("实发合计数", "rel_salary");
        List<String> fieldList = new ArrayList<String>();
        List<String> titleList = new ArrayList<String>();
        for (Map.Entry<String, String> entry : fieldMap.entrySet()) {
            fieldList.add(entry.getValue());
            titleList.add(entry.getKey());
        }
        String[] fieldArr = new String[fieldList.size()];
        fieldList.toArray(fieldArr);
        String fieldString = StringUtils.join(fieldArr, ",");
        List<Map<String, String>> recordList = ExcelUtil.readExcel(file);
        for (int i = 0; i < recordList.size(); i++) {
            Map<String, String> tempMap = recordList.get(i);
            String valueString = "'" + SysTools.getGUID() + "',";
            for (int k = 0; k < titleList.size(); k++) {
                if (titleList.get(k).equals("人员姓名")) {
                    if (StringUtils.isNotBlank(tempMap.get(titleList.get(k)))) {
                        String sql1 = "select user_id from hr_user_info where user_name ='" + tempMap.get(titleList.get(k)) + "' and org_id='" + account.getOrgId() + "'";
                        String userId = jdbcTemplate.queryForObject(sql1, String.class);
                        valueString += "'" + userId + "',";
                    } else {
                        valueString += "'',";
                    }
                } else {
                    valueString += "'" + tempMap.get(titleList.get(k)) + "',";
                }
            }
            valueString += "'" + SysTools.getTime("yyyy-MM-dd HH:mm:ss") + "',";
            valueString += "'" + account.getAccountId() + "',";
            valueString += "'" + account.getOrgId() + "'";
            String insertSql = "insert into hr_salary_record(record_id," + fieldString + ",create_time,create_user,org_id) values" + "(" + valueString + ")";
            jdbcTemplate.execute(insertSql);
        }
        return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
    }

}
