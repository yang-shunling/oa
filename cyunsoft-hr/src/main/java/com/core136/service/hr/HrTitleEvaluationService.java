package com.core136.service.hr;

import com.core136.bean.hr.HrTitleEvaluation;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.hr.HrTitleEvaluationMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class HrTitleEvaluationService {
    private HrTitleEvaluationMapper hrTitleEvaluationMapper;

    @Autowired
    public void setHrTitleEvaluationMapper(HrTitleEvaluationMapper hrTitleEvaluationMapper) {
        this.hrTitleEvaluationMapper = hrTitleEvaluationMapper;
    }

    public int insertHrTitleEvaluation(HrTitleEvaluation hrTitleEvaluation) {
        return hrTitleEvaluationMapper.insert(hrTitleEvaluation);
    }

    public int deleteHrTitleEvaluation(HrTitleEvaluation hrTitleEvaluation) {
        return hrTitleEvaluationMapper.delete(hrTitleEvaluation);
    }

    public int updateHrTitleEvaluation(Example example, HrTitleEvaluation hrTitleEvaluation) {
        return hrTitleEvaluationMapper.updateByExampleSelective(hrTitleEvaluation, example);
    }

    public HrTitleEvaluation selectOneHrTitleEvaluation(HrTitleEvaluation hrTitleEvaluation) {
        return hrTitleEvaluationMapper.selectOne(hrTitleEvaluation);
    }

    /**
     * @param orgId
     * @param userId
     * @param beginTime
     * @param endTime
     * @param getType
     * @param search
     * @return List<Map < String, String>>
     * @Title: getHrTitleEvaluationList
     * @Description:  获取人员职称评定列表
     */
    public List<Map<String, String>> getHrTitleEvaluationList(String orgId, String userId, String beginTime, String endTime, String getType, String search) {
        return hrTitleEvaluationMapper.getHrTitleEvaluationList(orgId, userId, beginTime, endTime, getType, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param userId
     * @param beginTime
     * @param endTime
     * @param getType
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getHrTitleEvaluationList
     * @Description:  获取人员职称评定列表
     */
    public PageInfo<Map<String, String>> getHrTitleEvaluationList(PageParam pageParam, String userId, String beginTime, String endTime, String getType) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHrTitleEvaluationList(pageParam.getOrgId(), userId, beginTime, endTime, getType, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
