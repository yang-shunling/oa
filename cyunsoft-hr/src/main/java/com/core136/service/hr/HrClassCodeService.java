package com.core136.service.hr;

import com.core136.bean.hr.HrClassCode;
import com.core136.mapper.hr.HrClassCodeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class HrClassCodeService {
    private HrClassCodeMapper hrClassCodeMapper;

    @Autowired
    public void setHrClassCodeMapper(HrClassCodeMapper hrClassCodeMapper) {
        this.hrClassCodeMapper = hrClassCodeMapper;
    }

    public int insertHrClassCode(HrClassCode hrclassCode) {
        return hrClassCodeMapper.insert(hrclassCode);
    }

    public int deleteHrClassCode(HrClassCode hrClassCode) {
        return hrClassCodeMapper.delete(hrClassCode);
    }

    public int updateHrClassCode(Example example, HrClassCode hrClassCode) {
        return hrClassCodeMapper.updateByExampleSelective(hrClassCode, example);
    }

    public HrClassCode selectOneHrClassCode(HrClassCode hrClassCode) {
        return hrClassCodeMapper.selectOne(hrClassCode);
    }

    /**
     * @param orgId
     * @param module
     * @return List<Map < String, String>>
     * @Title: getCodeListByModule
     * @Description:   获取下拉列表
     */
    public List<Map<String, String>> getCodeListByModule(String orgId, String module) {
        return hrClassCodeMapper.getCodeListByModule(orgId, module);
    }

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getAllParentCodeList
     * @Description:  获取所有的主分类
     */
    public List<Map<String, String>> getAllParentCodeList(String orgId) {
        return hrClassCodeMapper.getAllParentCodeList(orgId);
    }

    /**
     * @param orgId
     * @param module
     * @param codeValue
     * @return List<Map < String, String>>
     * @Title: getHrClassCodeName
     * @Description:  获取分类码名称
     */
    public List<Map<String, String>> getHrClassCodeName(String orgId, String module, String codeValue) {
        return hrClassCodeMapper.getHrClassCodeName(orgId, module, codeValue);
    }

}
