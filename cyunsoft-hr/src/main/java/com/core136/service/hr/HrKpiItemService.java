package com.core136.service.hr;

import com.core136.bean.hr.HrKpiItem;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.hr.HrKpiItemMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class HrKpiItemService {

    private HrKpiItemMapper hrKpiItemMapper;

    @Autowired
    public void setHrKpiItemMapper(HrKpiItemMapper hrKpiItemMapper) {
        this.hrKpiItemMapper = hrKpiItemMapper;
    }

    public int insertHrKpiItem(HrKpiItem hrKpiItem) {
        return hrKpiItemMapper.insert(hrKpiItem);
    }

    public int deleteHrKpiItem(HrKpiItem hrKpiItem) {
        return hrKpiItemMapper.delete(hrKpiItem);
    }

    public int updateHrKpiItem(Example example, HrKpiItem hrKpiItem) {
        return hrKpiItemMapper.updateByExampleSelective(hrKpiItem, example);
    }

    public HrKpiItem selectOneHrKpiItem(HrKpiItem hrKpiItem) {
        return hrKpiItemMapper.selectOne(hrKpiItem);
    }

    /**
     * @param orgId
     * @param kpiType
     * @param search
     * @return List<Map < String, String>>
     * @Title: getHrKpiItemList
     * @Description:  获取考核指标列表
     */
    public List<Map<String, String>> getHrKpiItemList(String orgId, String createUser, String kpiType, String search) {
        return hrKpiItemMapper.getHrKpiItemList(orgId, createUser, kpiType, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param kpiType
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getHrKpiItemList
     * @Description:  获取考核指标列表
     */
    public PageInfo<Map<String, String>> getHrKpiItemList(PageParam pageParam, String createUser, String kpiType) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHrKpiItemList(pageParam.getOrgId(), createUser, kpiType, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param kpiType
     * @return List<Map < String, String>>
     * @Title: getHrKpiItemListForSelect
     * @Description:  获取考核指标集
     */
    public List<Map<String, String>> getHrKpiItemListForSelect(String orgId, String kpiType) {
        return hrKpiItemMapper.getHrKpiItemListForSelect(orgId, kpiType);
    }
}
