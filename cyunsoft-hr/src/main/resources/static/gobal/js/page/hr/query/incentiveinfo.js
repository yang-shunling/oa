$(function () {
    query();
})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/hrget/getMyHrIncentiveList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: false,//是否显示 数据条数选择框
        sortable: true,//排序
        search: false,//启用搜索
        sortOrder: "asc",
        showColumns: false,//是否显示 内容列下拉框
        showRefresh: false,//显示刷新按钮
        idField: 'incentiveId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',//标题  可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'incentiveItem',
            width: '100px',
            title: '奖惩事件',
            formatter: function (value, row, index) {
                return "<a href=\"javascript:void(0);details('" + row.incentiveId + "')\" style='cursor: pointer'>" + getHrClassCodeName('incentiveItem', value) + "</a>";
            }
        }, {
            field: 'userId',
            title: '奖惩人员',
            sortable: true,
            width: '80px',
            formatter: function (value, row, index) {
                if (row.userName == "" || row.userName == null) {
                    return getHrUserNameByStr(row.userId);
                } else {
                    return row.userName;
                }
            }
        }, {
            field: 'incentiveType',
            width: '100px',
            title: '奖惩类型',
            formatter: function (value, row, index) {
                if (value == "0") {
                    return "奖励";
                } else if (value == "1") {
                    return "惩罚";
                }
            }
        }, {
            field: 'incentiveTime',
            width: '50px',
            title: '处理日期'
        }, {
            field: 'incentiveAmount',
            width: '100px',
            title: '金额'
        }, {
            field: 'salaryMonth',
            title: '工资月份',
            width: '100px'
        }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list
                    //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        userId: $("#userIdQuery").attr("data-value"),
        incentiveItem: $("#incentiveItemQuery").val(),
        incentiveType: $("#incentiveTypeQuery").val(),
        endTime: $("#endTimeQuery").val(),
        beginTime: $("#beginTimeQuery").val()
    };
    return temp;
};


function details(incentiveId) {
    window.open("/app/core/hr/incentivedetails?incentiveId=" + incentiveId);
}
