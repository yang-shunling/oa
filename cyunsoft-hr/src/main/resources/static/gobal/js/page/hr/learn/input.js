let ue = UE.getEditor("remark");
$(function () {
    jeDate("#beginTime", {
        format: "YYYY-MM",
        maxDate: getSysDate()
    });
    jeDate("#endTime", {
        format: "YYYY-MM"
    });
    $(".js-add-save").unbind("click").click(function () {
        addLearnRecord();
    })
    $(".js-auto-select").each(function () {
        var module = $(this).attr("module");
        createAutoSelect(module);
    })

})

function addLearnRecord() {
    if($("#userId").attr("data-value")=="")
    {
        layer.msg("相关人员不能为空!");
        return;
    }
    $.ajax({
        url: "/set/hrset/insertHrLearnRecord",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            shoolName: $("#shoolName").val(),
            userId: $("#userId").attr("data-value"),
            major: $("#major").val(),
            beginTime: $("#beginTime").val(),
            endTime: $("#endTime").val(),
            highsetDegree: $("#highsetDegree").val(),
            cerifier: $("#cerifier").val(),
            cerificate: $("#cerificate").val(),
            honor: $("#honor").val(),
            attach: $("#hrattach").attr("data_value"),
            remark: ue.getContent()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}
