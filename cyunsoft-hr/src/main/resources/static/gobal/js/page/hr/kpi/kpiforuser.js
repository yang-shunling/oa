$(function () {
    $.ajax({
        url: "/ret/hrget/getKipItemForUserById",
        type: "post",
        dataType: "json",
        data: {planId: planId, accountId: accountId},
        success: function (data) {
            if (data.status == "200") {
                var itemList = data.list;
                for (var i = 0; i < itemList.length; i++) {
                    var title = itemList[i].title;
                    var optType = itemList[i].optType;
                    var itemId = itemList[i].itemId;
                    var childItem = JSON.parse(itemList[i].childItem);
                    if (optType == "1") {
                        getRadioItem(i, itemId, title, childItem)
                    } else if (optType == "2") {
                        getCheckBoxItem(i, itemId, title, childItem)
                    } else if (optType == "3") {
                        getTextItem(i, itemId, title, childItem)
                    }
                }

            } else if (data.statu == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
    $(".js-add-save").unbind("click").click(function () {
        setScoreToUser();
    });
})

function getScoreList() {
    var returtArr = [];
    $(".itemrow").each(function () {
        var json = {};
        var optType = $(this).attr("optType");
        var itemId = $(this).attr("data-id");
        var score = 0;
        var comment = "";
        if (optType == "1") {
            score = $("input:radio[name='" + itemId + "']:checked").val()
        }
        if (optType == "2") {
            var returnStr = [];
            $('input[name="' + itemId + '"]:checked').each(function () {
                returnStr.push(parseFloat($(this).val()));
            });
            score = sum(returnStr)
        }
        if (optType == "3") {
            score = $("#" + itemId + "_childScore").val()
            comment = $("#" + itemId + "_comment").val()
        }
        if (score == "") {
            layer.msg("还有考核项未打分！");
            return;
        }
        json.itemId = itemId;
        json.optType = optType;
        json.score = score;
        json.comment = comment;
        returtArr.push(json);
    })
    return returtArr;
}

function sum(arr) {
    var len = arr.length;
    if (len == 0) {
        return 0;
    } else if (len == 1) {
        return arr[0];
    } else {
        return arr[0] + sum(arr.slice(1));
    }
}

function checkScore(Obj, value) {
    var score = $(Obj).val();
    if (score > value) {
        $(Obj).val("");
        layer.msg("总分不能超过预设分值！");
    }
}

function setScoreToUser() {
    if (confirm("考核完成后不可再次修改？")) {
        $.ajax({
            url: "/set/hrset/setScoreToUser",
            type: "post",
            dataType: "json",
            data: {
                planId: planId,
                accountId: accountId,
                scoreList: JSON.stringify(getScoreList())
            },
            success: function (data) {
                if (data.status == "200") {
                    history.go(-1);
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }

        });
    }
}

function getRadioItem(index, itemId, title, arr) {
    var html = "<tr  class='itemrow' data-id=\"" + itemId + "\" optType='1'><td>" + (index + 1) + ":" + title;
    for (var i = 0; i < arr.length; i++) {
        html += "<div class=\"radio\"><label><input name=\"" + itemId + "\" type=\"radio\" value=\"" + arr[i].childScore + "\">" +
            "<span class=\"text\">" + arr[i].childTitle + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;分值" + arr[i].childScore + "</span></label></div>";
    }
    html += "</td></tr>"
    $("#child-item-table").append(html);
}

function getCheckBoxItem(index, itemId, title, arr) {
    var html = "<tr class='itemrow' data-id=\"" + itemId + "\" optType='2'><td>" + (index + 1) + ":" + title;
    for (var i = 0; i < arr.length; i++) {
        html += "<div class=\"checkbox\"><label><input name=\"" + itemId + "\" type=\"checkbox\" value=\"" + arr[i].childScore + "\">" +
            "<span class=\"text\">" + arr[i].childTitle + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;分值" + arr[i].childScore + "</span></label></div>";
    }
    html += "</td></tr>"
    $("#child-item-table").append(html);
}

function getTextItem(index, itemId, title, arr) {
    var html = "<tr class='itemrow' data-id=\"" + itemId + "\" optType='3'><td>" + (index + 1) + ":" + title;
    for (var i = 0; i < arr.length; i++) {
        html += "<div><textarea id=\"" + itemId + "_comment\" class=\"form-control\" rows='5'></textarea><div'>总分值:" + arr[i].childScore + "</div></div>" +
            "<div style='float:right;'>总得分:<input onchange='checkScore(this," + arr[i].childScore + ")' style='width:150px;display: inline;' type='number' id='" + itemId + "_childScore' class='form-control'></div>";
    }
    html += "</td></tr>"
    $("#child-item-table").append(html);
}
