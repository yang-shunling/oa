$(function () {
    $.ajax({
        url: "/ret/hrget/getHrRecruitTaskById",
        type: "post",
        dataType: "json",
        data: {taskId: taskId},
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "attach") {
                        $("#hrattach").attr("data_value", recordInfo.attach);
                        createAttach("hrattach", 1);
                    } else if (id == "workType") {
                        $("#workType").html(getHrClassCodeName('workType', recordInfo[id]));
                    } else if (id == "planId") {
                        $.ajax({
                            url: "/ret/hrget/getHrRecruitPlanById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {planId: recordInfo[id]},
                            success: function (data) {
                                if (data.status == "200") {
                                    $("#planId").html(data.list.title)
                                } else if (data.status == "100") {
                                    layer.msg(sysmsg[data.msg]);
                                } else {
                                    console.log(data.msg);
                                }
                            }
                        });
                    } else {
                        $("#" + id).html(recordInfo[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})
