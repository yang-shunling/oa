package com.core136.service.capital;

import com.core136.bean.capital.CapitalPlan;
import com.core136.mapper.capital.CapitalPlanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

@Service
public class CapitalPlanService {
    private CapitalPlanMapper capitalPlanMapper;
    @Autowired
    public void setCapitalPlanMapper(CapitalPlanMapper capitalPlanMapper)
    {
        this.capitalPlanMapper = capitalPlanMapper;
    }
    public int insertCapitalPlan(CapitalPlan capitalPlan)
    {
        return capitalPlanMapper.insert(capitalPlan);
    }
    public int deleteCapitalPlan(CapitalPlan capitalPlan)
    {
        return capitalPlanMapper.delete(capitalPlan);
    }
    public CapitalPlan selectOneCapitalPlan(CapitalPlan capitalPlan)
    {
        return capitalPlanMapper.selectOne(capitalPlan);
    }
    public int updateCapitalPlan(Example example,CapitalPlan capitalPlan)
    {
        return capitalPlanMapper.updateByExampleSelective(capitalPlan,example);
    }
}
