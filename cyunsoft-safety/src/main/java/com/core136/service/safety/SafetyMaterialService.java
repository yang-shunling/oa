package com.core136.service.safety;

import com.core136.bean.safety.SafetyMaterial;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.safety.SafetyMaterialMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class SafetyMaterialService {
    private SafetyMaterialMapper safetyMaterialMapper;

    @Autowired
    public void setSafetyMaterialMapper(SafetyMaterialMapper safetyMaterialMapper) {
        this.safetyMaterialMapper = safetyMaterialMapper;
    }

    public int insertSafetyMaterial(SafetyMaterial safetyMaterial) {
        return safetyMaterialMapper.insert(safetyMaterial);
    }

    public int deleteSafetyMaterial(SafetyMaterial safetyMaterial) {
        return safetyMaterialMapper.delete(safetyMaterial);
    }

    public SafetyMaterial selectOneSafetyMaterial(SafetyMaterial safetyMaterial) {
        return safetyMaterialMapper.selectOne(safetyMaterial);
    }

    public int updateSafetyMaterial(Example example, SafetyMaterial safetyMaterial) {
        return safetyMaterialMapper.updateByExampleSelective(safetyMaterial, example);
    }

    /**
     * 获取急应物资列表
     * @param orgId
     * @param entId
     * @param codeNo
     * @param search
     * @return
     */
    public List<Map<String,String>> getSafetyMaterialList(String orgId,String materialType, String entId, String codeNo,String search)
    {
        return safetyMaterialMapper.getSafetyMaterialList(orgId,materialType,entId,codeNo,"%"+search+"%");
    }

    /**
     * 获取急应物资列表
     * @param pageParam
     * @param materialType
     * @param entId
     * @param codeNo
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getSafetyMaterialList(PageParam pageParam, String materialType,String entId, String codeNo) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getSafetyMaterialList(pageParam.getOrgId(), materialType,entId, codeNo, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }
}
