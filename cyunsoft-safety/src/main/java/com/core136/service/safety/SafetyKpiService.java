package com.core136.service.safety;

import com.core136.bean.safety.SafetyKpi;
import com.core136.mapper.safety.SafetyKpiMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

@Service
public class SafetyKpiService {
    private SafetyKpiMapper safetyKpiMapper;

    @Autowired
    public void setSafetyKpiMapper(SafetyKpiMapper safetyKpiMapper) {
        this.safetyKpiMapper = safetyKpiMapper;
    }

    public int insertSafetyKpi(SafetyKpi safetyKpi) {
        return safetyKpiMapper.insert(safetyKpi);
    }

    public int deleteSafetyKpi(SafetyKpi safetyKpi) {
        return safetyKpiMapper.delete(safetyKpi);
    }

    public SafetyKpi selectOneSafetyKpi(SafetyKpi safetyKpi) {
        return safetyKpiMapper.selectOne(safetyKpi);
    }

    public int updateSafetyKpi(Example example, SafetyKpi safetyKpi) {
        return safetyKpiMapper.updateByExampleSelective(safetyKpi, example);
    }
}
