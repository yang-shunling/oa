package com.core136.service.safety;

import com.core136.bean.safety.SafetyTargetSort;
import com.core136.mapper.safety.SafetyTargetSortMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class SafetyTargetSortService {
    private SafetyTargetSortMapper safetyTargetSortMapper;
    @Autowired
    public void setSafetyTargetSortMapper(SafetyTargetSortMapper safetyTargetSortMapper)
    {
        this.safetyTargetSortMapper = safetyTargetSortMapper;
    }

    public int insertSafetyTargetSort(SafetyTargetSort safetyTargetSort) {
        return safetyTargetSortMapper.insert(safetyTargetSort);
    }

    public int deleteSafetyTargetSort(SafetyTargetSort safetyTargetSort) {
        return safetyTargetSortMapper.delete(safetyTargetSort);
    }

    public SafetyTargetSort selectOneSafetyTargetSort(SafetyTargetSort safetyTargetSort) {
        return safetyTargetSortMapper.selectOne(safetyTargetSort);
    }

    public int updateSafetyTargetSort(Example example,SafetyTargetSort safetyTargetSort) {
        return safetyTargetSortMapper.updateByExampleSelective(safetyTargetSort, example);
    }

    /**
     * 获取目标分类树结构
     */

    public List<Map<String, String>> getTargetSortTree(String orgId, String sortLevel) {
        return safetyTargetSortMapper.getTargetSortTree(orgId, sortLevel);
    }

    /**
     * 判断是否还有子集
     */

    public int isExistChild(String orgId, String sortId) {
        return safetyTargetSortMapper.isExistChild(orgId, sortId);
    }
}
