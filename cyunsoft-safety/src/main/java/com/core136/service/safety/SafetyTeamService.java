package com.core136.service.safety;

import com.core136.bean.safety.SafetyTeam;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.safety.SafetyTeamMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class SafetyTeamService {
    private SafetyTeamMapper safetyTeamMapper;

    @Autowired
    public void setSafetyTeamMapper(SafetyTeamMapper safetyTeamMapper) {
        this.safetyTeamMapper = safetyTeamMapper;
    }

    public int insertSafetyTeam(SafetyTeam safetyTeam) {
        return safetyTeamMapper.insert(safetyTeam);
    }

    public int deleteSafetyTeam(SafetyTeam safetyTeam) {
        return safetyTeamMapper.delete(safetyTeam);
    }

    public SafetyTeam selectOneSafetyTeam(SafetyTeam safetyTeam) {
        return safetyTeamMapper.selectOne(safetyTeam);
    }

    public int updateSafetyTeam(Example example, SafetyTeam safetyTeam) {
        return safetyTeamMapper.updateByExampleSelective(safetyTeam, example);
    }

    /**
     * 获取应急小组列表
     * @param orgId
     * @param teamType
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String,String>> getSafetyTeamList(String orgId, String teamType, String beginTime, String endTime, String search)
    {
        return safetyTeamMapper.getSafetyTeamList(orgId,teamType,beginTime,endTime,"%"+search+"%");
    }

    /**
     * 获取应急小组列表
     * @param pageParam
     * @param teamType
     * @param beginTime
     * @param endTime
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getSafetyTeamList(PageParam pageParam, String teamType, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getSafetyTeamList(pageParam.getOrgId(), teamType, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
