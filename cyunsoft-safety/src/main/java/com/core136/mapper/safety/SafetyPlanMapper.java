package com.core136.mapper.safety;

import com.core136.bean.safety.SafetyPlan;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface SafetyPlanMapper extends MyMapper<SafetyPlan> {

    /**
     * 获取应急方案
     * @param orgId
     * @param planType
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String,String>> getSafetyPlanList(@Param(value="orgId")String orgId,@Param(value="planType")String planType,
                                                      @Param(value = "beginTime")String beginTime,@Param(value = "endTime")String endTime,@Param(value="search")String search);

}
