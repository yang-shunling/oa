package com.core136.mapper.safety;

import com.core136.bean.safety.SafetyEntInfo;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface SafetyEntInfoMapper extends MyMapper<SafetyEntInfo> {

    /**
     * 获取企业列表
     *
     * @param orgId
     * @param type
     * @param search
     * @return
     */
    public List<Map<String, String>> getEntInfoList(@Param(value = "orgId") String orgId, @Param(value = "type") String type, @Param(value = "search") String search);

    public List<Map<String, String>> selectEntInfoByName(@Param(value = "orgId") String orgId, @Param(value = "search") String search);

    public List<Map<String,String>>getEntInfoAllList(@Param(value="orgId")String orgId);

}
