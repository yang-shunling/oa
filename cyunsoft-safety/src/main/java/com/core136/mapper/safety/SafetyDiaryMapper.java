package com.core136.mapper.safety;

import com.core136.bean.safety.SafetyDiary;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface SafetyDiaryMapper extends MyMapper<SafetyDiary> {
    /**
     * 获取安全日志列表
     * @param orgId
     * @param diaryType
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String,String>> getSafetyDiaryList(@Param(value="orgId")String orgId,@Param(value="diaryType")String diaryType,
                                                       @Param(value="beginTime")String beginTime,@Param(value="endTime")String endTime,@Param(value = "search")String search);
}
