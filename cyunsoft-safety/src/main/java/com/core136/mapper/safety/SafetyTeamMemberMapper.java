package com.core136.mapper.safety;

import com.core136.bean.safety.SafetyTeamMember;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SafetyTeamMemberMapper extends MyMapper<SafetyTeamMember> {
}
