package com.core136.mapper.safety;

import com.core136.bean.safety.SafetyLicence;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface SafetyLicenceMapper extends MyMapper<SafetyLicence> {
    /**
     * 获取证照管理列表
     *
     * @param orgId
     * @param licenceType
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String, String>> getSafetyLicenceList(@Param(value = "orgId") String orgId, @Param(value = "licenceType") String licenceType, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);
}
