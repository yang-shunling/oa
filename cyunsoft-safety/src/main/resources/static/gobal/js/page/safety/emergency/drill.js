$(function () {
    jeDate("#beginTime", {
        format: "YYYY-MM-DD hh:mm"
    });
    jeDate("#endTime", {
        format: "YYYY-MM-DD hh:mm"
    });
    getSafetyCodeClass("drillType", "drillType");
    $('#remark').summernote({height: 200});
    $(".js-add-save").unbind("click").click(function () {
        createDrill();
    })
})

function createDrill() {
    if($("#title").val()=="")
    {
        layer.msg("演练标题不能为空！");
        return;
    }
    $.ajax({
        url: "/set/safetyset/insertSafetyDrill",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            title: $("#title").val(),
            drillType: $("#drillType").val(),
            beginTime: $("#beginTime").val(),
            endTime: $("#endTime").val(),
            sponsor: $("#sponsor").val(),
            address: $("#address").val(),
            remark: $("#remark").code(),
            joinOrg: $("#joinOrg").val(),
            attach: $("#attach").attr("data_value")
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}
