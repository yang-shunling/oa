$(function () {
    $.ajax({
        url: "/ret/safetyget/getSafetyDrillById",
        type: "post",
        dataType: "json",
        data: {
            drillId: drillId
        },
        success: function (data) {
            if (data.status == "200") {
                let info = data.list;
                for (let id in info) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", info.attach);
                        createAttach("attach", 1);
                    } else if (id == "drillType") {
                        $("#drillType").html(getSafetyCodeClassName(info[id], "drillType"));
                    } else {
                        $("#" + id).html(info[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})
