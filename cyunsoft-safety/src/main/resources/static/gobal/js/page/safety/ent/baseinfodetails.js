$(function () {
    $.ajax({
        url: "/ret/safetyget/getSafetyEntInfoById",
        type: "post",
        dataType: "json",
        data: {
            entId: entId
        },
        success: function (data) {
            if (data.status == "200") {
                let info = data.list;
                for (let id in info) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", info.attach);
                        createAttach("attach", 1);
                    } else if (id == "type") {
                        if (info[id] == "0") {
                            $("#" + id).html("私有");
                        } else if (value == "1") {
                            $("#" + id).html("国有");
                        } else if (info[id] == "2") {
                            $("#" + id).html("合资");
                        } else if (info[id] == "3") {
                            $("#" + id).html("外资");
                        } else {
                            $("#" + id).html("未知");
                        }
                    } else {
                        $("#" + id).html(info[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})
